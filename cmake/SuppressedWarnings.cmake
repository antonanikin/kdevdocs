include(CheckCXXCompilerFlag)

check_cxx_compiler_flag(-Wimplicit-fallthrough=0 W_IMPLICIT_FALLTHROUGH)
if(W_IMPLICIT_FALLTHROUGH)
    list(APPEND SUPPRESSED_WARNINGS -Wimplicit-fallthrough=0)
endif()

check_cxx_compiler_flag(-Wno-cast-align W_NO_CAST_ALIGN)
if(W_NO_CAST_ALIGN)
    list(APPEND SUPPRESSED_WARNINGS -Wno-cast-align)
endif()

check_cxx_compiler_flag(-Wno-deprecated-register W_NO_DEPRECATED_REGISTER)
if(W_NO_DEPRECATED_REGISTER)
    list(APPEND SUPPRESSED_WARNINGS -Wno-deprecated-register)
endif()

check_cxx_compiler_flag(-Wno-unused-const-variable W_NO_UNUSED_CONST_VARIABLE)
if(W_NO_UNUSED_CONST_VARIABLE)
    list(APPEND SUPPRESSED_WARNINGS -Wno-unused-const-variable)
endif()
