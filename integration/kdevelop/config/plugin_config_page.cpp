/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "plugin_config_page.h"

#include "plugin_config.h"
#include "version_check.h"

#include <KLocalizedString>
#include <KMessageWidget>
#include <KUrlRequester>

#include <QCheckBox>
#include <QFormLayout>
#include <QGroupBox>

namespace KDevelop
{

namespace KDevDocs
{

PluginConfigPage::PluginConfigPage(IPlugin* plugin, QWidget* parent)
    : ConfigPage(plugin, PluginConfig::self(), parent)
{
    auto mainLayout = new QVBoxLayout(this);

    auto executableBox = new QGroupBox;
    auto executableBoxLayout = new QVBoxLayout(executableBox);

    auto executableLayout = new QFormLayout;
    auto kcfg_kdevdocsExecutable = new KUrlRequester;
    kcfg_kdevdocsExecutable->setObjectName(QStringLiteral("kcfg_kdevdocsExecutable"));
    kcfg_kdevdocsExecutable->setMode(KFile::File | KFile::ExistingOnly | KFile::LocalOnly);
    executableLayout->addRow(i18n("KDevDocs:"), kcfg_kdevdocsExecutable);
    executableBoxLayout->addLayout(executableLayout);

    auto kcfg_startAtLoad = new QCheckBox;
    kcfg_startAtLoad->setObjectName(QStringLiteral("kcfg_startAtLoad"));
    kcfg_startAtLoad->setText(i18n("Start KDevDocs at KDevelop load"));
    executableBoxLayout->addWidget(kcfg_startAtLoad);

    auto errorMessageWidget = new KMessageWidget;
    errorMessageWidget->setText(i18n("Unable to find KDevDocs, check your settings/installation."));
    errorMessageWidget->setMessageType(KMessageWidget::Error);
    errorMessageWidget->setWordWrap(true);
    errorMessageWidget->setCloseButtonVisible(true);
    executableBoxLayout->addWidget(errorMessageWidget);

    mainLayout->addWidget(executableBox);
    mainLayout->addStretch();

    connect(kcfg_kdevdocsExecutable, &KUrlRequester::textChanged,
            this, [errorMessageWidget, kcfg_startAtLoad](const QString& url)
            {
                bool exists = QFile::exists(url);
                errorMessageWidget->setVisible(!exists);
                kcfg_startAtLoad->setEnabled(exists);
            });
}

PluginConfigPage::~PluginConfigPage()
{
}

ConfigPage::ConfigPageType
PluginConfigPage::configPageType() const
{
    // FIXME check version when API was changed
    #if KDEVPLATFORM_VERSION < KDEVPLATFORM_VERSION_CHECK(5, 1, 0)
    return DefaultConfigPage;
    #else
    return DocumentationConfigPage;
    #endif
}

QIcon
PluginConfigPage::icon() const
{
    return QIcon::fromTheme(QStringLiteral("help-browser"));
}

QString
PluginConfigPage::name() const
{
    return i18n("KDevDocs");
}

}

}
