/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <interfaces/configpage.h>

class KMessageWidget;

namespace KDevelop
{

class IProject;

namespace KDevDocs
{

class ActionSelector;
class Plugin;

class ProjectConfigPage : public ConfigPage
{
    Q_OBJECT

public:
    ProjectConfigPage(IPlugin* plugin, IProject* project, QWidget* parent);

    ~ProjectConfigPage() override;

    QIcon
    icon() const override;

    QString
    name() const override;

protected:
    void
    loadAvailableDocsets();

    Plugin* m_plugin;

    KMessageWidget* m_startMessageWidget;
    KMessageWidget* m_errorMessageWidget;
    KMessageWidget* m_noteMessageWidget;
    ActionSelector* m_kcfg_selectedDocsets;
};

}

}
