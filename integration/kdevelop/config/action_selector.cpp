/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "action_selector.h"

#include <QListWidget>
#include <QListWidgetItem>

namespace KDevelop
{

namespace KDevDocs
{

ActionSelector::ActionSelector(QWidget* parent)
    : KActionSelector(parent)
{
    // FIXME AtBottom not works - bug in the KActionSelector
    // https://cgit.kde.org/kwidgetsaddons.git/tree/src/kactionselector.cpp
    // line 535
//     setSelectedInsertionPolicy(AtBottom);

    connect(this, &ActionSelector::added, this, &ActionSelector::changed);
    connect(this, &ActionSelector::removed, this, &ActionSelector::changed);
    connect(this, &ActionSelector::movedUp, this, &ActionSelector::changed);
    connect(this, &ActionSelector::movedDown, this, &ActionSelector::changed);
}

ActionSelector::~ActionSelector()
{
}

QStringList
ActionSelector::selected() const
{
    return m_selected;
}

void
ActionSelector::setSelected(const QStringList& selected)
{
    QHash<QString, QListWidgetItem*> selectedItems;

    for (int row = 0; row < availableListWidget()->count(); ++row)
    {
        auto name = availableListWidget()->item(row)->text();
        if (selected.contains(name))
        {
            selectedItems[name] = availableListWidget()->takeItem(row--);
        }
    }

    while (selectedListWidget()->count())
    {
        auto item = selectedListWidget()->takeItem(0);
        if (selected.contains(item->text()))
        {
            selectedItems[item->text()] = item;
        }
        else
        {
            availableListWidget()->addItem(item);
        }
    }

    for (const QString& name : selected)
    {
        if (selectedItems.contains(name))
        {
            selectedListWidget()->addItem(selectedItems.take(name));
        }
    }

    availableListWidget()->sortItems();

    if (selected != m_selected)
    {
        m_selected = selected;
        emit selectedChanged(m_selected);
    }
}

void
ActionSelector::changed()
{
    QStringList selected;
    for (int row = 0; row < selectedListWidget()->count(); ++row)
    {
        selected += selectedListWidget()->item(row)->text();
    }

    if (selected != m_selected)
    {
        m_selected = selected;
        emit selectedChanged(m_selected);
    }
}

}

}
