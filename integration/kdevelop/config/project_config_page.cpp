/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "project_config_page.h"

#include "action_selector.h"
#include "plugin.h"
#include "project_config.h"

#include <interfaces/iproject.h>

#include <KLocalizedString>
#include <KMessageWidget>

#include <QDBusInterface>
#include <QDBusReply>
#include <QListWidgetItem>
#include <QVBoxLayout>

namespace KDevelop
{

namespace KDevDocs
{

ProjectConfigPage::ProjectConfigPage(IPlugin* plugin, IProject* project, QWidget* parent)
    : ConfigPage(plugin, new ProjectConfig, parent)
    , m_plugin(static_cast<Plugin*>(plugin))
{
    auto mainLayout = new QVBoxLayout(this);

    m_startMessageWidget = new KMessageWidget;
    m_startMessageWidget->setText(i18n("KDevDocs is starting..."));
    m_startMessageWidget->setMessageType(KMessageWidget::Information);
    m_startMessageWidget->setWordWrap(true);
    m_startMessageWidget->setCloseButtonVisible(false);
    mainLayout->addWidget(m_startMessageWidget);

    m_errorMessageWidget = new KMessageWidget;
    m_errorMessageWidget->setText(i18n("Unable to start KDevDocs, check your settings."));
    m_errorMessageWidget->setMessageType(KMessageWidget::Error);
    m_errorMessageWidget->setWordWrap(true);
    m_errorMessageWidget->setCloseButtonVisible(false);
    mainLayout->addWidget(m_errorMessageWidget);

    m_kcfg_selectedDocsets = new ActionSelector;
    m_kcfg_selectedDocsets->setObjectName(QStringLiteral("kcfg_selectedDocsets"));
    mainLayout->addWidget(m_kcfg_selectedDocsets);

    m_noteMessageWidget = new KMessageWidget;
    m_noteMessageWidget->setText(i18n("No docsets selected - the plugin will use all available during search."));
    m_noteMessageWidget->setMessageType(KMessageWidget::Information);
    m_noteMessageWidget->setWordWrap(true);
    m_noteMessageWidget->setCloseButtonVisible(false);
    mainLayout->addWidget(m_noteMessageWidget);

    connect(m_kcfg_selectedDocsets, &ActionSelector::selectedChanged,
            this, [this](const QStringList& selected)
            {
                m_noteMessageWidget->setVisible(selected.isEmpty());
            });

    configSkeleton()->setSharedConfig(project->projectConfiguration());
    configSkeleton()->load();

    connect(m_plugin, &Plugin::kdevdocsStarted, this, &ProjectConfigPage::loadAvailableDocsets);

    m_kcfg_selectedDocsets->setEnabled(false);

    if (m_plugin->interface()->isValid())
    {
        loadAvailableDocsets();
    }
    else
    {
        bool started = m_plugin->startKdevdocs();

        m_startMessageWidget->setVisible(started);
        m_errorMessageWidget->setVisible(!started);
        m_noteMessageWidget->hide();
    }
}

ProjectConfigPage::~ProjectConfigPage()
{
}

void
ProjectConfigPage::loadAvailableDocsets()
{
    QDBusReply<QByteArray> r = m_plugin->interface()->call(QStringLiteral("docsets"));
    QDataStream ds(r.value());
    QString name;
    QPixmap pixmap;

    m_kcfg_selectedDocsets->availableListWidget()->clear();
    while (!ds.atEnd())
    {
        ds >> name;
        ds >> pixmap;
        new QListWidgetItem(QIcon(pixmap), name, m_kcfg_selectedDocsets->availableListWidget());
    }

    auto config = static_cast<ProjectConfig*>(configSkeleton());

    m_kcfg_selectedDocsets->setEnabled(true);
    m_kcfg_selectedDocsets->setSelected(config->selectedDocsets());

    m_startMessageWidget->hide();
    m_errorMessageWidget->hide();
    m_noteMessageWidget->setVisible(config->selectedDocsets().isEmpty());
}

QIcon
ProjectConfigPage::icon() const
{
    return QIcon::fromTheme(QStringLiteral("help-browser"));
}

QString
ProjectConfigPage::name() const
{
    return i18n("KDevDocs");
}

}

}
