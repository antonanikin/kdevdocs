# kdevplatform_add_plugin() introduced in 5.0.0
# TODO check and update minimal version
find_package(KDevPlatform "5.0.0" CONFIG)
set_package_properties(KDevPlatform PROPERTIES
    PURPOSE "Needed to build the KDevelop integration plugin"
)

if (NOT KDevPlatform_FOUND)
    return()
endif()

set(kdevdocs_integration_SRCS
    plugin.cpp

    config/action_selector.cpp
    config/plugin_config_page.cpp
    config/project_config_page.cpp
)

kconfig_add_kcfg_files(kdevdocs_integration_SRCS
    config/plugin_config.kcfgc
    config/project_config.kcfgc
)

kdevplatform_add_plugin(kdevdocsintegration
    JSON plugin.json
    SOURCES ${kdevdocs_integration_SRCS}
)

target_link_libraries(kdevdocsintegration
    KDev::Shell
    KDev::Language
)
