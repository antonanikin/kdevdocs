/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "plugin.h"

#include "config/plugin_config_page.h"
#include "config/project_config_page.h"
#include "plugin_config.h"
#include "project_config.h"

#include <project/projectconfigpage.h>
#include <interfaces/contextmenuextension.h>
#include <interfaces/icore.h>
#include <interfaces/idocumentcontroller.h>
#include <interfaces/iprojectcontroller.h>
#include <kactioncollection.h>
#include <kpluginfactory.h>
#include <klocalizedstring.h>

#include <ktexteditor/document.h>
#include <ktexteditor/view.h>

#include <language/interfaces/editorcontext.h>
#include <language/duchain/declaration.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/duchainutils.h>
#include <language/duchain/types/functiontype.h>
#include <language/duchain/types/enumeratortype.h>
#include <language/duchain/types/typeutils.h>

#include <QAction>
#include <QProcess>
#include <QDBusInterface>
#include <QDBusReply>
#include <QRegularExpression>
#include <QTimer>

K_PLUGIN_FACTORY_WITH_JSON(KDevDocsFactory, "plugin.json", registerPlugin<KDevelop::KDevDocs::Plugin>();)

namespace KDevelop
{

namespace KDevDocs
{

Plugin::Plugin(QObject* parent, const QVariantList&)
    : IPlugin(QStringLiteral("kdevdocsintegration"), parent)
    , m_interface(new QDBusInterface(QStringLiteral("org.kde.kdevdocs"),
                                     QStringLiteral("/KDevDocs"),
                                     QStringLiteral("org.kde.kdevdocs")))
    , m_startingTimer(new QTimer(this))
    , m_project(nullptr)
    , m_showAction(new QAction(QIcon::fromTheme(QStringLiteral("help-browser")),
                               i18n("Show Documentation in KDevDocs"),
                               this))
    , m_pendingQuery(false)
{
    connect(m_startingTimer, &QTimer::timeout,
            this, [this]()
            {
                if (m_interface->isValid())
                {
                    m_startingTimer->stop();
                    emit kdevdocsStarted();

                    if (m_pendingQuery)
                    {
                        m_pendingQuery = false;
                        showSlot();
                    }
                }
            });

    connect(m_showAction, &QAction::triggered, this, &Plugin::showSlot);
    actionCollection()->addAction(QStringLiteral("showDocumentation"), m_showAction);
    // FIXME add config
    actionCollection()->setDefaultShortcut(m_showAction, Qt::META + Qt::Key_F1);

    connect(core()->documentController(), &IDocumentController::documentClosed,
            this, &Plugin::updateProject);
    connect(core()->documentController(), &IDocumentController::documentActivated,
            this, &Plugin::updateProject);

    updateProject();

    if (PluginConfig::startAtLoad() && !m_interface->isValid())
    {
        startKdevdocs();
    }
}

Plugin::~Plugin()
{
    delete m_interface;
}

QDBusInterface*
Plugin::interface()
{
    return m_interface;
}

bool
Plugin::startKdevdocs()
{
    bool ok = QProcess::startDetached(QUrl(PluginConfig::kdevdocsExecutable()).toLocalFile(),
                                      { QStringLiteral("-m") });
    if (ok)
    {
        m_startingTimer->start(100);
    }

    return ok;
}

void
Plugin::updateProject()
{
    m_project = nullptr;

    auto activeDocument = core()->documentController()->activeDocument();
    if (!activeDocument)
    {
        return;
    }

    m_project = core()->projectController()->findProjectForUrl(activeDocument->url());
}

Declaration*
usefulDeclaration(Declaration* decl)
{
    if (!decl)
    {
        return nullptr;
    }

    // First: Attempt to find the declaration of a definition
    decl = DUChainUtils::declarationForDefinition(decl);

    // Convenience feature: Retrieve the type declaration of instances,
    // it makes no sense to pass the declaration pointer of instances of types
    if (decl->kind() == Declaration::Instance)
    {
        auto type = TypeUtils::targetTypeKeepAliases(decl->abstractType(), decl->topContext());
        auto idType = dynamic_cast<IdentifiedType*>(type.data());
        auto idDecl = idType ? idType->declaration(decl->topContext()) : nullptr;
        if (idDecl)
        {
            decl = idDecl;
        }
    }

    return decl;
}

// FIXME use KTextEditor::Document::wordAt ?

QString
declarationUnderCursor(const KTextEditor::View* view)
{
    if (!view)
    {
        return QString();
    }

    if (view->selection())
    {
        return view->selectionText();
    }

    using namespace KTextEditor;

    Cursor start = view->cursorPosition();
    Cursor end = start;
    Cursor step(0, 1);

    auto acceptableCharacter = [](QChar c)
    {
        return (c.isLetter() || c.isDigit() || c == QChar('_'));
    };

    while (acceptableCharacter(view->document()->characterAt(start)))
    {
        start -= step;
    }
    start += step;

    while (acceptableCharacter(view->document()->characterAt(end)))
    {
        end += step;
    }

    QString declaration = view->document()->text(Range(start, end));
    if (!declaration.isEmpty() && declaration.at(0).isDigit())
    {
        declaration.clear();
    }

    return declaration;
}

bool
Plugin::show(const QString& declaration)
{
    QString queryString(declaration);

    if (m_project)
    {
        ProjectConfig config;
        config.setSharedConfig(m_project->projectConfiguration());
        config.load();

        auto selectedDocsets = config.selectedDocsets();
        if (!selectedDocsets.isEmpty())
        {
            queryString += QStringLiteral("//%1").arg(selectedDocsets.join(QChar(',')));
        }
    }

    if (m_interface->isValid())
    {
        QDBusReply<bool> result = m_interface->call(QStringLiteral("test"), queryString);
        if (result.isValid() && result.value())
        {
            m_interface->call(QStringLiteral("query"), queryString);
            return true;
        }
    }

    return false;
}

void
Plugin::showSlot()
{
    using namespace KDevelop;

    auto view = core()->documentController()->activeTextDocumentView();
    if (!view)
    {
        return;
    }

    if (!m_interface->isValid())
    {
        m_pendingQuery = true;
        startKdevdocs();
        return;
    }

//     qDebug() << declarationUnderCursor(view) << view->document()->wordAt(view->cursorPosition());

    DUChainReadLocker lock(DUChain::lock());
    auto item = DUChainUtils::itemUnderCursor(view->document()->url(), view->cursorPosition());
    auto declaration = usefulDeclaration(item.declaration);
    if (declaration)
    {
        QStringList identifiers;
        QString identifierTail;

        if (declaration->kind() == Declaration::Macro)
        {
            identifierTail = QStringLiteral("()");
        }

        else if (auto type = declaration->type<EnumeratorType>())
        {
            // First we try to find enumerator value in enum parent's scope.
            // For example, Qt::WA_DeleteOnClose full identifier (qualifiedIdentifier()) is
            // Qt::WidgetAttribute::WA_DeleteOnClose. But Qt documentation contains index only for
            // Qt::WA_DeleteOnClose. Such situation also happens for Doxygen documentation
            // (KApiDox, for example). So we should reduce parent scope level to fix the problem.

            auto parentScope = declaration->context()->parentContext()->scopeIdentifier(true).toString();
            if (!parentScope.isEmpty())
            {
                parentScope += QStringLiteral("::");
            }

            identifiers += parentScope + declaration->identifier().toString();
        }

        else if (auto type = declaration->type<FunctionType>())
        {
            // KDevDocs provides documentation (kdd:// docsets) which contains parameter types
            // and "const" modifier information, so we should try to use it for exact search.
            // This helps to show right documentation when we have overloaded methods such as
            // QString::append(const QString &str), QString::append(QChar ch), ...

            identifierTail = type->partToString(FunctionType::SignatureArguments);

            static const QRegularExpression privateSignalRE(
                QStringLiteral(",?\\s*[\\w:]*QPrivateSignal"));

            // Remove QPrivateSignal parameter(s) from arguments signature since our docsets
            // doesn't contains such information (tested on Qt 5.11.2 documentation)
            identifierTail.remove(privateSignalRE);

            if (type->modifiers() & FunctionType::ConstModifier)
            {
                identifierTail += QStringLiteral(" const");
            }
        }

        #if KDEVPLATFORM_VERSION < KDEVPLATFORM_VERSION_CHECK(5, 1, 0)
            const auto simpleIdentifier = declaration->identifier().toString();
            const auto fullIdentifier = declaration->qualifiedIdentifier().toString();
        #else
            const auto simpleIdentifier = declaration->identifier().toString(RemoveTemplateInformation);
            const auto fullIdentifier = declaration->qualifiedIdentifier().toString(RemoveTemplateInformation);
        #endif

        if (!identifiers.contains(simpleIdentifier))
        {
            identifiers += simpleIdentifier;
        }

        if (!identifiers.contains(fullIdentifier))
        {
            identifiers += fullIdentifier;
        }

        std::sort(identifiers.begin(), identifiers.end(), [](const QString& a, const QString& b)
        {
            return a.length() > b.length();
        });

        for (const auto& identifier : identifiers)
        {
            if (show(identifier + identifierTail))
            {
                return;
            }

            // Also try to run search with dropped external information about parameter types
            // and "const" modifier since only some docsets support this feature.
            if (!identifierTail.isEmpty() && show(identifier))
            {
                return;
            }
        }
    }

    show(declarationUnderCursor(view));
}

int
Plugin::configPages() const
{
    return 1;
}

ConfigPage*
Plugin::configPage(int number, QWidget* parent)
{
    return number ? nullptr : new PluginConfigPage (this, parent);
}

int
Plugin::perProjectConfigPages() const
{
    return 1;
}

ConfigPage*
Plugin::perProjectConfigPage(int number,
                             const ProjectConfigOptions& options,
                             QWidget* parent)
{
    return number ? nullptr : new ProjectConfigPage(this, options.project, parent);
}

ContextMenuExtension
#if KDEVPLATFORM_VERSION < KDEVPLATFORM_VERSION_CHECK(5, 2, 0)
Plugin::contextMenuExtension(Context* context)
{
#else
Plugin::contextMenuExtension(Context* context, QWidget* parent)
{
    Q_UNUSED(parent);
#endif

    ContextMenuExtension extension;

    if (context->hasType(Context::EditorContext))
    {
        // FIXME add test for doc exists before adding item ?
        extension.addAction(ContextMenuExtension::ExtensionGroup, m_showAction);
    }

    return extension;
}

}

}

#include "plugin.moc"
