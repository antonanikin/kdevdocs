/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <interfaces/iplugin.h>
#include <QVariant>

#include "version_check.h"

class QDBusInterface;
class QTimer;

namespace KDevelop
{

class Declaration;
class IProject;

namespace KDevDocs
{

class Plugin : public IPlugin
{
    Q_OBJECT

public:
    Plugin(QObject* parent, const QVariantList& = QVariantList());

    ~Plugin() override;

    bool
    startKdevdocs();

    QDBusInterface*
    interface();

    int
    configPages() const override;

    ConfigPage*
    configPage(int number, QWidget* parent) override;

    int
    perProjectConfigPages() const override;

    ConfigPage*
    perProjectConfigPage(int number, const ProjectConfigOptions& options, QWidget* parent) override;

    ContextMenuExtension
#if KDEVPLATFORM_VERSION < KDEVPLATFORM_VERSION_CHECK(5, 2, 0)
    contextMenuExtension(Context* context) override;
#else
    contextMenuExtension(Context* context, QWidget* parent) override;
#endif

signals:
    void
    kdevdocsStarted();

private:
    void
    updateProject();

    bool
    show(const QString& declaration);

    void
    showSlot();

private:
    QDBusInterface* m_interface;
    QTimer* m_startingTimer;

    IProject* m_project;

    QAction* m_showAction;

    bool m_pendingQuery;
};

}

}
