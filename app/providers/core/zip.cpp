/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "zip.h"

#include "kuba_zip/zip.h"

#include <KLocalizedString>

#include <QFile>

namespace KDevDocs
{

Zip::Zip(const QString& name)
    : m_name(name)
    , m_zip(nullptr)
{
}

Zip::~Zip()
{
    close();
}

QString
Zip::error() const
{
    return m_error;
}

bool
Zip::open()
{
    close();

    if (!QFile::exists(m_name))
    {
        m_error = i18n("Archive '%1' does not exists", m_name);
        return false;
    }

    m_zip = zip_open(qPrintable(m_name), 0, 'r');
    if (!m_zip)
    {
        m_error = i18n("Unable to open archive '%1'", m_name);
        return false;
    }

    return true;
}

void
Zip::close()
{
    zip_close(m_zip);
    m_zip = nullptr;
}

bool
Zip::entryOpen(const QString& zipPath)
{
    if (!m_zip)
    {
        m_error = i18n("Archive '%1' is not open", m_name);
        return false;
    }

    if (zip_entry_open(m_zip, qPrintable(zipPath)) < 0)
    {
        m_error = i18n("Can't find '%1' in archive '%2'", zipPath, m_name);
        return false;
    }

    return true;
}

QString
fixedPath(const QString& path)
{
    auto fixedPath = path;

    int slashCount = 0;
    while (slashCount < path.size() && path.at(slashCount) ==  QLatin1Char('/'))
    {
        ++slashCount;
    }

    return path.mid(slashCount);
}

bool
Zip::extract(const QString& zipPath, const QString& extractedPath)
{
    const auto zipFixedPath = fixedPath(zipPath);
    if (!entryOpen(zipFixedPath))
    {
        return false;
    }

    if (zip_entry_fread(m_zip, qPrintable(extractedPath)) < 0)
    {
        m_error = i18n("Can't extract '%1' from archive '%2'", zipFixedPath, m_name);
        zip_entry_close(m_zip);
        return false;
    }

    zip_entry_close(m_zip);
    return true;
}

QByteArray
Zip::read(const QString& zipPath)
{
    QByteArray fileData;

    const auto zipFixedPath = fixedPath(zipPath);
    if (entryOpen(zipFixedPath))
    {
        const int fileSize = zip_entry_size(m_zip);
        fileData.resize(fileSize);

        zip_entry_noallocread(m_zip, fileData.data(), fileSize);
        zip_entry_close(m_zip);
    }

    return fileData;
}

}
