/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QString>

struct zip_t;

namespace KDevDocs
{

class Zip
{
public:
    explicit
    Zip(const QString& name);

    ~Zip();

    bool
    open();

    void
    close();

    bool
    extract(const QString& zipPath, const QString& extractedPath);

    QByteArray
    read(const QString& zipPath);

    QString
    error() const;

private:
    bool
    entryOpen(const QString& zipPath);

private:
    QString m_name;
    QString m_error;
    zip_t* m_zip;
};

}
