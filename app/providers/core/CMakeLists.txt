add_subdirectory(kuba_zip)

add_library(kdevdocs_provider_core STATIC
    zip.cpp)

target_link_libraries(kdevdocs_provider_core
    kdevdocs_core

    kuba_zip
)
