add_library(kuba_zip STATIC zip.c)

set_target_properties(kuba_zip PROPERTIES
    C_STANDARD 99
)

target_compile_options(kuba_zip
    PRIVATE ${SUPPRESSED_WARNINGS}
)
