/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "kdd_installed.h"
#include "kdd_provider.h"

#include "content_item.h"
#include "index_item.h"
#include "zip.h"

#include <KLocalizedString>

#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLoggingCategory>

namespace KDevDocs
{

namespace KDD
{

Q_LOGGING_CATEGORY(INSTALLED, "kdevdocs.kapidox.installed");

Installed::Installed(const QString& path)
    : InstalledDocset(path, Provider::self())
{
    init(nullptr);
}

Installed::Installed(const QString& path, InstalledDocset* parent)
    : InstalledDocset(path, parent)

{
    init(parent);
}

Installed::~Installed() = default;

void
Installed::init(InstalledDocset* parent)
{
    // TODO icon inheritance ?
    setIcon(QIcon::fromTheme("kde")); // FIXME

    if (parent)
    {
        setId(parent->id() + QLatin1Char('/') + id());
    }
}

// FIXME move into class ?
static const QHash<QString, IndexItem::Type> typeHash =
{
    { QStringLiteral("namespace"),        IndexItem::Type::NAMESPACE },
    { QStringLiteral("class"),            IndexItem::Type::CLASS },
    { QStringLiteral("qmlclass"),         IndexItem::Type::CLASS },
    { QStringLiteral("enum"),             IndexItem::Type::ENUMERATION },
    { QStringLiteral("enumeration"),      IndexItem::Type::ENUMERATION },
    { QStringLiteral("value"),            IndexItem::Type::VALUE },
    { QStringLiteral("enumvalue"),        IndexItem::Type::VALUE },
    { QStringLiteral("typedef"),          IndexItem::Type::TYPE },
    { QStringLiteral("qmlbasictype"),     IndexItem::Type::TYPE },
    { QStringLiteral("method"),           IndexItem::Type::METHOD },
    { QStringLiteral("qmlmethod"),        IndexItem::Type::METHOD },
    { QStringLiteral("signal"),           IndexItem::Type::METHOD }, // FIXME
    { QStringLiteral("slot"),             IndexItem::Type::METHOD }, // FIXME
    { QStringLiteral("qmlsignal"),        IndexItem::Type::METHOD }, // FIXME
    { QStringLiteral("function"),         IndexItem::Type::FUNCTION },
    { QStringLiteral("operator"),         IndexItem::Type::OPERATOR },
    { QStringLiteral("constructor"),      IndexItem::Type::CONSTRUCTOR },
    { QStringLiteral("variable"),         IndexItem::Type::VARIABLE },
    { QStringLiteral("property"),         IndexItem::Type::PROPERTY },
    { QStringLiteral("qmlproperty"),      IndexItem::Type::PROPERTY },
    { QStringLiteral("qmlmodule"),        IndexItem::Type::MODULE },
    { QStringLiteral("qmlpropertygroup"), IndexItem::Type::MODULE } // FIXME
};

void
Installed::processContentsSection(const QJsonObject& sectionObject, TreeItem* parent)
{
    const auto name = sectionObject.value(QStringLiteral("name")).toString();
    const auto href = sectionObject.value(QStringLiteral("href")).toString();

    auto item = new ContentItem(this, parent, name, href);

    const auto sections = sectionObject.value(QStringLiteral("sections")).toArray();
    for (const auto& section : sections)
    {
        processContentsSection(section.toObject(), item);
    }
};

void
Installed::loadIndexContents(ContentItem* contentsRoot)
{
    m_zip.reset(new Zip(QStringLiteral("%1/data.zip").arg(path())));
    if (!m_zip->open() && isComposite())
    {
        setState(ERROR);
        setError(m_zip->error());
        return;
    }

    {
        const auto indexFileData = fileData(QStringLiteral("index.json"));
        const auto items = QJsonDocument::fromJson(indexFileData).array();
        for (const auto& item : items)
        {
            const auto object = item.toObject();

            const auto name = object.value(QStringLiteral("name")).toString();
            const auto href = object.value(QStringLiteral("href")).toString();
            const auto type = object.value(QStringLiteral("type")).toString();
            const auto realType = typeHash.value(type, IndexItem::Type::UNKNOWN);
            if (realType == IndexItem::Type::UNKNOWN)
            {
                qDebug() << "UNKNOWN TYPE" << type;
            }

            m_index += new IndexItem(this, name, href, realType);
        }
    }

    {
        auto tocFileData = fileData(QStringLiteral("toc.json"));
        const auto topSectionObject = QJsonDocument::fromJson(tocFileData).object();
        processContentsSection(topSectionObject, contentsRoot);

        contentsRoot->fixSingleChild();
    }
}

QByteArray
Installed::fileData(const QString& zipPath) const
{
    return m_zip->read(zipPath);
}

}

}
