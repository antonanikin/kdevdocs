/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "kdd_source.h"
#include "kdd_provider.h"

#include <KLocalizedString>

namespace KDevDocs
{

namespace KDD
{

Source::Source()
    : KDevDocs::Source(Provider::self())
{
    setName(i18n("KDE and Qt API"));
}

Source::~Source() = default;

void
Source::load()
{
    setState(BUSY);
    clear();
    setState(IDLE);
}

void
Source::update()
{
    setState(BUSY);
    setState(IDLE);
}

}

}
