/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "qthelp_available_qmake.h"

#include "globals.h"
#include "installed_docset.h"
#include "metadata_config.h"
#include "provider.h"
#include "qthelp_qmake_information.h"
#include "qthelp_source.h"

#include <KLocalizedString>

#include <QDir>

namespace KDevDocs
{

namespace QtHelp
{

AvailableQmake::AvailableQmake(Source* source, const QString& qmakeExecutable)
    : AvailableDocset(source)
    , m_qmakeExecutable(qmakeExecutable)
{
    if (!qmakeExecutable.isEmpty())
    {
        QMakeInformation info(qmakeExecutable);

        setId(createId(qmakeExecutable));
        setName(i18n("Docset from Qt %1 installation", info.fullVersion));
        setVersion(info.fullVersion);
    }
    else
    {
        setId(createId(QStringLiteral("4d47d2a2-745d-4922-9fcf-539c2ef05cf1")));
        setName(i18n("Docset from custom Qt installation"));
    }
}

AvailableQmake::~AvailableQmake() = default;

void
AvailableQmake::install()
{
    if (m_qmakeExecutable.isEmpty())
    {
        // FIXME add creator
        return;
    }

    setState(INSTALLING);
    createDocsetDir();

    {
        // FIXME add info as class member ?
        QMakeInformation info(m_qmakeExecutable);
        MetadataConfig config(this, QStringLiteral("Qt%1").arg(info.majorVersion));
        // FIXME add child config class ?
        config.setValue(QStringLiteral("qmake"), m_qmakeExecutable);
        config.save();
    }

    setState(INSTALLED);

    auto docset = provider()->addDocset(path());
    docset->reload();
}

void
AvailableQmake::breakInstallation()
{
}

}

}
