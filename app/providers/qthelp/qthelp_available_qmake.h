/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "available_docset.h"

namespace KDevDocs
{

namespace QtHelp
{

class Source;

class AvailableQmake : public AvailableDocset
{
    Q_OBJECT

public:
    AvailableQmake(Source* source, const QString& qmakeExecutable);

    ~AvailableQmake() override;

    void
    install() override;

    void
    breakInstallation() override;

protected:
    QString m_qmakeExecutable;
};

}

}
