/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "qthelp_installed.h"

#include "metadata_config.h"
#include "qthelp_qmake_information.h"
#include "qthelp_database.h"
#include "qthelp_provider.h"

#include <QLoggingCategory>

namespace KDevDocs
{

namespace QtHelp
{

Q_LOGGING_CATEGORY(DOCSET, "kdevdocs.qthelp.docset");

Installed::Installed(const QString& path)
    : InstalledDBDocset(path, Provider::self(), QStringLiteral("qch"))
{
    loadDatabases();

    // FIXME add child config class ?
    auto qmakeExecutable = metadataConfig()->value(QStringLiteral("qmake"));
    if (!qmakeExecutable.isEmpty())
    {
        QMakeInformation information(qmakeExecutable);
        if (information.qchPath.isEmpty())
        {
            return;
        }

        addDirectory(information.qchPath);

        metadataConfig()->setVersion(information.fullVersion);
        metadataConfig()->save();
    }
}

Installed::~Installed() = default;

QByteArray
Installed::fileData(const QString& path) const
{
    auto elements = path.split(QLatin1Char('/'), QString::SkipEmptyParts);
    if (elements.size() < 4)
    {
        qCDebug(DOCSET, "invalid path - it should contains at least 4 elements: '%s'", qPrintable(path));
        return {};
    }


    const auto baseName = elements.takeFirst();
    elements.takeFirst(); // skip unused nameSpace

    const auto folderName = elements.takeFirst();
    const auto filePath   = elements.join(QLatin1Char('/'));

    if (auto db = m_dbs.value(baseName, nullptr))
    {
        auto data = db->fileData(filePath);
        if (!data.isEmpty())
        {
            return data;
        }
    }

    // Try to find proper QCH-file, when required file is not present in the current one.
    //
    // Example:
    //
    // QPushButton page (Qt Widgets QCH) contains link to QString page (Qt Core QCH). Link
    // looks like "<a href="../qtcore/qstring.html">QString</a>". But our url hierarchy
    // (see QtHelp::Database::createHref() for details) doesn't "redirect" such link into
    // proper QCH, so we should perform additional search based on url's "folder" part.

    for (auto db : m_dbs)
    {
        auto qthelpDB = static_cast<Database*>(db);
        Q_ASSERT(qthelpDB);

        if (qthelpDB->folderName() == folderName)
        {
            auto data = qthelpDB->fileData(filePath);
            if (!data.isEmpty())
            {
                return data;
            }

            // If data is empty we still try to find another QCH-file with same folder value.
        }
    }

    return {};
}

InstalledDBDocsetDatabase*
Installed::createDatabase(const QString& filePath)
{
    return new Database(this, filePath);
}

}

}
