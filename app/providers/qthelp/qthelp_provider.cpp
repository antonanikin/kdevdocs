/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "qthelp_provider.h"

#include "core.h"
#include "qthelp_installed.h"
#include "qthelp_source.h"

#include <KLocalizedString>

#include <QDir>

namespace KDevDocs
{

namespace QtHelp
{

Provider* Provider::self()
{
    static Provider* instance = nullptr;

    if (!instance)
    {
        instance = new Provider;
        instance->m_sources += new Source();
    }

    return instance;
}

Provider::Provider()
    : KDevDocs::Provider(QStringLiteral("qthelp"))
{
}

Provider::~Provider() = default;

InstalledDocset*
Provider::createDocset(const QString& path, InstalledDocset* /*parent*/)
{
    return new Installed(path);
}

}

}
