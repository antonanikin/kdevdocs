/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "installed_dbdocset_database.h"

#include <QUrl>

class QSqlDatabase;

namespace KDevDocs
{

namespace QtHelp
{

class Installed;

class Database : public InstalledDBDocsetDatabase
{
public:
    Database(Installed* docset, const QString& filePath);

    ~Database() override;

    QString
    folderName() const;

    virtual void
    loadIndexContents(ContentItem* contentsRoot, IndexVector& index) override;

    QByteArray
    fileData(const QString& filePath) override;

private:
    void
    loadIndex(IndexVector& index);

    void
    loadContents(ContentItem* contentsRoot);

    QString
    createHref(const QString& filePath, const QString& fragment = QString()) const;

private:
    QString m_connectionName;
    QSqlDatabase* m_db;
    QString m_nameSpace;
    QString m_folderName;
};

}

}
