/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "qthelp_source.h"

#include "qthelp_available_qmake.h"
#include "qthelp_qmake_information.h"
#include "qthelp_provider.h"
#include "available_dbdocset.h"

#include <KLocalizedString>

namespace KDevDocs
{

namespace QtHelp
{

Source::Source()
    : KDevDocs::Source(Provider::self())
{
    setName(i18n("QtHelp"));
}

Source::~Source() = default;

void
Source::load()
{
    setState(BUSY);
    clear();

    for (int majorVersion : { 4, 5 })
    {
        auto executable = qmakeExecutable(majorVersion);
        if (!executable.isEmpty())
        {
            auto detectedQmakeDocset = new AvailableQmake(this, executable);
            detectedQmakeDocset->setPriority(0);

            addDocset(detectedQmakeDocset);
        }
    }

    // FIXME not implemented yet
//     auto customQmakeDocset = new AvailableQmake(QString());
//     customQmakeDocset->setPriority(1);
//     m_docsets += customQmakeDocset;

    auto fileDocset = new AvailableDBDocset(
        this,
        AvailableDBDocset::FILE,
        QStringLiteral("qch"));

    fileDocset->setPriority(2);
    addDocset(fileDocset);

    auto directoryDocset = new AvailableDBDocset(
        this,
        AvailableDBDocset::DIRECTORY,
        QStringLiteral("qch"));

    directoryDocset->setPriority(3);
    addDocset(directoryDocset);

    setState(IDLE);
}

void
Source::update()
{
}

}

}
