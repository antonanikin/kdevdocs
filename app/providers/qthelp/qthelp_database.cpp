/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "qthelp_database.h"

#include "content_item.h"
#include "globals.h"
#include "index_item.h"
#include "qthelp_installed.h"

#include <QDataStream>
#include <QFileInfo>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStack>
#include <QVariant>

namespace KDevDocs
{

namespace QtHelp
{

Database::Database(Installed* docset, const QString& filePath)
    : InstalledDBDocsetDatabase(docset, filePath)
    , m_connectionName(createId())
    , m_db(new QSqlDatabase(QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), m_connectionName)))
{
    m_db->setDatabaseName(filePath);
    if (!m_db->open())
    {
        return;
    }

    QSqlQuery q(*m_db);
    q.exec(QStringLiteral("SELECT Name FROM NamespaceTable"));
    if (!q.next())
    {
        return;
    }
    m_nameSpace = q.value(0).toString();

    q.exec(QStringLiteral("SELECT Name FROM FolderTable WHERE Id=1"));
    if (!q.next())
    {
        return;
    }
    m_folderName = q.value(0).toString();

    m_isValid = true;
}

Database::~Database()
{
    delete m_db;
    QSqlDatabase::removeDatabase(m_connectionName);
}

QString
Database::folderName() const
{
    return m_folderName;
}

void
Database::loadIndexContents(ContentItem* contentsRoot, IndexVector& index)
{
    loadIndex(index);
    loadContents(contentsRoot);
}

void
Database::loadIndex(IndexVector& index)
{
    QSqlQuery q(*m_db);
    q.exec(QStringLiteral(
        "SELECT IndexTable.identifier, FolderTable.Name, FileNameTable.Name, IndexTable.Anchor "
        "FROM IndexTable "
        "LEFT JOIN FileNameTable ON FileNameTable.FileId = IndexTable.FileId "
        "LEFT JOIN FolderTable ON FolderTable.Id = FileNameTable.FolderId "
        "LEFT JOIN NamespaceTable ON NamespaceTable.Id = IndexTable.NamespaceId"));

    while (q.next())
    {
        const auto name = q.value(0).toString();
        const auto href = createHref(q.value(2).toString(), q.value(3).toString());

        index += new IndexItem(m_docset, name, href);
    }
}

void
Database::loadContents(ContentItem* contentsRoot)
{
    QSqlQuery q(*m_db);
    q.exec(QStringLiteral(
        "SELECT ContentsTable.Data, FolderTable.Name "
        "FROM ContentsTable "
        "LEFT JOIN FolderTable ON FolderTable.id = ContentsTable.NamespaceId"));

    if (!q.next())
    {
        // FIXME logging category
//         qCDebug(KDEVDOCS_QTHELP_DOCSET) << nameSpace << "DB error: ContentsTable.Data is empty";
        return;
    }

    auto data = q.value(0).toByteArray();
    QDataStream stream(data);

    TreeItem* parent  = contentsRoot;
    TreeItem* current = nullptr;
    QStack<int> stack;

    int depth;
    QString name;
    QString fileName;

    stack.push(0);
    while (!stream.atEnd())
    {
        stream >> depth;
        stream >> fileName;
        stream >> name;

        if (depth > stack.top())
        {
            parent = current;
            stack.push(depth);
        }
        else if (depth < stack.top())
        {
            while (stack.top() != depth)
            {
                current = current->parentItem();
                parent = current->parentItem();
                stack.pop();
            }
        }

        current = new ContentItem(m_docset, parent, name, createHref(fileName));
    }
}

QByteArray
Database::fileData(const QString& filePath)
{
    QByteArray data;

    QSqlQuery q(*m_db);
    q.exec(QStringLiteral(
        "SELECT FileDataTable.Data "
        "FROM FileDataTable "
        "LEFT JOIN FileNameTable ON FileDataTable.Id = FileNameTable.FileId "
        "LEFT JOIN FolderTable ON FolderTable.Id = FileNameTable.FolderId "
        "LEFT JOIN NamespaceTable ON NamespaceTable.Id = FolderTable.NamespaceId "
        "WHERE NamespaceTable.Name='%1' "
        "AND FolderTable.Name='%2' "
        "AND (FileNameTable.Name='%3' OR FileNameTable.Name='./%3') ").arg(
               m_nameSpace,
               m_folderName,
            filePath));

    if (q.next())
    {
        data = qUncompress(q.value(0).toByteArray());
    }

    return data;
}

QString
Database::createHref(const QString& filePath, const QString& fragment) const
{
    // Selected url hierarchy looks too complex (basename prefix before namespace + folder),
    // but it allows us to have QCH files with same namespace inside one docset - such situation
    // may happens when user creates docset from some QCH-directory.

    auto href = QStringLiteral("%1/%2/%3/%4").arg(m_baseName, m_nameSpace, m_folderName, filePath);
    if (!fragment.isEmpty())
    {
        href += QLatin1Char('#') + fragment;
    }

    return href;
}

}

}
