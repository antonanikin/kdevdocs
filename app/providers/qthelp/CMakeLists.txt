add_library(kdevdocs_qthelp STATIC
    qthelp_installed.cpp
    qthelp_provider.cpp
    qthelp_source.cpp
    qthelp_database.cpp
    qthelp_available_qmake.cpp
    qthelp_qmake_information.cpp
)

target_link_libraries(kdevdocs_qthelp
    kdevdocs_provider_core
)

install(FILES icons/qthelp.png icons/qthelp@2x.png DESTINATION ${PROVIDER_ICONS_INSTALL_DIR})
