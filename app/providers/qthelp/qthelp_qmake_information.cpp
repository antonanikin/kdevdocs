/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "qthelp_qmake_information.h"

#include "globals.h"
#include "qthelp_provider.h"
#include "utils.h"

#include <QDir>
#include <QStandardPaths>
#include <QVariant>

namespace KDevDocs
{

namespace QtHelp
{

QString
qmakeExecutable(int majorVersion)
{
    QStringList executableCandidates;
    if (majorVersion == 4)
    {
        executableCandidates += QStringLiteral("qmake-qt4");
    }
    else
    {
        executableCandidates += QStringLiteral("qmake-qt5");
    }
    executableCandidates += QStringLiteral("qmake");

    for (const QString& candidate : qAsConst(executableCandidates))
    {
        auto executable = QStandardPaths::findExecutable(candidate);
        if (!executable.isEmpty() &&
            QMakeInformation(executable).majorVersion == majorVersion)
        {
            return executable;
        }
    }

    return QString();
}

QStringList
qchFiles(const QString& qchPath)
{
    QStringList files;

    if (!QFileInfo::exists(qchPath))
    {
        return files;
    }

    QDir dir(qchPath);
    const auto names = dir.entryList({ QStringLiteral("*.qch") }, QDir::Files);
    for (const QString& name : names)
    {
        files += dir.filePath(name);
    }

    return files;
}

QMakeInformation::QMakeInformation()
{
}

QMakeInformation::QMakeInformation(const QString& qmakeExecutable)
{
    if (!QFileInfo::exists(qmakeExecutable))
    {
        return;
    }
    executable = qmakeExecutable;

    executeProcess(executable,
                   { QStringLiteral("-query"), QStringLiteral("QT_VERSION") },
                   fullVersion);

    if (fullVersion.isEmpty() || !fullVersion.at(0).isDigit())
    {
        return;
    }

    majorVersion = fullVersion.leftRef(1).toInt();

    executeProcess(executable,
                   { QStringLiteral("-query"), QStringLiteral("QT_INSTALL_DOCS") },
                   qchPath);

    if (qchPath.isEmpty())
    {
        return;
    }

    if (majorVersion == 4)
    {
        auto path = QStringLiteral("%1/qch").arg(qchPath);
        if (QFileInfo::exists(path))
        {
            qchPath = path;
        }
    }

    qchFiles = QtHelp::qchFiles(qchPath);

    isValid = true;
    // FIXME this leads to recursion - DocsetSource created from Provider constructor
//     exists = Provider::self()->hasDocsetWithQchPath(qchPath);
}

QMakeInformation::operator QVariant() const
{
    QVariant value;
    value.setValue(*this);

    return value;
}

}

}
