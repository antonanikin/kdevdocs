find_package(KF5Archive REQUIRED)

add_library(kdevdocs_dash STATIC
    dash_archive.cpp
    dash_available.cpp
    dash_installed.cpp
    dash_provider.cpp
    dash_source.cpp
)

target_link_libraries(kdevdocs_dash
    kdevdocs_provider_core

    KF5::Archive
)

install(FILES icons/dash.png icons/dash@2x.png DESTINATION ${PROVIDER_ICONS_INSTALL_DIR})
