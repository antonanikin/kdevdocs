/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "dash_installed.h"

#include "content_item.h"
#include "core.h"
#include "dash_archive.h"
#include "dash_provider.h"
#include "index_item.h"
#include "metadata_config.h"
#include "zip.h"

#include <KLocalizedString>

#include <QDir>
#include <QLoggingCategory>
#include <QRegularExpression>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QTemporaryDir>
#include <QXmlStreamReader>

namespace KDevDocs
{

namespace Dash
{

Q_LOGGING_CATEGORY(INSTALLED, "kdevdocs.dash.installed");

DocsetItemType::Type
parseSymbolType(const QString& str);

Installed::Installed(const QString& path)
    : InstalledDocset(path, Provider::self())
{
    m_archiveName = metadataConfig()->value(QStringLiteral("archiveName"));
    m_archivePath = QStringLiteral("%1/%2.zip").arg(path, m_archiveName);
}

Installed::Installed(const QString& path, InstalledDocset* parent)
    : InstalledDocset(path, parent)
{
    m_archiveName = metadataConfig()->value(QStringLiteral("archiveName"));
    m_archivePath = QStringLiteral("%1/%2.zip").arg(path, m_archiveName);
}

Installed::~Installed() = default;

QString
Installed::createHref(const QString& path, const QString& fragment) const
{
    QString realPath;
    QString realFragment;

    if (fragment.isEmpty())
    {
        const QStringList urlParts = path.split(QLatin1Char('#'));
        realPath = urlParts[0];
        if (urlParts.size() > 1)
        {
            realFragment = urlParts[1];
        }
    }
    else
    {
        realPath = path;
        realFragment = fragment;
    }

    static const QRegularExpression dashEntryRE(QStringLiteral("<dash_entry_.*>"));
    realPath.remove(dashEntryRE);
    realFragment.remove(dashEntryRE);

    // TODO test this, applying may break some urls?
    static const QRegularExpression dashAppleRefRE(QStringLiteral("\\/\\/((dash)|(apple))_ref.*\\/"));
    realFragment.remove(dashAppleRefRE);

    auto href = QStringLiteral("%1/Contents/Resources/Documents/%2").arg(m_archiveName, realPath);
    if (!realFragment.isEmpty())
    {
        href += QLatin1Char('#') + realFragment;
    }

    return href;
}

void
Installed::loadIndexContents(ContentItem* contentsRoot)
{
    m_zip.reset(new Zip(m_archivePath));
    if (!m_zip->open())
    {
        setState(ERROR);
        setError(m_zip->error());
        return;
    }

    if (loadBinToc(contentsRoot, m_archivePath))
    {
        for (int i = 0; i < contentsRoot->childItemsCount(); ++i)
        {
            auto groupItem = contentsRoot->childItem(i);
            for (int j = 0; j < groupItem->childItemsCount(); ++j)
            {
                auto contentItem = groupItem->childItem(j)->cast<ContentItem>();
                m_index += new IndexItem(this, contentItem->name(), contentItem->href(), contentItem->type());
            }
        }

        return;
    }

    qCDebug(INSTALLED,
            "unable to load binary contents for docset '%s', start loading from database",
            qPrintable(name()));

    if (!loadIndexContentsFromDB(contentsRoot))
    {
        setState(ERROR);
        return;
    }

    parsePlist(contentsRoot);
    if (contentsRoot->url().isEmpty())
    {
        const auto mainPagePath = createHref(QStringLiteral("index.html"));
        if (!fileData(mainPagePath).isEmpty())
        {
            contentsRoot->setHref(mainPagePath);
        }
        else
        {
            qCDebug(INSTALLED, "unable to detect start page for docset '%s'", qPrintable(name()));
        }
    }

    contentsRoot->sortChilds();
    saveBinToc(contentsRoot, m_archivePath);
}

bool
Installed::loadIndexContentsFromDB(ContentItem* contentsRoot)
{
    // https://www.sqlite.org/tempfiles.html
    const static QStringList suffixes =
    {
        QString(),
        QStringLiteral("-journal"),
        QStringLiteral("-shm"),
        QStringLiteral("-wal"),
    };

    QTemporaryDir workDir;

    const auto zipDirectory = QStringLiteral("%1/Contents/Resources/").arg(m_archiveName);
    for (const auto& suffix : suffixes)
    {
        const auto fileName = QStringLiteral("docSet.dsidx") + suffix;
        const auto zipPath = zipDirectory + fileName;
        const auto extractedPath = QStringLiteral("%1/%2").arg(workDir.path(), fileName);

        m_zip->extract(zipPath, extractedPath);
    }

    const auto dbPath = QStringLiteral("%1/docSet.dsidx").arg(workDir.path());
    if (!QFile::exists(dbPath))
    {
        setError(i18n("Docset archive does not contains database"));
        return false;
    }

    bool result = false;
    // https://stackoverflow.com/a/48835416
    while (true)
    {
        auto db = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), dbPath);
        db.setDatabaseName(dbPath);
        if (!db.open())
        {
            setError(i18n("Unable to open docset database: %1", db.lastError().text()));
            break;
        }

        QSqlQuery query(db);
        if (db.tables().contains(QStringLiteral("searchIndex")))
        {
            query.exec(QStringLiteral("SELECT id, name, type, path FROM searchIndex ORDER BY name ASC"));
        }
        else
        {
            query.exec(QStringLiteral(
                "SELECT ztoken.z_pk, ztokenname, ztypename, zpath, zanchor "
                "FROM ztoken "
                "LEFT JOIN ztokentype ON ztoken.ztokentype = ztokentype.z_pk "
                "LEFT JOIN ztokenmetainformation ON ztoken.z_pk = ztokenmetainformation.ztoken "
                "LEFT JOIN zfilepath ON ztokenmetainformation.zfile = zfilepath.z_pk "
                "ORDER BY ztokenname ASC"));
        }

        if (query.size() == 0)
        {
            setError(i18n("Docset database contains empty search index"));
            break;
        }

        QMap<DocsetItemType::Type, ContentItem*> groups;
        while (query.next())
        {
            const auto name = query.value(1).toString();
            const auto type = parseSymbolType(query.value(2).toString());
            const auto href = createHref(query.value(3).toString(), query.value(4).toString());

            auto indexItem = new IndexItem(this, name, href, type);
            m_index += indexItem;

            ContentItem* groupItem;
            if (groups.contains(type))
            {
                groupItem = groups[type];
            }
            else
            {
                groupItem = new ContentItem(this, contentsRoot, DocsetItemType::toString(type, true), QString(), type);
                groups[type] = groupItem;
            }

            new ContentItem(this, groupItem, name, href, type);
        }

        result = true;
        break;
    }

    QSqlDatabase::removeDatabase(dbPath);

    return result;
}

void
Installed::parsePlist(ContentItem* contentsRoot)
{
    const auto plistData = fileData(QStringLiteral("%1/Contents/Info.plist").arg(m_archiveName));
    if (plistData.isEmpty())
    {
        return;
    }

    QXmlStreamReader xml(plistData);
    while (!xml.atEnd())
    {
        auto token = xml.readNext();
        if (token != QXmlStreamReader::StartElement)
        {
            continue;
        }

        if (xml.name() != QStringLiteral("key"))
        {
            continue; // TODO: Should it fail here?
        }

        if (xml.readElementText() != QStringLiteral("dashIndexFilePath"))
        {
            continue;
        }

        // Skip whitespaces between tags
        while (xml.readNext() == QXmlStreamReader::Characters);

        if (xml.tokenType() != QXmlStreamReader::StartElement ||
            xml.name() != QStringLiteral("string"))
        {
            break;
        }

        auto value = xml.readElementText();
        if (!value.isEmpty())
        {
            auto mainPagePath = createHref(value);
            auto mainPageData = fileData(mainPagePath);

            if (!mainPageData.isEmpty())
            {
                contentsRoot->setHref(mainPagePath);
                break;
            }
        }

        break;
    }
}

QByteArray
Installed::fileData(const QString& path) const
{
    return m_zip->read(path);
}

DocsetItemType::Type
parseSymbolType(const QString& str)
{
    const static QHash<QString, DocsetItemType::Type> aliases =
    {
        { QStringLiteral("Package Attributes"), DocsetItemType::ATTRIBUTE },
        { QStringLiteral("Private Attributes"), DocsetItemType::ATTRIBUTE },
        { QStringLiteral("Protected Attributes"), DocsetItemType::ATTRIBUTE },
        { QStringLiteral("Public Attributes"), DocsetItemType::ATTRIBUTE },
        { QStringLiteral("Static Package Attributes"), DocsetItemType::ATTRIBUTE },
        { QStringLiteral("Static Private Attributes"), DocsetItemType::ATTRIBUTE },
        { QStringLiteral("Static Protected Attributes"), DocsetItemType::ATTRIBUTE },
        { QStringLiteral("Static Public Attributes"), DocsetItemType::ATTRIBUTE },
        { QStringLiteral("XML Attributes"), DocsetItemType::ATTRIBUTE },

        { QStringLiteral("binding"), DocsetItemType::BINDING },

        { QStringLiteral("cat"), DocsetItemType::CATEGORY },
        { QStringLiteral("Groups"), DocsetItemType::CATEGORY },
        { QStringLiteral("Pages"), DocsetItemType::CATEGORY },

        { QStringLiteral("cl"), DocsetItemType::CLASS },
        { QStringLiteral("specialization"), DocsetItemType::CLASS },
        { QStringLiteral("tmplt"), DocsetItemType::CLASS },

        { QStringLiteral("data"), DocsetItemType::CONSTANT },
        { QStringLiteral("econst"), DocsetItemType::CONSTANT },
        { QStringLiteral("enumdata"), DocsetItemType::CONSTANT },
        { QStringLiteral("enumelt"), DocsetItemType::CONSTANT },
        { QStringLiteral("clconst"), DocsetItemType::CONSTANT },
        { QStringLiteral("structdata"), DocsetItemType::CONSTANT },
        { QStringLiteral("writerid"), DocsetItemType::CONSTANT },
        { QStringLiteral("Notifications"), DocsetItemType::CONSTANT },

        { QStringLiteral("structctr"), DocsetItemType::CONSTRUCTOR },
        { QStringLiteral("Public Constructors"), DocsetItemType::CONSTRUCTOR },

        { QStringLiteral("enum"), DocsetItemType::ENUMERATION },
        { QStringLiteral("Enumerations"), DocsetItemType::ENUMERATION },

        { QStringLiteral("event"), DocsetItemType::EVENT },
        { QStringLiteral("Public Events"), DocsetItemType::EVENT },
        { QStringLiteral("Inherited Events"), DocsetItemType::EVENT },
        { QStringLiteral("Private Events"), DocsetItemType::EVENT },

        { QStringLiteral("Data Fields"), DocsetItemType::FIELD },

        { QStringLiteral("dcop"), DocsetItemType::FUNCTION },
        { QStringLiteral("func"), DocsetItemType::FUNCTION },
        { QStringLiteral("ffunc"), DocsetItemType::FUNCTION },
        { QStringLiteral("signal"), DocsetItemType::FUNCTION },
        { QStringLiteral("slot"), DocsetItemType::FUNCTION },
        { QStringLiteral("grammar"), DocsetItemType::FUNCTION },
        { QStringLiteral("Function Prototypes"), DocsetItemType::FUNCTION },
        { QStringLiteral("Functions/Subroutines"), DocsetItemType::FUNCTION },
        { QStringLiteral("Members"), DocsetItemType::FUNCTION },
        { QStringLiteral("Package Functions"), DocsetItemType::FUNCTION },
        { QStringLiteral("Private Member Functions"), DocsetItemType::FUNCTION },
        { QStringLiteral("Private Slots"), DocsetItemType::FUNCTION },
        { QStringLiteral("Protected Member Functions"), DocsetItemType::FUNCTION },
        { QStringLiteral("Protected Slots"), DocsetItemType::FUNCTION },
        { QStringLiteral("Public Member Functions"), DocsetItemType::FUNCTION },
        { QStringLiteral("Public Slots"), DocsetItemType::FUNCTION },
        { QStringLiteral("Signals"), DocsetItemType::FUNCTION },
        { QStringLiteral("Static Package Functions"), DocsetItemType::FUNCTION },
        { QStringLiteral("Static Private Member Functions"), DocsetItemType::FUNCTION },
        { QStringLiteral("Static Protected Member Functions"), DocsetItemType::FUNCTION },
        { QStringLiteral("Static Public Member Functions"), DocsetItemType::FUNCTION },

        { QStringLiteral("doc"), DocsetItemType::GUIDE },

        { QStringLiteral("ns"), DocsetItemType::NAMESPACE },

        { QStringLiteral("macro"), DocsetItemType::MACRO },

        { QStringLiteral("clm"), DocsetItemType::METHOD },
        { QStringLiteral("enumcm"), DocsetItemType::METHOD },
        { QStringLiteral("enumctr"), DocsetItemType::METHOD },
        { QStringLiteral("enumm"), DocsetItemType::METHOD },
        { QStringLiteral("intfctr"), DocsetItemType::METHOD },
        { QStringLiteral("intfcm"), DocsetItemType::METHOD },
        { QStringLiteral("intfm"), DocsetItemType::METHOD },
        { QStringLiteral("intfsub"), DocsetItemType::METHOD },
        { QStringLiteral("instsub"), DocsetItemType::METHOD },
        { QStringLiteral("instctr"), DocsetItemType::METHOD },
        { QStringLiteral("instm"), DocsetItemType::METHOD },
        { QStringLiteral("structcm"), DocsetItemType::METHOD },
        { QStringLiteral("structm"), DocsetItemType::METHOD },
        { QStringLiteral("structsub"), DocsetItemType::METHOD },
        { QStringLiteral("Class Methods"), DocsetItemType::METHOD },
        { QStringLiteral("Inherited Methods"), DocsetItemType::METHOD },
        { QStringLiteral("Instance Methods"), DocsetItemType::METHOD },
        { QStringLiteral("Private Methods"), DocsetItemType::METHOD },
        { QStringLiteral("Protected Methods"), DocsetItemType::METHOD },
        { QStringLiteral("Public Methods"), DocsetItemType::METHOD },

        { QStringLiteral("intfopfunc"), DocsetItemType::OPERATOR },
        { QStringLiteral("opfunc"), DocsetItemType::OPERATOR },

        { QStringLiteral("enump"), DocsetItemType::PROPERTY },
        { QStringLiteral("intfdata"), DocsetItemType::PROPERTY },
        { QStringLiteral("intfp"), DocsetItemType::PROPERTY },
        { QStringLiteral("instp"), DocsetItemType::PROPERTY },
        { QStringLiteral("structp"), DocsetItemType::PROPERTY },
        { QStringLiteral("Inherited Properties"), DocsetItemType::PROPERTY },
        { QStringLiteral("Private Properties"), DocsetItemType::PROPERTY },
        { QStringLiteral("Protected Properties"), DocsetItemType::PROPERTY },
        { QStringLiteral("Public Properties"), DocsetItemType::PROPERTY },

        { QStringLiteral("intf"), DocsetItemType::PROTOCOL },

        { QStringLiteral("struct"), DocsetItemType::STRUCTURE },
        { QStringLiteral("Data Structures"), DocsetItemType::STRUCTURE },
        { QStringLiteral("Struct"), DocsetItemType::STRUCTURE },

        { QStringLiteral("tag"), DocsetItemType::TYPE },
        { QStringLiteral("tdef"), DocsetItemType::TYPE },
        { QStringLiteral("Data Types"), DocsetItemType::TYPE },
        { QStringLiteral("Package Types"), DocsetItemType::TYPE },
        { QStringLiteral("Private Types"), DocsetItemType::TYPE },
        { QStringLiteral("Protected Types"), DocsetItemType::TYPE },
        { QStringLiteral("Public Types"), DocsetItemType::TYPE },
        { QStringLiteral("Typedefs"), DocsetItemType::TYPE },

        { QStringLiteral("var"), DocsetItemType::VARIABLE }
    };

    auto type = DocsetItemType::fromString(str);
    if (type == DocsetItemType::UNKNOWN)
    {
        type = aliases.value(str, DocsetItemType::UNKNOWN);
    }

    return type;
}

}

}
