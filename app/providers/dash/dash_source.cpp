/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "dash_source.h"

#include "dash_available.h"
#include "dash_provider.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <KCompressionDevice>
#include <KLocalizedString>
#include <KIO/FileCopyJob>

namespace KDevDocs
{

namespace Dash
{

Source::Source(SourceType type)
    : KDevDocs::Source(Provider::self())
    , m_type(type)
{
    m_feedPath = Provider::self()->feedPath(QStringLiteral("%1.json.xz").arg(toString(type)));
    m_feedUrl = QUrl(QStringLiteral("https://kdevdocs.anikin.xyz/feeds/dash_%1.json.xz").arg(toString(type)));

    setName((type == KAPELI) ? i18n("Dash Kapeli") : i18n("Dash User"));
}

Source::~Source() = default;

SourceType
Source::type() const
{
    return m_type;
}

void
Source::load()
{
    KCompressionDevice feedFile(m_feedPath, KCompressionDevice::Xz);
    if (!feedFile.open(QIODevice::ReadOnly))
    {
        update();
        return;
    }

    setState(BUSY);
    clear();

    const auto items = QJsonDocument::fromJson(feedFile.readAll()).array();
    for (const auto& item : items)
    {
        addDocset(new Available(this, item.toObject()));
    }

    setState(IDLE);
}

void
Source::update()
{
    setState(BUSY);

    auto job = KIO::file_copy(m_feedUrl,
                              QUrl::fromLocalFile(m_feedPath),
                              -1,
                              KIO::Overwrite | KIO::HideProgressInfo);

    connect(job, &KIO::FileCopyJob::result,
            this, [this](KJob* job)
            {
                if (!job->error())
                {
                    load();
                }

                setState(IDLE);
            });
}

}

}
