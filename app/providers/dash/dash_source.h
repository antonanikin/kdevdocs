/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "source.h"

#include "dash_source_type.h"
#include <QUrl>

namespace KDevDocs
{

namespace Dash
{

class Source : public KDevDocs::Source
{
    Q_OBJECT

public:
    explicit
    Source(SourceType type);

    ~Source() override;

    SourceType
    type() const;

    void
    load() override;

    void
    update() override;

protected:
    SourceType m_type;
    QString m_feedPath;
    QUrl m_feedUrl;
};

}

}
