/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "dash_archive.h"

#include <KLocalizedString>
#include <KTar>
#include <KZip>

#include <QLoggingCategory>
#include <QtConcurrent>

namespace KDevDocs
{

namespace Dash
{

Q_LOGGING_CATEGORY(ARCHIVE, "kdevdocs.dash.archive");

ArchiveWorker::ArchiveWorker(ArchiveType type, QObject* parent)
    : QObject(parent)
    , m_type(type)
    , m_watcher(new QFutureWatcher<void>(this))
{
    connect(m_watcher, &QFutureWatcher<void>::finished, this, &ArchiveWorker::finished);
}

ArchiveWorker::~ArchiveWorker()
{
    if (m_watcher->isRunning())
    {
        m_watcher->cancel();
        m_watcher->waitForFinished();
    }
}

QString
ArchiveWorker::error() const
{
    return m_error;
}

inline KArchive*
archiveForType(ArchiveType type, const QString& archiveFile)
{
    if (type == ArchiveType::Tar)
    {
        return new KTar(archiveFile);
    }

    if (type == ArchiveType::Zip)
    {
        return new KZip(archiveFile);
    }

    return nullptr;
}

void
ArchiveWorker::extract(const QString& archiveFile, const QString& destinationDirectory)
{
    if (m_watcher->isRunning())
    {
        qCDebug(ARCHIVE) << "ArchiveWorker is already runned";
        return;
    }

    m_error.clear();
    if (!QDir(destinationDirectory).exists())
    {
        m_error = i18n("Destination directory %1 does not extsts", destinationDirectory);
        return;
    }

    auto extractBody = [this, archiveFile, destinationDirectory]()
    {
        QScopedPointer<KArchive> archive(archiveForType(m_type, archiveFile));
        Q_ASSERT(archive);

        if (!archive->open(QIODevice::ReadOnly))
        {
            m_error = i18n("Unable to open %1 for reading", archiveFile);
            return;
        }

        if (!archive->directory()->copyTo(destinationDirectory))
        {
            m_error = i18n("Unable to extract %1 into %2", archiveFile, destinationDirectory);
            return;
        }
    };

    m_watcher->setFuture(QtConcurrent::run(extractBody));
}

void
ArchiveWorker::compress(const QString& sourceDirectory, const QString& archiveFile)
{
    if (m_watcher->isRunning())
    {
        qCDebug(ARCHIVE) << "ArchiveWorker is already runned";
        return;
    }

    m_error.clear();
    if (!QDir(sourceDirectory).exists())
    {
        m_error = i18n("Source directory %1 does not extsts", sourceDirectory);
        return;
    }

    auto compressBody = [this, sourceDirectory, archiveFile]()
    {
        QScopedPointer<KArchive> archive(archiveForType(m_type, archiveFile));
        Q_ASSERT(archive);

        if (!archive->open(QIODevice::WriteOnly))
        {
            m_error = i18n("Unable to open %1 for reading", archiveFile);
            return;
        }

        if (!archive->addLocalDirectory(sourceDirectory, QString()))
        {
            m_error = i18n("Unable to compress %1 into %2", sourceDirectory, archiveFile);
            return;
        }
    };

    m_watcher->setFuture(QtConcurrent::run(compressBody));
}

}

}
