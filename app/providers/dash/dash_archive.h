/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QObject>

template<typename T>
class QFutureWatcher;

namespace KDevDocs
{

namespace Dash
{

enum class ArchiveType
{
    Tar,
    Zip
};

class ArchiveWorker : public QObject
{
    Q_OBJECT

public:
    explicit
    ArchiveWorker(ArchiveType type, QObject* parent = nullptr);

    ~ArchiveWorker() override;

    QString
    error() const;

    void
    extract(const QString& archiveFile, const QString& destinationDirectory);

    void
    compress(const QString& sourceDirectory, const QString& archiveFile);

Q_SIGNALS:
    void
    finished();

private:
    ArchiveType m_type;
    QString m_error;
    QFutureWatcher<void>* m_watcher;
};

}

}
