/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "dash_provider.h"

#include "dash_installed.h"
#include "dash_available.h"
#include "dash_source.h"

namespace KDevDocs
{

namespace Dash
{

Provider*
Provider::self()
{
    static Provider* instance = nullptr;
    if (!instance)
    {
        instance = new Provider;
        instance->m_sources += new Source (KAPELI);
        instance->m_sources += new Source (USER);
    }

    return instance;
}

Provider::Provider()
    : KDevDocs::Provider(QStringLiteral("dash"))
{
}

Provider::~Provider() = default;

InstalledDocset*
Provider::createDocset(const QString& path, InstalledDocset* parent)
{
    if (parent)
    {
        return new Installed(path, parent);
    }
    else
    {
        return new Installed(path);
    }
}

}

}
