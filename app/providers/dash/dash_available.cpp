/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "dash_available.h"

#include "dash_archive.h"
#include "dash_installed.h"
#include "dash_provider.h"
#include "dash_source.h"
#include "globals.h"
#include "metadata_config.h"

#include <KIO/FileCopyJob>
#include <KLocalizedString>

#include <QMessageBox>
#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QTemporaryDir>
#include <QUrl>

namespace KDevDocs
{

namespace Dash
{

Q_LOGGING_CATEGORY(AVAILABLE, "kdevdocs.dash.available");

Available::Available(Source* source, const QJsonObject& docsetObject)
    : AvailableDocset(source)
    , m_source(source)
    , m_hasCustomIcon(false)
    , m_tempDir(nullptr)
    , m_downloadJob(nullptr)
    , m_tarExtractor(new ArchiveWorker(ArchiveType::Tar))
    , m_zipCompressor(new ArchiveWorker(ArchiveType::Zip))
{
    QString origName = docsetObject.value(QStringLiteral("name")).toString();

    setName(QString(origName).replace(QChar('_'), QChar(' ')));
    setId(createId(QStringLiteral("%1_%2").arg(toString(source->type()), origName)));

    QIcon jsonIcon;
    auto iconData = QByteArray::fromBase64(docsetObject.value(QStringLiteral("icon")).toString().toLatin1());
    if (!iconData.isEmpty())
    {
        jsonIcon.addPixmap(QPixmap::fromImage(QImage::fromData(iconData)));
    }

    iconData = QByteArray::fromBase64(docsetObject.value(QStringLiteral("icon2x")).toString().toLatin1());
    if (!iconData.isEmpty())
    {
        jsonIcon.addPixmap(QPixmap::fromImage(QImage::fromData(iconData)));
    }

    if (!jsonIcon.availableSizes().isEmpty())
    {
        setIcon(jsonIcon);
        m_hasCustomIcon = true;
    }

    auto version = docsetObject.value(QStringLiteral("version")).toString();
    // fix wrong version string for some docsets
    // FIXME move to server scripts?
    if (version.startsWith('.'))
    {
        const auto name = this->name();
        if (!name.isEmpty())
        {
            const auto lastChar = name.at(name.size() - 1);
            if (lastChar.isDigit())
            {
                version = lastChar + version;
            };
        }
    }
    setVersion(version);

    // FIXME move into provider
    static const QStringList mirrors =
    {
        QStringLiteral("http://sanfrancisco.kapeli.com"),
        QStringLiteral("http://london.kapeli.com"),
        QStringLiteral("http://newyork.kapeli.com"),
        QStringLiteral("http://tokyo.kapeli.com"),
        QStringLiteral("http://frankfurt.kapeli.com"),
        QStringLiteral("http://sydney.kapeli.com"),
        QStringLiteral("http://singapore.kapeli.com")
    };

    m_archiveName = docsetObject.value(QStringLiteral("archive")).toString();

    for (const QString& mirror : mirrors)
    {
        if (source->type() == KAPELI)
        {
            m_urls.append(QUrl(QStringLiteral("%1/feeds/%2").arg(mirror, m_archiveName)));
        }
        else
        {
            m_urls.append(QUrl(QStringLiteral("%1/feeds/zzz/user_contributed/build/%2/%3").arg(
                mirror, origName, m_archiveName)));
        }
    }

    m_archiveName = m_archiveName.left(m_archiveName.length() - 4); // remove ".tgz" from end

    const auto urls = docsetObject.value(QStringLiteral("urls")).toArray();
    for (const auto& url : urls)
    {
        m_urls.append(QUrl(url.toString()));
    }

    connect(m_tarExtractor.data(),  &ArchiveWorker::finished, this, &Available::tarExtracted);
    connect(m_zipCompressor.data(), &ArchiveWorker::finished, this, &Available::zipCompressed);
}

Available::~Available() = default;

void
Available::showError(const QString& error)
{
    QMessageBox::critical(nullptr, name(), error);

    setState(ERROR);

    delete m_tempDir;
    m_tempDir = nullptr;
}

void
Available::install()
{
    m_tempDir = new QTemporaryDir;

    m_tarPath = QDir(m_tempDir->path()).absoluteFilePath(m_urls.first().fileName());
    m_zipPath = QStringLiteral("%1/%2.zip").arg(path(), m_archiveName);

    setDownloadingProgress(0);
    setState(DOWNLOADING);

    m_downloadJob = KIO::file_copy(
        m_urls.first(),
        QUrl::fromLocalFile(m_tarPath),
        -1,
        KIO::Overwrite | KIO::HideProgressInfo);

    connect(m_downloadJob, SIGNAL(percent(KJob*,ulong)), // clazy:exclude=old-style-connect
            this, SLOT(percentSlot(KJob*,ulong)));

    connect(m_downloadJob, &KIO::FileCopyJob::result,
            this, &Available::tarDownloaded);

    connect(m_downloadJob, &KIO::FileCopyJob::destroyed,
            this, [this]()
            {
                m_downloadJob = nullptr;
            });
}

void
Available::tarDownloaded()
{
    if (m_downloadJob->error())
    {
        if (m_downloadJob->error() == KJob::KilledJobError)
        {
            if (installed())
            {
                setState(INSTALLED);
            }
            else
            {
                setState(NOT_INSTALLED);
            }
        }
        else
        {
            showError(m_downloadJob->errorString());
        }
        return;
    }

    m_downloadJob->deleteLater();
    m_downloadJob = nullptr;

    setState(EXTRACTING);
    m_tarExtractor->extract(m_tarPath, m_tempDir->path());
}

void
Available::tarExtracted()
{
    if (!m_tarExtractor->error().isEmpty())
    {
        showError(m_tarExtractor->error());
        return;
    }

    QFile::remove(m_tarPath);

    QDir tempDir(m_tempDir->path());
    auto dirs = tempDir.entryList({ QStringLiteral("*.docset") }, QDir::Dirs);

    // FIXME remove this check ?
    if (dirs.size() != 1)
    {
        showError(QStringLiteral("dirs.size() != 1"));
        return;
    }

    tempDir.rename(dirs.first(), m_archiveName);

    setState(INSTALLING);
    createDocsetDir();

    m_zipCompressor->compress(m_tempDir->path(), m_zipPath);
}

void
Available::zipCompressed()
{
    if (!m_zipCompressor->error().isEmpty())
    {
        showError(m_zipCompressor->error());
        removeDocsetDir();
        return;
    }

    // FIXME add this to parent class ?
    if (m_hasCustomIcon)
    {
        MetadataConfig config(this);
        config.setIcon(icon());
    }

    QDir(path()).remove(QStringLiteral("contents.bin"));

    if (installed())
    {
        // FIXME close before install ? or close zip when reload
        auto docset = static_cast<Installed*>(installed());
        docset->metadataConfig()->setVersion(version());
        docset->metadataConfig()->save();
        docset->reload();
    }
    else
    {
        {
            MetadataConfig config(this);
            // FIXME add child config class ?
            config.setValue(QStringLiteral("archiveName"), m_archiveName);
            config.save();
        }

        auto docset = provider()->addDocset(path());
        docset->reload();
    }

    setState(INSTALLED);
}

void
Available::breakInstallation()
{
    if (state() == DOWNLOADING)
    {
        m_downloadJob->kill(KJob::EmitResult);
    }
}

void
Available::percentSlot(KJob* /*job*/, ulong percent)
{
    setDownloadingProgress(percent);
}

}

}
