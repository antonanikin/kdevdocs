/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "available_docset.h"

#include "dash_source_type.h"

class QTemporaryDir;
class KJob;

namespace KDevDocs
{

namespace Dash
{

class ArchiveWorker;
class Source;

class Available : public AvailableDocset
{
    Q_OBJECT

public:
    Available(Source* source, const QJsonObject& docsetObject);

    ~Available() override;

    void
    install() override;

    void
    breakInstallation() override;

protected:
    // FIXME move into base class ?
    void
    showError(const QString& error);

    void
    tarDownloaded();

    void
    tarExtracted();

    void
    zipCompressed();

    Source* m_source;
    QString m_archiveName;
    bool m_hasCustomIcon;
    QList<QUrl> m_urls; // FIXME

    QTemporaryDir* m_tempDir;

    QString m_zipPath;
    QString m_tarPath;

    KJob* m_downloadJob;
    QScopedPointer<ArchiveWorker> m_tarExtractor;
    QScopedPointer<ArchiveWorker> m_zipCompressor;

protected Q_SLOTS:
    void
    percentSlot(KJob* job, ulong percent);
};

}

}
