/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "man_available.h"

#include "globals.h"
#include "installed_docset.h"
#include "man_source.h"
#include "metadata_config.h"
#include "provider.h"

#include <KLocalizedString>

#include <QDir>

namespace KDevDocs
{

namespace Man
{

AvailableAll::AvailableAll(Source* source)
    : AvailableDocset(source)
{
    setName(i18n("Docset from all installed man pages"));
    setId(createId(QStringLiteral("952085c3-f879-425a-ba51-af0cf2bda374")));
}

AvailableAll::~AvailableAll()
{
}

void
AvailableAll::install()
{
    createDocsetDir();

    {
        MetadataConfig config(this, i18n("Man Pages"));
        // FIXME add child config class ?
        config.setValue(QStringLiteral("allInstalled"), QStringLiteral("true"));
        config.save();
    }

    auto docset = provider()->addDocset(path());
    docset->reload();
}

void
AvailableAll::breakInstallation()
{
}

}

}
