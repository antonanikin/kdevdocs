/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "man_provider.h"

#include "man_installed.h"
#include "man_source.h"
#include "core.h"

#include <klocalizedstring.h>

#include <QDir>

namespace KDevDocs
{

namespace Man
{

Provider*
Provider::self()
{
    static Provider* instance = nullptr;

    if (!instance)
    {
        instance = new Provider;
        instance->m_sources += new Source;
    }

    return instance;
}

Provider::Provider()
    : KDevDocs::Provider(QStringLiteral("man"))
{
}

Provider::~Provider()
{
}

InstalledDocset*
Provider::createDocset(const QString& path, InstalledDocset* /*parent*/)
{
    return new Installed(path);
}

}

}
