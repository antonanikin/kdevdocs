/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "man_paths.h"

#include "utils.h"

#include <QDir>
#include <QRegularExpression>
#include <QSet>

namespace KDevDocs
{

namespace Man
{

QStringList
standardManPaths()
{
    QStringList paths;

    QString processOutput;
    if (!executeProcess(QStringLiteral("manpath"), {}, processOutput))
    {
        return paths;
    }

    const auto outputPaths = processOutput.split(QChar(':'));
    for (auto path : outputPaths)
    {
        path = QFileInfo(path).canonicalFilePath();
        if (path.isEmpty() || paths.contains(path))
        {
            continue;
        }
        paths += path;
    }
    paths.sort();

    return paths;
}

QMap<int, QStringList>
sectionPaths()
{
    return sectionPaths(standardManPaths());
}

QMap<int, QStringList>
sectionPaths(const QStringList& manPaths)
{
    QMap<int, QStringList> result;
    QSet<QString> sectionPaths;

    for (const QString& manPath : manPaths)
    {
        auto sections = QDir(manPath).entryList({ QStringLiteral("man?") }, QDir::Dirs);
        for (const auto& sectionName : sections)
        {
            bool ok;
            int section = sectionName.rightRef(1).toInt(&ok);
            if (!ok)
            {
                continue;
            }

            auto sectionPath(QStringLiteral("%1/%2").arg(manPath, sectionName));
            sectionPath = QFileInfo(sectionPath).canonicalFilePath();
            if (sectionPath.isEmpty() || sectionPaths.contains(sectionPath))
            {
                continue;
            }
            sectionPaths += sectionPath;

            result[section] += sectionPath;
        }
    }

    return result;
}

}

}
