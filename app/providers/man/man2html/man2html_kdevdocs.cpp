/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "man2html_kdevdocs.h"

#include "kio_man_debug.h"
#include "man2html.h"

#include <KFilterDev>

#include <QFile>
#include <QFileInfo>
#include <QDir>

static QByteArray htmlData;
static QString lastDir;

// called by man2html
void output_real(const char* insert)
{
    htmlData.append(insert);
}

char* readManPage(const QString& fileName)
{
    const auto info = QFileInfo(fileName);

    const auto dir = info.dir();
    if (!dir.exists()) {
        return nullptr;
    }

    auto fixedFileName(fileName);
    if (!info.exists()) {
        auto files = dir.entryList({ info.fileName() + QLatin1Char('*') }, QDir::Files);
        if (files.isEmpty()) {
            return nullptr;
        }

        fixedFileName = dir.absoluteFilePath(files.first());
    }

    lastDir = dir.path();

    KFilterDev fd(fixedFileName);
    if (!fd.open(QIODevice::ReadOnly))
    {
        qCDebug(KIO_MAN_LOG) << "read_man_page: can not open " << fixedFileName;
        return nullptr;
    }

    const auto data = fd.readAll();
    if (data.isEmpty()) {
        return nullptr;
    }

    return manPageToUtf8(data, QFile::encodeName(dir.dirName()));
}

// called by man2html
char* read_man_page(const char* filename)
{
    auto fileName = QFile::decodeName(filename);

    // TODO Use Provider man DB to lookup?
    if (QDir::isRelativePath(fileName)) {
        // We assume what relative filename point to 1st upper directory level: "../some_path"
        // (grep man2html.cpp for "read_man_page"). But we also should check 2nd upper directory
        // level: "../../some_path" when open translated man pages for example:
        // /usr/share/man/{de,it,ru,...}/man{1,2,3,...}/.

        // test 1st upper level
        const auto upperName = QDir::cleanPath(QStringLiteral("%1/%2").arg(lastDir, fileName));
        if (auto data = readManPage(upperName)) {
            return data;
        }

        // test 2nd upper level
        fileName = QDir::cleanPath(QStringLiteral("%1/../%2").arg(lastDir, fileName));
    }

    return readManPage(fileName);
}

namespace KDevDocs
{

namespace Man
{

QByteArray man2html(const QString& fileName)
{
    htmlData.clear();

    char* buf = read_man_page(qPrintable(fileName));
    if (buf)
    {
        scan_man_page(buf);
        delete [] buf;
    }

    return htmlData;
}

}

}
