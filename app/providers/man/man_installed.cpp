/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "man_installed.h"

#include "content_item.h"
#include "core.h"
#include "globals.h"
#include "index_item.h"
#include "man_paths.h"
#include "man_provider.h"
#include "metadata_config.h"
#include "user_interface_config.h"

#include "man2html/man2html_kdevdocs.h"

#include <KLocalizedString>

#include <QDir>
#include <QDirIterator>
#include <QRegularExpression>
#include <QUrl>
#include <QIcon>

namespace KDevDocs
{

namespace Man
{

QString fixedSubSection(const QString& subSection);

Installed::Installed(const QString& path)
    : InstalledDocset(path, Provider::self())
    , m_allInstalled(false)
{
    if (metadataConfig()->value(QStringLiteral("allInstalled")) == QStringLiteral("true"))
    {
        m_allInstalled = true;
    }

    // FIXME darkMode
//     m_icon = IconManager::self()->providerIcon(provider()->name());
//     if (isDarkMode())
//     {
//         m_icon = QIcon(QStringLiteral(":icons/man/dark/Man.svg"));
//     }
//     else
//     {
//         m_icon = QIcon(QStringLiteral(":icons/man/Man.svg"));
//     }

//     connect(Settings::self(), &Settings::pathsSettingsChanged, this, &Docset::reload);
}

Installed::~Installed() = default;

void
Installed::loadIndexContents(ContentItem* contentsRoot)
{
    if (!m_allInstalled)
    {
        // FIXME reserved for not-implemented custom docsets
        return;
    }

    static const QRegularExpression pageRegex(QStringLiteral("(.+\\.\\d)(\\w*)"));

    auto sections = sectionPaths();
    for (auto section = sections.begin(); section != sections.end(); ++section)
    {
        auto sectionItem = createSectionItem(contentsRoot, section.key());

        QHash<QString, int> counts;
        QHash<TreeItem*, QString> subSections;

        // First run - create page items, fill counts
        for (const QString& sectionPath : qAsConst(section.value()))
        {
            QDirIterator di(sectionPath, QDir::Files);
            while (di.hasNext())
            {
                const auto pagePath = di.next();

                auto pageName = QFileInfo(pagePath).fileName();
                auto match = pageRegex.match(pageName);
                if (!match.hasMatch())
                {
                    continue;
                }
                pageName = match.captured(1);

                auto pageItem = new ContentItem(this, sectionItem, pageName, pagePath, sectionItem->type());

                subSections[pageItem] = match.captured(2);
                counts[pageName] += 1;
            }
        }

        // Second run - fix page items, create index items
        for (int i = 0; i < sectionItem->childItemsCount(); ++i)
        {
            auto pageItem = sectionItem->childItem(i)->cast<ContentItem>();
            Q_ASSERT(pageItem);

            auto name = pageItem->name();
            // remove end part (\.\d)
            name = name.left(name.size() - 2);

            m_pathHash[QStringLiteral("/%1(%2)").arg(name).arg(section.key())] += pageItem->href();

            const auto subSection = subSections[pageItem];
            if (!subSection.isEmpty() && (counts[name] > 1))
            {
                // show sub-section
                name += QStringLiteral(" (%1)").arg(fixedSubSection(subSection));
            }
            pageItem->setName(name);

            m_index += new IndexItem(this, name, pageItem->href(), sectionItem->type());
        }

        sectionItem->sortChilds();
    }
}

ContentItem*
Installed::createSectionItem(ContentItem* parent, int section)
{
    static const QStringList displayName =
    {
        i18n("User Commands"),
        i18n("System Calls"),
        i18n("Library Calls"),
        i18n("Devices"),
        i18n("File Formats"),
        i18n("Games"),
        i18n("Miscellaneous"),
        i18n("System Administration")
    };

    auto name = QStringLiteral("Man%1").arg(section);
    auto type = DocsetItemType::UNKNOWN;

    if (section >= 1 && section <= 8)
    {
        name = displayName[section - 1];
        type = ContentItem::Type(DocsetItemType::MAN1 + section - 1);
    }

    return new ContentItem(this, parent, name, QString(), type);
}

QByteArray
Installed::fileData(const QString& path) const
{
    if (QFile::exists(path))
    {
        return man2html(path);
    }

    // Fix urls provided by pages internal links like "man:/tar(1)".
    for (const QString& hashPath : m_pathHash[path])
    {
        if (QFile::exists(hashPath))
        {
            return man2html(hashPath);
        }
    }

    return {};
}

QString
fixedSubSection(const QString& subSection)
{
    // names are taken from ubuntu 16.04 packages
    // skipped items: "a", "L"
    static QHash<QString, QString> displayName =
    {
        { QStringLiteral("am"), QStringLiteral("Awk") },
        { QStringLiteral("aolserver"), QStringLiteral("AOLserver") },
        { QStringLiteral("avr"), QStringLiteral("AVR") },
        { QStringLiteral("b"), QStringLiteral("TORQUE") },
        { QStringLiteral("bobcat"), QStringLiteral("Bobcat") },
        { QStringLiteral("bsd"), QStringLiteral("BSD") },
        { QStringLiteral("caca"), QStringLiteral("libcaca") },
        { QStringLiteral("cdebconf"), QStringLiteral("C Debconf") },
        { QStringLiteral("clc"), QStringLiteral("OpenCL") },
        { QStringLiteral("cocci"), QStringLiteral("Cocinelle") },
        { QStringLiteral("curses"), QStringLiteral("curses") },
        { QStringLiteral("cxx"), QStringLiteral("C++") },
        { QStringLiteral("debug"), QStringLiteral("libdebug") },
        { QStringLiteral("e"), QStringLiteral("EMBOSS") },
        { QStringLiteral("edit"), QStringLiteral("libedit") },
        { QStringLiteral("elektra"), QStringLiteral("Elektra") },
        { QStringLiteral("erl"), QStringLiteral("Erlang") },
        { QStringLiteral("form"), QStringLiteral("Ncurses Form") },
        { QStringLiteral("freebsd"), QStringLiteral("FreeBSD") },
        { QStringLiteral("fun"), QStringLiteral("Fun") },
        { QStringLiteral("g"), QStringLiteral("OpenGL") },
        { QStringLiteral("gcc"), QStringLiteral("GNU") },
        { QStringLiteral("gle"), QStringLiteral("libgle") },
        { QStringLiteral("gmt"), QStringLiteral("GMT") },
        { QStringLiteral("grass"), QStringLiteral("GRASS") },
        { QStringLiteral("guile"), QStringLiteral("Guile") },
        { QStringLiteral("gv"), QStringLiteral("Geomview") },
        { QStringLiteral("heimdal"), QStringLiteral("Heimdal") },
        { QStringLiteral("i"), QStringLiteral("InterViews") },
        { QStringLiteral("itcl"), QStringLiteral("incr Tcl") },
        { QStringLiteral("itk"), QStringLiteral("incr Tk") },
        { QStringLiteral("iv"), QStringLiteral("Open Inventor") },
        { QStringLiteral("iwidget"), QStringLiteral("incr Widgets") },
        { QStringLiteral("j"), QStringLiteral("4store") },
        { QStringLiteral("lsh"), QStringLiteral("lsh") },
        { QStringLiteral("lua"), QStringLiteral("Lua") },
        { QStringLiteral("m"), QStringLiteral("nn") },
        { QStringLiteral("mandos"), QStringLiteral("Mandos") },
        { QStringLiteral("menu"), QStringLiteral("Ncurses Menu") },
        { QStringLiteral("mh"), QStringLiteral("MH") },
        { QStringLiteral("nas"), QStringLiteral("NAS") },
        { QStringLiteral("ncar"), QStringLiteral("NCAR") },
        { QStringLiteral("ncarg"), QStringLiteral("NCAR") },
        { QStringLiteral("ncurses"), QStringLiteral("Ncurses") },
        { QStringLiteral("nhl"), QStringLiteral("NCAR") },
        { QStringLiteral("nx"), QStringLiteral("NSF Tcl") },
        { QStringLiteral("o"), QStringLiteral("OCaml") },
        { QStringLiteral("olvwm"), QStringLiteral("olvwm") },
        { QStringLiteral("openmpi"), QStringLiteral("OpenMPI") },
        { QStringLiteral("ossp"), QStringLiteral("libossp-uuid") },
        { QStringLiteral("p"), QStringLiteral("Perl") },
        { QStringLiteral("pcap"), QStringLiteral("libpcap") },
        { QStringLiteral("perl"), QStringLiteral("Perl") },
        { QStringLiteral("plplot"), QStringLiteral("PLplot") },
        { QStringLiteral("pm"), QStringLiteral("Perl") },
        { QStringLiteral("posix"), QStringLiteral("POSIX") },
        { QStringLiteral("postfix"), QStringLiteral("Postfix") },
        { QStringLiteral("pub"), QStringLiteral("publib") },
        { QStringLiteral("pvm"), QStringLiteral("PVM") },
        { QStringLiteral("python"), QStringLiteral("Python") },
        { QStringLiteral("qmail"), QStringLiteral("qmail") },
        { QStringLiteral("readline"), QStringLiteral("readline") },
        { QStringLiteral("rheolef"), QStringLiteral("Rheolef") },
        { QStringLiteral("ruby"), QStringLiteral("Ruby") },
        { QStringLiteral("sr"), QStringLiteral("Surfraw") },
        { QStringLiteral("ssl"), QStringLiteral("SSL") },
        { QStringLiteral("stap"), QStringLiteral("SystemTap") },
        { QStringLiteral("t"), QStringLiteral("libtirpc") },
        { QStringLiteral("tcl"), QStringLiteral("Tcl") },
        { QStringLiteral("tcllib"), QStringLiteral("Tcl") },
        { QStringLiteral("tclx"), QStringLiteral("TclX") },
        { QStringLiteral("tiff"), QStringLiteral("TIFF") },
        { QStringLiteral("tk"), QStringLiteral("Tk") },
        { QStringLiteral("trf"), QStringLiteral("Trf Tcl") },
        { QStringLiteral("ts"), QStringLiteral("Apache Traffic Server") },
        { QStringLiteral("u"), QStringLiteral("InterViews") },
        { QStringLiteral("viewos"), QStringLiteral("View-OS") },
        { QStringLiteral("visa"), QStringLiteral("VISA") },
        { QStringLiteral("wn"), QStringLiteral("WordNet") },
        { QStringLiteral("x"), QStringLiteral("X") }
    };

    return displayName.value(subSection.toLower(), subSection);
}

}

}
