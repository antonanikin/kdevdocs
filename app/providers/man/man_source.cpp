/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "man_source.h"

#include "man_available.h"
#include "man_provider.h"

#include <KLocalizedString>

namespace KDevDocs
{

namespace Man
{

Source::Source()
    : KDevDocs::Source(Provider::self())
{
    setName(i18n("Man Pages"));
}

Source::~Source() = default;

void
Source::load()
{
    static bool loadDone = false;
    if (loadDone)
    {
        return;
    }

    setState(BUSY);
    clear();
    addDocset(new AvailableAll(this));

    setState(IDLE);
}

void
Source::update()
{
}

}

}
