add_library(chm_ebook STATIC
    ebook.cpp
    ebook_chm.cpp
    ebook_chm_encoding.cpp
    helper_entitydecoder.cpp
)

target_link_libraries(chm_ebook
    PUBLIC kdevdocs_provider_core
    PRIVATE ${LIBCHM_LIBRARY}
)

target_compile_options(chm_ebook
    PRIVATE ${SUPPRESSED_WARNINGS}
)
