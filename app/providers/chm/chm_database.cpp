/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "chm_database.h"

#include "chm_installed.h"
#include "content_item.h"
#include "globals.h"
#include "index_item.h"
#include "libebook/ebook_chm.h"

#include <QStack>

namespace KDevDocs
{

namespace Chm
{

Database::Database(Installed* docset, const QString& filePath)
    : InstalledDBDocsetDatabase(docset, filePath)
    , db(new EBook_CHM)
{
    if (!db->load(filePath))
    {
        return;
    }

    m_isValid = true;
}

Database::~Database()
{
    delete db;
}

void
Database::loadIndexContents(ContentItem* contentsRoot, IndexVector& index)
{
    bool hasIndex = false;

    QList<EBookIndexEntry> chmIndex;
    if (db->getIndex(chmIndex))
    {
        hasIndex = true;
        for (const auto& chmIndexItem : qAsConst(chmIndex))
        {
            for (const QUrl& indexUrl : chmIndexItem.urls)
            {
                if (indexUrl.scheme().isEmpty()) // "seealso" links
                {
                    continue;
                }

                const auto href = createHref(indexUrl);
                QString name = chmIndexItem.name;

                bool itemExists = false;
                for (auto item : index)
                {
                    if (item->href() == href && item->name() == name)
                    {
                        itemExists = true;
                        break;
                    }
                }

                if (itemExists)
                {
                    continue;
                }

                if (chmIndexItem.urls.size() > 1)
                {
                    name += QStringLiteral(" (%1)").arg(db->getTopicByUrl(indexUrl));
                }

                index += new IndexItem(m_docset, name, href);
            }
        }
    }

    QList<EBookTocEntry> toc;
    if (db->getTableOfContents(toc))
    {
        TreeItem* parent  = contentsRoot;
        TreeItem* current = nullptr;
        QStack<int> stack;

        stack.push(0);
        for (const auto& tocItem : qAsConst(toc))
        {
            if (tocItem.indent > stack.top())
            {
                parent = current;
                stack.push(tocItem.indent);
            }
            else if (tocItem.indent < stack.top())
            {
                while (stack.top() != tocItem.indent)
                {
                    current = current->parentItem();
                    parent = current->parentItem();
                    stack.pop();
                }
            }

            const auto href = createHref(tocItem.url);
            current = new ContentItem(m_docset, parent, tocItem.name, href);

            if (!hasIndex)
            {
                index += new IndexItem(m_docset, tocItem.name, href);
            }
        }
    }
}

QString
Database::createHref(const QUrl& chmUrl) const
{
    auto href = QStringLiteral("%1/%2%3").arg(m_baseName, chmUrl.host(), chmUrl.path());
    if (!chmUrl.fragment().isEmpty())
    {
        href += QLatin1Char('#') + chmUrl.fragment();
    }

    return href;
}

QByteArray
Database::fileData(const QString& filePath)
{
    QUrl url(QStringLiteral("ms-its://%1").arg(filePath));
    QByteArray data;

    db->getFileContentAsBinary(data, url);

    return data;
}

}

}
