/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "chm_source.h"

#include "available_dbdocset.h"
#include "chm_provider.h"

#include <KLocalizedString>

namespace KDevDocs
{

namespace Chm
{

Source::Source()
    : KDevDocs::Source(Provider::self())
{
    setName(i18n("CHM"));
}

Source::~Source() = default;

void
Source::load()
{
    static bool loadDone = false;
    if (loadDone)
    {
        return;
    }

    setState(BUSY);
    clear();

    auto fileDocset = new AvailableDBDocset(
        this,
        AvailableDBDocset::FILE,
        QStringLiteral("chm"));

    fileDocset->setPriority(0);
    addDocset(fileDocset);

    auto directoryDocset = new AvailableDBDocset(
        this,
        AvailableDBDocset::DIRECTORY,
        QStringLiteral("chm"));

    directoryDocset->setPriority(1);
    addDocset(directoryDocset);

    setState(IDLE);
    loadDone = true;
}

void
Source::update()
{
}

}

}
