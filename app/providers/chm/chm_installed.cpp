/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "chm_installed.h"

#include "chm_database.h"
#include "chm_provider.h"

#include <QLoggingCategory>
#include <QDir>

namespace KDevDocs
{

namespace Chm
{

Q_LOGGING_CATEGORY(DOCSET, "kdevdocs.chm.docset");

Installed::Installed(const QString& path)
    : InstalledDBDocset(path, Provider::self(), QStringLiteral("chm"))
{
    loadDatabases();
}

Installed::~Installed()
{
}

QByteArray
Installed::fileData(const QString& path) const
{
    auto elements = path.split(QChar('/'), QString::SkipEmptyParts);
    if (elements.size() < 3)
    {
        qCDebug(DOCSET, "invalid path - it should contains at least 3 elements: '%s'", qPrintable(path));
        return {};
    }

    const auto baseName = elements.takeFirst();
    const auto filePath = elements.join(QChar('/'));

    if (auto db = m_dbs.value(baseName, nullptr))
    {
        return db->fileData(filePath);
    }

    return {};
}

InstalledDBDocsetDatabase*
Installed::createDatabase(const QString& filePath)
{
    return new Database(this, filePath);
}

}

}
