/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "installed_dbdocset.h"

namespace KDevDocs
{

namespace Chm
{

class Installed : public InstalledDBDocset
{
    Q_OBJECT

public:
    explicit
    Installed(const QString& path);

    ~Installed() override;

protected:
    InstalledDBDocsetDatabase*
    createDatabase(const QString& filePath) override;

    QByteArray
    fileData(const QString& path) const override;
};

}

}
