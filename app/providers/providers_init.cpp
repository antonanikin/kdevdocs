/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "providers_init.h"

#include "dash_provider.h"
#include "kdd_provider.h"
#include "qthelp_provider.h"

#ifdef CHM_ENABLED
    #include "chm_provider.h"
#endif

#ifdef MAN_ENABLED
    #include "man_provider.h"
#endif

namespace KDevDocs
{

void
providersInit()
{
    Dash::Provider::self()->init();
    KDD::Provider::self()->init();
    QtHelp::Provider::self()->init();

#ifdef CHM_ENABLED
    Chm::Provider::self()->init();
#endif

#ifdef MAN_ENABLED
    Man::Provider::self()->init();
#endif
}

}
