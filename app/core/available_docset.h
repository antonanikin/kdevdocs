/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "docset.h"

#include <QString>

namespace KDevDocs
{

class InstalledDocset;
class Source;

class AvailableDocset : public Docset
{
    friend class CorePrivate;

    Q_OBJECT
    Q_DISABLE_COPY(AvailableDocset)

public:
    enum State
    {
        NOT_INSTALLED = IDLE | IDLE << 1,
        INSTALLED     = IDLE | IDLE << 2,

        DOWNLOADING   = BUSY | BUSY << 1,
        EXTRACTING    = BUSY | BUSY << 2,
        INSTALLING    = BUSY | BUSY << 3,
    };

    ~AvailableDocset() override;

    Source*
    source() const;

    QString
    path() const override;

    virtual void
    install() = 0;

    virtual void
    breakInstallation() = 0;

    InstalledDocset*
    installed() const;

    int
    downloadingProgress() const;

    void
    setDownloadingProgress(unsigned long progress);

    int
    priority() const;

    void
    setPriority(int priority);

    bool
    lessThan(const TreeItem* other, const QString& searchText = QString()) const override;

protected:
    explicit
    AvailableDocset(Source* source);

    explicit
    AvailableDocset(AvailableDocset* parent);

private:
    AvailableDocset(Source* source, TreeItem* parent);

    void
    setInstalled(InstalledDocset* installed);

private:
    Source* m_source;
    InstalledDocset* m_installed;

    int m_priority;
    unsigned long m_downloadingProgress;
};

}
