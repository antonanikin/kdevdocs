/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "tree_item.h"

namespace KDevDocs
{

class Provider;

class Docset : public TreeObjectItem
{
    Q_OBJECT
    Q_DISABLE_COPY(Docset)

public:
    ~Docset() override;

    Provider*
    provider() const;

    QString
    id() const;

    QString
    version() const;

    virtual QString
    path() const = 0;

    Docset*
    parentDocset() const;

    bool
    isComposite() const;

protected:
    Docset(Provider* provider, TreeItem* parent);

    void
    setId(const QString& id);

    void
    setVersion(const QString& version);

    void
    setComposite(bool composite);

    bool
    createDocsetDir() const;

    bool
    removeDocsetDir() const;

    void
    emitChanged() override;

private:
    Provider* m_provider;

    QString m_id;
    QString m_version;

    bool m_composite;
};

}
