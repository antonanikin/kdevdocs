/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QDir>
#include <QNetworkAccessManager>
#include <QUrl>

namespace KDevDocs
{

class InstalledDocset;
class WebConfig;

class CacheItem
{
public:
    QUrl
    localUrl() const;

    QString
    mimeType() const;

    void
    readData(QByteArray& data) const;

private:
    CacheItem(const QUrl& localUrl, const QString& mimeType);

private:
    QUrl m_localUrl;
    QString m_mimeType;

    friend class NetworkAccessManager;
};

class NetworkAccessManager : public QNetworkAccessManager
{
    Q_OBJECT

public:
    static NetworkAccessManager*
    self();

    const CacheItem*
    cacheItem(const QUrl& url);

protected:
    using QNetworkAccessManager::createRequest;

private:
    NetworkAccessManager();

    ~NetworkAccessManager() override;

    const CacheItem*
    get(const InstalledDocset* docset, const QUrl& url);

    QNetworkReply*
    createRequest(
        const QUrl& pageRequestedUrl,
        const WebConfig* pageConfig,
        Operation op,
        const QNetworkRequest& request,
        QIODevice* outgoingData);

private:
    QDir m_cacheDir;
    QHash<QString, CacheItem*> m_cache;

    friend class WebPageNetworkAccessManager;
};

}
