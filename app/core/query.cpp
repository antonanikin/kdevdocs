/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "query.h"

#include "core.h"
#include "index_item.h"
#include "private/core_private.h"

namespace KDevDocs
{

Query::Query(const QString& queryString)
    : m_isValid(false)
    , m_queryString(queryString.toLower())
    , m_text(m_queryString)
{
    if (queryString.isEmpty())
    {
        return;
    }

    int sourceStart = m_queryString.lastIndexOf(QStringLiteral("//"));
    if (sourceStart < 0)
    {
        m_isValid = !m_text.isEmpty();
        return;
    }
    else
    {
        m_text = m_queryString.left(sourceStart);
        sourceStart += 2;
    }

    auto sourcesString = m_queryString.mid(sourceStart);
    if (sourcesString.isEmpty())
    {
        m_sources += { sourceStart, QString(), nullptr };
    }
    else
    {
        const auto names = sourcesString.split(QChar(','));
        for (const auto& name : names)
        {
            auto docset = Core::self()->docset(name);
            m_sources += { sourceStart, name, docset };
            m_isValid |= (docset != nullptr);

            sourceStart += name.size() + 1;
        }
    }

    m_isValid &= !m_text.isEmpty();
}

bool
Query::operator==(const Query& other) const
{
    return m_queryString == other.m_queryString;
}

bool
Query::isValid() const
{
    return m_isValid;
}

QString
Query::text() const
{
    return m_text;
}

int
Query::sourcesCount() const
{
    return m_sources.size();
}

const QuerySourceItem&
Query::source(int index) const
{
    return m_sources[index];
}

QList<InstalledDocset*>
Query::docsets() const
{
    if (m_sources.isEmpty())
    {
        return Core::self()->docsets();
    }

    QList<InstalledDocset*> result;
    for (const auto& source : m_sources)
    {
        if (source.docset)
        {
            result += source.docset;
        }
    }
    return result;
}

bool
Query::test() const
{
    return CorePrivate::self()->testQuery(*this);
}

QList<QueryResultItem>
Query::exec() const
{
    return CorePrivate::self()->execQuery(*this);
}

QueryResultItem::QueryResultItem(int priority)
    : m_priority(priority)
    , m_indexOfText(-1)
    , m_indexItem(nullptr)
{
}

const IndexItem*
QueryResultItem::indexItem() const
{
    return m_indexItem;
}

bool
QueryResultItem::hasMatch(const IndexItem* indexItem, const QString& text)
{
    m_indexOfText = indexItem->nameLowerCase().indexOf(text);
    if (m_indexOfText >= 0)
    {
        m_indexItem = indexItem;
        return true;
    }

    return false;
}

bool
QueryResultItem::lessThan(const QueryResultItem& other) const
{
    if (m_priority < other.m_priority)
    {
        return true;
    }

    if (m_priority > other.m_priority)
    {
        return false;
    }

    if (m_indexOfText < other.m_indexOfText)
    {
        return true;
    }

    if (m_indexOfText > other.m_indexOfText)
    {
        return false;
    }

    return m_indexItem->nameLowerCase() < other.m_indexItem->nameLowerCase();
}

}
