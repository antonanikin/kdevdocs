/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "item.h"
#include "docset_item_type.h"

namespace KDevDocs
{

class InstalledDocset;

class IndexItem : public Item
{
public:
    using Type = DocsetItemType::Type;
    static const Type DefaultType = Type::INDEX;

    IndexItem(const InstalledDocset* docset, const QString& name, const QString& href, Type type = DefaultType);

    ~IndexItem() override;

    Type
    type() const;

    void
    setType(Type type);

    QIcon
    icon() const override;

    void
    setIcon(const QIcon& icon) override;

    QIcon
    docsetIcon() const;

    QUrl
    url() const override;

    void
    setUrl(const QUrl& url) override;

    QString
    href() const;

    void
    setHref(const QString& href);

    QString
    nameLowerCase() const;

private:
    const InstalledDocset* m_docset;
    QString m_href;
    QString m_nameLowerCase;
    Type m_type;
};

}
