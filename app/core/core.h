/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QObject>

class QAbstractItemModel;

namespace KDevDocs
{

class InstalledDocset;
class Provider;

class Core : public QObject
{
    Q_OBJECT

public:
    enum State
    {
        NOT_LOADED,
        LOADING,
        READY
    };

    ~Core() override = default;

    static Core*
    self();

    virtual void
    init() = 0;

    virtual void
    shutdown() = 0;

    virtual State
    state() const = 0;

    virtual void
    reload() = 0;

    // FIXME remove from public API ?
    virtual const QList<Provider*>&
    providers() = 0;

    virtual Provider*
    provider(const QUrl& url) const = 0;

    virtual void
    updateSources() = 0;

    virtual const QList<InstalledDocset*>&
    docsets() const = 0;

    virtual InstalledDocset*
    docset(const QString& name) const = 0;

    virtual InstalledDocset*
    docset(const QUrl& url) const = 0;

    virtual QAbstractItemModel*
    contentsModel() const = 0;

    virtual QAbstractItemModel*
    installedModel() const = 0;

    virtual QAbstractItemModel*
    availableModel() const = 0;

signals:
    void
    stateChanged();

protected:
    Core() = default;
    Q_DISABLE_COPY(Core)
};

}
