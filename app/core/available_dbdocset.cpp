/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "available_dbdocset.h"

#include "core.h"
#include "globals.h"
#include "installed_docset.h"
#include "metadata_config.h"
#include "provider.h"

#include <KIconButton>
#include <KLocalizedString>
#include <KMessageWidget>
#include <KUrlRequester>

#include <QDialog>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QLineEdit>

namespace KDevDocs
{

class AvailableDBDocsetInstaller : public QDialog
{
    Q_OBJECT

public:
    AvailableDBDocsetInstaller(AvailableDBDocset* docset, const QString& suffix);

    ~AvailableDBDocsetInstaller() override;

protected:
    bool
    checkUrl();

    bool
    checkName();

    void
    updateOkButton();

    void
    createDocset();

    AvailableDBDocset* m_docset;
    QString m_suffix;

    QString m_path;
    QString m_name;

    bool m_iconChanged;
    bool m_urlOk;
    bool m_nameOk;

    KUrlRequester* m_urlRequester;
    KMessageWidget* m_urlMessageWidget;
    QLineEdit* m_nameEdit;
    KMessageWidget* m_nameMessageWidget;
    KIconButton* m_iconButton;
    QDialogButtonBox* m_buttonBox;
};

AvailableDBDocset::AvailableDBDocset(Source* source, Mode mode, const QString& suffix)
    : AvailableDocset(source)
    , m_mode(mode)
    , m_suffix(suffix)
{
    if (mode == FILE)
    {
        setName(i18n("Docset from %1 file", suffix.toUpper()));
    }
    else
    {
        setName(i18n("Docset from %1 directory", suffix.toUpper()));
    }
}

AvailableDBDocset::~AvailableDBDocset() = default;

AvailableDBDocset::Mode
AvailableDBDocset::mode() const
{
    return m_mode;
}

void
AvailableDBDocset::install()
{
    auto creator = new AvailableDBDocsetInstaller(this, m_suffix);
    creator->show();
}

void
AvailableDBDocset::breakInstallation()
{
}

AvailableDBDocsetInstaller::AvailableDBDocsetInstaller(AvailableDBDocset* docset, const QString& suffix)
    : QDialog(nullptr) // FIXME use qApp?
    , m_docset(docset)
    , m_suffix(suffix)
    , m_iconChanged(false)
    , m_urlOk(false)
    , m_nameOk(false)
{
    setModal(true);
    setAttribute(Qt::WA_DeleteOnClose, true);
    setWindowTitle(docset->name());

    auto formLayout = new QFormLayout;

    m_urlRequester = new KUrlRequester;
    if (docset->mode() == AvailableDBDocset::FILE)
    {
        // https://bugs.kde.org/show_bug.cgi?id=369542
        m_urlRequester->setFilter(i18n("*.%1|%2 files", suffix, suffix.toUpper()));
        m_urlRequester->setMode(KFile::File | KFile::ExistingOnly | KFile::LocalOnly);

        formLayout->addRow(i18n("%1 file:", m_suffix.toUpper()), m_urlRequester);
    }
    else
    {
        m_urlRequester->setMode(KFile::Directory | KFile::ExistingOnly | KFile::LocalOnly);
        formLayout->addRow(i18n("%1 directory:", m_suffix.toUpper()), m_urlRequester);
    }

    m_urlMessageWidget = new KMessageWidget;
    m_urlMessageWidget->setMessageType(KMessageWidget::Error);
    m_urlMessageWidget->setWordWrap(true);
    m_urlMessageWidget->setCloseButtonVisible(false);
    m_urlMessageWidget->setVisible(false);
    formLayout->addWidget(m_urlMessageWidget);

    m_nameEdit = new QLineEdit;
    formLayout->addRow(i18n("Docset name:"), m_nameEdit);

    m_nameMessageWidget = new KMessageWidget;
    m_nameMessageWidget->setMessageType(KMessageWidget::Error);
    m_nameMessageWidget->setWordWrap(true);
    m_nameMessageWidget->setCloseButtonVisible(false);
    m_nameMessageWidget->setVisible(false);
    formLayout->addWidget(m_nameMessageWidget);

    m_iconButton = new KIconButton;
    m_iconButton->setIconSize(16); // FIXME hidpi
    m_iconButton->setIcon(m_docset->icon());
    formLayout->addRow(i18n("Docset icon:"), m_iconButton);

    m_buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(m_buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(m_buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    connect(this, &QDialog::accepted, this, &AvailableDBDocsetInstaller::createDocset);

    auto mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(formLayout);
    mainLayout->addStretch();
    mainLayout->addWidget(m_buttonBox);

    // ========================================================================

    connect(m_iconButton, &KIconButton::iconChanged,
    this, [this]()
    {
        m_iconChanged = true;
    });

    auto urlChanged = [this]()
    {
        m_urlOk = checkUrl();
        updateOkButton();
    };

    connect(m_urlRequester, &KUrlRequester::urlSelected, this, urlChanged);
    connect(m_urlRequester, &KUrlRequester::textChanged, this, urlChanged);

    connect(m_nameEdit, &QLineEdit::textChanged,
            this, [this](const QString& text)
            {
                if (!m_iconChanged && !QIcon::fromTheme(text).isNull())
                {
                    m_iconButton->setIcon(text);
                }

                m_nameOk = checkName();
                updateOkButton();
            });

    updateOkButton();
}

AvailableDBDocsetInstaller::~AvailableDBDocsetInstaller() = default;

bool
AvailableDBDocsetInstaller::checkUrl()
{
    m_path = m_urlRequester->url().toLocalFile();
    if (m_path.isEmpty())
    {
        m_urlMessageWidget->setText(QString());
        return false;
    }

    if (m_docset->mode() == AvailableDBDocset::FILE)
    {
        if (!QFile::exists(m_path))
        {
            m_urlMessageWidget->setText(i18n("Selected file does not exists."));
            return false;
        }
    }
    else
    {
        auto dir = QDir(m_path);
        if (!dir.exists())
        {
            m_urlMessageWidget->setText(i18n("Selected directory does not exists."));
            return false;
        }

        if (dir.entryList({ QString("*.%1").arg(m_suffix) }, QDir::Files).isEmpty())
        {
            m_urlMessageWidget->setText(
                i18n("Selected directory does not contains %1 files.", m_suffix.toUpper()));
            return false;
        }
    }

    return true;
}

bool
AvailableDBDocsetInstaller::checkName()
{
    m_name = m_nameEdit->text().trimmed();
    if (m_name.isEmpty())
    {
        m_nameMessageWidget->setText(QString());
        return false;
    }

    if (Core::self()->docset(m_name))
    {
        m_nameMessageWidget->setText(i18n("The docset with the chosen name already exists."));
        return false;
    }

    return true;
}

void
AvailableDBDocsetInstaller::updateOkButton()
{
    m_urlMessageWidget->setVisible(!m_urlOk && !m_urlMessageWidget->text().isEmpty());
    m_nameMessageWidget->setVisible(!m_nameOk && !m_nameMessageWidget->text().isEmpty());

    m_buttonBox->button(QDialogButtonBox::Ok)->setEnabled(m_urlOk && m_nameOk);
}

void
AvailableDBDocsetInstaller::createDocset()
{
    {
        m_docset->createDocsetDir();

        MetadataConfig config(m_docset);
        config.setName(m_name);
        config.setOriginalName(m_name);
        // FIXME add child config class ?
        if (m_docset->mode() == AvailableDBDocset::FILE)
        {
            config.setValue(QStringLiteral("%1File").arg(m_suffix), m_path);
        }
        else
        {
            config.setValue(QStringLiteral("%1Directory").arg(m_suffix), m_path);
        }

        config.setIcon(QIcon::fromTheme(m_iconButton->icon()));
        config.save();
    }

    auto docset = m_docset->provider()->addDocset(m_docset->path());
    docset->reload();

    close();
}

}

#include "available_dbdocset.moc"
