/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <KConfigSkeleton>

#include "globals.h"

namespace KDevDocs
{

class UserInterfaceConfig : public KConfigSkeleton
{
    Q_OBJECT

public:
    enum Layout
    {
        DashLayout = 0,
        ZealLayout = 1
    };

    enum DarkMode
    {
        DarkModeAutoDetect = 0,
        DarkModeEnabled = 1,
        DarkModeDisabled = 2
    };

    ~UserInterfaceConfig() override;

    static UserInterfaceConfig*
    self();

    static bool
    isDashLayout();

    static bool
    isZealLayout();

    static bool
    isDarkTheme();

    static bool
    isDarkMode();

    static QColor
    webBackgroundColor();

    static QColor
    webBorderColor();

    static int
    panelHeight();

    static TruncationMode
    truncationMode();

signals:
    /**
     * Emitted when layout settings are changed.
     */
    void
    layoutChanged();

    /**
     * Emitted when current application's palette is changed or when layout settings
     * are changed (since our layouts have different look-and-feel).
     */
    void
    paletteChanged();

    /**
     * Emitted when truncation mode is changed.
     */
    void
    truncationModeChanged(KDevDocs::TruncationMode mode);

protected:
    bool
    usrSave() override;

private:
    UserInterfaceConfig();

    void
    itemChanged(quint64 flags);

    uint m_settingsChanged;

    int m_layout;
    int m_darkMode;
    int m_truncationMode;
};

}
