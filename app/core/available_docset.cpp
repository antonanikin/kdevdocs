/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "available_docset.h"

#include "installed_docset.h"
#include "provider.h"
#include "source.h"
#include "utils.h"

namespace KDevDocs
{

AvailableDocset::AvailableDocset(Source* source)
    : AvailableDocset(source, source)
{
}

AvailableDocset::AvailableDocset(AvailableDocset* parent)
    : AvailableDocset(parent->source(), parent)
{
}

AvailableDocset::AvailableDocset(Source* source, TreeItem* parent)
    : Docset(source->provider(), parent)
    , m_source(source)
    , m_installed(nullptr)
    , m_priority(std::numeric_limits<int>::max())
    , m_downloadingProgress(0)
{
    setState(NOT_INSTALLED);
    setIcon(m_source->icon());
}

AvailableDocset::~AvailableDocset()
{
    source()->removeDocset(this);
}

Source*
AvailableDocset::source() const
{
    return m_source;
}

int
AvailableDocset::priority() const
{
    return m_priority;
}

void
AvailableDocset::setPriority(int priority)
{
    if (m_priority != priority)
    {
        m_priority = priority;
        emitChanged();
    }
}

int
AvailableDocset::downloadingProgress() const
{
    return m_downloadingProgress;
}

void
AvailableDocset::setDownloadingProgress(unsigned long progress)
{
    if (m_downloadingProgress != progress)
    {
        m_downloadingProgress = progress;
        emitChanged();
    }
}

QString
AvailableDocset::path() const
{
    if (m_installed)
    {
        return m_installed->path();
    }

    return provider()->docsetsPath() + id();
}

InstalledDocset*
AvailableDocset::installed() const
{
    return m_installed;
}

void
AvailableDocset::setInstalled(InstalledDocset* installed)
{
    if (installed == m_installed)
    {
        return;
    }

    m_installed = installed;
    if (installed)
    {
        setState(INSTALLED);
    }
    else
    {
        setState(NOT_INSTALLED);
    }
}

bool
AvailableDocset::lessThan(const TreeItem* other, const QString& searchText) const
{
    Q_ASSERT(other);

    if (searchText.isEmpty())
    {
        if (auto otherDocset = other->cast<AvailableDocset>())
        {
            if (otherDocset->state() == NOT_INSTALLED && otherDocset->state() < state())
            {
                return true;
            }

            if (searchText.isEmpty() && m_priority != otherDocset->m_priority)
            {
                return m_priority < otherDocset->m_priority;
            }
        }
    }

    return Docset::lessThan(other, searchText);
}

}
