/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "metadata_config.h"

#include "available_docset.h"
#include "installed_docset.h"

namespace KDevDocs
{

MetadataConfig::MetadataConfig(const QString& path, QObject* parent)
    : KConfigSkeleton(path + QStringLiteral("/config"), parent)
    , m_path(path)
{
    m_group = config()->group(QStringLiteral("docset"));
    setCurrentGroup(m_group.name());

    auto originalNameItem = addItemString(QStringLiteral("originalName"), m_originalName);
    originalNameItem->setDefaultValue(m_originalName);

    auto versionItem = addItemString(QStringLiteral("version"), m_version);
    versionItem->setDefaultValue(m_version);

    auto nameItem = addItemString(QStringLiteral("name"), m_name);
    nameItem->setDefaultValue(m_originalName);
    if (m_name.isEmpty())
    {
        nameItem->setValue(m_originalName);
        save();
    }

    m_isComposite = m_group.readEntry(QStringLiteral("composite"), false);
}

MetadataConfig::MetadataConfig(InstalledDocset* docset)
    : MetadataConfig(docset->path(), docset)
{
}

MetadataConfig::MetadataConfig(AvailableDocset* docset, const QString& customName)
    : MetadataConfig(docset->path(), docset)
{
    auto name = customName.isEmpty() ? docset->name() : customName;
    setName(name);
    setOriginalName(name);
    setVersion(docset->version());
}

MetadataConfig::~MetadataConfig()
{
}

QString
MetadataConfig::name() const
{
    return m_name;
}

void
MetadataConfig::setName(const QString& name)
{
    m_name = name;
}

QString
MetadataConfig::originalName() const
{
    return m_originalName;
}

void
MetadataConfig::setOriginalName(const QString& originalName)
{
    m_originalName = originalName;
}

QString
MetadataConfig::version() const
{
    return m_version;
}

void
MetadataConfig::setVersion(const QString& version)
{
    m_version = version;
}

bool
MetadataConfig::isComposite() const
{
    return m_isComposite;
}

void
MetadataConfig::setIcon(const QIcon& icon)
{
    if (!icon.isNull())
    {
        icon.pixmap(16, 16).save(m_path + QStringLiteral("/icon.png"));
        icon.pixmap(32, 32).save(m_path + QStringLiteral("/icon@2x.png"));
    }
}

QString
MetadataConfig::value(const QString& key) const
{
    return m_group.readEntry(key);
}

void
MetadataConfig::setValue(const QString& key, const QString& value)
{
    m_group.writeEntry(key, value);
}

}
