find_package(Qt5Concurrent REQUIRED)
find_package(Qt5Sql REQUIRED)
find_package(Qt5WebKitWidgets REQUIRED)

find_package(KF5Config REQUIRED)
find_package(KF5I18n REQUIRED)
find_package(KF5IconThemes REQUIRED)
find_package(KF5KIO REQUIRED)

kconfig_add_kcfg_files(config_SRCS GENERATE_MOC
    kcfg/general_config.kcfgc
    kcfg/search_config.kcfgc
)

add_library(kdevdocs_core STATIC
    ${config_SRCS}

    available_dbdocset.cpp
    available_docset.cpp
    content_item.cpp
    core.cpp
    docset.cpp
    docset_item_type.cpp
    globals.cpp
    icon_manager.cpp
    index_item.cpp
    installed_dbdocset.cpp
    installed_dbdocset_database.cpp
    installed_docset.cpp
    item.cpp
    metadata_config.cpp
    network_access_manager.cpp
    provider.cpp
    query.cpp
    source.cpp
    tree_item.cpp
    user_interface_config.cpp
    utils.cpp
    web_config.cpp

    private/available_model.cpp
    private/contents_model.cpp
    private/core_private.cpp
    private/css.cpp
    private/installed_model.cpp
    private/network_reply.cpp
    private/tree_model.cpp
    private/tree_root_item.cpp
)

target_link_libraries(kdevdocs_core
    Qt5::Concurrent
    Qt5::Sql
    Qt5::WebKitWidgets

    KF5::ConfigWidgets
    KF5::I18n
    KF5::IconThemes
    KF5::KIOWidgets
)
