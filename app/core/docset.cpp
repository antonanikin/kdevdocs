/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "docset.h"
#include "provider.h"

#include <QDir>

namespace KDevDocs
{

Docset::Docset(Provider* provider, TreeItem* parent)
    : TreeObjectItem(parent, provider)
    , m_provider(provider)
    , m_composite(false)
{
    Q_ASSERT(provider);
    setIcon(provider->icon());
}

Docset::~Docset() = default;

Provider*
Docset::provider() const
{
    return m_provider;
}

QString
Docset::id() const
{
    return m_id;
}

void
Docset::setId(const QString& id)
{
    m_id = id;
}

QString
Docset::version() const
{
    return m_version;
}

void
Docset::setVersion(const QString& version)
{
    m_version = version;
}

bool
Docset::isComposite() const
{
    return m_composite;
}

void
Docset::setComposite(bool composite)
{
    m_composite = composite;
}

bool
Docset::createDocsetDir() const
{
    return QDir().mkpath(path());
}

bool
Docset::removeDocsetDir() const
{
    return QDir(path()).removeRecursively();
}

Docset*
Docset::parentDocset() const
{
    return parentItem()->cast<Docset>();
}

void
Docset::emitChanged()
{
    TreeObjectItem::emitChanged();
    if (auto parent = parentDocset())
    {
        parent->emitChanged();
    }
}

}
