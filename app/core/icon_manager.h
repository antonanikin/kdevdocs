/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QHash>
#include <QObject>
#include <QVector>

#include "docset_item_type.h"

namespace KDevDocs
{

class IconManager : public QObject
{
    Q_OBJECT

public:
    static IconManager*
    self();

    QIcon
    providerIcon(const QString& name);

    QIcon
    typeIcon(DocsetItemType::Type type);

    QIcon
    loadingIcon() const;

    QIcon
    webPageIcon(const QUrl& url) const;

    void
    setWebPageIcon(const QUrl& url, const QIcon& icon);

signals:
    void
    loadingIconChanged(const QIcon& icon);

    void
    webPageIconChanged(const QUrl& url);

private:
    IconManager();

    ~IconManager() override;

    Q_DISABLE_COPY(IconManager)

private:
    QString m_providerIconsPath;
    QString m_typeIconsPath;

    QVector<QIcon> m_loadingIcons;
    int m_loadingIconIndex = 0;

    QHash<DocsetItemType::Type, QIcon> m_typeIcons;
    QHash<QString, QIcon> m_webPageIcons;
};

}
