/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "globals.h"

#include "network_access_manager.h"

#include <QCryptographicHash>
#include <QUuid>

namespace KDevDocs
{

QString
truncate(const QString& text, TruncationMode mode, int charsCount)
{
    static const auto dots = QStringLiteral("...");

    if (mode == TruncateRight)
    {
        return text.left(charsCount) + dots;
    }
    else if (mode == TruncateLeft)
    {
        return dots + text.right(charsCount);
    }
    else
    {
        return text.left(charsCount) + dots + text.right(charsCount);
    }
}

bool
isExternalUrl(const QUrl& url)
{
    return url.scheme().startsWith(QStringLiteral("http"));
}

QUrl
browserUrl(const QUrl& url)
{
    if (isExternalUrl(url))
    {
        return url;
    }

    if (auto cacheItem = NetworkAccessManager::self()->cacheItem(url))
    {
        return cacheItem->localUrl();
    }

    return QUrl();
}

QString
createId(const QString& key)
{
    QString fixedKey = key.isEmpty() ? QUuid::createUuid().toString() : key;

    // Use Sha224 instead Sha256 for using id as host in urls - maximum host length is 63
    // when we don't use '.' to split parts.
    return QCryptographicHash::hash(fixedKey.toUtf8(), QCryptographicHash::Sha224).toHex();
}

}
