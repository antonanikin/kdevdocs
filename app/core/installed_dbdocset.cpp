/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "installed_dbdocset.h"

#include "content_item.h"
#include "globals.h"
#include "installed_dbdocset_database.h"
#include "metadata_config.h"

#include <QDir>

namespace KDevDocs
{

InstalledDBDocset::InstalledDBDocset(const QString& path, Provider* provider, const QString& suffix)
    : InstalledDocset(path, provider)
    , m_suffix(suffix)
{
}

InstalledDBDocset::~InstalledDBDocset()
{
    deleteAll(m_dbs);
}

void
InstalledDBDocset::loadDatabases()
{
    QString dbPath;

    dbPath = metadataConfig()->value(QStringLiteral("%1Directory").arg(m_suffix));
    if (!dbPath.isEmpty())
    {
        addDirectory(dbPath);
        return;
    }

    dbPath = metadataConfig()->value(QStringLiteral("%1File").arg(m_suffix));
    if (!dbPath.isEmpty())
    {
        addFile(dbPath);
    }
}

void
InstalledDBDocset::addFile(const QString& filePath)
{
    auto db = createDatabase(filePath);
    if (!db->isValid())
    {
        delete db;
        return;
    }

    m_dbs[db->baseName()] = db;
}

void
InstalledDBDocset::addDirectory(const QString& directoryPath)
{
    QDir dbDirectory(directoryPath);
    auto dbFiles = dbDirectory.entryList({ QString("*.%1").arg(m_suffix) }, QDir::Files);
    for (const QString& dbFile : dbFiles)
    {
        addFile(dbDirectory.filePath(dbFile));
    }
}

void
InstalledDBDocset::loadIndexContents(ContentItem* contentsRoot)
{
    for (auto db : qAsConst(m_dbs))
    {
        static const QString tocSuffix = QStringLiteral(".toc.bin");
        static const QString indexSuffix = QStringLiteral(".index.bin");

        IndexVector dbIndex;

        auto dbTocRoot = contentsRoot;
        if (m_dbs.size() > 1)
        {
            dbTocRoot = new ContentItem(this, contentsRoot, QString(), QString());
        }

        if (loadBinToc(dbTocRoot, db->path(), db->baseName() + tocSuffix) &&
            loadBinIndex(dbIndex, db->path(), db->baseName() + indexSuffix))
        {
            m_index += dbIndex;
            continue;
        }

        db->loadIndexContents(dbTocRoot, dbIndex);

        dbTocRoot->fixSingleChild();
        m_index += dbIndex;

        saveBinToc(dbTocRoot, db->path(), db->baseName() + tocSuffix);
        saveBinIndex(dbIndex, db->path(), db->baseName() + indexSuffix);
    }

    if (m_dbs.size() > 1)
    {
        contentsRoot->sortChilds();
    }
}

}
