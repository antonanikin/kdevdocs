/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "docset_item_type.h"

#include <QHash>
#include <QVector>

namespace KDevDocs
{

struct DocsetItemTypeData
{
    DocsetItemTypeData();

    QHash<QString, DocsetItemType::Type> stringToType;
    QVector<QString> typeToString;
    QVector<QString> typeToPluralString;

};
Q_GLOBAL_STATIC(DocsetItemTypeData, typeData)

DocsetItemTypeData::DocsetItemTypeData()
{
    stringToType =
    {
        { QStringLiteral("Abbreviation"), DocsetItemType::ABBREVIATION },
        { QStringLiteral("Alias"), DocsetItemType::ALIAS },
        { QStringLiteral("Annotation"), DocsetItemType::ANNOTATION },
        { QStringLiteral("Attribute"), DocsetItemType::ATTRIBUTE },
        { QStringLiteral("Axiom"), DocsetItemType::AXIOM },
        { QStringLiteral("Block"), DocsetItemType::BLOCK },
        { QStringLiteral("Binding"), DocsetItemType::BINDING },
        { QStringLiteral("Bookmark"), DocsetItemType::BOOKMARK },
        { QStringLiteral("Builtin"), DocsetItemType::BUILTIN },
        { QStringLiteral("Callback"), DocsetItemType::CALLBACK },
        { QStringLiteral("Category"), DocsetItemType::CATEGORY },
        { QStringLiteral("Class"), DocsetItemType::CLASS },
        { QStringLiteral("Collection"), DocsetItemType::COLLECTION },
        { QStringLiteral("Column"), DocsetItemType::COLUMN },
        { QStringLiteral("Command"), DocsetItemType::COMMAND },
        { QStringLiteral("Component"), DocsetItemType::COMPONENT },
        { QStringLiteral("Constant"), DocsetItemType::CONSTANT },
        { QStringLiteral("Constructor"), DocsetItemType::CONSTRUCTOR },
        { QStringLiteral("Control Structure"), DocsetItemType::CONTROL_STRUCTURE },
        { QStringLiteral("Conversion"), DocsetItemType::CONVERSION },
        { QStringLiteral("Data Source"), DocsetItemType::DATA_SOURCE },
        { QStringLiteral("Database"), DocsetItemType::DATABASE },
        { QStringLiteral("Decorator"), DocsetItemType::DECORATOR },
        { QStringLiteral("Define"), DocsetItemType::DEFINE },
        { QStringLiteral("Delegate"), DocsetItemType::DELEGATE },
        { QStringLiteral("DeletedSnippet"), DocsetItemType::DELETED_SNIPPET },
        { QStringLiteral("Diagram"), DocsetItemType::DIAGRAM },
        { QStringLiteral("Directive"), DocsetItemType::DIRECTIVE },
        { QStringLiteral("Element"), DocsetItemType::ELEMENT },
        { QStringLiteral("Entry"), DocsetItemType::ENTRY },
        { QStringLiteral("Enum"), DocsetItemType::ENUMERATION },
        { QStringLiteral("Environment"), DocsetItemType::ENVIRONMENT },
        { QStringLiteral("Error"), DocsetItemType::ERROR },
        { QStringLiteral("Event"), DocsetItemType::EVENT },
        { QStringLiteral("Exception"), DocsetItemType::EXCEPTION },
        { QStringLiteral("Expression"), DocsetItemType::EXPRESSION },
        { QStringLiteral("Extension"), DocsetItemType::EXTENSION },
        { QStringLiteral("Field"), DocsetItemType::FIELD },
        { QStringLiteral("File"), DocsetItemType::FILE },
        { QStringLiteral("Filter"), DocsetItemType::FILTER },
        { QStringLiteral("Flag"), DocsetItemType::FLAG },
        { QStringLiteral("Foreign Key"), DocsetItemType::FOREIGN_KEY },
        { QStringLiteral("Framework"), DocsetItemType::FRAMEWORK },
        { QStringLiteral("Function"), DocsetItemType::FUNCTION },
        { QStringLiteral("Global"), DocsetItemType::GLOBAL },
        { QStringLiteral("Glossary"), DocsetItemType::GLOSSARY },
        { QStringLiteral("Guide"), DocsetItemType::GUIDE },
        { QStringLiteral("Handler"), DocsetItemType::HANDLER },
        { QStringLiteral("Helper"), DocsetItemType::HELPER },
        { QStringLiteral("Hook"), DocsetItemType::HOOK },
        { QStringLiteral("Index"), DocsetItemType::INDEX },
        { QStringLiteral("Indirection"), DocsetItemType::INDIRECTION },
        { QStringLiteral("Inductive"), DocsetItemType::INDUCTIVE },
        { QStringLiteral("Instance"), DocsetItemType::INSTANCE },
        { QStringLiteral("Instruction"), DocsetItemType::INSTRUCTION },
        { QStringLiteral("Interface"), DocsetItemType::INTERFACE },
        { QStringLiteral("Iterator"), DocsetItemType::ITERATOR },
        { QStringLiteral("Keyword"), DocsetItemType::KEYWORD },
        { QStringLiteral("Lemma"), DocsetItemType::LEMMA },
        { QStringLiteral("Library"), DocsetItemType::LIBRARY },
        { QStringLiteral("Literal"), DocsetItemType::LITERAL },
        { QStringLiteral("Macro"), DocsetItemType::MACRO },
        { QStringLiteral("Member"), DocsetItemType::MEMBER },
        { QStringLiteral("Message"), DocsetItemType::MESSAGE },
        { QStringLiteral("Method"), DocsetItemType::METHOD },
        { QStringLiteral("Mixin"), DocsetItemType::MIXIN },
        { QStringLiteral("Modifier"), DocsetItemType::MODIFIER },
        { QStringLiteral("Module"), DocsetItemType::MODULE },
        { QStringLiteral("Namespace"), DocsetItemType::NAMESPACE },
        { QStringLiteral("NewSnippet"), DocsetItemType::NEW_SNIPPET },
        { QStringLiteral("Node"), DocsetItemType::NODE },
        { QStringLiteral("Notation"), DocsetItemType::NOTATION },
        { QStringLiteral("Object"), DocsetItemType::OBJECT },
        { QStringLiteral("Operator"), DocsetItemType::OPERATOR },
        { QStringLiteral("Option"), DocsetItemType::OPTION },
        { QStringLiteral("Package"), DocsetItemType::PACKAGE },
        { QStringLiteral("Parameter"), DocsetItemType::PARAMETER },
        { QStringLiteral("Pattern"), DocsetItemType::PATTERN },
        { QStringLiteral("Pipe"), DocsetItemType::PIPE },
        { QStringLiteral("Plugin"), DocsetItemType::PLUGIN },
        { QStringLiteral("Procedure"), DocsetItemType::PROCEDURE },
        { QStringLiteral("Projection"), DocsetItemType::PROJECTION },
        { QStringLiteral("Property"), DocsetItemType::PROPERTY },
        { QStringLiteral("Protocol"), DocsetItemType::PROTOCOL },
        { QStringLiteral("Provider"), DocsetItemType::PROVIDER },
        { QStringLiteral("Provisioner"), DocsetItemType::PROVISIONER },
        { QStringLiteral("Query"), DocsetItemType::QUERY },
        { QStringLiteral("Record"), DocsetItemType::RECORD },
        { QStringLiteral("Reference"), DocsetItemType::REFERENCE },
        { QStringLiteral("Register"), DocsetItemType::REGISTER },
        { QStringLiteral("Relationship"), DocsetItemType::RELATIONSHIP },
        { QStringLiteral("Report"), DocsetItemType::REPORT },
        { QStringLiteral("Request"), DocsetItemType::REQUEST },
        { QStringLiteral("Resource"), DocsetItemType::RESOURCE },
        { QStringLiteral("Role"), DocsetItemType::ROLE },
        { QStringLiteral("Sample"), DocsetItemType::SAMPLE },
        { QStringLiteral("Schema"), DocsetItemType::SCHEMA },
        { QStringLiteral("Script"), DocsetItemType::SCRIPT },
        { QStringLiteral("Section"), DocsetItemType::SECTION },
        { QStringLiteral("Sender"), DocsetItemType::SENDER },
        { QStringLiteral("Service"), DocsetItemType::SERVICE },
        { QStringLiteral("Setting"), DocsetItemType::SETTING },
        { QStringLiteral("Shortcut"), DocsetItemType::SHORTCUT },
        { QStringLiteral("Signature"), DocsetItemType::SIGNATURE },
        { QStringLiteral("Snippet"), DocsetItemType::SNIPPET },
        { QStringLiteral("Special Form"), DocsetItemType::SPECIAL_FORM },
        { QStringLiteral("State"), DocsetItemType::STATE },
        { QStringLiteral("Statement"), DocsetItemType::STATEMENT },
        { QStringLiteral("Struct"), DocsetItemType::STRUCTURE },
        { QStringLiteral("Style"), DocsetItemType::STYLE },
        { QStringLiteral("Subroutine"), DocsetItemType::SUBROUTINE },
        { QStringLiteral("Syntax"), DocsetItemType::SYNTAX },
        { QStringLiteral("Table"), DocsetItemType::TABLE },
        { QStringLiteral("Tactic"), DocsetItemType::TACTIC },
        { QStringLiteral("Tag"), DocsetItemType::TAG },
        { QStringLiteral("Template"), DocsetItemType::TEMPLATE },
        { QStringLiteral("Test"), DocsetItemType::TEST },
        { QStringLiteral("Trait"), DocsetItemType::TRAIT },
        { QStringLiteral("Trigger"), DocsetItemType::TRIGGER },
        { QStringLiteral("Type"), DocsetItemType::TYPE },
        { QStringLiteral("Union"), DocsetItemType::UNION },
        { QStringLiteral("Unknown"), DocsetItemType::UNKNOWN },
        { QStringLiteral("Value"), DocsetItemType::VALUE },
        { QStringLiteral("Variable"), DocsetItemType::VARIABLE },
        { QStringLiteral("Variant"), DocsetItemType::VARIANT },
        { QStringLiteral("View"), DocsetItemType::VIEW },
        { QStringLiteral("Web"), DocsetItemType::WEB },
        { QStringLiteral("WebSearch"), DocsetItemType::WEB_SEARCH },
        { QStringLiteral("Widget"), DocsetItemType::WIDGET },
        { QStringLiteral("Word"), DocsetItemType::WORD },

        { QStringLiteral("Man1"), DocsetItemType::MAN1 },
        { QStringLiteral("Man2"), DocsetItemType::MAN2 },
        { QStringLiteral("Man3"), DocsetItemType::MAN3 },
        { QStringLiteral("Man4"), DocsetItemType::MAN4 },
        { QStringLiteral("Man5"), DocsetItemType::MAN5 },
        { QStringLiteral("Man6"), DocsetItemType::MAN6 },
        { QStringLiteral("Man7"), DocsetItemType::MAN7 },
        { QStringLiteral("Man8"), DocsetItemType::MAN8 }
    };

    typeToString.resize(DocsetItemType::count());
    typeToPluralString.resize(DocsetItemType::count());

    for (auto it = stringToType.cbegin(); it != stringToType.cend(); ++it)
    {
        auto type = it.value();
        auto typeString = it.key();

        typeToString[type] = typeString;

        if (typeString.endsWith(QChar('y')))
        {
            typeString = typeString.left(typeString.length() - 1) + QStringLiteral("ies");
        }
        else if (typeString.endsWith(QChar('s')))
        {
            typeString += QStringLiteral("es");
        }
        else
        {
            typeString += QStringLiteral("s");
        }

        typeToPluralString[type] = typeString;
    }
}

DocsetItemType::Type
DocsetItemType::fromString(const QString& typeString)
{
    return typeData->stringToType.value(typeString, DocsetItemType::UNKNOWN);
}

QString
DocsetItemType::toString(Type type, bool pluralize)
{
    Q_ASSERT(type < DocsetItemType::count());

    if (pluralize)
    {
        return typeData->typeToPluralString[type];
    }
    else
    {
        return typeData->typeToString[type];
    }
}

}
