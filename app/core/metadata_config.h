/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <KConfigSkeleton>

namespace KDevDocs
{

class AvailableDocset;
class InstalledDocset;

class MetadataConfig : public KConfigSkeleton
{
    Q_OBJECT

public:
    explicit
    MetadataConfig(const QString& path, QObject* parent = nullptr);

    explicit
    MetadataConfig(InstalledDocset* docset);

    explicit
    MetadataConfig(AvailableDocset* docset, const QString& customName = QString());

    ~MetadataConfig() override;

    QString
    name() const;

    void
    setName(const QString& name);

    QString
    originalName() const;

    void
    setOriginalName(const QString& originalName);

    QString
    version() const;

    void
    setVersion(const QString& version);

    bool
    isComposite() const;

    void
    setIcon(const QIcon& icon);

    QString
    value(const QString& key) const;

    void
    setValue(const QString& key, const QString& value);

private:
    QString m_name;
    QString m_originalName;
    QString m_version;
    bool m_isComposite;

    QString m_path;
    KConfigGroup m_group;
};

}
