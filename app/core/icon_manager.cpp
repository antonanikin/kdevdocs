/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "icon_manager.h"

#include "core.h"
#include "installed_docset.h"
#include "user_interface_config.h"

#include <QApplication>
#include <QDir>
#include <QIcon>
#include <QLoggingCategory>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QTimer>
#include <QUrl>
#include <QWebSettings>

namespace KDevDocs
{

Q_LOGGING_CATEGORY(IM, "kdevdocs.icon_manager");

QIcon
loadPngIcon(const QString& directoryPath, const QString& iconName)
{
    const static QIcon missingIcon = QIcon::fromTheme(QStringLiteral("emblem-error"));
    const auto iconPath = QStringLiteral("%1/%2.png").arg(directoryPath, iconName);

    QIcon icon(iconPath);
    if (icon.isNull())
    {
        icon = missingIcon;
        qCDebug(IM) << "No such icon:" << iconPath;
    }

    return icon;
}

void
initLoadingIcons(QVector<QIcon>& loadingIcons)
{
    QImage loadingImage(QStandardPaths::locate(
        QStandardPaths::GenericDataLocation,
        QStringLiteral("kdevdocs/icons/loading.png")));

    int h = loadingImage.height();
    Q_ASSERT(h); // FIXME - load some error image and disable animation

    if (UserInterfaceConfig::isDarkMode())
    {
        loadingImage.invertPixels();
    }

    loadingIcons.resize(loadingImage.width() / h);
    for (int frame = 0; frame < loadingIcons.size(); ++frame)
    {
        QRect frameRect(0, 0, h, h);
        frameRect.moveLeft(h * frame);

        loadingIcons[frame] = QIcon(QPixmap::fromImage(loadingImage.copy(frameRect)));
    }
}

IconManager::IconManager()
{
    connect(qApp, &QCoreApplication::aboutToQuit, this, &IconManager::deleteLater);

    // init provider icons
    {
        m_providerIconsPath = QStandardPaths::locate(
            QStandardPaths::GenericDataLocation,
            QStringLiteral("kdevdocs/icons/providers"),
            QStandardPaths::LocateDirectory);
        Q_ASSERT(!m_providerIconsPath.isEmpty());
    }

    // init type icons
    {
        m_typeIconsPath = QStandardPaths::locate(
            QStandardPaths::GenericDataLocation,
            QStringLiteral("kdevdocs/icons/types"),
            QStandardPaths::LocateDirectory);
        Q_ASSERT(!m_typeIconsPath.isEmpty());
    }

    // init loading icons
    {
        initLoadingIcons(m_loadingIcons);
        connect(UserInterfaceConfig::self(), &UserInterfaceConfig::paletteChanged, this, [this]()
        {
            initLoadingIcons(m_loadingIcons);
        });

        auto loadingTimer = new QTimer(this);
        connect(loadingTimer, &QTimer::timeout, this, [this]()
        {
            m_loadingIconIndex = (m_loadingIconIndex + 1) % m_loadingIcons.size();
            emit loadingIconChanged(m_loadingIcons[m_loadingIconIndex]);
        });
        loadingTimer->start(50);
    }

    // init site icons
    {
        static const auto siteIconsDirName = QStringLiteral("favicons");

        QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
        dir.mkdir(siteIconsDirName);
        dir.cd(siteIconsDirName);

        // Enable favicons support for WebKit, since it disabled by default.
        QWebSettings::setIconDatabasePath(dir.canonicalPath());

        static const auto dbConnectionName = QStringLiteral("39944d9c-120a-4026-aad0-c720be38c01a");

        // QWebSettings::iconForUrl() has a strange logic - if we call it here we will receive an
        // empty icon. If we repeat this call after small delay (>10 ms for example) we will get
        // normal icon. Looks like a bug, so we load all stored icons here in our hand-made
        // "cache" which will be used and updated in the future.
        {
            auto db = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), dbConnectionName);
            db.setDatabaseName(QWebSettings::iconDatabasePath());
            db.open();

            QSqlQuery query(db);
            query.exec(QStringLiteral(
                "SELECT url, data FROM PageURL, IconData WHERE PageURL.iconID = IconData.iconID"));

            QPixmap pixmap;
            while (query.next())
            {
                const auto url = query.value(0).toUrl();
                pixmap.loadFromData(query.value(1).toByteArray());

                m_webPageIcons[url.toDisplayString()] = pixmap;
            }

            db.close();
        }
        QSqlDatabase::removeDatabase(dbConnectionName);
    }

}

IconManager::~IconManager()
{
    // Force close WebKit favicons database. This is necessary to flush all loaded icons to disk.
    // If we open some web page with new favicon, wait for site icon loading and immediately close
    // our app, then this icon will not be stored in database by WebKit engine. So fix it this way.
    QWebSettings::setIconDatabasePath(QString());
}

IconManager*
IconManager::self()
{
    static IconManager* m_self = nullptr;
    if (!m_self)
    {
        m_self = new IconManager;
    }

    return m_self;
}

QIcon
IconManager::providerIcon(const QString& name)
{
    return loadPngIcon(m_providerIconsPath, name);
}

QIcon
IconManager::typeIcon(DocsetItemType::Type type)
{
    if (!m_typeIcons.contains(type))
    {
        m_typeIcons[type] = loadPngIcon(m_typeIconsPath, DocsetItemType::toString(type));
    }

    return m_typeIcons[type];
}

QIcon
IconManager::loadingIcon() const
{
    return m_loadingIcons[m_loadingIconIndex];
}

QIcon
IconManager::webPageIcon(const QUrl& url) const
{
    if (url.scheme() == QStringLiteral("qrc"))
    {
        return QApplication::windowIcon();
    }

    if (auto docset = Core::self()->docset(url))
    {
        return docset->icon();
    }

    const auto icon = m_webPageIcons.value(url.toDisplayString(), QIcon());
    return icon.isNull() ? QIcon::fromTheme(QStringLiteral("internet-services")) : icon;
}

void
IconManager::setWebPageIcon(const QUrl& url, const QIcon& icon)
{
    if (!icon.isNull())
    {
        m_webPageIcons[url.toDisplayString()] = icon;
        emit webPageIconChanged(url);
    }
}

}
