/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "installed_docset.h"

namespace KDevDocs
{

class InstalledDBDocsetDatabase;

class InstalledDBDocset : public InstalledDocset
{
    Q_OBJECT

public:
    InstalledDBDocset(const QString& path, Provider* provider, const QString& suffix);

    ~InstalledDBDocset() override;

protected:
    void
    loadDatabases();

    virtual InstalledDBDocsetDatabase*
    createDatabase(const QString& filePath) = 0;

    void
    addFile(const QString& filePath);

    void
    addDirectory(const QString& directoryPath);

    void
    loadIndexContents(ContentItem* contentsRoot) override;

protected:
    QString m_suffix;
    QHash<QString, InstalledDBDocsetDatabase*> m_dbs;
};

}
