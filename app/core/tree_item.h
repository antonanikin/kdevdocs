/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "item.h"

#include <QObject>

namespace KDevDocs
{

class TreeItem : public Item
{
    Q_DISABLE_COPY(TreeItem)

public:
    enum State
    {
        IDLE  = 0x000001,
        BUSY  = 0x000100,
        ERROR = 0x010000,
    };

    ~TreeItem() override;

    void
    setName(const QString & name) override;

    virtual int
    state() const;

    virtual bool
    lessThan(const TreeItem* other, const QString& searchText = QString()) const;

    virtual bool
    isOrdered() const;

    TreeItem*
    parentItem() const;

    int
    itemRow() const;

    int
    childItemsCount() const;

    TreeItem*
    childItem(int row) const;

    QList<TreeItem*>
    takeChildItems();

    void
    setChildItems(const QList<TreeItem*>& childItems);

    void
    removeChildItems();

    template<typename T>
    const T*
    cast() const
    {
        return dynamic_cast<const T*>(this);
    }

    template<typename T>
    T*
    cast()
    {
        return dynamic_cast<T*>(this);
    }

protected:
    explicit
    TreeItem(TreeItem* parentItem = nullptr);

    virtual void
    setState(int state);

    void
    emitChanged() override;

private:
    TreeItem*
    rootItem();

    void
    addItem(TreeItem* item);

    void
    insertItem(TreeItem* item, int row);

    virtual void
    beginInsertItems(TreeItem* parent, int first, int last);

    virtual void
    endInsertItems();

    void
    removeItem(TreeItem* item);

    void
    removeItem(int row);

    virtual void
    beginRemoveItems(TreeItem* parent, int first, int last);

    virtual void
    endRemoveItems();

    void
    moveItem(int from, int to);

    virtual void
    beginMoveItem(TreeItem* parent, int from, int to);

    virtual void
    endMoveItem();

    virtual void
    emitItemChanged(TreeItem* item);

private:
    int m_state;
    TreeItem* m_parentItem;
    QList<TreeItem*> m_childItems;
};

class TreeObjectItem : public QObject, public TreeItem
{
    Q_OBJECT
    Q_DISABLE_COPY(TreeObjectItem)

public:
    ~TreeObjectItem()  override;

signals:
    void
    changed(QPrivateSignal);

protected:
    explicit
    TreeObjectItem(TreeItem* parentItem, QObject* parent = nullptr);

    void
    setState(int state) override;

    void
    emitChanged() override;
};

}

Q_DECLARE_METATYPE(KDevDocs::TreeItem*);
