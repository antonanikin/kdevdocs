/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "tree_item.h"
#include "docset_item_type.h"

namespace KDevDocs
{

class InstalledDocset;

class ContentItem : public TreeItem
{
    Q_DISABLE_COPY(ContentItem)

public:
    using Type = DocsetItemType::Type;
    static const Type DefaultType = Type::GUIDE;

    ContentItem(const InstalledDocset* docset);
    ContentItem(const InstalledDocset* docset, TreeItem* parent, const QString& name, const QString& href, Type type = DefaultType);

    ~ContentItem() override;

    Type
    type() const;

    void
    setType(Type type);

    QIcon
    icon() const override;

    void
    setIcon(const QIcon& icon) override;

    QUrl
    url() const override;

    void
    setUrl(const QUrl& url) override;

    QString
    href() const;

    void
    setHref(const QString& href);

    void
    sortChilds();

    void
    fixSingleChild();

private:
    const InstalledDocset* m_docset;
    QString m_href;
    Type m_type;
};

}
