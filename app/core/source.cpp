/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "source.h"

#include "available_docset.h"
#include "globals.h"
#include "provider.h"
#include "utils.h"
#include "private/core_private.h"

namespace KDevDocs
{

Source::Source(Provider* provider)
    : TreeObjectItem(CorePrivate::self()->sourcesRoot())
    , m_provider(provider)
{
    Q_ASSERT(provider);
    setIcon(provider->icon());
}

Source::~Source()
{
    clear();
}

Provider*
Source::provider() const
{
    return m_provider;
}

void
Source::clear()
{
    while (!m_docsets.isEmpty())
    {
        delete m_docsets.first();
    }
}

void
Source::addDocset(AvailableDocset* docset)
{
    Q_ASSERT(docset);

    m_docsets += docset;
    CorePrivate::self()->addDocset(docset);
}

void
Source::removeDocset(AvailableDocset* docset)
{
    Q_ASSERT(docset);

    docset->disconnect(this);
    CorePrivate::self()->removeDocset(docset);
    m_docsets.removeOne(docset);
}

bool
Source::lessThan(const TreeItem* other, const QString& /*searchText*/) const
{
    return nameCompare((const TreeItem*)this, other);
}

}
