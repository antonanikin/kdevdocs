/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QAbstractItemModel>

namespace KDevDocs
{

class TreeItem;
class TreeRootItem;

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit
    TreeModel(TreeRootItem* root);

    ~TreeModel() override;

    int
    rowCount(const QModelIndex& parent = QModelIndex()) const override;

    int
    columnCount(const QModelIndex& parent = QModelIndex()) const override;

    QModelIndex
    parent(const QModelIndex& child) const override;

    QModelIndex
    index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;

    QVariant
    data(const QModelIndex& index, int role) const override;

private:
    TreeItem*
    indexItem(const QModelIndex& index) const;

    QModelIndex
    itemIndex(TreeItem* item) const;

private:
    TreeRootItem* m_root;
};

}
