/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "available_model.h"

#include "globals.h"
#include "available_docset.h"

#include <QLoggingCategory>

namespace KDevDocs
{

AvailableModel::AvailableModel(QAbstractItemModel* sourceModel, QObject* parent)
    : QSortFilterProxyModel(parent)
{
    Q_ASSERT(sourceModel);
    setSourceModel(sourceModel);
}

AvailableModel::~AvailableModel() = default;

bool
AvailableModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    const auto index = sourceModel()->index(source_row, 0, source_parent);
    if (auto docset = index.data(AvailableDocsetRole).value<AvailableDocset*>())
    {
        return (docset->state() != AvailableDocset::INSTALLED);
    }

    return true;
}

void
AvailableModel::sort(int /*column*/, Qt::SortOrder /*order*/)
{
}

}
