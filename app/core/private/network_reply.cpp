/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "network_reply.h"

#include "network_access_manager.h"
#include "web_config.h"
#include "private/css.h"

#include <QTextCodec>
#include <QTimer>

namespace KDevDocs
{

NetworkReply::NetworkReply(
    QNetworkAccessManager::Operation op,
    const QNetworkRequest& request,
    const QUrl& url,
    OpenMode openMode,
    const WebConfig* config)

    : QNetworkReply(nullptr)
    , m_config(config)
{
    // https://stackoverflow.com/a/26674253
    // without setUrl() CSS parsing by QWebView is broken (@import command not works).
    setOperation(op);
    setRequest(request);
    setUrl(url);

    setOpenMode(openMode);
}

NetworkReply::~NetworkReply() = default;

void
NetworkReply::abort()
{
}

qint64
NetworkReply::bytesAvailable() const
{
    // Don't remove QNetworkReply::bytesAvailable()!
    return QNetworkReply::bytesAvailable() + m_data.length();
}

qint64
NetworkReply::readData(char* buffer, qint64 maxSize)
{
    const qint64 length = qMin(qint64(m_data.length()), maxSize);
    if (length)
    {
        memcpy(buffer, m_data.constData(), length);
        m_data.remove(0, length);
    }

    return length;
}

bool
NetworkReply::checkMetadata()
{
    m_dataIsCSS = m_dataIsHTML = false;

    const auto contentType = header(QNetworkRequest::ContentTypeHeader).toString();
    if (contentType.startsWith(QStringLiteral("text/css")))
    {
        m_dataIsCSS = true;
    }
    else if (contentType.startsWith(QStringLiteral("text/html")))
    {
        m_dataIsHTML = true;
    }

    return m_dataIsCSS || m_dataIsHTML;
}

void
NetworkReply::processData()
{
    if (m_dataIsProcessed)
    {
        return;
    }
    m_dataIsProcessed = true;

    if (m_data.isEmpty() | !m_config->forceFonts())
    {
        return;
    }

    if (m_dataIsCSS)
    {
        // FIXME is it correct to always use utf8?
        m_data = fixExternalCss(QString::fromUtf8(m_data)).toUtf8();
    }
    else if (m_dataIsHTML)
    {
        const auto htmlCodec = QTextCodec::codecForHtml(m_data);
        const auto html = htmlCodec->toUnicode(m_data);
        m_data = htmlCodec->fromUnicode(fixHtmlInlineStyles(html));
    }
}

BlockedNetworkReply::BlockedNetworkReply(
    QNetworkAccessManager::Operation op,
    const QNetworkRequest& request,
    const WebConfig* config)

    : NetworkReply(op, request, request.url(), ReadOnly, config)
{
    setError(ContentAccessDenied, QStringLiteral("Content is blocked"));
    QTimer::singleShot(0, this, &BlockedNetworkReply::finished);
}

BlockedNetworkReply::~BlockedNetworkReply() = default;

DocsetNetworkReply::DocsetNetworkReply(
    QNetworkAccessManager::Operation op,
    const QNetworkRequest& request,
    const CacheItem* cacheItem,
    const WebConfig* config)

    : NetworkReply(op, request, request.url(), ReadOnly, config)
{
    if (cacheItem)
    {
        cacheItem->readData(m_data);

        setHeader(QNetworkRequest::ContentLengthHeader, m_data.size());
        setHeader(QNetworkRequest::ContentTypeHeader, cacheItem->mimeType());
        QTimer::singleShot(0, this, &DocsetNetworkReply::metaDataChanged);

        checkMetadata();
        processData();
    }
    else
    {
        setError(ContentNotFoundError, QStringLiteral("Content not found"));
    }

    if (m_data.isEmpty())
    {
        QTimer::singleShot(0, this, &DocsetNetworkReply::finished);
    }
    else
    {
        QTimer::singleShot(0, this, &DocsetNetworkReply::readyRead);
    }
}

DocsetNetworkReply::~DocsetNetworkReply() = default;

qint64
DocsetNetworkReply::readData(char* buffer, qint64 maxSize)
{
    auto length = NetworkReply::readData(buffer, maxSize);

    if (m_data.isEmpty())
    {
        QTimer::singleShot(0, this, &DocsetNetworkReply::finished);
    }

    return length;
}

WrappedNetworkReply::WrappedNetworkReply(QNetworkReply* reply, const WebConfig* config)
    : NetworkReply(reply->operation(), reply->request(), reply->url(), reply->openMode(), config)
    , m_reply(reply)
{
    // Change parent of m_reply to avoid it's destroying by default parent (QNetworkAccsessManager?)
    // during our wrapper class lifetime.
    reply->setParent(this);

    // redirect reply signals
    connect(reply, &QNetworkReply::encrypted, this, &WrappedNetworkReply::encrypted);
    connect(reply, &QNetworkReply::sslErrors, this, &WrappedNetworkReply::sslErrors);
    connect(reply, &QNetworkReply::bytesWritten, this, &WrappedNetworkReply::bytesWritten);
    connect(reply, &QNetworkReply::aboutToClose, this, &WrappedNetworkReply::aboutToClose);
    connect(reply, &QNetworkReply::uploadProgress, this, &WrappedNetworkReply::uploadProgress);
    connect(reply, &QNetworkReply::downloadProgress, this, &WrappedNetworkReply::downloadProgress);
    connect(reply, &QNetworkReply::readChannelFinished, this, &WrappedNetworkReply::readChannelFinished);
    connect(reply, &QNetworkReply::preSharedKeyAuthenticationRequired, this, &WrappedNetworkReply::preSharedKeyAuthenticationRequired);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
    connect(reply, &QNetworkReply::redirected, this, &WrappedNetworkReply::redirected);
#endif

#if (QT_VERSION >= QT_VERSION_CHECK(5, 7, 0))
    connect(reply, &QNetworkReply::channelReadyRead, this, &WrappedNetworkReply::channelReadyRead);
    connect(reply, &QNetworkReply::channelBytesWritten, this, &WrappedNetworkReply::channelBytesWritten);
#endif

#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
    connect(reply, &QNetworkReply::redirectAllowed, this, &WrappedNetworkReply::redirectAllowed);
#endif

    // update our metadata
    connect(reply, &QNetworkReply::metaDataChanged, this, &WrappedNetworkReply::updateMetaData);

    // accumulate reply data
    connect(reply, &QNetworkReply::readyRead, this, [this]()
    {
        m_data += m_reply->readAll();

        if (!m_accumulationMode)
        {
            emit readyRead();
        }
    });

    // process reply data
    connect(reply, &QNetworkReply::finished, this, [this]()
    {
        if (m_accumulationMode)
        {
            processData();
        }

        emit finished();
    });

    connect(reply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
            this, [this](NetworkError errorCode)
            {
                setError(errorCode, m_reply->errorString());
                emit error(errorCode);
            });
}

WrappedNetworkReply::~WrappedNetworkReply() = default;

void
WrappedNetworkReply::close()
{
    m_reply->close();
}

bool
WrappedNetworkReply::isSequential() const
{
    return m_reply->isSequential();
}

void
WrappedNetworkReply::setReadBufferSize(qint64 size)
{
    m_reply->setReadBufferSize(size);
    NetworkReply::setReadBufferSize(size);
}

qint64
WrappedNetworkReply::bytesToWrite() const
{
    return m_reply->bytesToWrite();
}

void
WrappedNetworkReply::abort()
{
    m_reply->abort();
}

void
WrappedNetworkReply::ignoreSslErrors()
{
    m_reply->ignoreSslErrors();
}

void
WrappedNetworkReply::updateMetaData()
{
    // TODO update this when new Qt versions will be released.
    static const QList<QNetworkRequest::KnownHeaders> knownHeaders =
    {
        QNetworkRequest::ContentTypeHeader,
        QNetworkRequest::ContentLengthHeader,
        QNetworkRequest::LocationHeader,
        QNetworkRequest::LastModifiedHeader,
        QNetworkRequest::CookieHeader,
        QNetworkRequest::SetCookieHeader,
        QNetworkRequest::ContentDispositionHeader,
        QNetworkRequest::UserAgentHeader,
        QNetworkRequest::ServerHeader
    };

    // TODO update this when new Qt versions will be released.
    static const QList<QNetworkRequest::Attribute> attributes =
    {
        QNetworkRequest::HttpStatusCodeAttribute,
        QNetworkRequest::HttpReasonPhraseAttribute,
        QNetworkRequest::RedirectionTargetAttribute,
        QNetworkRequest::ConnectionEncryptedAttribute,
        QNetworkRequest::CacheLoadControlAttribute,
        QNetworkRequest::CacheSaveControlAttribute,
        QNetworkRequest::SourceIsFromCacheAttribute,
        QNetworkRequest::DoNotBufferUploadDataAttribute,
        QNetworkRequest::HttpPipeliningAllowedAttribute,
        QNetworkRequest::HttpPipeliningWasUsedAttribute,
        QNetworkRequest::CustomVerbAttribute,
        QNetworkRequest::CookieLoadControlAttribute,
        QNetworkRequest::AuthenticationReuseAttribute,
        QNetworkRequest::CookieSaveControlAttribute,

        // internal
        QNetworkRequest::MaximumDownloadBufferSizeAttribute,
        QNetworkRequest::DownloadBufferAttribute,
        QNetworkRequest::SynchronousRequestAttribute,

        QNetworkRequest::BackgroundRequestAttribute,
        QNetworkRequest::SpdyAllowedAttribute,
        QNetworkRequest::SpdyWasUsedAttribute,
        QNetworkRequest::EmitAllUploadProgressSignalsAttribute,

    #if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
        QNetworkRequest::FollowRedirectsAttribute,
    #endif

    #if (QT_VERSION >= QT_VERSION_CHECK(5, 8, 0))
        // Qt docs indicates nothing about minimal version, but present in 5.8
        // TODO report Qt bug about wrong docs
        QNetworkRequest::HTTP2AllowedAttribute,

        // Qt docs indicates 5.9 as minimal version but it also present in 5.8
        QNetworkRequest::HTTP2WasUsedAttribute,
    #endif

    #if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
        QNetworkRequest::OriginalContentLengthAttribute,
        QNetworkRequest::RedirectPolicyAttribute,
    #endif

    #if (QT_VERSION >= QT_VERSION_CHECK(5, 11, 0))
        QNetworkRequest::Http2DirectAttribute,

        // internal
        QNetworkRequest::ResourceTypeAttribute,
    #endif
    };

    const auto headerNames = m_reply->rawHeaderList();
    for (const auto& headerName : headerNames)
    {
        setRawHeader(headerName, m_reply->rawHeader(headerName));
    }

    for (auto header : knownHeaders)
    {
        setHeader(header, m_reply->header(header));
    }

    for (auto attribute : attributes)
    {
        setAttribute(attribute, m_reply->attribute(attribute));
    }

    m_accumulationMode = checkMetadata();

    emit metaDataChanged();
}

}
