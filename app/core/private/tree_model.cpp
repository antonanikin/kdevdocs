/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "tree_model.h"

#include "globals.h"
#include "icon_manager.h"
#include "available_docset.h"
#include "installed_docset.h"
#include "tree_root_item.h"

namespace KDevDocs
{

TreeModel::TreeModel(TreeRootItem* root)
    : QAbstractItemModel(root)
    , m_root(root)
{
    Q_ASSERT(root);

    connect(root, &TreeRootItem::itemsAboutToBeInserted, this, [this](TreeItem* parent, int first, int last)
    {
        beginInsertRows(itemIndex(parent), first, last);
    });

    connect(root, &TreeRootItem::itemsInserted, this, &TreeModel::endInsertRows);

    connect(root, &TreeRootItem::itemsAboutToBeRemoved, this, [this](TreeItem* parent, int first, int last)
    {
        beginRemoveRows(itemIndex(parent), first, last);
    });

    connect(root, &TreeRootItem::itemsRemoved, this, &TreeModel::endRemoveRows);

    connect(root, &TreeRootItem::itemAboutToBeMoved, this, [this](TreeItem* parent, int from, int to)
    {
        auto index = itemIndex(parent);
        if (to > from)
        {
            // QList::move() (used by TreeItem::moveChild()) and
            // QAbstractItemModel::beginMoveRows() have different logic in selecting destination
            // position.
            ++to;
        }

        beginMoveRows(index, from, from, index, to);
    });

    connect(root, &TreeRootItem::itemMoved, this, &TreeModel::endMoveRows);

    connect(root, &TreeRootItem::itemChanged, this, [this](TreeItem* parent)
    {
        auto index = itemIndex(parent);
        emit dataChanged(index, index);
    });
}

TreeModel::~TreeModel() = default;

TreeItem*
TreeModel::indexItem(const QModelIndex& index) const
{
    auto item = static_cast<TreeItem*>(index.internalPointer());
    return item ? item : m_root;
}

QModelIndex
TreeModel::itemIndex(KDevDocs::TreeItem* item) const
{
    if (!item || item == m_root)
    {
        // If standard createIndex() is used then new row is not displayed after its addition
        // to the root item.
        return QModelIndex();
    }

    return createIndex(item->itemRow(), 0, item);
}

int
TreeModel::rowCount(const QModelIndex& parent) const
{
    return indexItem(parent)->childItemsCount();
}

int
TreeModel::columnCount(const QModelIndex& /*parent*/) const
{
    return 1;
}

QModelIndex
TreeModel::parent(const QModelIndex& child) const
{
    auto childItem = indexItem(child);
    auto parentItem = childItem->parentItem();

    return itemIndex(parentItem);
}

QModelIndex
TreeModel::index(int row, int /*column*/, const QModelIndex& parent) const
{
    auto parentItem = indexItem(parent);
    auto childItem = parentItem->childItem(row);

    return itemIndex(childItem);
}

QVariant
TreeModel::data(const QModelIndex& index, int role) const
{
    auto item = indexItem(index);

    if (role == Qt::DisplayRole)
    {
        return item->name();
    }

    if (role == Qt::DecorationRole)
    {
        if (item->state() & TreeItem::BUSY)
        {
            return IconManager::self()->loadingIcon();
        }
        else
        {
            return item->icon();
        }
    }

    if (role == UrlRole)
    {
        return item->url();
    }

    if (role == TreeItemRole)
    {
        return QVariant::fromValue(item);
    }

    if (role == InstalledDocsetRole)
    {
        return QVariant::fromValue(item->cast<InstalledDocset>());
    }

    if (role == AvailableDocsetRole)
    {
        return QVariant::fromValue(item->cast<AvailableDocset>());
    }

    return QVariant();
}

}
