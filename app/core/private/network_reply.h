/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QNetworkReply>

namespace KDevDocs
{

class CacheItem;
class WebConfig;

class NetworkReply : public QNetworkReply
{
    Q_OBJECT

public:
    void
    abort() override;

    qint64
    bytesAvailable() const override;

protected:
    NetworkReply(
        QNetworkAccessManager::Operation op,
        const QNetworkRequest& request,
        const QUrl& url,
        OpenMode openMode,
        const WebConfig* config);

    ~NetworkReply() override;

    qint64
    readData(char* buffer, qint64 maxSize) override;

    bool
    checkMetadata();

    void
    processData();

protected:
    QByteArray m_data;
    const WebConfig* m_config = nullptr;

private:
    bool m_dataIsCSS = false;
    bool m_dataIsHTML = false;
    bool m_dataIsProcessed = false;
};

class BlockedNetworkReply : public NetworkReply
{
    Q_OBJECT

public:
    BlockedNetworkReply(
        QNetworkAccessManager::Operation op,
        const QNetworkRequest& request,
        const WebConfig* config);

    ~BlockedNetworkReply() override;
};

class DocsetNetworkReply : public NetworkReply
{
    Q_OBJECT

public:
    DocsetNetworkReply(
        QNetworkAccessManager::Operation op,
        const QNetworkRequest& request,
        const CacheItem* cacheItem,
        const WebConfig* config);

    ~DocsetNetworkReply() override;

protected:
    qint64
    readData(char* buffer, qint64 maxSize) override;
};

class WrappedNetworkReply : public NetworkReply
{
    Q_OBJECT

public:
    WrappedNetworkReply(QNetworkReply* reply, const WebConfig* config);

    ~WrappedNetworkReply() override;

    void
    close() override;

    bool
    isSequential() const override;

    void
    setReadBufferSize(qint64 size) override;

    qint64
    bytesToWrite() const override;

public slots:
    void
    abort() override;

    void
    ignoreSslErrors() override;

private:
    void
    updateMetaData();

private:
    QNetworkReply* m_reply = nullptr;
    bool m_accumulationMode = false;
};

}
