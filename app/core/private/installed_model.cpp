/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "installed_model.h"

#include "globals.h"
#include "installed_docset.h"

namespace KDevDocs
{

InstalledModel::InstalledModel(QAbstractItemModel* sourceModel, QObject* parent)
    : QSortFilterProxyModel(parent)
{
    Q_ASSERT(sourceModel);
    setSourceModel(sourceModel);
}

InstalledModel::~InstalledModel() = default;

bool
InstalledModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    auto index = sourceModel()->index(source_row, 0, source_parent);

    return index.data(InstalledDocsetRole).value<InstalledDocset*>();
}

void
InstalledModel::sort(int /*column*/, Qt::SortOrder /*order*/)
{
}

}
