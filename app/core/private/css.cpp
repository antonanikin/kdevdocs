/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "css.h"

#include <QLoggingCategory>
#include <QRegularExpression>

namespace KDevDocs
{

QString
removeHtmlComments(const QString& html)
{
    if (html.isEmpty())
    {
        return QString();
    }

    static const auto openTag = QStringLiteral("<!--");
    static const auto closeTag = QStringLiteral("-->");

    auto result = html;
    int start = 0;

    while (true)
    {
        const int openPos = result.indexOf(openTag, start);
        if (openPos < 0)
        {
            break;
        }

        const int closePos = result.indexOf(closeTag, openPos + openTag.size());
        if (closePos < 0)
        {
            break;
        }

        result.remove(openPos, closePos + closeTag.size() - openPos);
        start = openPos;
    }

    return result;
}

QString
fixHtmlInlineStyles(const QString& html)
{
    if (html.isEmpty())
    {
        return QString();
    }

    static const auto styleStart = QRegularExpression(QStringLiteral("<style[^>]*>"));
    static const auto styleEnd = QStringLiteral("</style>");

    auto result = removeHtmlComments(html);
    int start = 0;

    while (true)
    {
        auto match = styleStart.match(result, start);
        if (!match.hasMatch())
        {
            break;
        }

        const int cssStart = match.capturedEnd();
        const int cssEnd = result.indexOf(styleEnd, cssStart);

        if (cssEnd < 0)
        {
            break;
        }

        const auto originalCss = result.mid(cssStart, cssEnd - cssStart);
        const auto fixedCss = fixExternalCss(originalCss);

        result.replace(cssStart, cssEnd - cssStart, fixedCss);
        start = cssStart + fixedCss.length() + styleEnd.length();
    }

    return result;
}

bool
getCssLine(const QString& css, int& pos, QString& line, QChar& delimiter)
{
    static const QString delimiters = QStringLiteral("{};");

    bool isComment = false;
    bool isString = false;
    bool isBrackets = false;

    line.clear();
    delimiter = QChar();

    for (; pos < css.size(); ++pos)
    {
        const auto c = css[pos];
        if (c == QLatin1Char('\n'))
        {
            continue;
        }

        if (delimiters.contains(c) && !(isComment || isString || isBrackets))
        {
            delimiter = c;
            ++pos;
            break;
        }

        if (c == QLatin1Char('/') && !isString)
        {
            if (pos < (css.size() - 1) && css[pos + 1] == QLatin1Char('*'))
            {
                // +2 will be +3 by for and for empty comment /**/ we will be placed
                // on closing '/' (micro-optimization)
                pos += 2;

                isComment = true;
                continue;
            }
            else if (pos > 0 && css[pos - 1] == QLatin1Char('*'))
            {
                isComment = false;
                continue;
            }
        }

        if (isComment)
        {
            continue;
        }

        if (c == QLatin1Char('"'))
        {
            // Skip quoted marks
            if (css[pos - 1] != QLatin1Char('\\'))
            {
                isString = !isString;
            }

            line += c;
            continue;
        }

        line += c;

        if (!isString)
        {
            if (c == QLatin1Char('('))
            {
                isBrackets = true;
            }
            else if (c == QLatin1Char(')'))
            {
                isBrackets = false;
            }
        }
    }

    line = line.trimmed();

    if (isComment)
    {
        // unclosed comment at EOF, no printable string
        return false;
    }

    if (line.isEmpty() && delimiter.isNull())
    {
        // EOF
        return false;
    }

    return true;
}

QString
fixExternalCss(const QString& css)
{
    if (css.isEmpty())
    {
        return QString();
    }

    QString result;
    QString line;
    QChar delimiter;
    int pos = 0;

    while (getCssLine(css, pos, line, delimiter))
    {
        if (delimiter == QLatin1Char('}') || delimiter == QLatin1Char(';'))
        {
            const int index = line.indexOf(QLatin1Char(':'));
            if (index > 3) // "font" word length is 4
            {
                if (line.startsWith(QStringLiteral("font")))
                {
                    if (delimiter == QLatin1Char('}'))
                    {
                        result += delimiter;
                    }
                    continue;
                }
            }
        }

        result += line;
        result += delimiter;
    }

    return result;
}

}
