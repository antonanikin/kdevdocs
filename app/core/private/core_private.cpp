/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "core_private.h"

#include "available_docset.h"
#include "available_model.h"
#include "content_item.h"
#include "contents_model.h"
#include "globals.h"
#include "index_item.h"
#include "installed_docset.h"
#include "installed_model.h"
#include "provider.h"
#include "query.h"
#include "search_config.h"
#include "tree_model.h"
#include "tree_root_item.h"

#include <QLoggingCategory>
#include <QtConcurrent>

namespace KDevDocs
{

Q_LOGGING_CATEGORY(CORE, "kdevdocs.core");

CorePrivate* CorePrivate::m_self = nullptr;

CorePrivate::CorePrivate()
{
    connect(qApp, &QCoreApplication::aboutToQuit, this, &CorePrivate::shutdown);
}

CorePrivate::~CorePrivate()
{
    m_self = nullptr;
}

CorePrivate*
CorePrivate::self()
{
    if (!m_self)
    {
        m_self = new CorePrivate;
    }

    return m_self;
}

void
CorePrivate::init()
{
    static bool initDone = false;
    if (initDone)
    {
        return;
    }

    m_providersRoot = new TreeRootItem(this);

    {
        auto installedRoot = new TreeRootItem(this);
        m_installedRoot = installedRoot;

        auto installedFullModel = new TreeModel(installedRoot);
        m_installedModel = new InstalledModel(installedFullModel, this);
        m_contentsModel = new ContentsModel(installedFullModel, this);
    }

    {
        auto sourcesRoot = new TreeRootItem(this);
        m_sourcesRoot = sourcesRoot;

        auto availableFullModel = new TreeModel(sourcesRoot);
        m_availableModel = new AvailableModel(availableFullModel, this);
    }

    initDone = true;
}

void
CorePrivate::shutdown()
{
    for (auto watcher : qAsConst(m_watchers))
    {
        watcher->disconnect(this);
        watcher->waitForFinished();
    }

    deleteAll(m_providers);
    m_providers.clear();

    deleteLater();
}

Core::State
CorePrivate::state() const
{
    return m_state;
}

void
CorePrivate::setState(State state)
{
    if (m_state != state)
    {
        m_state = state;
        emit stateChanged();
    }
}

void
CorePrivate::reload()
{
    for (auto docset : qAsConst(m_docsets))
    {
        reload(docset);
    }
}

void
CorePrivate::reload(InstalledDocset* docset)
{
    Q_ASSERT(docset);

    docset->setState(InstalledDocset::LOADING);

    if (!docset->isComposite())
    {
        // clear index
        deleteAll(docset->m_index);
        docset->m_index.clear();

        // clear contents
        docset->removeChildItems();
    }

    if (m_watchers.isEmpty())
    {
        setState(LOADING);
    }

    auto watcher = new QFutureWatcher<void>(this);
    m_watchers += watcher;

    auto root = new ContentItem(docset);

    connect(watcher, &QFutureWatcher<void>::finished, this, [this, docset, root, watcher]()
    {
        m_watchers.removeOne(watcher);
        watcher->deleteLater();

        if (m_watchers.isEmpty())
        {
            setState(READY);
        }

        docset->setUrl(root->url());

        if (!docset->isComposite())
        {
            docset->setChildItems(root->takeChildItems());
        }

        if (docset->state() != InstalledDocset::ERROR)
        {
            docset->setState(InstalledDocset::LOADED);
        }

        delete root;
    });

    watcher->setFuture(QtConcurrent::run([docset, root]()
    {
        QTime start_i = QTime::currentTime();
        docset->loadIndexContents(root);
        qDebug() << "LOAD FINISH" << -QTime::currentTime().msecsTo(start_i) << "ms.;" << docset->name();
    }));

    if (docset->isComposite())
    {
        for (int i = 0; i < docset->childItemsCount(); ++i)
        {
            if (auto child = docset->childItem(i)->cast<InstalledDocset>())
            {
                reload(child);
            }
        }
    }
}

void
CorePrivate::addProvider(Provider* provider)
{
    Q_ASSERT(provider);
    m_providers += provider;
    m_schemes.insert(provider->urlScheme(), provider);
}

void
CorePrivate::removeProvider(Provider* provider)
{
    Q_UNUSED(provider)
    m_schemes.remove(provider->urlScheme());
    // FIXME not implemented yet
}

const QList<Provider*>&
CorePrivate::providers()
{
    return m_providers;
}

Provider*
CorePrivate::provider(const QUrl& url) const
{
    return m_schemes.value(url.scheme(), nullptr);
}

void
CorePrivate::updateSources()
{
    for (auto provider : qAsConst(m_providers))
    {
        provider->updateSources();
    }
}

void
CorePrivate::addDocset(InstalledDocset* installed)
{
    Q_ASSERT(installed);

    m_installedMap.insert(installed->id(), installed);
    if (auto available = m_availableMap.value(installed->id(), nullptr))
    {
        installed->setAvailable(available);
        available->setInstalled(installed);
    }

    if (!installed->parentDocset())
    {
        insertWithOrder(m_docsets, installed);
    }
}

void
CorePrivate::removeDocset(InstalledDocset* installed)
{
    Q_ASSERT(installed);

    installed->disconnect(this);
    if (auto available = installed->available())
    {
        installed->setAvailable(nullptr);
        available->setInstalled(nullptr);
    }

    m_installedMap.remove(installed->id());

    const int row = m_docsets.indexOf(installed);
    if (row >= 0)
    {
        m_docsets.removeAt(row);
    }

    // FIXME remove all correspondent elements from hashes, cache, etc
}

void
CorePrivate::addDocset(AvailableDocset* available)
{
    Q_ASSERT(available);

    m_availableMap.insert(available->id(), available);
    if (auto installed = m_installedMap.value(available->id(), nullptr))
    {
        installed->setAvailable(available);
        available->setInstalled(installed);
    }

    // FIXME workaround for loading icon repaint during Source update
    qApp->processEvents();
}

void
CorePrivate::removeDocset(AvailableDocset* available)
{
    Q_ASSERT(available);

    if (auto installed = available->installed())
    {
        installed->setAvailable(nullptr);
        available->setInstalled(nullptr);
    }
    m_availableMap.remove(available->id());

    // FIXME workaround for loading icon repaint during Source update
    qApp->processEvents();
}

const QList<InstalledDocset*>&
CorePrivate::docsets() const
{
    return m_docsets;
}

InstalledDocset*
CorePrivate::docset(const QString& name) const
{
    for (auto docset : m_docsets)
    {
        if (docset->name().compare(name, Qt::CaseInsensitive) == 0)
        {
            return docset;
        }
    }

    return nullptr;
}

InstalledDocset*
CorePrivate::docset(const QUrl& url) const
{
    InstalledDocset* docset = nullptr;
    QString id = url.host() + url.path();
    int index = 0;

    // Try to find docset by it's id which can be url host (not-composite docset without parent),
    // or url host + part of url path (composite docsets).
    while (true)
    {
        index = id.indexOf(QLatin1Char('/'), index + 1);
        if (index < 0)
        {
            break;
        }

        if (auto idDocset = m_installedMap.value(id.left(index), nullptr))
        {
            docset = idDocset;
        }
        else
        {
            break;
        }
    }

    if (docset)
    {
        return docset;
    }

    // FIXME ugly hack for short man internal links like "man:/tar(1)"
    // Fix this with "right way"
    if (url.scheme() == QStringLiteral("man") && url.host().isEmpty())
    {
        for (auto docset : m_docsets)
        {
            if (docset->provider()->urlScheme() == QStringLiteral("man"))
            {
                return docset;
            }
        }
    }

    return nullptr;
}

TreeItem*
CorePrivate::providersRoot() const
{
    return m_providersRoot;
}

TreeItem*
CorePrivate::installedRoot() const
{
    return m_installedRoot;
}

TreeItem*
CorePrivate::sourcesRoot() const
{
    return m_sourcesRoot;
}

QAbstractItemModel*
CorePrivate::contentsModel() const
{
    return m_contentsModel;
}

QAbstractItemModel*
CorePrivate::installedModel() const
{
    return m_installedModel;
}

QAbstractItemModel*
CorePrivate::availableModel() const
{
    return m_availableModel;
}

QList<QueryResultItem>
CorePrivate::execQuery(const Query& query) const
{
    QList<QueryResultItem> filteredItems;

    if (!query.isValid())
    {
        return filteredItems;
    }

    QTime start = QTime::currentTime();

    const int maxSize = SearchConfig::resultsLimit();
    int indexCount = 0;
    int priority = 0;

    const auto docsets = query.docsets();
    for (auto docset : docsets)
    {
        docset->execQuery(query, priority, filteredItems, maxSize, indexCount);

        // Priority sorting should be used only when query has custom docset selection, otherwise
        // for any query we will have top items from first accepted docset instead of best matched
        // ones from any other "lower" docsets.
        if (query.sourcesCount())
        {
            ++priority;
        }
    }

    qCDebug(CORE)
        << "QUERY EXEC FINISHED"
        << -QTime::currentTime().msecsTo(start) << "ms;"
        << "resultsCount =" << filteredItems.size() << ";"
        << "indicesCount =" << indexCount;

    return filteredItems;
}

bool
CorePrivate::testQuery(const Query& query) const
{
    if (!query.isValid())
    {
        return false;
    }

    QTime start = QTime::currentTime();

    for (auto docset : m_docsets)
    {
        if (docset->testQuery(query))
        {
            qCDebug(CORE) << "QUERY TEST FINISHED" << -QTime::currentTime().msecsTo(start) << "ms";
            return true;
        }
    }

    return false;
}

}


