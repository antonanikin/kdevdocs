/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "core.h"

#include <QDir>
#include <QHash>

template<typename T>
class QFutureWatcher;

namespace KDevDocs
{

class Provider;
class Query;
class TreeItem;
class QueryResultItem;
class AvailableDocset;

class CorePrivate : public Core
{
    Q_OBJECT

public:
    static CorePrivate*
    self();

    void
    init() override;

    void
    shutdown() override;

    void
    reload() override;

    void
    reload(InstalledDocset* docset);

    State
    state() const override;

    void
    addProvider(Provider* provider);

    void
    removeProvider(Provider* provider);

    const QList<Provider*>&
    providers() override;

    Provider*
    provider(const QUrl& url) const override;

    void
    updateSources() override;

    void
    addDocset(InstalledDocset* installed);

    void
    removeDocset(InstalledDocset* installed);

    void
    addDocset(AvailableDocset* available);

    void
    removeDocset(AvailableDocset* available);

    const QList<InstalledDocset*>&
    docsets() const override;

    InstalledDocset*
    docset(const QString& name) const override;

    InstalledDocset*
    docset(const QUrl& url) const override;

    TreeItem*
    installedRoot() const;

    TreeItem*
    providersRoot() const;

    TreeItem*
    sourcesRoot() const;

    QAbstractItemModel*
    contentsModel() const override;

    QAbstractItemModel*
    installedModel() const override;

    QAbstractItemModel*
    availableModel() const override;

    QList<QueryResultItem>
    execQuery(const Query& query) const;

    // FIXME rename ?
    bool
    testQuery(const Query& query) const;

private:
    CorePrivate();

    ~CorePrivate() override;

    Q_DISABLE_COPY(CorePrivate)

    void
    setState(State state);

private:
    static CorePrivate* m_self;
    State m_state;

    QList<QFutureWatcher<void>*> m_watchers;

    QList<Provider*> m_providers;
    QHash<QString, Provider*> m_schemes;

    QList<InstalledDocset*> m_docsets;

    QHash<QString, InstalledDocset*> m_installedMap;
    QHash<QString, AvailableDocset*> m_availableMap;

    TreeItem* m_providersRoot;
    TreeItem* m_installedRoot;
    TreeItem* m_sourcesRoot;

    QAbstractItemModel* m_contentsModel;
    QAbstractItemModel* m_installedModel;
    QAbstractItemModel* m_availableModel;
};

}
