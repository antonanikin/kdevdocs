/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "tree_item.h"

namespace KDevDocs
{

class TreeRootItem : public TreeObjectItem
{
    Q_OBJECT
    Q_DISABLE_COPY(TreeRootItem)

public:
    explicit
    TreeRootItem(QObject* parent = nullptr);

    ~TreeRootItem() override;

signals:
    void
    itemsAboutToBeInserted(KDevDocs::TreeItem* parent, int first, int last);

    void
    itemsInserted();

    void
    itemsAboutToBeRemoved(KDevDocs::TreeItem* parent, int first, int last);

    void
    itemsRemoved();

    void
    itemAboutToBeMoved(KDevDocs::TreeItem* parent, int from, int to);

    void
    itemMoved();

    void
    itemChanged(KDevDocs::TreeItem* item);

protected:
    bool
    isOrdered() const override;

private:
    void
    beginInsertItems(TreeItem* parent, int first, int last) override;

    void
    endInsertItems() override;

    void
    beginRemoveItems(TreeItem* parent, int first, int last) override;

    void
    endRemoveItems() override;

    void
    beginMoveItem(TreeItem* parent, int from, int to) override;

    void
    endMoveItem() override;

    void
    emitItemChanged(TreeItem* parent) override;
};

}
