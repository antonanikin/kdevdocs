/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "tree_root_item.h"

namespace KDevDocs
{

TreeRootItem::TreeRootItem(QObject* parent)
    : TreeObjectItem(nullptr, parent)
{
}

TreeRootItem::~TreeRootItem() = default;

bool
TreeRootItem::isOrdered() const
{
    return true;
}

void
TreeRootItem::beginInsertItems(TreeItem* parent, int first, int last)
{
    emit itemsAboutToBeInserted(parent, first, last);
}

void
TreeRootItem::endInsertItems()
{
    emit itemsInserted();
}

void
TreeRootItem::beginRemoveItems(TreeItem* parent, int first, int last)
{
    emit itemsAboutToBeRemoved(parent, first, last);
}

void
TreeRootItem::endRemoveItems()
{
    emit itemsRemoved();
}

void
TreeRootItem::beginMoveItem(TreeItem* parent, int from, int to)
{
    emit itemAboutToBeMoved(parent, from, to);
}

void
TreeRootItem::endMoveItem()
{
    emit itemMoved();
}

void
TreeRootItem::emitItemChanged(TreeItem* parent)
{
    emit itemChanged(parent);
}

}
