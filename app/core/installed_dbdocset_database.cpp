/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "installed_dbdocset_database.h"

#include <QFileInfo>

namespace KDevDocs
{

InstalledDBDocsetDatabase::InstalledDBDocsetDatabase(InstalledDBDocset* docset, const QString& filePath)
    : m_docset(docset)
    , m_isValid(false)
    , m_baseName(QFileInfo(filePath).baseName())
{
    Q_ASSERT(docset);
}

InstalledDBDocsetDatabase::~InstalledDBDocsetDatabase()
{
}

bool
InstalledDBDocsetDatabase::isValid() const
{
    return m_isValid;
}

QString
InstalledDBDocsetDatabase::path() const
{
    return m_path;
}

QString
InstalledDBDocsetDatabase::baseName() const
{
    return m_baseName;
}

}
