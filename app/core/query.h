/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QList>
#include <QString>

namespace KDevDocs
{

class InstalledDocset;
class IndexItem;

struct QuerySourceItem
{
    int startPosition;
    QString name; // lower-case
    InstalledDocset* docset; // FIXME add support for profiles (like in Dash) in next versions
};

class QueryResultItem
{
public:
    QueryResultItem(int priority);

    ~QueryResultItem() = default;

    const IndexItem*
    indexItem() const;

    bool
    lessThan(const QueryResultItem& other) const;

    bool
    hasMatch(const IndexItem* indexItem, const QString& text);

private:
    int m_priority;
    int m_indexOfText;
    const IndexItem* m_indexItem;
};

class Query
{
public:
    explicit
    Query(const QString& queryString = QString());

    bool
    isValid() const;

    QString
    text() const; // lower-case

    int
    sourcesCount() const;

    const QuerySourceItem&
    source(int index) const;

    QList<InstalledDocset*>
    docsets() const;

    bool
    operator==(const Query& other) const;

    bool
    test() const;

    QList<QueryResultItem>
    exec() const;

protected:
    bool m_isValid;
    QString m_queryString; // lower-case
    QString m_text; // lower-case
    QList<QuerySourceItem> m_sources;
};

}
