/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "index_item.h"

#include "icon_manager.h"
#include "installed_docset.h"

#include <QLoggingCategory>

namespace KDevDocs
{

Q_LOGGING_CATEGORY(INDEX_ITEM, "kdevdocs.index_item");

IndexItem::IndexItem(const InstalledDocset* docset, const QString& name, const QString& href, Type type)
    : Item()
    , m_docset(docset)
    , m_href(href)
    , m_nameLowerCase(name.toLower())
    , m_type(type)
{
    Q_ASSERT(docset);
    setName(name);
}

IndexItem::~IndexItem() = default;

IndexItem::Type
IndexItem::type() const
{
    return m_type;
}

void
IndexItem::setType(Type type)
{
    m_type = type;
}

QString
IndexItem::nameLowerCase() const
{
    return m_nameLowerCase;
}

QIcon
IndexItem::icon() const
{
    return IconManager::self()->typeIcon(m_type);
}

void
IndexItem::setIcon(const QIcon& /*icon*/)
{
    qCDebug(INDEX_ITEM, "setIcon() called, do nothing since we use externally stored icon");
}

QIcon
IndexItem::docsetIcon() const
{
    return m_docset->icon();
}

QUrl
IndexItem::url() const
{
    return m_docset->createUrl(m_href);
}

void
IndexItem::setUrl(const QUrl& /*url*/)
{
    qCDebug(INDEX_ITEM, "setUrl() called, do nothing since we use generated url");
}

QString
IndexItem::href() const
{
    return m_href;
}

void
IndexItem::setHref(const QString& href)
{
    m_href = href;
}

}
