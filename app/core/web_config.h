/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <KConfigSkeleton>

namespace KDevDocs
{

class InstalledDocset;

class WebConfig : public KConfigSkeleton
{
    Q_OBJECT

public:
    enum ExternalLinksMode
    {
        ExternalLinksUserSelection,
        ExternalLinksDesktopBrowser,
        ExternalLinksInternalBrowser
    };

    explicit
    WebConfig(InstalledDocset* docset, QObject* parent = nullptr);

    ~WebConfig() override;

    static WebConfig*
    globalConfig();

    static WebConfig*
    config(InstalledDocset* docset);

    static WebConfig*
    config(const QUrl& url);

    QString
    sansSerifFamily() const;

    QString
    serifFamily() const;

    QString
    monospaceFamily() const;

    int
    proportionalType() const;

    int
    proportionalSize() const;

    int
    monospaceSize() const;

    int
    minimalSize() const;

    bool
    forceFonts() const;

    bool
    javascriptEnabled() const;

    bool
    scrollAnimatorEnabled() const;

    bool
    externalContentEnabled() const;

    int
    externalLinksMode() const;

signals:
    void
    changed(KDevDocs::WebConfig* config);

private:
    WebConfig();

    void
    onConfigChanged();

    void
    update();

    void
    updateGlobalSettings();

private:
    static WebConfig* m_globalConfig;

    QString m_sansSerifFamily;
    ItemString* m_sansSerifFamilyItem;

    QString m_serifFamily;
    ItemString* m_serifFamilyItem;

    QString m_monospaceFamily;
    ItemString* m_monospaceFamilyItem;

    int m_proportionalType;
    ItemInt* m_proportionalTypeItem;

    int m_proportionalSize;
    ItemInt* m_proportionalSizeItem;

    int m_monospaceSize;
    ItemInt* m_monospaceSizeItem;

    int m_minimalSize;
    ItemInt* m_minimalSizeItem;

    int m_externalContentEnabled;
    ItemInt* m_externalContentEnabledItem;

    int m_externalLinksMode;
    ItemInt* m_externalLinksModeItem;

    bool m_forceFonts;
    ItemBool* m_forceFontsItem;

    bool m_javascriptEnabled;
    ItemBool* m_javascriptEnabledItem;

    bool m_scrollAnimatorEnabled;
    ItemBool* m_scrollAnimatorEnabledItem;
};

}
