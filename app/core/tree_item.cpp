/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "tree_item.h"

#include "globals.h"
#include "icon_manager.h"
#include "private/tree_root_item.h"

#include <QLoggingCategory>

namespace KDevDocs
{

TreeItem::TreeItem(TreeItem* parentItem)
    : m_state(IDLE)
    , m_parentItem(parentItem)
{
    if (m_parentItem)
    {
        m_parentItem->addItem(this);
    }
}

TreeItem::~TreeItem()
{
    // First we remove the item from parent to skip unneeded child items processing by root item.
    if (m_parentItem)
    {
        m_parentItem->removeItem(this);
    }

    removeChildItems();
}

int
TreeItem::state() const
{
    return m_state;
}

void
TreeItem::setState(int state)
{
    if (m_state != state)
    {
        m_state = state;
        emitChanged();
    }
}

bool
TreeItem::lessThan(const TreeItem* other, const QString& searchText) const
{
    Q_ASSERT(other);

    if (!searchText.isEmpty())
    {
        auto index = name().indexOf(searchText, 0, Qt::CaseInsensitive);
        auto otherIndex = other->name().indexOf(searchText, 0, Qt::CaseInsensitive);

        if (index != otherIndex)
        {
            return index < otherIndex;
        }
    }

    return nameCompare(this, other);
}

bool
TreeItem::isOrdered() const
{
    return false;
}

TreeItem*
TreeItem::rootItem()
{
    TreeItem* root = this;
    while (root->parentItem())
    {
        root = root->parentItem();
    }

    return root;
}

TreeItem*
TreeItem::parentItem() const
{
    return m_parentItem;
}

int
TreeItem::itemRow() const
{
    if (m_parentItem)
    {
        return m_parentItem->m_childItems.indexOf(const_cast<TreeItem*>(this));
    }

    return 0;
}

int
TreeItem::childItemsCount() const
{
    return m_childItems.count();
}

TreeItem*
TreeItem::childItem(int row) const
{
    return m_childItems.value(row);
}

void
TreeItem::setName(const QString & name)
{
    Item::setName(name);

    if (m_parentItem && m_parentItem->isOrdered())
    {
        const int from = itemRow();
        int to = findInsertPosition(m_parentItem->m_childItems, this, nameCompare<TreeItem*>);

        if (to > from)
        {
            // See QList::move() docs
            --to;
        }

        if (to != from)
        {
            m_parentItem->moveItem(from, to);
        }
    }
}

void
TreeItem::addItem(TreeItem* item)
{
    int row = m_childItems.count();
    if (isOrdered() && !item->name().isEmpty())
    {
        row = findInsertPosition(m_childItems, item, nameCompare<TreeItem*>);
    }

    insertItem(item, row);
}

void
TreeItem::insertItem(TreeItem* item, int row)
{
    auto root = rootItem();

    root->beginInsertItems(this, row, row);
    m_childItems.insert(row, item);
    root->endInsertItems();
}

void
TreeItem::beginInsertItems(TreeItem* /*parent*/, int /*first*/, int /*last*/)
{
}

void
TreeItem::endInsertItems()
{
}

void
TreeItem::removeItem(TreeItem* item)
{
    removeItem(item->itemRow());
}

void
TreeItem::removeItem(int row)
{
    auto root = rootItem();

    root->beginRemoveItems(this, row, row);
    m_childItems.removeAt(row);
    root->endRemoveItems();
}

void
TreeItem::beginRemoveItems(TreeItem* /*parent*/, int /*first*/, int /*last*/)
{
}

void
TreeItem::endRemoveItems()
{
}

void
TreeItem::moveItem(int from, int to)
{
    auto root = rootItem();

    root->beginMoveItem(this, from, to);
    m_childItems.move(from, to);
    root->endMoveItem();
}

void
TreeItem::beginMoveItem(TreeItem* /*parent*/, int /*from*/, int /*to*/)
{
}

void
TreeItem::endMoveItem()
{
}

void
TreeItem::removeChildItems()
{
    if (!m_childItems.isEmpty())
    {
        deleteAll(takeChildItems());
    }
}

void
TreeItem::emitChanged()
{
    rootItem()->emitItemChanged(this);
}

void
TreeItem::emitItemChanged(TreeItem* /*item*/)
{
}

QList<TreeItem*>
TreeItem::takeChildItems()
{
    if (m_childItems.isEmpty())
    {
        return {};
    }

    auto root = rootItem();
    root->beginRemoveItems(this, 0, m_childItems.count() - 1);

    auto childItems = m_childItems;
    for (auto child : childItems)
    {
        child->m_parentItem = nullptr;
    }
    m_childItems.clear();

    root->endRemoveItems();

    return childItems;
}

void
TreeItem::setChildItems(const QList<TreeItem*>& childItems)
{
    removeChildItems();

    if (childItems.isEmpty())
    {
        return;
    }

    auto root = rootItem();
    root->beginInsertItems(this, 0, childItems.count() - 1);

    m_childItems = childItems;
    for (auto childItem : qAsConst(m_childItems))
    {
        childItem->m_parentItem = this;
    }

    root->endInsertItems();
}

TreeObjectItem::TreeObjectItem(TreeItem* parentItem, QObject* parent)
    : QObject(parent)
    , TreeItem(parentItem)
{
}

TreeObjectItem::~TreeObjectItem() = default;

void
TreeObjectItem::setState(int state)
{
    if (this->state() == state)
    {
        return;
    }
    TreeItem::setState(state);

    if (state & BUSY)
    {
        connect(IconManager::self(), &IconManager::loadingIconChanged, this, &TreeObjectItem::emitChanged);
    }
    else
    {
        disconnect(IconManager::self(), &IconManager::loadingIconChanged, this, &TreeObjectItem::emitChanged);
    }
}

void
TreeObjectItem::emitChanged()
{
    TreeItem::emitChanged();
    emit changed(QPrivateSignal());
}

}
