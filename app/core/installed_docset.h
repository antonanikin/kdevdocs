/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "docset.h"

namespace KDevDocs
{

class Query;
class AvailableDocset;
class MetadataConfig;
class WebConfig;

class ContentItem;
class IndexItem;
class QueryResultItem;

class InstalledDocset : public Docset
{
    friend class CorePrivate;

    Q_OBJECT
    Q_DISABLE_COPY(InstalledDocset)

public:
    using IndexVector = QVector<IndexItem*>;

    enum State
    {
        NOT_LOADED   = IDLE | IDLE << 1,
        LOADED       = IDLE | IDLE << 2,
        UNINSTALLING = IDLE | IDLE << 3,

        LOADING      = BUSY | BUSY << 1,
    };

    ~InstalledDocset() override;

    int
    state() const override;

    QString
    path() const override;

    MetadataConfig*
    metadataConfig() const;

    WebConfig*
    webConfig() const;

    QUrl
    createUrl(const QString& href) const;

    QByteArray
    fileData(const QUrl& url) const;

    AvailableDocset*
    available() const;

    bool
    hasUpdate() const;

    QString
    error() const;

    void
    reload();

    void
    uninstall();

    bool
    lessThan(const TreeItem* other, const QString& searchText = QString()) const override;

protected:
    InstalledDocset(const QString& path, Provider* provider);

    InstalledDocset(const QString& path, InstalledDocset* parent);

    void
    setState(int state) override;

    virtual QByteArray
    fileData(const QString& path) const = 0;

    virtual void
    loadIndexContents(ContentItem* contentsRoot) = 0;

    void
    setError(const QString& error);

    void
    execQuery(const Query& query, int priority, QList<QueryResultItem>& results, int maxResults, int& indexCount) const;

    bool
    testQuery(const Query& query) const;

    bool
    loadBinToc(ContentItem* tocRoot, const QString& sourceFilePath, const QString& inFileName = QStringLiteral("toc.bin")) const;

    bool
    saveBinToc(const ContentItem* tocRoot, const QString& sourceFilePath, const QString& outFileName = QStringLiteral("toc.bin")) const;

    bool
    loadBinIndex(IndexVector& index, const QString& sourceFilePath, const QString& inFileName = QStringLiteral("index.bin"));

    bool
    saveBinIndex(const IndexVector& index, const QString& sourceFilePath, const QString& outFileName = QStringLiteral("index.bin")) const;

protected:
    IndexVector m_index;

private:
    InstalledDocset(const QString& path, Provider* provider, TreeItem* parent);

    void
    setAvailable(AvailableDocset* available);

    QByteArray
    loadData(const QString& inFileName, const QString& sourceFilePath) const;

    bool
    saveData(const QString& outFileName, const QString& sourceFilePath, const QByteArray& data) const;

private:
    QString m_path;
    QString m_error;

    AvailableDocset* m_available;

    MetadataConfig* m_metadataConfig;
    WebConfig* m_webConfig;
};

}
