/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "content_item.h"

#include "globals.h"
#include "icon_manager.h"
#include "installed_docset.h"

#include <QLoggingCategory>

namespace KDevDocs
{

Q_LOGGING_CATEGORY(CONTENT_ITEM, "kdevdocs.content_item");

ContentItem::ContentItem(const InstalledDocset* docset)
    : m_docset(docset)
    , m_type(DefaultType)
{
    Q_ASSERT(docset);
}

ContentItem::ContentItem(const InstalledDocset* docset, TreeItem* parent, const QString& name, const QString& href, Type type)
    : TreeItem(parent)
    , m_docset(docset)
    , m_href(href)
    , m_type(type)
{
    Q_ASSERT(docset);
    setName(name);
}

ContentItem::~ContentItem() = default;

ContentItem::Type
ContentItem::type() const
{
    return m_type;
}

void
ContentItem::setType(Type type)
{
    m_type = type;
}

QIcon
ContentItem::icon() const
{
    return IconManager::self()->typeIcon(m_type);
}

void
ContentItem::setIcon(const QIcon& /*icon*/)
{
    qCDebug(CONTENT_ITEM, "setIcon() called, do nothing since we use externally stored icon");
}

QUrl
ContentItem::url() const
{
    return m_docset->createUrl(m_href);
}

void
ContentItem::setUrl(const QUrl& /*url*/)
{
    qCDebug(CONTENT_ITEM, "setUrl() called, do nothing since we use generated url");
}

QString
ContentItem::href() const
{
    return m_href;
}

void
ContentItem::setHref(const QString& href)
{
    m_href = href;
}

void
ContentItem::sortChilds()
{
    if (childItemsCount() > 1)
    {
        auto childItems = takeChildItems();
        sort(childItems, nameCompare<TreeItem*>);
        setChildItems(childItems);
    }
}

void
ContentItem::fixSingleChild()
{
    if (childItemsCount() == 1)
    {
        auto singleChild = childItem(0)->cast<ContentItem>();
        setName(singleChild->name());
        setHref(singleChild->href());
        setChildItems(singleChild->takeChildItems());
    }
}

}
