/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "item.h"

namespace KDevDocs
{

Item::Item() = default;

Item::~Item() = default;

QString
Item::name() const
{
    return m_name;
}

void
Item::setName(const QString& name)
{
    m_name = name;
    emitChanged();
}

QIcon
Item::icon() const
{
    return m_icon;
}

void
Item::setIcon(const QIcon& icon)
{
    m_icon = icon;
    emitChanged();
}

QUrl
Item::url() const
{
    return m_url;
}

void
Item::setUrl(const QUrl& url)
{
    m_url = url;
    emitChanged();
}

void
Item::emitChanged()
{
}

}
