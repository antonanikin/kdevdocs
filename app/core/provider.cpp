/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "provider.h"

#include "globals.h"
#include "icon_manager.h"
#include "installed_docset.h"
#include "source.h"
#include "private/core_private.h"

#include <QDir>
#include <QStandardPaths>
#include <QWebSecurityOrigin>

namespace KDevDocs
{

QString
initDataDir(const QString& path)
{
    QDir d(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    d.mkpath(path);
    d.cd(path);
    return d.canonicalPath() + QLatin1Char('/');
}

Provider::Provider(const QString& urlScheme)
    : TreeObjectItem(CorePrivate::self()->providersRoot())
    , m_urlScheme(urlScheme)
{
    setIcon(IconManager::self()->providerIcon(urlScheme));

    m_feedsPath = initDataDir(QStringLiteral("feeds/%1").arg(urlScheme));
    m_docsetsPath = initDataDir(QStringLiteral("docsets/%1").arg(urlScheme));

    QWebSecurityOrigin::addLocalScheme(m_urlScheme);
}

Provider::~Provider()
{
    while (!m_docsets.isEmpty())
    {
        delete m_docsets.first();
    }

    QWebSecurityOrigin::removeLocalScheme(m_urlScheme);
}

void
Provider::init()
{
    CorePrivate::self()->addProvider(this);

    QDir dir(m_docsetsPath);
    auto subDirNames = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    for (const auto& subDirName : subDirNames)
    {
        addDocset(dir.filePath(subDirName));
    }

    for (auto source : qAsConst(m_sources))
    {
        source->load();
    }
}

QString
Provider::urlScheme() const
{
    return m_urlScheme;
}

InstalledDocset*
Provider::addDocset(const QString& path, InstalledDocset* parent)
{
    auto docset = createDocset(path, parent);

    CorePrivate::self()->addDocset(docset);

    return docset;
}

void
Provider::removeDocset(InstalledDocset* docset)
{
    Q_ASSERT(docset);
    docset->disconnect(this);

    CorePrivate::self()->removeDocset(docset);
    m_docsets.removeOne(docset);
}

QString
Provider::docsetsPath() const
{
    return m_docsetsPath;
}

QString
Provider::feedPath(const QString& name) const
{
    return QStringLiteral("%1/%2").arg(m_feedsPath, name);
}

void
Provider::updateSources()
{
    for (auto source : qAsConst(m_sources))
    {
        source->update();
    }
}

}
