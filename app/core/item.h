/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QIcon>
#include <QUrl>

namespace KDevDocs
{

class Item
{
public:
    virtual
    ~Item();

    QString
    name() const;

    virtual void
    setName(const QString& name);

    virtual QIcon
    icon() const;

    virtual void
    setIcon(const QIcon& icon);

    virtual QUrl
    url() const;

    virtual void
    setUrl(const QUrl& url);

protected:
    Item();

    virtual void
    emitChanged();

private:
    QString m_name;
    QIcon m_icon;
    QUrl m_url;
};

}
