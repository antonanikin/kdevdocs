/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "network_access_manager.h"

#include "core.h"
#include "installed_docset.h"
#include "globals.h"
#include "provider.h"
#include "utils.h"
#include "web_config.h"
#include "private/network_reply.h"

#include <QApplication>
#include <QFile>
#include <QLoggingCategory>
#include <QMimeDatabase>

namespace KDevDocs
{

Q_LOGGING_CATEGORY(NAM, "kdevdocs.network_access_manager");

CacheItem::CacheItem(const QUrl& localUrl, const QString& mimeType)
    : m_localUrl(localUrl)
    , m_mimeType(mimeType)
{
    Q_ASSERT(!localUrl.isEmpty());
    Q_ASSERT(!mimeType.isEmpty());
}

QUrl
CacheItem::localUrl() const
{
    return m_localUrl;
}

QString
CacheItem::mimeType() const
{
    return m_mimeType;
}

void
CacheItem::readData(QByteArray& data) const
{
    data.clear();

    const auto filePath = m_localUrl.toLocalFile();
    QFile dataFile(filePath);
    if (dataFile.open(QIODevice::ReadOnly))
    {
        data = dataFile.readAll();
        dataFile.close();
    }
    else
    {
        qCDebug(NAM) << "can't read from file" << filePath;
    }
}

NetworkAccessManager::NetworkAccessManager()
{

    m_cacheDir = QDir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));

    connect(qApp, &QApplication::aboutToQuit, this, &NetworkAccessManager::deleteLater);

    // FIXME not implemented yet.
    // We should also clear cache when docset content is changed during update for example.
//     connect(Core::self, &Core::docsetRemoved, this, [this](InstalledDocset* docset)
//     {
//         QMutableHashIterator<QString, CacheItem*> iter(m_cache);
//         while(iter.hasNext())
//         {
//             auto cacheItem = iter.next().value();
//             if (cacheItem->docset == docset)
//             {
//                 iter.remove();
//                 delete cacheItem;
//             }
//         }
//     });
}

NetworkAccessManager::~NetworkAccessManager()
{
    deleteAll(m_cache);
    m_cache.clear();

    m_cacheDir.removeRecursively();
}

NetworkAccessManager*
NetworkAccessManager::self()
{
    static NetworkAccessManager* m_self = nullptr;
    if (!m_self)
    {
        m_self = new NetworkAccessManager;
    }

    return m_self;
}

inline QString
cacheKeyForUrl(const QUrl& url)
{
    return QStringLiteral("%1/%2/%3").arg(url.scheme(), url.host(), url.path());
}

const CacheItem*
NetworkAccessManager::cacheItem(const QUrl& url)
{
    return m_cache.value(cacheKeyForUrl(url), nullptr);
}

const CacheItem*
NetworkAccessManager::get(const InstalledDocset* docset, const QUrl& url)
{
    Q_ASSERT(docset);

    const auto cacheKey = cacheKeyForUrl(url);
    if (auto item = m_cache.value(cacheKey, nullptr))
    {
        return item;
    }

    const auto data = docset->fileData(url);
    if (data.isEmpty())
    {
        qCDebug(NAM) << "empty data for" << url.toDisplayString();
        return nullptr;
    }

    static QMimeDatabase mimeDb;

    auto filePath = QFileInfo(m_cacheDir.filePath(cacheKey)).absoluteFilePath();
    auto mimeType = mimeDb.mimeTypeForUrl(url).name();

    // https://bugs.kde.org/show_bug.cgi?id=288277
    if (mimeType == QStringLiteral("application/x-extension-html"))
    {
        mimeType = QStringLiteral("text/html");
    }

    if (url.scheme() == QStringLiteral("man"))
    {
        mimeType = QStringLiteral("text/html");
        filePath += QStringLiteral(".html");
    }

    auto dirPath = QFileInfo(filePath).absolutePath();
    if (!m_cacheDir.mkpath(dirPath))
    {
        qCDebug(NAM) << "can't create" << dirPath;
        return nullptr;
    }

    QFile file(filePath);
    if (file.open(QIODevice::WriteOnly))
    {
        file.write(data);
        file.close();
    }
    else
    {
        qCDebug(NAM) << "can't write to file" << filePath;
        return nullptr;
    }

    auto localUrl = QUrl::fromLocalFile(filePath);
    localUrl.setFragment(url.fragment());

    auto item = new CacheItem(localUrl, mimeType);
    m_cache[cacheKey] = item;

    return item;
}

QNetworkReply*
NetworkAccessManager::createRequest(
    const QUrl& pageRequestedUrl,
    const WebConfig* pageConfig,
    Operation op,
    const QNetworkRequest& request,
    QIODevice* outgoingData)
{
    auto url = request.url();

    // Immediately create requests for start page to speedup loading
    if (url.scheme() == QStringLiteral("qrc"))
    {
        return createRequest(op, request, outgoingData);
    }

    if (Core::self()->provider(url))
    {
        if (auto docset = Core::self()->docset(url))
        {
            return new DocsetNetworkReply(op, request, get(docset, url), pageConfig);
        }

        // Here we fix broken urls with empty scheme. As a result such urls have docset scheme
        // but non-docset host. Try to get them via HTTPS.
        url.setScheme(QStringLiteral("https"));
    }

    QNetworkRequest fixedRequest(request);
    fixedRequest.setUrl(url);

    if (isExternalUrl(url) && !isExternalUrl(pageRequestedUrl) && !pageConfig->externalContentEnabled())
    {
        return new BlockedNetworkReply(op, fixedRequest, pageConfig);
    }

    auto standardReply = createRequest(op, fixedRequest, outgoingData);
    if (pageConfig->forceFonts())
    {
        return new WrappedNetworkReply(standardReply, pageConfig);
    }
    else
    {
        return standardReply;
    }
}

}
