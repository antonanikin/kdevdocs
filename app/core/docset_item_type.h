/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QString>

namespace KDevDocs
{

struct DocsetItemType
{
    enum Type
    {
        ABBREVIATION = 0,
        ALIAS,
        ANNOTATION,
        ATTRIBUTE,
        AXIOM,
        BINDING,
        BLOCK,
        BOOKMARK,
        BUILTIN,
        CALLBACK,
        CATEGORY,
        CLASS,
        COLLECTION,
        COLUMN,
        COMMAND,
        COMPONENT,
        CONSTANT,
        CONSTRUCTOR,
        CONTROL_STRUCTURE,
        CONVERSION,
        DATABASE,
        DATA_SOURCE,
        DECORATOR,
        DEFINE,
        DELEGATE,
        DELETED_SNIPPET,
        DIAGRAM,
        DIRECTIVE,
        ELEMENT,
        ENTRY,
        ENUMERATION,
        ENVIRONMENT,
        ERROR,
        EVENT,
        EXCEPTION,
        EXPRESSION,
        EXTENSION,
        FIELD,
        FILE,
        FILTER,
        FLAG,
        FOREIGN_KEY,
        FRAMEWORK,
        FUNCTION,
        GLOBAL,
        GLOSSARY,
        GUIDE,
        HANDLER,
        HELPER,
        HOOK,
        INDEX,
        INDIRECTION,
        INDUCTIVE,
        INSTANCE,
        INSTRUCTION,
        INTERFACE,
        ITERATOR,
        KEYWORD,
        LEMMA,
        LIBRARY,
        LITERAL,
        MACRO,
        MEMBER,
        MESSAGE,
        METHOD,
        MIXIN,
        MODIFIER,
        MODULE,
        NAMESPACE,
        NEW_SNIPPET,
        NODE,
        NOTATION,
        OBJECT,
        OPERATOR,
        OPTION,
        PACKAGE,
        PARAMETER,
        PATTERN,
        PIPE,
        PLUGIN,
        PROCEDURE,
        PROJECTION,
        PROPERTY,
        PROTOCOL,
        PROVIDER,
        PROVISIONER,
        QUERY,
        RECORD,
        REFERENCE,
        REGISTER,
        RELATIONSHIP,
        REPORT,
        REQUEST,
        RESOURCE,
        ROLE,
        SAMPLE,
        SCHEMA,
        SCRIPT,
        SECTION,
        SENDER,
        SERVICE,
        SETTING,
        SHORTCUT,
        SIGNATURE,
        SNIPPET,
        SPECIAL_FORM,
        STATE,
        STATEMENT,
        STRUCTURE,
        STYLE,
        SUBROUTINE,
        SYNTAX,
        TABLE,
        TACTIC,
        TAG,
        TEMPLATE,
        TEST,
        TRAIT,
        TRIGGER,
        TYPE,
        UNION,
        UNKNOWN,
        VALUE,
        VARIABLE,
        VARIANT,
        VIEW,
        WEB,
        WEB_SEARCH,
        WIDGET,
        WORD,

        MAN1,
        MAN2,
        MAN3,
        MAN4,
        MAN5,
        MAN6,
        MAN7,
        MAN8
    };

    static constexpr
    int
    version()
    {
        // increase this with DocsetItemType::Type enum changes
        return 1;
    }

    static constexpr
    int
    min()
    {
        return ABBREVIATION;
    }

    static constexpr
    int
    max()
    {
        return MAN8;
    }

    static constexpr
    int
    count()
    {
        return max() + 1;
    }

    static
    Type
    fromString(const QString& typeString);

    static
    QString
    toString(Type type, bool pluralize = false);
};

}
