/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "tree_item.h"

namespace KDevDocs
{

class AvailableDocset;
class Provider;

class Source : public TreeObjectItem
{
    Q_OBJECT

public:
    virtual
    ~Source();

    Provider*
    provider() const;

    virtual void
    load() = 0;

    virtual void
    update() = 0;

    void
    addDocset(AvailableDocset* docset);

    void
    removeDocset(AvailableDocset* docset);

    bool
    lessThan(const TreeItem* other, const QString& searchText = QString()) const override;

protected:
    explicit
    Source(Provider* provider);

    void
    clear();

private:
    Provider* m_provider;
    QList<AvailableDocset*> m_docsets;
};

}
