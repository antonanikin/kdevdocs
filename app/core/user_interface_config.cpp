/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "user_interface_config.h"

#include <QFontMetrics>
#include <QGuiApplication>
#include <QPalette>

namespace KDevDocs
{

enum UserInterfaceConfigSignals
{
    layoutSignal = 0x1 << 0,
    darkModeSignal = 0x1 << 1,
    truncationModeSignal = 0x1 << 2
};

UserInterfaceConfig::UserInterfaceConfig()
    : KConfigSkeleton()
    , m_settingsChanged(0)
{
    setCurrentGroup(QStringLiteral("UserInterface"));

    auto notify = static_cast<KConfigCompilerSignallingItem::NotifyFunction>(&UserInterfaceConfig::itemChanged);

    auto layoutItem = new ItemInt(currentGroup(), QStringLiteral("layout"), m_layout, DashLayout);
    auto layoutSignallingItem = new KConfigCompilerSignallingItem(layoutItem, this, notify, layoutSignal);
    addItem(layoutSignallingItem, QStringLiteral("layout"));

    auto darkModeItem = new ItemInt(currentGroup(), QStringLiteral("darkMode"), m_darkMode, DarkModeAutoDetect);
    auto darkModeSignallingItem = new KConfigCompilerSignallingItem(darkModeItem, this, notify, layoutSignal);
    addItem(darkModeSignallingItem, QStringLiteral("darkMode"));

    auto truncationModeItem = new ItemInt(currentGroup(), QStringLiteral("truncationMode"), m_truncationMode, TruncateRight);
    auto truncationModeSignallingItem = new KConfigCompilerSignallingItem(truncationModeItem, this, notify, truncationModeSignal);
    addItem(truncationModeSignallingItem, QStringLiteral("truncationMode"));

    connect(qGuiApp, &QGuiApplication::paletteChanged, this, &UserInterfaceConfig::paletteChanged);
}

UserInterfaceConfig::~UserInterfaceConfig()
{
}

UserInterfaceConfig*
UserInterfaceConfig::self()
{
    static UserInterfaceConfig* m_self = nullptr;
    if (!m_self)
    {
        m_self = new UserInterfaceConfig;
        m_self->read();
    }

    return m_self;
}

bool
UserInterfaceConfig::usrSave()
{
    if (!KConfigSkeleton::usrSave())
    {
        return false;
    }

    if (m_settingsChanged & layoutSignal)
    {
        emit layoutChanged();
        emit paletteChanged();
    }
    else if (m_settingsChanged & darkModeSignal)
    {
        emit paletteChanged();
    }
    else if (m_settingsChanged & truncationModeSignal)
    {
        emit truncationModeChanged((TruncationMode)m_truncationMode);
    }

    m_settingsChanged = 0;
    return true;
}

void
UserInterfaceConfig::itemChanged(quint64 flags)
{
    m_settingsChanged |= flags;
}

bool
UserInterfaceConfig::isDashLayout()
{
    return self()->m_layout == DashLayout;
}

bool
UserInterfaceConfig::isZealLayout()
{
    return !self()->isDashLayout();
}

bool
UserInterfaceConfig::isDarkTheme()
{
    return QPalette().text().color().lightness() > QPalette().window().color().lightness();
}

bool
UserInterfaceConfig::isDarkMode()
{
    if (self()->m_darkMode == DarkModeEnabled)
    {
        return true;
    }

    if (self()->m_darkMode == DarkModeDisabled)
    {
        return false;
    }

    return isDarkTheme();
}

QColor
UserInterfaceConfig::webBackgroundColor()
{
    return self()->isDarkMode() ? QColor(38, 38, 38) : Qt::white;
}

QColor
UserInterfaceConfig::webBorderColor()
{
    return self()->isDarkMode() ? QPalette().light().color() : QPalette().mid().color();
}

int
UserInterfaceConfig::panelHeight()
{
    return QFontMetrics(QGuiApplication::font()).height() + 25;
}

TruncationMode
UserInterfaceConfig::truncationMode()
{
    return (TruncationMode)self()->m_truncationMode;
}

}
