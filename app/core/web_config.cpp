/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "web_config.h"

#include "core.h"
#include "installed_docset.h"
#include "user_interface_config.h"

#include <QApplication>
#include <QFile>
#include <QFontDatabase>
#include <QFontInfo>
#include <QWebSettings>

namespace KDevDocs
{

WebConfig* WebConfig::m_globalConfig = nullptr;

WebConfig::WebConfig()
{
    setCurrentGroup(QStringLiteral("Web Settings"));

    m_sansSerifFamilyItem = addItemString(
        QStringLiteral("sansSerifFamily"),
        m_sansSerifFamily,
        QFontDatabase::systemFont(QFontDatabase::GeneralFont).family());

    m_serifFamilyItem = addItemString(
        QStringLiteral("serifFamily"),
        m_serifFamily,
        QStringLiteral("Serif"));

    m_monospaceFamilyItem = addItemString(
        QStringLiteral("monospaceFamily"),
        m_monospaceFamily,
        QFontDatabase::systemFont(QFontDatabase::FixedFont).family());

    m_proportionalTypeItem = addItemInt(
        QStringLiteral("proportionalType"),
        m_proportionalType,
        0 );

    m_proportionalSizeItem = addItemInt(
        QStringLiteral("proportionalSize"),
        m_proportionalSize,
        QFontInfo(QFontDatabase::systemFont(QFontDatabase::GeneralFont)).pixelSize());

    m_monospaceSizeItem = addItemInt(
        QStringLiteral("monospaceSize"),
        m_monospaceSize,
        QFontInfo(QFontDatabase::systemFont(QFontDatabase::FixedFont)).pixelSize());

    m_minimalSizeItem = addItemInt(
        QStringLiteral("minimalSize"),
        m_minimalSize,
        QFontInfo(QFontDatabase::systemFont(QFontDatabase::SmallestReadableFont)).pixelSize());

    m_forceFontsItem = addItemBool(
        QStringLiteral("forceFonts"),
        m_forceFonts,
        true);

    m_javascriptEnabledItem = addItemBool(
        QStringLiteral("javascriptEnabled"),
        m_javascriptEnabled,
        true);

    m_scrollAnimatorEnabledItem = addItemBool(
        QStringLiteral("scrollAnimatorEnabled"),
        m_scrollAnimatorEnabled,
        false);

    m_externalContentEnabledItem = addItemInt(
        QStringLiteral("externalContentEnabled"),
        m_externalContentEnabled,
        false);

    m_externalLinksModeItem = addItemInt(
        QStringLiteral("externalLinksMode"),
        m_externalLinksMode,
        ExternalLinksUserSelection);
}

WebConfig::WebConfig(InstalledDocset* docset, QObject* parent)
    : WebConfig()
{
    Q_ASSERT(docset);

    setParent(parent);

    connect(this, &KConfigSkeleton::configChanged, globalConfig(), &WebConfig::onConfigChanged);

    setSharedConfig(KSharedConfig::openConfig(docset->path() + QStringLiteral("/config")));
    load();

    connect(globalConfig(), &KConfigSkeleton::configChanged, this, &WebConfig::update);
    update();
}

WebConfig::~WebConfig()
{
    if (this == m_globalConfig)
    {
        m_globalConfig = nullptr;
    }
}

WebConfig*
WebConfig::globalConfig()
{
    if (!m_globalConfig)
    {
        m_globalConfig = new WebConfig;

        connect(qApp, &QCoreApplication::aboutToQuit, m_globalConfig, &WebConfig::deleteLater);

        connect(m_globalConfig, &KConfigSkeleton::configChanged, m_globalConfig, &WebConfig::onConfigChanged);

        connect(UserInterfaceConfig::self(), &UserInterfaceConfig::paletteChanged,
                m_globalConfig, &WebConfig::updateGlobalSettings);
        m_globalConfig->updateGlobalSettings();
    }

    return m_globalConfig;
}

WebConfig*
WebConfig::config(KDevDocs::InstalledDocset* docset)
{
    return docset ? docset->webConfig() : globalConfig();
}

WebConfig*
WebConfig::config(const QUrl& url)
{
    return config(Core::self()->docset(url));
}

void
WebConfig::onConfigChanged()
{
    auto config = static_cast<WebConfig*>(sender());
    Q_ASSERT(config);

    emit changed(config);
}

template<typename T>
bool
isDefaultValue(KConfigSkeletonGenericItem<T>* item)
{
    T value = item->value();
    item->swapDefault();

    T defaultValue = item->value();
    item->swapDefault();

    return (value == defaultValue);
}

#define updateItem(item, isChanged)\
{\
    auto globalValue = globalConfig()->item->value();\
    if (item->value() != globalValue)\
    {\
        if (isDefaultValue(item))\
        {\
            item->setValue(globalValue);\
            isChanged = true;\
        }\
        item->setDefaultValue(globalValue);\
    }\
}

void
WebConfig::update()
{
    bool isChanged = false;

    updateItem(m_sansSerifFamilyItem, isChanged)
    updateItem(m_serifFamilyItem, isChanged)
    updateItem(m_monospaceFamilyItem, isChanged)
    updateItem(m_proportionalTypeItem, isChanged)
    updateItem(m_proportionalSizeItem, isChanged)
    updateItem(m_monospaceSizeItem, isChanged)
    updateItem(m_minimalSizeItem, isChanged)
    updateItem(m_forceFontsItem, isChanged)
    updateItem(m_javascriptEnabledItem, isChanged)
    updateItem(m_scrollAnimatorEnabledItem, isChanged)
    updateItem(m_externalContentEnabledItem, isChanged)
    updateItem(m_externalLinksModeItem, isChanged)

    if (isChanged)
    {
        emit configChanged();
    }
}

void
WebConfig::updateGlobalSettings()
{
    // If HTML page doesn't set background (start page, generated man pages, "Bash" docset
    // for example) then it will be theme-dependent. When light theme is used with disabled dark
    // mode everything works well - we have black text on white background. When light theme is
    // used with enabled dark mode everything is fine again - we have light text on dark background.
    // But when dark theme is used with disabled dark mode we will see black text on dark background
    // (tested on Qt 5.11). Therefore when our dark mode CSS is applied we will see light text on
    // light background :(
    // So we should always set default background color to white - it will be properly processed
    // both for light and dark themes.
    QByteArray cssData = QByteArrayLiteral("body { background-color: white; }");

    if (UserInterfaceConfig::isDarkMode())
    {
        QFile cssFile(QStringLiteral(":/browser/darkmode.css"));
        if (cssFile.open(QIODevice::ReadOnly))
        {
            cssData += cssFile.readAll();
        }
    }

    static const QString cssUrl = QStringLiteral("data:text/css;charset=utf-8;base64,");
    QWebSettings::globalSettings()->setUserStyleSheetUrl(QUrl(cssUrl + cssData.toBase64()));
}

QString
WebConfig::sansSerifFamily() const
{
    return m_sansSerifFamily;
}

QString
WebConfig::serifFamily() const
{
    return m_serifFamily;
}

QString
WebConfig::monospaceFamily() const
{
    return m_monospaceFamily;
}

int
WebConfig::proportionalType() const
{
    return m_proportionalType;
}

int
WebConfig::proportionalSize() const
{
    return m_proportionalSize;
}

int
WebConfig::monospaceSize() const
{
    return m_monospaceSize;
}

int
WebConfig::minimalSize() const
{
    return m_minimalSize;
}

bool
WebConfig::forceFonts() const
{
    return m_forceFonts;
}

bool
WebConfig::javascriptEnabled() const
{
    return m_javascriptEnabled;
}

bool WebConfig::scrollAnimatorEnabled() const
{
    return m_scrollAnimatorEnabled;
}

bool
WebConfig::externalContentEnabled() const
{
    return m_externalContentEnabled;
}

int
WebConfig::externalLinksMode() const
{
    return m_externalLinksMode;
}

}
