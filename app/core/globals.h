/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QUrl>

#include "private/qtcompat_p.h"

namespace KDevDocs
{

enum DataRole
{
    TreeItemRole        = Qt::UserRole + 1,
    InstalledDocsetRole = Qt::UserRole + 2,
    AvailableDocsetRole = Qt::UserRole + 3,
    DocsetIconRole      = Qt::UserRole + 4,
    UrlRole             = Qt::UserRole + 5
};

enum TruncationMode
{
    TruncateRight,
    TruncateLeft,
    TruncateMiddle
};

QString
truncate(const QString& text, TruncationMode mode, int charsCount);

bool
isExternalUrl(const QUrl& url);

QUrl
browserUrl(const QUrl& url);

QString
createId(const QString& key = QString());

// On Ubuntu 16.04 with gcc-5.4.0 / clang-4.0.0 standard qDeleteAll() doesn't produce
// [-Wdelete-incomplete] warning for containers with incomplete item type :(
// Qt bug ?
template<typename Container>
inline void
deleteAll(const Container& container)
{
    for (auto item : container)
    {
        delete item;
    }
}

template<typename T, typename Compare>
int
findInsertPosition(const QList<T> items, const T& item, Compare compare)
{
    int a = 0;
    int b = items.count() - 1;

    while (b >= a)
    {
        if (compare(item, items[a]))
        {
            break;
        }

        if (compare(items[b], item))
        {
            a = b + 1;
            break;
        }

        int c = (a + b) / 2;
        if (c == a)
        {
            a = b;
            break;
        }

        if (compare(item, items[c]))
        {
            b = c;
        }
        else
        {
            a = c;
        }
    }

    return a;
}

template<typename T, class = typename std::enable_if<std::is_pointer<T>::value>::type>
int
findInsertPosition(const QList<T> items, const T item)
{
    return findInsertPosition(items, item, [](const T a, const T b)
    {
        return a->lessThan(b);
    });
}

template<typename T, class = typename std::enable_if<!std::is_pointer<T>::value>::type>
int
findInsertPosition(const QList<T> items, const T& item)
{
    return findInsertPosition(items, item, [](const T& a, const T& b)
    {
        return a.lessThan(b);
    });
}

template<typename T>
bool
insertWithOrder(QList<T>& items, T& item, int maxSize = -1)
{
    const int position = findInsertPosition(items, item);
    if (maxSize > 0 && position == maxSize)
    {
        return false;
    }

    items.insert(position, item);
    if (maxSize > 0 && items.size() > maxSize)
    {
        items.removeLast();
    }

    return true;
}

template<typename T, typename Compare>
void
sort(QList<T*>& list, Compare compare)
{
    std::sort(list.begin(), list.end(), compare);
}

template<typename T>
void
sort(QList<T*>& list)
{
    sort(list, [](T* a, T* b)
    {
        return a->lessThan(b);
    });
}

template<typename T, class = typename std::enable_if<std::is_pointer<T>::value>::type>
bool
nameCompare(const T a, const T b)
{
    return a->name().compare(b->name(), Qt::CaseInsensitive) < 0;
}

}
