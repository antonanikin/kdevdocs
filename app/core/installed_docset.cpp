/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "installed_docset.h"

#include "available_docset.h"
#include "content_item.h"
#include "globals.h"
#include "index_item.h"
#include "metadata_config.h"
#include "provider.h"
#include "query.h"
#include "source.h"
#include "web_config.h"
#include "private/core_private.h"

#include <QDir>
#include <QLoggingCategory>
#include <QStack>

namespace KDevDocs
{

Q_LOGGING_CATEGORY(INSTALLED, "kdevdocs.abstract.installed");

InstalledDocset::InstalledDocset(const QString& path, Provider* provider)
    : InstalledDocset(path, provider, CorePrivate::self()->installedRoot())
{
}

InstalledDocset::InstalledDocset(const QString& path, InstalledDocset* parent)
    : InstalledDocset(path, parent->provider(), parent)
{
}

InstalledDocset::InstalledDocset(const QString& path, Provider* provider, TreeItem* parent)
    : Docset(provider, parent)
    , m_path(path)
    , m_available(nullptr)
{
    setId(QDir(path).dirName());

    {
        auto updateMetadata = [this]()
        {
            setName(m_metadataConfig->name());
            setVersion(m_metadataConfig->version());
            setComposite(m_metadataConfig->isComposite());
        };

        m_metadataConfig = new MetadataConfig(this);
        connect(m_metadataConfig, &MetadataConfig::configChanged, this, updateMetadata);
        updateMetadata();
    }

    m_webConfig = new WebConfig(this, this);

    // Attempt to find the icon in any supported format
    QDir docsetDir(path);
    const auto files = docsetDir.entryList({ QStringLiteral("icon.*") }, QDir::Files);
    for (const QString& file : files)
    {
        auto icon = QIcon(docsetDir.absoluteFilePath(file));
        if (!icon.availableSizes().isEmpty())
        {
            setIcon(icon);
            break;
        }
    }

    if (isComposite())
    {
        QDir dir(path);
        const auto subDirNames = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (const auto& subDirname : subDirNames)
        {
            provider->addDocset(dir.filePath(subDirname), this);
        }
    }

    setState(NOT_LOADED);
}

InstalledDocset::~InstalledDocset()
{
    if (state() == UNINSTALLING)
    {
        removeDocsetDir();
    }

    deleteAll(m_index);
    m_index.clear();

    provider()->removeDocset(this);
}

int
InstalledDocset::state() const
{
    if (isComposite())
    {
        for (int i = 0; i < childItemsCount(); ++i)
        {
            if (childItem(i)->state() == LOADING)
            {
                return LOADING;
            }
        }
    }

    return Docset::state();
}

void
InstalledDocset::setState(int state)
{
    auto currentState = isComposite() ? Docset::state() : this->state();
    if (currentState == state)
    {
        return;
    }
    Docset::setState(state);

    if (state == UNINSTALLING)
    {
        deleteLater();
    }
}

QString
InstalledDocset::path() const
{
    return m_path;
}

MetadataConfig*
InstalledDocset::metadataConfig() const
{
    return m_metadataConfig;
}

WebConfig*
InstalledDocset::webConfig() const
{
    return m_webConfig;
}

QUrl
InstalledDocset::createUrl(const QString& href) const
{
    if (href.isEmpty())
    {
        return {};
    }

    return QUrl(QStringLiteral("%1://%2/%3").arg(provider()->urlScheme(), id(), href));
}

QByteArray
InstalledDocset::fileData(const QUrl& url) const
{
    auto path = url.host() + url.path();

    // "Normal" url always contain docset id. But some urls are "broken", for example,
    // internal links in man pages looks like "man:/tar(1)", so we should keep them "as is".
    if (path.startsWith(id()))
    {
        path.remove(0, id().size());
    }

    return fileData(path);
}

void
InstalledDocset::uninstall()
{
    setState(UNINSTALLING);
}

AvailableDocset*
InstalledDocset::available() const
{
    return m_available;
}

void
InstalledDocset::setAvailable(AvailableDocset* available)
{
    if (m_available == available)
    {
        return;
    }

    if (m_available)
    {
        m_available->disconnect(this);
    }

    m_available = available;
    if (m_available)
    {
        connect(available, &AvailableDocset::changed, this, &InstalledDocset::emitChanged);
    }
}

bool
InstalledDocset::hasUpdate() const
{
    if (isComposite())
    {
        for (int i = 0; i < childItemsCount(); ++i)
        {
            if (auto child = childItem(i)->cast<InstalledDocset>())
            {
                if (child->hasUpdate())
                {
                    return true;
                }
            }
        }
    }

    return (m_available && m_available->version() != version());
}

QString
InstalledDocset::error() const
{
    return m_error;
}

void
InstalledDocset::setError(const QString& error)
{
    m_error = error;
}

void
InstalledDocset::reload()
{
    CorePrivate::self()->reload(this);
}

void
InstalledDocset::execQuery(const Query& query, int priority, QList<QueryResultItem>& results, int maxResults, int& indexCount) const
{
    if (isComposite())
    {
        for (int i = 0; i < childItemsCount(); ++i)
        {
            if (auto child = childItem(i)->cast<InstalledDocset>())
            {
                child->execQuery(query, priority, results, maxResults, indexCount);
            }
        }

        return;
    }

    QueryResultItem item(priority);
    for (auto indexItem : qAsConst(m_index))
    {
        ++indexCount;
        if (item.hasMatch(indexItem, query.text()))
        {
            insertWithOrder(results, item, maxResults);
        }
    }
}

bool
InstalledDocset::testQuery(const Query& query) const
{
    if (isComposite())
    {
        for (int i = 0; i < childItemsCount(); ++i)
        {
            if (auto child = childItem(i)->cast<InstalledDocset>())
            {
                if (child->testQuery(query))
                {
                    return true;
                }
            }
        }

        return false;
    }

    for (auto indexItem : qAsConst(m_index))
    {
        if (indexItem->nameLowerCase() == query.text())
        {
            return true;
        }
    }

    return false;
}

bool
InstalledDocset::lessThan(const TreeItem* other, const QString& searchText) const
{
    Q_ASSERT(other);

    if (searchText.isEmpty())
    {
        if (auto otherDocset = other->cast<InstalledDocset>())
        {
            if (hasUpdate() && !otherDocset->hasUpdate())
            {
                return true;
            }

            if (!hasUpdate() && otherDocset->hasUpdate())
            {
                return false;
            }
        }
    }

    return Docset::lessThan(other, searchText);
}

int
storageFormat()
{
    // increase this with storage format changes
    constexpr int dataStorageFormat = 1;

    return dataStorageFormat * 1000 + DocsetItemType::version();
}

QByteArray
InstalledDocset::loadData(const QString& inFileName, const QString& sourceFilePath) const
{
    QFile file(QStringLiteral("%1/docset_data/%2").arg(path(), inFileName));
    if (!file.open(QIODevice::ReadOnly))
    {
        return {};
    }

    QDataStream stream(&file);

    int format;
    stream >> format;
    if (format != storageFormat())
    {
        return {};
    }

    QString id;
    stream >> id;
    if (id != this->id())
    {
        return {};
    }

    QDateTime lastModified;
    stream >> lastModified;
    if (lastModified != QFileInfo(sourceFilePath).lastModified())
    {
        return {};
    }

    QByteArray data;
    stream >> data;

    return qUncompress(data);
}

bool
InstalledDocset::saveData(const QString& outFileName, const QString& sourceFilePath, const QByteArray& data) const
{
    QDir(path()).mkdir(QStringLiteral("docset_data"));

    QFile file(QStringLiteral("%1/docset_data/%2").arg(path(), outFileName));
    if (!file.open(QIODevice::WriteOnly))
    {
        return false;
    }

    QDataStream stream(&file);

    stream << storageFormat();
    stream << id();
    stream << QFileInfo(sourceFilePath).lastModified();
    stream << qCompress(data);

    return true;
}

bool
InstalledDocset::loadBinToc(ContentItem* tocRoot, const QString& sourceFilePath, const QString& inFileName) const
{
    const auto data = loadData(inFileName, sourceFilePath);
    if (data.isEmpty())
    {
        return false;
    }

    QDataStream stream(data);

    TreeItem* parent  = tocRoot;
    TreeItem* current = nullptr;
    QStack<int> stack;

    QString name;
    QString href;
    int type;
    int depth;

    stack.push(0);
    while (!stream.atEnd())
    {
        stream >> depth;
        stream >> name;
        stream >> href;
        stream >> type;

        if (depth > stack.top())
        {
            parent = current;
            stack.push(depth);
        }
        else if (depth < stack.top())
        {
            while (stack.top() != depth)
            {
                current = current->parentItem();
                parent = current->parentItem();
                stack.pop();
            }
        }

        current = new ContentItem(this, parent, name, href, (ContentItem::Type)type);
    }

    tocRoot->fixSingleChild();
    return true;
}

bool
InstalledDocset::saveBinToc(const ContentItem* tocRoot, const QString& sourceFilePath, const QString& outFileName) const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);

    QStack<const ContentItem*> parentStack;
    QStack<int> childIndexStack;

    parentStack.push(tocRoot);
    childIndexStack.push(0);

    while (!parentStack.isEmpty())
    {
        auto current = parentStack.top();
        auto childIndex = childIndexStack.pop();

        if (childIndex == 0)
        {
            stream << parentStack.size() - 1;
            stream << current->name();
            stream << current->href();
            stream << (int)current->type();
        }

        if (childIndex < current->childItemsCount())
        {
            childIndexStack.push(childIndex + 1);

            parentStack.push(current->childItem(childIndex)->cast<ContentItem>());
            childIndexStack.push(0);
        }
        else
        {
            parentStack.pop();
        }
    }

    return saveData(outFileName, sourceFilePath, data);
}

bool
InstalledDocset::loadBinIndex(IndexVector& index, const QString& sourceFilePath, const QString& inFileName)
{
    const auto data = loadData(inFileName, sourceFilePath);
    if (data.isEmpty())
    {
        return false;
    }

    QDataStream stream(data);

    QString name;
    QString href;
    int type;

    int count;
    stream >> count;

    for (int i = 0 ; i < count; ++i)
    {
        stream >> name;
        stream >> href;
        stream >> type;

        index += new IndexItem(this, name, href, (IndexItem::Type)type);
    }

    return true;
}

bool
InstalledDocset::saveBinIndex(const IndexVector& index, const QString& sourceFilePath, const QString& outFileName) const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);

    stream << index.size();

    for (const auto& indexItem : index)
    {
        stream << indexItem->name();
        stream << indexItem->href();
        stream << (int)indexItem->type();
    }

    return saveData(outFileName, sourceFilePath, data);
}

}
