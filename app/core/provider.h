/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "tree_item.h"

namespace KDevDocs
{

class InstalledDocset;
class Source;

class Provider : public TreeObjectItem
{
    Q_OBJECT
    Q_DISABLE_COPY(Provider)

public:
    ~Provider() override;

    QString
    urlScheme() const;

    QString
    docsetsPath() const;

    QString
    feedPath(const QString& name) const;

    InstalledDocset*
    addDocset(const QString& path, InstalledDocset* parent = nullptr);

    void
    removeDocset(InstalledDocset* docset);

    void
    updateSources();

    void
    init();

protected:
    explicit
    Provider(const QString& urlScheme);

    virtual InstalledDocset*
    createDocset(const QString& path, InstalledDocset* parent = nullptr) = 0;

protected:
    QList<Source*> m_sources;

private:
    QString m_urlScheme;

    QString m_feedsPath;
    QString m_docsetsPath;

    QList<InstalledDocset*> m_docsets;
};

}
