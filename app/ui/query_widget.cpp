/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "query_widget.h"

#include "core.h"
#include "config/docset_config_dialog.h"
#include "installed_docset.h"
#include "query_edit.h"
#include "user_interface_config.h"

#include <KStandardAction>
#include <KLocalizedString>

#include <QMenu>
#include <QPainter>
#include <QStackedLayout>
#include <QTreeView>
#include <QListView>

namespace KDevDocs
{

QueryWidget::QueryWidget(QWidget* parent)
    : QWidget(parent)
    , m_contentsView(new QTreeView)
    , m_resultsView(new QListView)
    , m_sourcesView(new QListView)
    , m_queryEdit(new QueryEdit(m_contentsView, m_resultsView, m_sourcesView))
    , m_editWidget(new QWidget)
    , m_viewsLayout(new QStackedLayout)
    , m_contextDocset(nullptr)
{
    auto editLayout = new QVBoxLayout(m_editWidget);
    editLayout->addWidget(m_queryEdit);
    editLayout->setMargin(6);

    auto mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(m_editWidget);
    mainLayout->addLayout(m_viewsLayout);
    mainLayout->setSpacing(0);

    m_viewsLayout->addWidget(m_contentsView);
    m_viewsLayout->addWidget(m_resultsView);
    m_viewsLayout->addWidget(m_sourcesView);
    m_viewsLayout->setCurrentIndex(0);

    setFocusProxy(m_queryEdit);
    connect(m_queryEdit, &QueryEdit::viewActivated, m_viewsLayout, &QStackedLayout::setCurrentIndex);
    connect(m_queryEdit, &QueryEdit::urlActivated, this, &QueryWidget::urlToLoad);

    auto configureAction = KStandardAction::preferences(nullptr, nullptr, this);
    configureAction->setText(i18n("Configure"));
    connect(configureAction, &QAction::triggered, this, [this]()
    {
        if (m_contextDocset)
        {
            auto configPage = new DocsetConfigDialog (m_contextDocset, this);
            configPage->show();
        }
    });

    auto contextMenu = new QMenu(m_contentsView);
    contextMenu->addAction(configureAction);

    m_contentsView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_contentsView, &QTreeView::customContextMenuRequested, this, [this, contextMenu](QPoint point)
    {
        auto index = m_contentsView->indexAt(point);
        if ((m_contextDocset = index.data(InstalledDocsetRole).value<InstalledDocset*>()))
        {
            contextMenu->exec(m_contentsView->mapToGlobal(point));
        }
    });

    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::layoutChanged, this, &QueryWidget::updateLayout);
    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::paletteChanged, this, &QueryWidget::updatePalette);

    updateLayout();
    updatePalette();
}

void
QueryWidget::updateLayout()
{
    if (UserInterfaceConfig::isZealLayout())
    {
        layout()->setContentsMargins(1, 0, 0, 1);

        m_editWidget->setMinimumHeight(UserInterfaceConfig::panelHeight());
        m_editWidget->setMaximumHeight(UserInterfaceConfig::panelHeight());
    }
    else
    {
        layout()->setContentsMargins(0, 0, 0, 0);

        m_editWidget->setMinimumHeight(0);
        m_editWidget->setMaximumHeight(QWIDGETSIZE_MAX);
    }
}

void
QueryWidget::updatePalette()
{
    QPalette palette;
    if (UserInterfaceConfig::isDashLayout())
    {
        palette.setColor(QPalette::Base, palette.color(QPalette::Window));
    }

    m_contentsView->setPalette(palette);
    m_resultsView->setPalette(palette);
    m_sourcesView->setPalette(palette);
}

void
QueryWidget::goUp()
{
    m_queryEdit->goUp();
}

void
QueryWidget::query(const QString& queryString)
{
    m_queryEdit->query(queryString);
}

void
QueryWidget::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);

    if (UserInterfaceConfig::isZealLayout())
    {
        int x0 = 0;
        int x1 = width() - 1;
        int y0 = m_viewsLayout->currentWidget()->y() - 1;
        int y1 = height() - 1;

        QPainter painter(this);
        painter.setPen(UserInterfaceConfig::webBorderColor());

        painter.drawLine(x0, y0, x1, y0);
        painter.drawLine(x0, y0, x0, y1);
        painter.drawLine(x0, y1, x1, y1);
    }
}

}
