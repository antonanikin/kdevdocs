/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "general_config_page.h"
#include "general_config.h"

#include "key_sequence_widget.h"

#include <KLocalizedString>

#include <QCheckBox>
#include <QFormLayout>
#include <QGroupBox>

namespace KDevDocs
{

GeneralConfigPage::GeneralConfigPage(QWidget* parent)
    : ConfigPage(parent, GeneralConfig::self())
{
    auto mainLayout = new QVBoxLayout(this);

    // Startup ================================================================

    auto startupGroupBox = new QGroupBox;
    startupGroupBox->setTitle(i18n("Startup"));

    auto kcfg_startMinimized = new QCheckBox;
    kcfg_startMinimized->setObjectName(QStringLiteral("kcfg_startMinimized"));
    kcfg_startMinimized->setText(i18n("Start minimized"));

    auto kcfg_checkForUpdates = new QCheckBox;
    kcfg_checkForUpdates->setObjectName(QStringLiteral("kcfg_checkForUpdates"));
    kcfg_checkForUpdates->setText(i18n("Check for updates"));
    kcfg_checkForUpdates->setEnabled(false); // FIXME not implied yet

    auto startupGroupBoxLayout = new QVBoxLayout(startupGroupBox);
    startupGroupBoxLayout->addWidget(kcfg_startMinimized);
    startupGroupBoxLayout->addWidget(kcfg_checkForUpdates);

    mainLayout->addWidget(startupGroupBox);

    // System Tray ============================================================

    auto trayGroupBox = new QGroupBox;
    trayGroupBox->setTitle(i18n("System Tray"));

    auto kcfg_showTray = new QCheckBox;
    kcfg_showTray->setObjectName(QStringLiteral("kcfg_showTray"));
    kcfg_showTray->setText(i18n("Show system tray icon"));

    auto kcfg_minimizeToTray = new QCheckBox;
    kcfg_minimizeToTray->setObjectName(QStringLiteral("kcfg_minimizeToTray"));
    kcfg_minimizeToTray->setText(i18n("Minimize to system tray instead of taskbar"));
    connect(kcfg_showTray, &QCheckBox::toggled, kcfg_minimizeToTray, &QCheckBox::setEnabled);

    auto kcfg_hideToTray = new QCheckBox;
    kcfg_hideToTray->setObjectName(QStringLiteral("kcfg_closeToTray"));
    kcfg_hideToTray->setText(i18n("Hide to system tray on window close"));
    connect(kcfg_showTray, &QCheckBox::toggled, kcfg_hideToTray, &QCheckBox::setEnabled);

    auto trayGroupBoxLayout = new QVBoxLayout(trayGroupBox);
    trayGroupBoxLayout->addWidget(kcfg_showTray);
    trayGroupBoxLayout->addWidget(kcfg_minimizeToTray);
    trayGroupBoxLayout->addWidget(kcfg_hideToTray);

    mainLayout->addWidget(trayGroupBox);


    // Global Shortcuts =======================================================

    auto shortcutsGroupBox = new QGroupBox;
    shortcutsGroupBox->setTitle(i18n("Global Shortcuts"));

    auto kcfg_hideRestoreShortcut = new KeySequenceWidget;
    kcfg_hideRestoreShortcut->setObjectName(QStringLiteral("kcfg_windowToggleShortcut"));
    kcfg_hideRestoreShortcut->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    kcfg_hideRestoreShortcut->setCheckForConflictsAgainst(KKeySequenceWidget::None); // FIXME ?

    auto shortcutsGroupBoxLayout = new QFormLayout(shortcutsGroupBox);
    shortcutsGroupBoxLayout->addRow(i18n("Minimize/Show KDevDocs:"), kcfg_hideRestoreShortcut);

    mainLayout->addWidget(shortcutsGroupBox);
    mainLayout->addStretch();
}

GeneralConfigPage::~GeneralConfigPage()
{
}

QString
GeneralConfigPage::name() const
{
    return i18n("General");
}

QIcon
GeneralConfigPage::icon() const
{
    return QIcon::fromTheme(QStringLiteral("settings-configure"));
}

}
