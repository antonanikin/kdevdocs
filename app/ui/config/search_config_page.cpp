/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "search_config_page.h"
#include "search_config.h"

#include <KColorButton>
#include <KFontRequester>
#include <KLocalizedString>

#include <QCheckBox>
#include <QFormLayout>
#include <QGroupBox>
#include <QSpinBox>

namespace KDevDocs
{

SearchConfigPage::SearchConfigPage(QWidget* parent)
    : ConfigPage(parent, SearchConfig::self())
{
    auto mainLayout = new QVBoxLayout(this);

    // Font ===================================================================

    auto fontGroupBox = new QGroupBox;
    fontGroupBox->setTitle(i18n("Font"));

    auto kcfg_useFixedFont = new QCheckBox;
    kcfg_useFixedFont->setObjectName(QStringLiteral("kcfg_useFixedFont"));
    kcfg_useFixedFont->setText(i18n("Use fixed font to display query result"));

    auto kcfg_fixedFont = new KFontRequester;
    kcfg_fixedFont->setObjectName(QStringLiteral("kcfg_fixedFont"));
    kcfg_fixedFont->setFont(SearchConfig::fixedFont(), true);

    auto fontGroupBoxLayout = new QVBoxLayout(fontGroupBox);
    fontGroupBoxLayout->addWidget(kcfg_useFixedFont);
    fontGroupBoxLayout->addWidget(kcfg_fixedFont);

    mainLayout->addWidget(fontGroupBox);

    // Highlight ==============================================================

    auto highlightGroupBox = new QGroupBox;
    highlightGroupBox->setTitle(i18n("Highlight"));

    auto kcfg_highlightTextColor = new KColorButton;
    kcfg_highlightTextColor->setObjectName(QStringLiteral("kcfg_highlightTextColor"));
    kcfg_highlightTextColor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    auto kcfg_highlightBackroundColor = new KColorButton;
    kcfg_highlightBackroundColor->setObjectName(QStringLiteral("kcfg_highlightBackroundColor"));
    kcfg_highlightBackroundColor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    auto kcfg_highlightBackroundErrorColor = new KColorButton;
    kcfg_highlightBackroundErrorColor->setObjectName(QStringLiteral("kcfg_highlightBackroundErrorColor"));
    kcfg_highlightBackroundErrorColor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    auto highlightGroupBoxLayout = new QFormLayout(highlightGroupBox);
    highlightGroupBoxLayout->addRow(i18n("Text color:"), kcfg_highlightTextColor);
    highlightGroupBoxLayout->addRow(i18n("Background color:"), kcfg_highlightBackroundColor);
    highlightGroupBoxLayout->addRow(i18n("Background error color:"), kcfg_highlightBackroundErrorColor);

    mainLayout->addWidget(highlightGroupBox);

    // Performance ============================================================

    auto performanceGroupBox = new QGroupBox;
    performanceGroupBox->setTitle(i18n("Performance"));

    auto kcfg_resultsLimit = new QSpinBox;
    kcfg_resultsLimit->setObjectName(QStringLiteral("kcfg_resultsLimit"));
    kcfg_resultsLimit->setMinimum(100); // FIXME move this setup into .kfg file ?
    kcfg_resultsLimit->setMaximum(5000);
    kcfg_resultsLimit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    auto kcfg_searchDelay = new QSpinBox;
    kcfg_searchDelay->setObjectName(QStringLiteral("kcfg_searchDelay"));
    kcfg_searchDelay->setMaximum(1000);
    kcfg_searchDelay->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    auto kcfg_firstResultOpenDelay = new QSpinBox;
    kcfg_firstResultOpenDelay->setObjectName(QStringLiteral("kcfg_firstResultOpenDelay"));
    kcfg_firstResultOpenDelay->setMaximum(1000);
    kcfg_firstResultOpenDelay->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    auto performanceGroupBoxLayout = new QFormLayout(performanceGroupBox);
    performanceGroupBoxLayout->addRow(i18n("Results limit:"), kcfg_resultsLimit);
    performanceGroupBoxLayout->addRow(i18n("Search start delay (ms.):"), kcfg_searchDelay);
    performanceGroupBoxLayout->addRow(i18n("First result display delay (ms.):"), kcfg_firstResultOpenDelay);

    mainLayout->addWidget(performanceGroupBox);
    mainLayout->addStretch();
}

SearchConfigPage::~SearchConfigPage()
{
}

QString
SearchConfigPage::name() const
{
    return i18n("Search");
}

QIcon
SearchConfigPage::icon() const
{
    return QIcon::fromTheme(QStringLiteral("edit-find"));
}

}
