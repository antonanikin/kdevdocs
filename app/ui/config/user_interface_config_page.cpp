/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "user_interface_config_page.h"
#include "user_interface_config.h"

#include "navigation_title_label.h"

#include <KLocalizedString>
#include <KMessageWidget>

#include <QComboBox>
#include <QFormLayout>

namespace KDevDocs
{

UserInterfaceConfigPage::UserInterfaceConfigPage(QWidget* parent)
    : ConfigPage(parent, UserInterfaceConfig::self())
{
    auto formLayout = new QFormLayout;

    auto kcfg_mainWindowLayout = new QComboBox;
    kcfg_mainWindowLayout->setObjectName(QStringLiteral("kcfg_layout"));
    kcfg_mainWindowLayout->addItem(i18n("Dash-style"));
    kcfg_mainWindowLayout->addItem(i18n("Zeal-style"));
    formLayout->addRow(i18n("Main window layout:"), kcfg_mainWindowLayout);

    auto kcfg_truncationMode = new QComboBox;
    kcfg_truncationMode->setObjectName(QStringLiteral("kcfg_truncationMode"));
    kcfg_truncationMode->addItem(i18n("Truncate Right Part"));
    kcfg_truncationMode->addItem(i18n("Truncate Left Part"));
    kcfg_truncationMode->addItem(i18n("Truncate Middle Part"));

    static const auto longTitle = QStringLiteral("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890");
    auto longTitleLabel = new NavigationTitleLabel(longTitle);
    connect(kcfg_truncationMode, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, [kcfg_truncationMode, longTitleLabel]()
            {
                const auto mode = (TruncationMode)kcfg_truncationMode->currentIndex();
                longTitleLabel->setTruncationMode(mode);
            });

    auto truncationLayout = new QHBoxLayout;
    truncationLayout->setContentsMargins(0, 0, 0, 0);
    truncationLayout->addWidget(kcfg_truncationMode);
    truncationLayout->addWidget(longTitleLabel);

    formLayout->addRow(i18n("Truncation mode:"), truncationLayout);

    m_darkModeBox = new QComboBox;
    m_darkModeBox->setObjectName(QStringLiteral("kcfg_darkMode"));
    m_darkModeBox->addItem(i18n("Auto Detect"));
    m_darkModeBox->addItem(i18n("Force Enable"));
    m_darkModeBox->addItem(i18n("Force Disable"));
    formLayout->addRow(i18n("Dark mode:"), m_darkModeBox);

    connect(m_darkModeBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &UserInterfaceConfigPage::checkDarkModeSettings);

    m_darkModeMessage = new KMessageWidget;
    m_darkModeMessage->setMessageType(KMessageWidget::Warning);
    m_darkModeMessage->setCloseButtonVisible(false);

    auto mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(formLayout);
    mainLayout->addWidget(m_darkModeMessage);
    mainLayout->addStretch();

    checkDarkModeSettings();
}

void
UserInterfaceConfigPage::checkDarkModeSettings()
{
    int selectedMode = m_darkModeBox->currentIndex();
    bool isDarkTheme = UserInterfaceConfig::isDarkTheme();

    if (selectedMode == UserInterfaceConfig::DarkModeEnabled && !isDarkTheme)
    {
        m_darkModeMessage->setText(i18n("Dark mode is selected, but light theme is detected."));
        m_darkModeMessage->setVisible(true);
    }
    else if (selectedMode == UserInterfaceConfig::DarkModeDisabled && isDarkTheme)
    {
        m_darkModeMessage->setText(i18n("Light mode is selected, but dark theme is detected"));
        m_darkModeMessage->setVisible(true);
    }
    else
    {
        m_darkModeMessage->setVisible(false);
    }
}

QString
UserInterfaceConfigPage::name() const
{
    return i18n("User Interface");
}

QIcon
UserInterfaceConfigPage::icon() const
{
    return QIcon::fromTheme(QStringLiteral("preferences-desktop-theme"));
}

}
