/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "docsets_filtered_model.h"

#include "globals.h"
#include "docset.h"

namespace KDevDocs
{

DocsetsFilteredModel::DocsetsFilteredModel(QAbstractItemModel* sourceModel, QObject* parent)
    : QSortFilterProxyModel(parent)
{
    setSourceModel(sourceModel);
    sort(0);
}

DocsetsFilteredModel::~DocsetsFilteredModel() = default;

void
DocsetsFilteredModel::setFilter(const QString& text)
{
    m_filter = text;
    invalidate();
}

int
matchedChilds(const QModelIndex& parent, const QString& searchText)
{
    int count = 0;

    auto model = parent.model();
    const int rowCount = model->rowCount(parent);
    for (int i = 0; i < rowCount; ++i)
    {
        count += matchedChilds(model->index(i, 0, parent), searchText);
    }

    // find name match only for docset items
    if (auto docset = parent.data(TreeItemRole).value<TreeItem*>()->cast<Docset>())
    {
        if (docset->name().contains(searchText, Qt::CaseInsensitive))
        {
            ++count;
        }
    }

    return count;
}

bool
DocsetsFilteredModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    if (m_filter.isEmpty())
    {
        return true;
    }

    return matchedChilds(sourceModel()->index(source_row, 0, source_parent), m_filter);
}

bool
DocsetsFilteredModel::lessThan(const QModelIndex& left, const QModelIndex& right) const
{
    const auto leftItem = left.data(TreeItemRole).value<TreeItem*>();
    const auto rightItem = right.data(TreeItemRole).value<TreeItem*>();

    return leftItem->lessThan(rightItem, m_filter);
}

}
