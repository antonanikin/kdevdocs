/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "config_dialog.h"

#include "config_page.h"

#include <KConfigDialogManager>
#include <KLocalizedString>

#include <QCloseEvent>
#include <QMessageBox>
#include <QPushButton>

namespace KDevDocs
{

ConfigPage*
configPage(KPageWidgetItem* item)
{
    return item ? qobject_cast<ConfigPage*>(item->widget()) : nullptr;
}

ConfigDialog::ConfigDialog(QWidget* parent)
    : KPageDialog(parent)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setFaceType(KPageDialog::List);

    setStandardButtons(
        QDialogButtonBox::Ok |
        QDialogButtonBox::Cancel |
        QDialogButtonBox::Apply |
        QDialogButtonBox::Reset |
        QDialogButtonBox::RestoreDefaults);

    connect(button(QDialogButtonBox::Ok), &QPushButton::clicked, this, &ConfigDialog::apply);
    connect(button(QDialogButtonBox::Apply), &QPushButton::clicked, this, &ConfigDialog::apply);
    connect(button(QDialogButtonBox::RestoreDefaults), &QPushButton::clicked, this, &ConfigDialog::defaults);
    connect(button(QDialogButtonBox::Reset), &QPushButton::clicked, this, &ConfigDialog::reset);

    connect(this, &KPageDialog::currentPageChanged, this, &ConfigDialog::checkForUnsavedChanges);

    setModal(true);
    resize(1000, 500); // FIXME

    updateButtons();
    setWindowTitle(i18n("Configure"));
}

ConfigDialog::~ConfigDialog()
{
}

KPageWidgetItem*
ConfigDialog::addPage(ConfigPage* page)
{
    Q_ASSERT(page);
    if (page->configSkeleton())
    {
        // We should create manager before KPageDialog::addPage() call which emits
        // currentPageChanged() signal for first added page. Therefore if no manager exists
        // for our page then updateButtons() will made wrong settings.
        auto manager = new KConfigDialogManager(page, page->configSkeleton());
        connect(manager, &KConfigDialogManager::widgetModified, this, &ConfigDialog::updateButtons);
        m_managers[page] = manager;
    }

    auto item = KPageDialog::addPage(page, page->name());
    item->setHeader(page->fullName());
    item->setIcon(page->icon());

    return item;
}

bool
ConfigDialog::checkForUnsavedChanges(KPageWidgetItem* current, KPageWidgetItem* before)
{
    updateButtons();

    if (!before || !hasChanged(before))
    {
        return true;
    }

    setCurrentPage(before);

    auto button = QMessageBox::warning(
        this,
        i18n("Apply Settings"),
        i18n("The settings of the current module have changed."
                "Do you want to apply changes or discard them?"),
        QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel,
        QMessageBox::Save);

    if (button == QMessageBox::Save)
    {
        apply();
    }
    else if (button == QMessageBox::Discard)
    {
        reset();
    }
    else
    {
        return false;
    }

    setCurrentPage(current);
    return true;
}

bool
ConfigDialog::hasChanged(KPageWidgetItem* page) const
{
    auto m = manager(page);
    return m ? m->hasChanged() : false;
}

bool
ConfigDialog::isDefault(KPageWidgetItem* page) const
{
    auto m = manager(page);
    return m ? m->isDefault() : true;
}

void
ConfigDialog::apply()
{
    if (auto m = manager(currentPage()))
    {
        m->updateSettings();
        updateButtons();
    }
}

void
ConfigDialog::defaults()
{
    if (auto m = manager(currentPage()))
    {
        m->updateWidgetsDefault();
    }
}

void
ConfigDialog::reset()
{
    if (auto m = manager(currentPage()))
    {
        m->updateWidgets();
    }
}

void
ConfigDialog::updateButtons()
{
    const bool changed = currentPage() && hasChanged(currentPage());
    button(QDialogButtonBox::Apply)->setEnabled(changed);
    button(QDialogButtonBox::Reset)->setEnabled(changed);

    if (auto page = configPage(currentPage()))
    {
        page->setChanged(changed);
    }

    button(QDialogButtonBox::RestoreDefaults)->setEnabled(currentPage() && !isDefault(currentPage()));
}

void
ConfigDialog::closeEvent(QCloseEvent* event)
{
    if (checkForUnsavedChanges(currentPage(), currentPage()))
    {
        event->accept();
    }
    else
    {
        event->ignore();
    }
}

KConfigDialogManager*
ConfigDialog::manager(KPageWidgetItem* page) const
{
    return page ? m_managers.value(page->widget(), nullptr) : nullptr;
}

}
