/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "config_page.h"

#include <KCoreConfigSkeleton>
#include <KLocalizedString>

namespace KDevDocs
{

ConfigPage::ConfigPage(QWidget* parent, KCoreConfigSkeleton* config)
    : QWidget(parent)
    , m_config(config)
{
    Q_ASSERT(parent);
}

ConfigPage::~ConfigPage()
{
}

QString ConfigPage::fullName() const
{
    return i18n("%1 Settings", name());
}

KCoreConfigSkeleton*
ConfigPage::configSkeleton() const
{
    return m_config;
}

void
ConfigPage::setChanged(bool /*changed*/)
{
}

}
