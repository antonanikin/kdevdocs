/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <KPageDialog>

class KConfigDialogManager;

namespace KDevDocs
{

class ConfigPage;

class ConfigDialog : public KPageDialog
{
    Q_OBJECT

public:
    ~ConfigDialog() override;

protected:
    explicit
    ConfigDialog(QWidget* parent);

    KPageWidgetItem*
    addPage(ConfigPage* page);

    bool
    checkForUnsavedChanges(KPageWidgetItem* current, KPageWidgetItem* before);

    bool
    hasChanged(KPageWidgetItem* page) const;

    bool
    isDefault(KPageWidgetItem* page) const;

    void
    apply();

    void
    defaults();

    void
    reset();

    void
    updateButtons();

    void
    closeEvent(QCloseEvent* event) override;

    KConfigDialogManager*
    manager(KPageWidgetItem* page) const;

    QHash<QWidget*, KConfigDialogManager*> m_managers;
};

}
