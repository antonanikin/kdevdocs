/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "docset_delegate.h"

#include "globals.h"
#include "icon_manager.h"
#include "installed_docset.h"
#include "available_docset.h"
#include "../qtcompat.h"

#include <KLocalizedString>

#include <QAbstractItemView>
#include <QHoverEvent>
#include <QPainter>

namespace KDevDocs
{

DocsetDelegate::DocsetDelegate(QAbstractItemView* view)
    : SearchDelegate(view)
    , m_view(view)
    , m_isButtonHovered(false)
{
    Q_ASSERT(view);
    view->installEventFilter(this);
    view->setMouseTracking(true);
    view->setItemDelegate(this);

    connect(view, &QAbstractItemView::clicked, this, &DocsetDelegate::clicked);
}

DocsetDelegate::~DocsetDelegate() = default;

AvailableDocset*
DocsetDelegate::availableDocset(const QModelIndex& index) const
{
    auto item = index.data(TreeItemRole).value<TreeItem*>();
    Q_ASSERT(item);

    if (auto docset = item->cast<AvailableDocset>())
    {
        return docset;
    }
    else if (auto docset = item->cast<InstalledDocset>())
    {
        return docset->available();
    }

    return nullptr;
}

void
DocsetDelegate::clicked(const QModelIndex& index)
{
    if (!m_isButtonHovered)
    {
        return;
    }

    if (const auto docset = availableDocset(index))
    {
        if (docset->state() == AvailableDocset::DOWNLOADING)
        {
            docset->breakInstallation();
        }
        else
        {
            docset->install();
        }
    }
}

bool
DocsetDelegate::eventFilter(QObject* obj, QEvent* event)
{
    if (obj == m_view)
    {
        if (event->type() == QEvent::HoverEnter ||
            event->type() == QEvent::HoverLeave ||
            event->type() == QEvent::HoverMove)
        {
            auto hoverEvent = static_cast<QHoverEvent*>(event);
            m_mousePos = hoverEvent->pos();
            m_hoveredIndex = m_view->indexAt(m_mousePos);
            m_view->update(m_hoveredIndex);
        }
    }

    return QObject::eventFilter(obj, event);
}

void
DocsetDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    SearchDelegate::paint(painter, option, index);

    const auto docset = availableDocset(index);
    if (!docset)
    {
        return;
    }

    if (docset->state() == AvailableDocset::NOT_INSTALLED)
    {
        drawTextButton(i18n("Install"), painter, option, index);
    }
    else if (docset->state() == AvailableDocset::INSTALLED)
    {
        if (docset->installed()->hasUpdate())
        {
            drawTextButton(i18n("Update"), painter, option, index);
        }
    }
    else if (docset->state() == AvailableDocset::DOWNLOADING)
    {
        int right = drawIconButton(QIcon::fromTheme(QStringLiteral("tab-close")), painter, option, index);
        drawProgressBar(right, 100, docset->downloadingProgress(), painter, option);
    }
    else if (docset->state() == AvailableDocset::EXTRACTING)
    {
        int right = drawLoadingIcon(painter, option);
        drawStateText(right, i18n("Extracting..."), painter, option);
    }
    else if (docset->state() == AvailableDocset::INSTALLING)
    {
        int right = drawLoadingIcon(painter, option);
        drawStateText(right, i18n("Installing..."), painter, option);
    }
}

int
DocsetDelegate::drawLoadingIcon(
    QPainter* painter,
    const QStyleOptionViewItem& option) const
{
    auto iconRect = option.rect;
    iconRect.setLeft(iconRect.right() - iconRect.height());

    auto pixmap = IconManager::self()->loadingIcon().pixmap(option.decorationSize);
    const int pixmapMargin = (iconRect.height() - pixmap.height()) / 2;
    painter->drawPixmap(iconRect.left() + pixmapMargin,
                        iconRect.top() + pixmapMargin,
                        pixmap);

    return iconRect.left();
}

void
DocsetDelegate::drawStateText(
    int right,
    const QString& text,
    QPainter* painter,
    const QStyleOptionViewItem& option) const
{
    auto font = painter->font();
    font.setItalic(true);
    painter->setFont(font);

    const int textWidth = horizontalAdvance(painter->fontMetrics(), text);

    auto textRect = option.rect;
    textRect.setRight(right);
    textRect.setLeft(textRect.right() - textWidth);

    QTextOption textOption(Qt::AlignHCenter | Qt::AlignVCenter);
    textOption.setTextDirection(painter->layoutDirection());
    textOption.setWrapMode(QTextOption::NoWrap);

    painter->setPen(textColor(option));
    painter->drawText(textRect, text, textOption);
}

void
DocsetDelegate::drawTextButton(
    const QString& text,
    QPainter* painter,
    const QStyleOptionViewItem& option,
    const QModelIndex& index) const
{
    const int buttonHeight = qMax(painter->fontMetrics().height(), option.decorationSize.height());
    const int buttonMargin = (option.rect.height() - buttonHeight) / 2;

    const int textWidth = horizontalAdvance(painter->fontMetrics(), text);

    auto buttonRect = option.rect.adjusted(0, buttonMargin, -buttonMargin, -buttonMargin);
    buttonRect.setLeft(buttonRect.right() - textWidth - buttonHeight);

    if (buttonRect.contains(m_mousePos))
    {
        m_isButtonHovered = true;
    }
    else if (m_hoveredIndex == index)
    {
        m_isButtonHovered = false;
    }

    painter->setPen(buttonBorderColor(option, index, true));
    painter->setBrush(Qt::transparent);
    painter->drawRect(buttonRect);

    QTextOption textOption(Qt::AlignHCenter | Qt::AlignVCenter);
    textOption.setTextDirection(painter->layoutDirection());
    textOption.setWrapMode(QTextOption::NoWrap);

    painter->setPen(textColor(option));
    painter->drawText(buttonRect, text, textOption);
}

int
DocsetDelegate::drawIconButton(
    const QIcon& icon,
    QPainter* painter,
    const QStyleOptionViewItem& option,
    const QModelIndex& index) const
{
    const int buttonHeight = qMax(painter->fontMetrics().height(), option.decorationSize.height());
    const int buttonMargin = (option.rect.height() - buttonHeight) / 2;

    auto iconButtonRect = option.rect.adjusted(0, buttonMargin, -buttonMargin, -buttonMargin);
    iconButtonRect.setLeft(iconButtonRect.right() - buttonHeight + 1);

    if (iconButtonRect.contains(m_mousePos))
    {
        m_isButtonHovered = true;
    }
    else if (m_hoveredIndex == index)
    {
        m_isButtonHovered = false;
    }

    painter->setPen(buttonBorderColor(option, index, false));
    painter->setBrush(Qt::transparent);
    painter->drawRect(iconButtonRect);

    auto pixmap = icon.pixmap(option.decorationSize);
    const int pixmapMargin = (iconButtonRect.height() - pixmap.height()) / 2;
    painter->drawPixmap(iconButtonRect.left() + pixmapMargin,
                        iconButtonRect.top() + pixmapMargin,
                        pixmap);

    return iconButtonRect.left();
}

void
DocsetDelegate::drawProgressBar(
    int right,
    int width,
    int progress,
    QPainter* painter,
    const QStyleOptionViewItem& option) const
{
    auto progressBarRect = option.rect;
    progressBarRect.setRight(right);
    progressBarRect.setLeft(right - width);

    QStyleOptionProgressBar progressBarOption;
    progressBarOption.state = option.state;
    progressBarOption.rect = progressBarRect;
    progressBarOption.textVisible = false;
    progressBarOption.minimum = 0;
    progressBarOption.maximum = 100;
    progressBarOption.progress = progress;

    option.widget->style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter);
}

QColor
DocsetDelegate::buttonBorderColor(const QStyleOptionViewItem& option,
                                           const QModelIndex& index,
                                           bool displayUnhovered) const
{
    auto colorGroup = option.state.testFlag(QStyle::State_Active) ? QPalette::Normal : QPalette::Inactive;
    if (m_isButtonHovered && (m_hoveredIndex == index))
    {
        if (option.state.testFlag(QStyle::State_Selected))
        {
            return option.palette.color(colorGroup, QPalette::HighlightedText);
        }
        else
        {
            return option.palette.color(colorGroup, QPalette::Text);
        }
    }
    else
    {
        if (displayUnhovered)
        {
            return option.palette.color(colorGroup, QPalette::Mid);
        }
        else
        {
            return Qt::transparent;
        }
    }
}

}
