/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "main_config_dialog.h"

#include "docsets_config_page.h"
#include "general_config_page.h"
#include "search_config_page.h"
#include "user_interface_config_page.h"
#include "web_config_page.h"

namespace KDevDocs
{

MainConfigDialog::MainConfigDialog(QWidget* parent)
    : ConfigDialog(parent)
{
    addPage(new GeneralConfigPage(this));
    addPage(new UserInterfaceConfigPage(this));
    addPage(new SearchConfigPage(this));
    addPage(new WebConfigPage(this));
    addPage(new DocsetsConfigPage(this));
}

MainConfigDialog::~MainConfigDialog()
{
}

}
