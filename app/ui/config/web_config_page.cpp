/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "web_config_page.h"
#include "ui_web_config_page.h"

#include "installed_docset.h"
#include "web_config.h"

#include <KLocalizedString>

namespace KDevDocs
{

WebConfigPage::WebConfigPage(QWidget* parent, InstalledDocset* docset)
    : ConfigPage(parent, WebConfig::config(docset))
{
    Ui::WebConfigPage ui;
    ui.setupUi(this);

    ui.overrideMessage->setVisible(!docset);
}

WebConfigPage::~WebConfigPage() = default;

QString
WebConfigPage::name() const
{
    return i18n("Web Settings");
}

QString
WebConfigPage::fullName() const
{
    return name();
}

QIcon
WebConfigPage::icon() const
{
    return QIcon::fromTheme(QStringLiteral("internet-web-browser"));
}

}
