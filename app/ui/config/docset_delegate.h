/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "search_delegate.h"

namespace KDevDocs
{

class AvailableDocset;

class DocsetDelegate : public SearchDelegate
{
    Q_OBJECT
public:
    explicit
    DocsetDelegate(QAbstractItemView* view);

    ~DocsetDelegate() override;

    void
    paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;

protected:
    bool
    eventFilter(QObject* obj, QEvent* event) override;

private:
    AvailableDocset*
    availableDocset(const QModelIndex& index) const;

    int
    drawLoadingIcon(QPainter* painter, const QStyleOptionViewItem& option) const;

    void
    drawStateText(int right, const QString& text, QPainter* painter, const QStyleOptionViewItem& option) const;

    void
    drawTextButton(const QString& text, QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;

    int
    drawIconButton(const QIcon& icon, QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;

    void
    drawProgressBar(int right, int width, int progress, QPainter* painter, const QStyleOptionViewItem& option) const;

    QColor
    buttonBorderColor(const QStyleOptionViewItem& option, const QModelIndex& index, bool displayUnhovered) const;

    void
    clicked(const QModelIndex& index);

private:
    QAbstractItemView* m_view;
    QPoint m_mousePos;

    QModelIndex m_hoveredIndex;
    mutable bool m_isButtonHovered;
};

}
