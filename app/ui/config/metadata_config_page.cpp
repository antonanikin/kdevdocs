/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "metadata_config_page.h"

#include "core.h"
#include "metadata_config.h"
#include "installed_docset.h"

#include <KLocalizedString>
#include <KMessageWidget>

#include <QFormLayout>
#include <QLineEdit>

namespace KDevDocs
{

MetadataConfigPage::MetadataConfigPage(QWidget* parent, InstalledDocset* docset)
    : ConfigPage(parent, docset->metadataConfig())
{
    auto mainLayout = new QFormLayout(this);

    auto nameEdit = new QLineEdit;
    nameEdit->setObjectName(QStringLiteral("kcfg_name"));
    mainLayout->addRow(i18n("Name:"), nameEdit);

    auto nameMessageWidget = new KMessageWidget;
    nameMessageWidget->setMessageType(KMessageWidget::Error);
    nameMessageWidget->setCloseButtonVisible(false);
    nameMessageWidget->setWordWrap(true);
    nameMessageWidget->setVisible(false);
    mainLayout->addWidget(nameMessageWidget);

    auto versionEdit = new QLineEdit;
    versionEdit->setObjectName(QStringLiteral("kcfg_version"));
    versionEdit->setReadOnly(true);
    mainLayout->addRow(i18n("Version:"), versionEdit);

    connect(nameEdit, &QLineEdit::textChanged,
            this, [docset, nameMessageWidget](const QString& text)
            {
                QString errorMessage;

                if (text.isEmpty())
                {
                    errorMessage = i18n("The name is empty.");
                }
                else
                {
                    auto existDocset = Core::self()->docset(text);
                    if (existDocset && existDocset != docset)
                    {
                        errorMessage = i18n("The docset with the chosen name already exists.");
                    }
                }

                nameMessageWidget->setText(errorMessage);
                nameMessageWidget->setVisible(!errorMessage.isEmpty());

                // FIXME add signal to block parent's "Ok"/"Apply" buttons
            });
}

MetadataConfigPage::~MetadataConfigPage() = default;

QString
MetadataConfigPage::name() const
{
    return i18n("Metadata");
}

QString
MetadataConfigPage::fullName() const
{
    return i18n("Configure Docset Metadata");
}

QIcon
MetadataConfigPage::icon() const
{
    return QIcon::fromTheme(QStringLiteral("settings-configure"));
}

}
