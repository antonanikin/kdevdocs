/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "docsets_config_page.h"
#include "ui_docsets_config_page.h"

#include "core.h"
#include "globals.h"
#include "docset_delegate.h"
#include "docsets_filtered_model.h"
#include "installed_docset.h"

#include <KLocalizedString>

#include <QMessageBox>

namespace KDevDocs
{

DocsetsConfigPage::DocsetsConfigPage(QWidget* parent)
    : ConfigPage(parent)
    , m_ui(new Ui::DocsetsConfigPage)
{
    m_ui->setupUi(this);

    setupView(m_ui->installedView, m_ui->installedFilterEdit, Core::self()->installedModel());
    setupView(m_ui->availableView, m_ui->availableFilterEdit, Core::self()->availableModel());

    connect(m_ui->installedAddButton, &QPushButton::clicked, this, [this]()
    {
        m_ui->tabWidget->setCurrentWidget(m_ui->availableTab);
        m_ui->availableFilterEdit->setFocus();
    });

    connect(m_ui->installedView->selectionModel(), &QItemSelectionModel::currentChanged,
            this, [this](const QModelIndex& current, const QModelIndex& /*previous*/)
    {
        const auto docset = current.data(InstalledDocsetRole).value<InstalledDocset*>();
        m_ui->installedRemoveButton->setEnabled(docset);
    });

    m_ui->installedRemoveButton->setEnabled(false);
    connect(m_ui->installedRemoveButton, &QPushButton::clicked, this, [this]()
    {
        const auto index = m_ui->installedView->selectionModel()->currentIndex();
        const auto docset = index.data(InstalledDocsetRole).value<InstalledDocset*>();
        Q_ASSERT(docset);

        const auto answer = QMessageBox::question(
            this,
            name(),
            i18n("Are you sure to remove docset \"%1\"?", docset->name()),
            QMessageBox::Yes | QMessageBox::No,
            QMessageBox::No);

        if (answer == QMessageBox::Yes)
        {
            docset->uninstall();
        }
    });

    // FIXME block button during update
    connect(m_ui->availableUpdateButton, &QPushButton::clicked, Core::self(), &Core::updateSources);

    m_ui->tabWidget->setCurrentWidget(m_ui->installedTab);
    m_ui->installedFilterEdit->setFocus();
}

DocsetsConfigPage::~DocsetsConfigPage() = default;

QString
DocsetsConfigPage::name() const
{
    return i18n("Documentation");
}

QIcon
DocsetsConfigPage::icon() const
{
    return QIcon::fromTheme(QStringLiteral("help-browser"));
}

void
DocsetsConfigPage::setupView(QTreeView* view, QLineEdit* filterEdit, QAbstractItemModel* sourceModel)
{
    Q_ASSERT(view);
    Q_ASSERT(filterEdit);
    Q_ASSERT(sourceModel);

    auto model = new DocsetsFilteredModel(sourceModel, this);
    view->setModel(model);
    view->header()->hide();

    auto delegate = new DocsetDelegate(view);

    connect(filterEdit, &QLineEdit::textChanged, this, [model, view, delegate](const QString& text)
    {
        delegate->setHighlight(text);
        model->setFilter(text);

        if (text.isEmpty())
        {
            view->collapseAll();
        }
        else
        {
            view->expandAll();
        }
    });
}

}
