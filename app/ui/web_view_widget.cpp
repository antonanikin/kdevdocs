/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "web_view_widget.h"

#include "action.h"
#include "icon_manager.h"
#include "user_interface_config.h"
#include "web_config.h"
#include "web_page.h"
#include "web_page_find_widget.h"
#include "web_view.h"

#include <KLocalizedString>
#include <KMessageWidget>

#include <QKeyEvent>
#include <QLabel>
#include <QTimer>
#include <QVBoxLayout>

namespace KDevDocs
{

enum WebViewState
{
    LoadStarted,
    loadingIconStarted,
    LoadFinished
};

WebViewWidget::WebViewWidget(const QVector<QAction*>& actions, QWidget* parent)
    : GoUpWidget(parent)
    , m_state(LoadFinished)
    , m_bufferEnabled(true)
    , m_buffer(new QLabel(this))
    , m_loadingTimer(new QTimer(this))
    , m_page(new WebPage(actions, this))
    , m_pageFindWidget(new WebPageFindWidget(actions, m_page, this))
    , m_view(new WebView(actions, m_page, this))
    , m_messageWidget(new KMessageWidget(this))
    , m_messageTimer(new QTimer(this))
{
    m_loadingTimer->setSingleShot(true);
    connect(m_loadingTimer, &QTimer::timeout, this, [this]()
    {
        m_state = loadingIconStarted;
        connect(IconManager::self(), &IconManager::loadingIconChanged, this, &WebViewWidget::iconChanged);
    });

    setFocusProxy(m_view);
    connect(m_view, &WebView::windowCreateRequested, this, &WebViewWidget::windowCreateRequested);

    connect(m_page, &WebPage::loadStarted, this, &WebViewWidget::onLoadStarted);
    connect(m_page, &WebPage::loadFinished, this, &WebViewWidget::onLoadFinished);

    connect(m_page, &WebPage::titleChanged, this, [this]()
    {
        emit titleChanged(m_page->title());
    });

    connect(m_page, &WebPage::iconChanged, this, [this]()
    {
        if (m_state != loadingIconStarted)
        {
            emit iconChanged(m_page->icon());
        }
    });

    connect(m_page, &WebPage::findFinished, this, [this, actions](WebPage::FindResult result)
    {
        if (result == WebPage::FindWrappedBackward || result == WebPage::FindWrappedForward)
        {
            showMessage(
                i18n("Search wrapped"),
                result == WebPage::FindWrappedBackward ?
                actions[FindPrev]->icon() :
                actions[FindNext]->icon());
        }
    });

    m_messageWidget->setCloseButtonVisible(false);
    m_messageWidget->setMessageType(KMessageWidget::Positive);
    m_messageWidget->lower();

    m_messageTimer->setSingleShot(true);
    connect(m_messageTimer, &QTimer::timeout, m_messageWidget, &KMessageWidget::lower);

    auto layout = new QVBoxLayout(this);
    layout->setSpacing(0);
    layout->addWidget(m_view);
    layout->addWidget(m_pageFindWidget);

    m_pageFindWidget->hide();
    setBufferEnabled(false);

    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::layoutChanged, this, &WebViewWidget::updateLayout);
    updateLayout();

    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::paletteChanged,
            this, static_cast<void(WebViewWidget::*)()>(&WebViewWidget::update));
}

WebViewWidget::~WebViewWidget() = default;

QUrl
WebViewWidget::url() const
{
    return m_page->url();
}

WebHistory*
WebViewWidget::history() const
{
    return m_page->history();
}

WebPageFindWidget*
WebViewWidget::findWidget() const
{
    return m_pageFindWidget;
}

void
WebViewWidget::updateLayout()
{
    if (UserInterfaceConfig::isZealLayout())
    {
        layout()->setContentsMargins(0, 0, 1, 1);
    }
    else
    {
        layout()->setContentsMargins(1, 0, 1, 1);
    }
}

void
WebViewWidget::load(const QUrl& url, bool focusView)
{
    if (url.isValid())
    {
        m_page->load(url);
        if (focusView)
        {
            setFocus();
        }
    }
}

void
WebViewWidget::setBufferEnabled(bool enabled)
{
    if (m_bufferEnabled == enabled)
    {
        return;
    }
    m_bufferEnabled = enabled;

    if (enabled)
    {
        QPixmap pixmap(m_view->size());
        m_view->render(&pixmap);

        m_buffer->setGeometry(m_view->geometry());
        m_buffer->setPixmap(pixmap);
        m_buffer->raise();
    }
    else
    {
        m_buffer->lower();

        // We should resize our buffer label to small (null) size after the lowering.
        // This fixes paintEvent() when we open find widget when buffer already filled with previous
        // view content. In such case buffer's size will be greater than new view's size so we
        // can't draw line over the buffer label.
        static const QPixmap empty;
        m_buffer->setPixmap(empty);
    }
}

void
WebViewWidget::onLoadStarted()
{
    m_state = LoadStarted;
    setBufferEnabled(true);
    m_loadingTimer->start(300); // FIXME add config param ?

    emit loadStarted();

    // Quick-and-dirty hack for pages which load takes too many time. Without force buffer
    // disabling we will see old page with loading icon until load finished/failed.
    // FIXME add config param for delay value ?
    // FIXME disable buffering for for external links?
    QTimer::singleShot(1000, this, [this]()
    {
        setBufferEnabled(false);
    });
}

void
WebViewWidget::onLoadFinished()
{
    m_loadingTimer->stop();
    IconManager::self()->disconnect(this);

    setBufferEnabled(false);
    m_state = LoadFinished;

    emit iconChanged(m_page->icon());
    emit loadFinished();
}

void
WebViewWidget::zoomIn()
{
    m_view->setZoomFactor(std::min(m_view->zoomFactor() + 0.1, 5.0));
}

void
WebViewWidget::zoomOut()
{
    m_view->setZoomFactor(std::max(m_view->zoomFactor() - 0.1, 0.1));
}

void
WebViewWidget::zoomReset()
{
    m_view->setZoomFactor(1.0);
}

bool
WebViewWidget::goUp()
{
    if (m_pageFindWidget->hasFocusedWidget())
    {
        if (m_pageFindWidget->goUp())
        {
            m_pageFindWidget->hide();
            update();
        }
        else
        {
            return false;
        }
    }

    if (hasFocus())
    {
        return true;
    }

    setFocus();
    return false;
}

void
WebViewWidget::showMessage(const QString& text, const QIcon& icon)
{
    m_messageWidget->setText(text);
    m_messageWidget->setIcon(icon);
    m_messageWidget->adjustSize();

    updateMessagePosition();
    m_messageWidget->raise();

    m_messageTimer->start(1000);
}

void
WebViewWidget::updateMessagePosition()
{
    const auto messageRect = m_messageWidget->rect();
    m_messageWidget->move(
        qMax((rect().width() - messageRect.width()) / 2, 0),
        qMax((rect().height() - messageRect.height()) / 2, 0));
}

void
WebViewWidget::paintEvent(QPaintEvent* event)
{
    GoUpWidget::paintEvent(event);

    QPainter painter(this);
    painter.setPen(UserInterfaceConfig::webBorderColor());

    auto rect = m_view->rect();
    if (UserInterfaceConfig::isDashLayout())
    {
        rect.adjust(0, 0, 1, 0);
    }
    painter.drawRect(rect);
}

void
WebViewWidget::resizeEvent(QResizeEvent* event)
{
    GoUpWidget::resizeEvent(event);
    updateMessagePosition();
}

}
