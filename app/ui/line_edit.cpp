/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "line_edit.h"

#include "user_interface_config.h"

#include <KColorScheme>

namespace KDevDocs
{

LineEdit::LineEdit(QWidget* parent)
    : QLineEdit(parent)
    , m_paletteType(NeutralPalette)
{
    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::paletteChanged, this, &LineEdit::updatePalette);
    updatePalette();
}

void
LineEdit::setPalette(PaletteType type)
{
    m_paletteType = type;

    if (type == NeutralPalette)
    {
        QLineEdit::setPalette(m_neutralPalette);
    }
    else if (type == NegativePalette)
    {
        QLineEdit::setPalette(m_negativePalette);
    }
    else
    {
        QLineEdit::setPalette(m_positivePalette);
    }
}

void
LineEdit::updatePalette()
{
    m_neutralPalette = QPalette();
    if (UserInterfaceConfig::isDashLayout())
    {
        m_neutralPalette.setColor(QPalette::Base, m_neutralPalette.mid().color());
    }

    m_negativePalette = m_neutralPalette;
    KColorScheme::adjustBackground(m_negativePalette, KColorScheme::NegativeBackground);

    m_positivePalette = m_neutralPalette;
    KColorScheme::adjustBackground(m_positivePalette, KColorScheme::PositiveBackground);

    setPalette(m_paletteType);
}

}
