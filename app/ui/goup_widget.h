/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QWidget>

namespace KDevDocs
{

class GoUpWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GoUpWidget(QWidget* parent = nullptr);
    ~GoUpWidget() override = default;

    /**
     * Return true if the widget or it's any (recursive) child has focus.
     */
    bool
    hasFocusedWidget() const;

    /**
     * Perform 'goUp' action. This method should be called on widgets wich hasFocusedWidget()
     * method return 'true'.
     *
     * If some actions are done inside the widget (focus change, text clear, etc) the method
     * should return 'false'. When 'true' is returned 'goUp' action processing can be started
     * for another widget so current widget may lose focus.
     */
    virtual bool
    goUp() = 0;
};

}
