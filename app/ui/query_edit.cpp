/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "query_edit.h"

#include "core.h"
#include "icon_manager.h"
#include "query_results_model.h"
#include "query_sources_model.h"
#include "search_config.h"
#include "search_delegate.h"
#include "user_interface_config.h"

#include <KLocalizedString>

#include <QApplication>
#include <QHeaderView>
#include <QKeyEvent>
#include <QListView>
#include <QPainter>
#include <QTextLayout>
#include <QTimer>
#include <QTreeView>

namespace KDevDocs
{

inline void highlightFirstRow(QAbstractItemView* view)
{
    QKeyEvent keyEvent(QEvent::KeyPress, Qt::Key_Home, Qt::NoModifier);
    QCoreApplication::sendEvent(view, &keyEvent);
}

QueryEdit::QueryEdit(QTreeView* contentsView, QListView* resultsView, QListView* sourcesView, QWidget* parent)
    : LineEdit(parent)

    , m_sourceIndex(0)

    , m_useDelays(true)
    , m_queryTimer(new QTimer(this))
    , m_urlTimer(new QTimer(this))

    , m_contentsView(contentsView)
    , m_contentsModel(Core::self()->contentsModel())
    , m_contentsDelegate(new SearchDelegate(this))

    , m_resultsView(resultsView)
    , m_resultsModel(new QueryResultsModel(this))
    , m_resultsDelegate(new SearchDelegate(this, { DocsetIconRole, Qt::DecorationRole }))

    , m_sourcesView(sourcesView)
    , m_sourcesModel(new QuerySourcesModel(this))
    , m_sourcesDelegate(new SearchDelegate(this))
{
    setState(ContentsState);

    auto changeFont = [this]()
    {
        if (SearchConfig::useFixedFont())
        {
            setFont(SearchConfig::fixedFont());
        }
        else
        {
            setFont(QFontDatabase::systemFont(QFontDatabase::GeneralFont));
        }
    };

    changeFont();

    connect(SearchConfig::self(), &SearchConfig::fontChanged, this, changeFont);
    connect(SearchConfig::self(), &SearchConfig::paletteChanged, this, &QueryEdit::checkPosition);

    m_contentsView->setModel(m_contentsModel);
    m_contentsView->setItemDelegate(m_contentsDelegate);
    m_contentsView->header()->hide();
    m_contentsView->setFrameShape(QFrame::NoFrame);
    connect(m_contentsView, &QTreeView::activated, this, &QueryEdit::activateIndex);

    m_resultsView->setModel(m_resultsModel);
    m_resultsView->setItemDelegate(m_resultsDelegate);
    m_resultsView->setFrameShape(QFrame::NoFrame);
    connect(m_resultsView, &QListView::activated, this, &QueryEdit::activateIndex);

    m_sourcesView->setModel(m_sourcesModel);
    m_sourcesView->setItemDelegate(m_sourcesDelegate);
    m_sourcesView->setFrameShape(QFrame::NoFrame);
    connect(m_sourcesView, &QListView::activated, this, &QueryEdit::activateSource);
    connect(m_sourcesModel, &QuerySourcesModel::rowsCountChanged, this, [this]()
    {
        highlightFirstRow(m_sourcesView);
    });

    connect(this, &QLineEdit::textChanged, this, [this](const QString& text)
    {
        m_query = Query(text);
        m_sourcesModel->setQuery(m_query);
        queryInternal();
    });
    connect(this, &QLineEdit::cursorPositionChanged, this, &QueryEdit::checkPosition);
    connect(this, &QLineEdit::selectionChanged, this, &QueryEdit::checkPosition);

    m_queryTimer->setSingleShot(true);
    connect(m_queryTimer, &QTimer::timeout, this, &QueryEdit::displayQueryResults);

    m_urlTimer->setSingleShot(true);
    connect(m_urlTimer, &QTimer::timeout, this, &QueryEdit::displayQueryFirstIndex);

    connect(Core::self(), &Core::stateChanged, this, &QueryEdit::checkCoreState);
    checkCoreState();
}

void
QueryEdit::checkCoreState()
{
    if (Core::self()->state() == Core::LOADING)
    {
        setPlaceholderText(i18n("Loading docsets data..."));
        setReadOnly(true);
        connect(IconManager::self(), &IconManager::loadingIconChanged, this, &QueryEdit::setIcon);
        return;
    }

    if (Core::self()->state() == Core::READY)
    {
        IconManager::self()->disconnect(this);
        setIcon(QIcon());

        setPlaceholderText(i18n("Enter your query"));
        setReadOnly(false);

        if (!m_pendingQuery.isEmpty())
        {
            query(m_pendingQuery);
        }
    }
}

void
QueryEdit::setIcon(const QIcon& icon)
{
    m_icon = icon;
    update();
}

void
QueryEdit::query(const QString& queryString)
{
    if (Core::self()->state() != Core::READY)
    {
        m_pendingQuery = queryString;
        return;
    }

    m_useDelays = false;

    setText(queryString);
    setCursorPosition(m_query.text().size());

    m_useDelays = true;
    m_pendingQuery.clear();
}

void
QueryEdit::queryInternal()
{
    if (m_useDelays)
    {
        m_queryTimer->start(SearchConfig::searchDelay());
    }
    else
    {
        displayQueryResults();
    }
}

void
QueryEdit::displayQueryResults()
{
    m_resultsDelegate->setHighlight(m_query.text());
    if (m_resultsModel->setQuery(m_query))
    {
        highlightFirstRow(m_resultsView);

        if (m_useDelays)
        {
            m_urlTimer->start(SearchConfig::firstResultOpenDelay());
        }
        else
        {
            displayQueryFirstIndex();
        }
    }
}

void
QueryEdit::displayQueryFirstIndex()
{
    if (m_resultsModel->rowCount() > 0)
    {
        displayIndex(m_resultsModel->index(0, 0), !m_useDelays);
    }
}

void
QueryEdit::activateIndex(const QModelIndex& index)
{
    displayIndex(index, true);
}

void
QueryEdit::displayIndex(const QModelIndex& index, bool focusView)
{
    emit urlActivated(index.data(UrlRole).toUrl(), focusView);
}

void
QueryEdit::activateSource(const QModelIndex& index)
{
    const auto name = index.data(Qt::DisplayRole).toString();
    const auto& source = m_query.source(m_sourceIndex);

    int position = cursorPosition();
    QString newText = text();

    if (position == source.startPosition && source.docset)
    {
        newText.insert(position, QStringLiteral("%1,").arg(name));
    }
    else
    {
        newText.replace(source.startPosition, source.name.size(), name);
        position = source.startPosition;
    }

    setText(newText);
    setCursorPosition(position + name.size());

    // Hide sources view, show current query results
    setState(ResultsState);
}

void
QueryEdit::goUp()
{
    if (qApp->focusWidget() != this)
    {
        setFocus();
        return;
    }

    if (text().isEmpty())
    {
        m_contentsView->collapseAll();
        return;
    }

    if (m_query.sourcesCount() <= 0 || cursorPosition() <= m_query.text().size())
    {
        clear();
        setState(ContentsState);
        return;
    }

    setCursorPosition(0);

    // DON'T DELETE. This forces edit repaint after scrolling cursor position to 0.
    // If we skip this step then cursor will be at right position after next
    // setCursorPosition() call but alignment of the text will be wrong - we will see
    // most of "right part" (search sources) of the text instead "left part"
    // (searched text).
    repaint();

    setCursorPosition(m_query.text().size());
}

void
QueryEdit::setState(State state)
{
    m_state = state;
    switch (state)
    {
        case ContentsState:
            m_currentView = m_contentsView;
            break;

        case ResultsState:
            m_currentView = m_resultsView;
            break;

        case SourcesState:
            m_currentView = m_sourcesView;
            break;
    }

    emit viewActivated(state);
}

void
QueryEdit::checkPosition()
{
    if (hasSelectedText())
    {
        return;
    }

    QInputMethodEvent clearFormatEvent(QString(), {});
    if (text().isEmpty())
    {
        setState(ContentsState);
        event(&clearFormatEvent);
        return;
    }

    const int position = cursorPosition();
    if (m_query.sourcesCount() <= 0 || position < m_query.source(0).startPosition)
    {
        setState(ResultsState);
        event(&clearFormatEvent);
        return;
    }

    QString namePrefix;
    InstalledDocset* currentDocset = nullptr;

    for (m_sourceIndex = 0; m_sourceIndex < m_query.sourcesCount(); ++m_sourceIndex)
    {
        const auto& source = m_query.source(m_sourceIndex);
        const int start = source.startPosition;
        const int end = start + source.name.length();
        if (position >= start && position <= end)
        {
            namePrefix = source.name.left(position - start);
            if (!namePrefix.isEmpty())
            {
                currentDocset = source.docset;
            }
            break;
        }
    }

    setState(SourcesState);
    m_sourcesDelegate->setHighlight(namePrefix);
    m_sourcesModel->update(currentDocset, namePrefix);

    // https://stackoverflow.com/a/14424003
    QTextCharFormat sourceTextFormat;
    sourceTextFormat.setForeground(SearchConfig::highlightTextColor());

    if (m_sourcesModel->rowCount() > 0)
    {
        sourceTextFormat.setBackground(SearchConfig::highlightBackroundColor());
    }
    else
    {
        sourceTextFormat.setBackground(SearchConfig::highlightBackroundErrorColor());
    }

    const QInputMethodEvent::Attribute sourceNameAttribute(
        QInputMethodEvent::TextFormat,
        m_query.source(m_sourceIndex).startPosition - position,
        namePrefix.size(),
        sourceTextFormat);

    QInputMethodEvent setFormatEvent(QString(), { sourceNameAttribute });
    event(&setFormatEvent);
}

void
QueryEdit::keyPressEvent(QKeyEvent* event)
{
    const int key = event->key();

    if (key == Qt::Key_Return ||
        key == Qt::Key_Enter ||
        key == Qt::Key_Up ||
        key == Qt::Key_Down ||
        key == Qt::Key_PageUp ||
        key == Qt::Key_PageDown)
    {
        // Allow navigation and item activation for all our views
        qApp->sendEvent(m_currentView, event);
        return;
    }

    if (m_state == ContentsState && (key == Qt::Key_Left || key == Qt::Key_Right))
    {
        // Allow tree items collapse/expand only for contents view
        qApp->sendEvent(m_currentView, event);
        return;
    }

    QLineEdit::keyPressEvent(event);
}

void
QueryEdit::paintEvent(QPaintEvent* event)
{
    QLineEdit::paintEvent(event);
    if (m_icon.isNull())
    {
        return;
    }

    int margin = 3; // FIXME hidpi
    QPixmap pixmap = m_icon.pixmap(height() - margin * 2, height() - margin * 2);
    margin = (height() - pixmap.height()) / 2;

    QPainter painter(this);
    painter.drawPixmap(width() - height() + margin, margin, pixmap);
}

}
