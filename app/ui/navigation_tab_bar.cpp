/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "navigation_tab_bar.h"

#include "qtcompat.h"
#include "user_interface_config.h"

#include <KLocalizedString>

#include <QApplication>
#include <QIcon>
#include <QLinearGradient>
#include <QEnterEvent>
#include <QMouseEvent>
#include <QPainter>

namespace KDevDocs
{

NavigationTabBar::NavigationTabBar(QWidget* parent)
    : QWidget(parent)
    , m_count(0)
    , m_currentIndex(-1)
    , m_hoveredIndex(-1)
    , m_isButtonHovered(false)
    , m_mousePosition(-1, -1)
    , m_iconSize(16) // FIXME for hidpi
    , m_defaultTabWidth(200) // FIXME for hidpi
{
    // Rects for "Add tab" button
    m_rect += QRect();
    m_buttonRect += QRect();

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    setMouseTracking(true);

    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::paletteChanged, this, [this]()
    {
        updateTabsGeometry();
        update();
    });
}

NavigationTabBar::~NavigationTabBar() = default;

QSize
NavigationTabBar::sizeHint() const
{
    return QSize(0, fontMetrics().height() + 8); // FIXME 8
}

int
NavigationTabBar::currentIndex() const
{
    return m_currentIndex;
}

void
NavigationTabBar::setCurrentIndex(int index, bool doEmit)
{
    if (m_currentIndex == index)
    {
        return;
    }

    m_currentIndex = index;
    update();

    if (doEmit)
    {
        emit currentChanged(m_currentIndex);
    }
}

void
NavigationTabBar::addTab()
{
    m_text += i18n("Welcome!");
    m_icon += qApp->windowIcon();

    // Add new rects before "Add tab" button
    m_rect.insert(m_buttonRect.size() - 1, QRect());
    m_buttonRect.insert(m_buttonRect.size() - 1, QRect());

    ++m_count;

    updateTabsGeometry();
    update();
}

void
NavigationTabBar::removeTab(int index)
{
    if (index >= 0 && index < m_count)
    {
        m_text.removeAt(index);
        m_icon.removeAt(index);
        m_buttonRect.removeAt(index);
        --m_count;

        updateTabsGeometry();
        update();
    }
}

QString
NavigationTabBar::tabText(int index) const
{
    if (index >= 0 && index < m_count)
    {
        return m_text[index];
    }

    return {};
}

void
NavigationTabBar::setTabText(int index, const QString& text)
{
    if (index >= 0 && index < m_count)
    {
        m_text[index] = text;
        update();
    }
}

void
NavigationTabBar::setTabIcon(int index, const QIcon& icon)
{
    if (index >= 0 && index < m_count)
    {
        m_icon[index] = icon;
        update();
    }
}

void
NavigationTabBar::drawTab(int index, QPainter& painter)
{
    if (index < 0 || index >= m_count)
    {
        return;
    }

    auto textColor = palette().text().color();

    // drawTabBasis ================================================================================

    QRect fillRect = m_rect[index].adjusted(1, 1, 0, 0);
    QColor fillColor = UserInterfaceConfig::webBackgroundColor();

    painter.setPen(UserInterfaceConfig::webBorderColor());
    painter.drawRect(m_rect[index]);

    if (index == m_currentIndex)
    {
        fillColor = UserInterfaceConfig::webBackgroundColor();
        fillRect.adjust(0, 0, 0, 1);
    }
    else
    {
        if (UserInterfaceConfig::isDashLayout())
        {
            textColor = palette().color(QPalette::Disabled, QPalette::Text);
        }

        if (index == m_hoveredIndex)
        {
            fillColor = palette().highlight().color();
            fillColor.setAlpha(30);
        }
        else
        {
            if (UserInterfaceConfig::isZealLayout())
            {
                fillColor = UserInterfaceConfig::webBorderColor();
                fillColor.setAlpha(120);
            }
        }
    }

    painter.fillRect(fillRect, fillColor);

    // drawTabIcon =================================================================================

    QPixmap iconPixmap;

    if (index == m_hoveredIndex)
    {
        auto mode = m_isButtonHovered ? QIcon::Normal : QIcon::Disabled;
        iconPixmap = QIcon::fromTheme(QStringLiteral("tab-close")).pixmap(m_iconSize, m_iconSize, mode);
    }
    else
    {
        iconPixmap = m_icon[index].pixmap(m_iconSize, m_iconSize);
    }

    painter.drawPixmap(m_buttonRect[index], iconPixmap);

    // drawTabText =================================================================================

    QRect textRect = m_rect[index].adjusted(m_tabHeight, 0, -m_tabMargin, 0);

    QPen textPen(textColor);
    if (horizontalAdvance(painter.fontMetrics(), m_text[index]) > textRect.width())
    {
        QLinearGradient gradient(textRect.topLeft(), textRect.bottomRight());
        gradient.setColorAt(0.0, textColor);
        gradient.setColorAt(0.8, textColor);
        gradient.setColorAt(1.0, Qt::transparent);
        textPen = QPen(QBrush(gradient), 1);
    }

    painter.setPen(textPen);
    painter.drawText(textRect, Qt::AlignLeft | Qt::AlignVCenter, m_text[index], &textRect);
}

void
NavigationTabBar::paintEvent(QPaintEvent* /*event*/)
{
    QPainter painter(this);

    checkMousePosition(false);

    for (int index = 0; index < m_count; ++index)
    {
        drawTab(index, painter);
    }

    if (UserInterfaceConfig::isZealLayout())
    {
        painter.setPen(UserInterfaceConfig::webBorderColor());
    }
    else
    {
        QLinearGradient gradient(m_tabWidth * m_count, 0, width(), m_tabHeight);
        gradient.setColorAt(0.0,  UserInterfaceConfig::webBorderColor());
        gradient.setColorAt(0.75, Qt::transparent);

        painter.setPen(QPen(QBrush(gradient), 1));
    }
    painter.drawLine(m_tabWidth * m_count, m_tabHeight, width(), m_tabHeight);

    if (m_hoveredIndex == m_count)
    {
        auto fillColor = palette().highlight().color();
        fillColor.setAlpha(30);
        painter.fillRect(m_buttonRect[m_count], fillColor);
    }

    auto x0 = m_buttonRect[m_count].center().x();
    auto y0 = m_buttonRect[m_count].center().y();
    int delta = 6; // FIXME hidpi

    painter.setPen(palette().text().color());
    painter.drawLine(x0 - delta, y0, x0 + delta, y0);
    painter.drawLine(x0, y0 - delta, x0, y0 + delta);
}

void
NavigationTabBar::mousePressEvent(QMouseEvent* /*event*/)
{
    if (m_hoveredIndex < 0)
    {
        return;
    }

    if (m_hoveredIndex == m_count)
    {
        emit tabAddRequested();
        return;
    }

    if (m_isButtonHovered)
    {
        emit tabCloseRequested(m_hoveredIndex);
    }
    else
    {
        setCurrentIndex(m_hoveredIndex, true);
    }
}

void
NavigationTabBar::updateTabsGeometry()
{
    m_tabWidth  = qMin(width() / (m_count + 1), m_defaultTabWidth);
    m_tabHeight = height() - 1;
    m_tabMargin = (m_tabHeight - m_iconSize) / 2 + (m_tabHeight - m_iconSize) % 2;

    for (int index = 0; index < m_count; ++index)
    {
        m_rect[index] = QRect(m_tabWidth * index, 0, m_tabWidth, m_tabHeight);
        m_buttonRect[index] = QRect(m_tabWidth * index + m_tabMargin, m_tabMargin, m_iconSize, m_iconSize);
    }

    int height = UserInterfaceConfig::isZealLayout() ? m_tabHeight : m_tabHeight - 1;
    int indent = UserInterfaceConfig::isZealLayout() ? 0 : 1;

    m_rect[m_count] = m_buttonRect[m_count] = QRect(m_tabWidth * m_count + indent, indent, height, height);
}

void
NavigationTabBar::checkMousePosition(bool doUpdate)
{
    bool changed = false;
    for (int index = 0; index <= m_count; ++index)
    {
        if (m_rect[index].contains(m_mousePosition))
        {
            if (m_hoveredIndex != index)
            {
                m_hoveredIndex = index;
                changed = true;
            }

            const bool isButtonHovered = m_buttonRect[index].contains(m_mousePosition);
            if (m_isButtonHovered != isButtonHovered)
            {
                m_isButtonHovered = isButtonHovered;
                changed = true;
            }

            break;
        }
    }

    if (changed && doUpdate)
    {
        update();
    }
}

void
NavigationTabBar::resizeEvent(QResizeEvent* event)
{
    updateTabsGeometry();
    QWidget::resizeEvent(event);
}

void
NavigationTabBar::mouseMoveEvent(QMouseEvent* event)
{
    m_mousePosition = event->pos();
    checkMousePosition(true);
}

void
NavigationTabBar::enterEvent(QEvent* event)
{
    m_mousePosition = static_cast<QEnterEvent*>(event)->pos();
    checkMousePosition(true);
}

void
NavigationTabBar::leaveEvent(QEvent* event)
{
    Q_UNUSED(event);

    m_mousePosition = QPoint(-1, -1);

    if (m_hoveredIndex >= 0)
    {
        m_hoveredIndex = -1;
        m_isButtonHovered = false;
        update();
    }
}

}
