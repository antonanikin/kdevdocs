/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QSplitter>

class QStackedWidget;

namespace KDevDocs
{

class MainWindow;
class NavigationWidget;
class QueryWidget;
class WebViewWidget;

class CentralWidget : public QSplitter
{
    Q_OBJECT

public:
    CentralWidget(const QVector<QAction*>& actions, MainWindow* mainWindow);

    ~CentralWidget() override;

    void
    query(const QString& queryString);

protected:
    void
    resizeEvent(QResizeEvent* event) override;

    void
    paintEvent(QPaintEvent* event) override;

private:
    int
    currentIndex() const;

    int
    count() const;

    void
    addStartPageTab();

    void
    addTab(const QUrl& url, bool focusView);

    void
    closeCurrentTab();

    void
    closeTab(int index);

    void
    selectPreviousTab();

    void
    selectNextTab();

    void
    selectTab(int index);

    void
    goUp();

    void
    setSplitterPosition();

    void
    focusSave();

    void
    focusRestore();

    void
    updateActions();

private:
    const QVector<QAction*>& m_actions;
    MainWindow* m_mainWindow;

    int m_splitterPosition;

    NavigationWidget* m_navigationWidget;

    QStackedWidget* m_queryStack;
    QList<QueryWidget*> m_queryWidgets;

    QStackedWidget* m_webStack;
    QList<WebViewWidget*> m_webViews;

    QList<QWidget*> m_focused;
};

}
