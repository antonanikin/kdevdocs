/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "main_window.h"
#include "kdevdocsadaptor.h"

#include "action.h"
#include "central_widget.h"
#include "core.h"
#include "general_config.h"
#include "installed_docset.h"
#include "query.h"
#include "user_interface_config.h"

#include "config/main_config_dialog.h"

#include <KActionCollection>
#include <KGlobalAccel>
#include <KLocalizedString>
#include <KStandardAction>
#include <KToggleFullScreenAction>
#include <KToolBar>

#include <kconfigwidgets_version.h>

#include <QAction>
#include <QApplication>
#include <QDesktopWidget>
#include <QMenu>
#include <QMenuBar>
#include <QScreen>
#include <QStackedWidget>
#include <QSystemTrayIcon>
#include <QVBoxLayout>
#include <QWindow>

namespace KDevDocs
{

MainWindow::MainWindow(bool startMinimized, QWidget* parent)
    : KXmlGuiWindow(parent)
    , m_systemTray(nullptr)
{
    setAttribute(Qt::WA_DeleteOnClose, false);
//     setWindowFlags(Qt::CustomizeWindowHint); // border-less window

    connect(GeneralConfig::self(), &GeneralConfig::showTrayChanged,
            this, &MainWindow::setupTray);

    setupActions();
    setupTray();

    // Set window size to 0.75 of the default screen.
    // It will be fixed by setupGUI() if we have saved values in the config.
    resize(QGuiApplication::screens().at(0)->availableGeometry().size() * 0.75);

    setupGUI(Keys | Save | Create, QStringLiteral("kdevdocs.rc"));

    // Remove main toolbar
    delete toolBar(QString());

    {
        auto menuState = autoSaveConfigGroup().readEntry(QStringLiteral("MenuBar"), QString());
        m_actions[MenuToggle]->setChecked(menuState != QStringLiteral("Disabled"));
    }

    {
        auto mainMenu = new QMenu(this);

        const auto menuActions = menuBar()->actions();
        for (auto action : menuActions)
        {
            mainMenu->addAction(action);
        }

        m_actions[Menu] = new QAction(actionCollection());
        m_actions[Menu]->setIcon(QIcon::fromTheme(QStringLiteral("application-menu")));
        m_actions[Menu]->setMenu(mainMenu);
    }

    m_centralWidget = new CentralWidget(m_actions, this);
    setCentralWidget(m_centralWidget);

    m_fullScreen = autoSaveConfigGroup().readEntry(QStringLiteral("Full Screen"), false);

    addTab();

    auto dbusRegister = [this]()
    {
        auto bus = QDBusConnection::sessionBus();
        bus.registerObject(QStringLiteral("/KDevDocs"), this);
        bus.registerService(QStringLiteral("org.kde.kdevdocs"));
    };

    new KDevDocsAdaptor(this);

    // FIXME remove ?
    connect(Core::self(), &Core::stateChanged,
            this, [dbusRegister]()
            {
                if (Core::self()->state() == Core::LOADING)
                {
                    auto bus = QDBusConnection::sessionBus();
                    bus.unregisterService(QStringLiteral("org.kde.kdevdocs"));
                    bus.unregisterObject(QStringLiteral("/KDevDocs"));
                }
                else if (Core::self()->state() == Core::READY)
                {
                    dbusRegister();
                }
            });

    if (Core::self()->state() == Core::READY)
    {
        dbusRegister();
    }

    if (startMinimized || GeneralConfig::startMinimized())
    {
        if (!GeneralConfig::showTray() || !GeneralConfig::minimizeToTray())
        {
            showMinimized();
        }
    }
    else
    {
        raiseWindow();
    }
}

MainWindow::~MainWindow()
{
    // Delete our central widget to force it's config save. Otherwise it's destructor
    // will be called with already destroyed autoSaveConfigGroup() instance.
    delete m_centralWidget;

    autoSaveConfigGroup().writeEntry(QStringLiteral("Full Screen"), m_fullScreen);

    // Force save window size, this fixes restore process. KWindowConfig does not perform size
    // save/load for maximized windows. This leads to visible repainting of WebWidget at start,
    // especially when WebEngine is used.
    const QRect screen = windowHandle()->screen()->geometry();
    autoSaveConfigGroup().writeEntry(QStringLiteral("Height %1").arg(screen.height()), height());
    autoSaveConfigGroup().writeEntry(QStringLiteral("Width %1").arg(screen.width()), width());
    autoSaveConfigGroup().deleteEntry(
        QStringLiteral("Window-Maximized %1x%2").arg(screen.height()).arg(screen.width()));
}

void
MainWindow::addTab()
{
    m_actions[TabAdd]->trigger();
}

void
MainWindow::setupTray()
{
    if (GeneralConfig::showTray())
    {
        if (!m_systemTray)
        {
            m_systemTray = new QSystemTrayIcon(this);
            connect(m_systemTray, &QSystemTrayIcon::activated, this, &MainWindow::toggleWindow);

            m_systemTray->setIcon(qApp->windowIcon());

            m_systemTray->setContextMenu(new QMenu(this));
            m_systemTray->contextMenu()->addAction(m_actions[WindowToggle]);
            m_systemTray->contextMenu()->addSeparator();
            m_systemTray->contextMenu()->addAction(m_actions[Quit]);

            m_systemTray->show();
        }
    }
    else
    {
        if (m_systemTray)
        {
            delete m_systemTray;
            m_systemTray = nullptr;
        }
    }
}

void
MainWindow::setupActions()
{
    auto ac = actionCollection();

    m_actions.resize(Action::COUNT);
    m_actions.fill(nullptr);

    m_actions[TabAdd] = ac->addAction(KStandardAction::New, QStringLiteral("tab_add"));
    m_actions[TabAdd]->setText(i18n("New Tab"));
    m_actions[TabAdd]->setIcon(QIcon::fromTheme(QStringLiteral("tab-new")));

    m_actions[TabClose] = ac->addAction(KStandardAction::Close, QStringLiteral("tab_close"));
    m_actions[TabClose]->setText(i18n("Close Tab"));
    m_actions[TabClose]->setIcon(QIcon::fromTheme(QStringLiteral("tab-close")));

    m_actions[TabPrevious] = ac->addAction(KStandardAction::Prior, QStringLiteral("tab_previous"));
    m_actions[TabPrevious]->setText(i18n("Previous Tab"));
    ac->setDefaultShortcut(m_actions[TabPrevious], Qt::CTRL + Qt::SHIFT + Qt::Key_Tab);

    m_actions[TabNext] = ac->addAction(KStandardAction::Next, QStringLiteral("tab_next"));
    m_actions[TabNext]->setText(i18n("Next Tab"));
    ac->setDefaultShortcut(m_actions[TabNext], Qt::CTRL + Qt::Key_Tab);

    m_actions[GoUp] = ac->addAction(KStandardAction::Up, QStringLiteral("focus_transfer"));
    m_actions[GoUp]->setText(i18n("Transfer Focus"));
    ac->setDefaultShortcut(m_actions[GoUp], Qt::Key_Escape);

    m_actions[GoBack] = ac->addAction(KStandardAction::Back, QStringLiteral("history_back"));
    m_actions[GoForward] = ac->addAction(KStandardAction::Forward, QStringLiteral("history_forward"));

    m_actions[ZoomReset] = ac->addAction(KStandardAction::ActualSize, QStringLiteral("zoom_reset"));
    ac->setDefaultShortcut(m_actions[ZoomReset], Qt::CTRL + Qt::Key_0);

    m_actions[ZoomIn] = ac->addAction(KStandardAction::ZoomIn, QStringLiteral("zoom_in"));
    m_actions[ZoomOut] = ac->addAction(KStandardAction::ZoomOut, QStringLiteral("zoom_out"));

    m_actions[Find] = ac->addAction(KStandardAction::Find);
    m_actions[FindPrev] = ac->addAction(KStandardAction::FindPrev);
    m_actions[FindNext] = ac->addAction(KStandardAction::FindNext);

    const auto browserIcon = QIcon::fromTheme(QStringLiteral("internet-services"));

    m_actions[OpenLink] = new QAction(i18n("Open Link in Current Tab"), ac);
    m_actions[OpenLinkInBrowser] = new QAction(browserIcon, i18n("Open Link in Desktop Browser"), ac);

    m_actions[OpenImage] = new QAction(i18n("Show Image in Current Tab"), ac);
    m_actions[OpenImageInBrowser] = new QAction(browserIcon, i18n("Show Image in Desktop Browser"), ac);

    m_actions[OpenPageInBrowser] = new QAction(browserIcon, i18n("Open Current Page in Desktop Browser"), ac);

    auto delayedWindowToggle = [this]()
    {
        // We need to use delayed hideRestore() call, due to the fact that when
        // you activate the global shortcut then current active window loses its
        // "active status" for a short period of time. So the call of isActiveWindow()
        // will always return "false".
        QTimer::singleShot(100, this, &MainWindow::toggleWindow);
    };

// ubuntu 16.04 support; TODO drop this on 16.04 lifetime end
#if KCONFIGWIDGETS_VERSION < ((5<<16)|(23<<8)|(0))
    auto preferencesAction = ac->addAction(KStandardAction::Preferences);
    connect(preferencesAction, &QAction::triggered, this, &MainWindow::openPreferences);

    m_actions[MenuToggle] = ac->addAction(KStandardAction::ShowMenubar);
    connect(m_actions[MenuToggle], &QAction::triggered, menuBar(), &QMenuBar::setVisible);

    m_actions[FullscreenToggle] = ac->addAction(KStandardAction::FullScreen);
    connect(m_actions[FullscreenToggle], &QAction::triggered, this, &MainWindow::toggleFullScreen);

    m_actions[WindowToggle] = ac->addAction(QStringLiteral("window_toggle"));
    connect(m_actions[WindowToggle], &QAction::triggered, this, delayedWindowToggle);

    m_actions[Quit] = ac->addAction(KStandardAction::Quit);
    connect(m_actions[Quit], &QAction::triggered, qApp, &QApplication::quit);

#else
    KStandardAction::preferences(this, &MainWindow::openPreferences, ac);
    m_actions[MenuToggle] = KStandardAction::showMenubar(menuBar(), &QMenuBar::setVisible, ac);
    m_actions[FullscreenToggle] = KStandardAction::fullScreen(this, &MainWindow::toggleFullScreen, this, ac);
    m_actions[WindowToggle] = ac->addAction(QStringLiteral("window_toggle"), this, delayedWindowToggle);
    m_actions[Quit] = KStandardAction::quit(qApp, &QApplication::quit, ac);
#endif

    auto updateWindowToggleShortcut = [this]()
    {
        KGlobalAccel::self()->setShortcut(
            m_actions[WindowToggle],
            { GeneralConfig::windowToggleShortcut() },
            KGlobalAccel::NoAutoloading);
    };

    connect(GeneralConfig::self(), &GeneralConfig::windowToggleShortcutChanged, this, updateWindowToggleShortcut);
    updateWindowToggleShortcut();
    ac->setShortcutsConfigurable(m_actions[WindowToggle], false);
}

void
MainWindow::openPreferences()
{
    auto configPage = new MainConfigDialog(this);
    configPage->show();
}

void
MainWindow::raiseWindow()
{
    if (GeneralConfig::showTray() && GeneralConfig::minimizeToTray())
    {
        toggleFullScreen(m_fullScreen);
    }
    else
    {
        setWindowState(windowState() & ~Qt::WindowMinimized);
    }

    setVisible(true);
    activateWindow();
}

void
MainWindow::query(const QString& queryString)
{
    m_centralWidget->query(queryString);
    raiseWindow();
}

bool
MainWindow::test(const QString& queryString)
{
    return Query(queryString).test();
}

QByteArray
MainWindow::docsets()
{
    QByteArray ba;
    QDataStream ds(&ba, QIODevice::WriteOnly);

    for (auto docset : Core::self()->docsets())
    {
        ds << docset->name();
        ds << docset->icon().pixmap(32, 32); // FIXME size ?
    }

    return ba;
}

void
MainWindow::toggleFullScreen(bool fullScreen)
{
    KToggleFullScreenAction::setFullScreen(this, fullScreen);

    // When we close/hide full-screen'ned window our m_actions[FullscreenToggle] will
    // be automatically unchecked, so we should keep current fullScreen value.
    if (isVisible())
    {
        m_fullScreen = fullScreen;
    }
}

void
MainWindow::toggleWindow()
{
    if (isVisible() && isActiveWindow())
    {
        if (GeneralConfig::showTray() && GeneralConfig::minimizeToTray())
        {
            setVisible(false);
        }
        else
        {
            setWindowState(windowState() | Qt::WindowMinimized);
        }
    }
    else
    {
        raiseWindow();
    }
}

void
MainWindow::hideEvent(QHideEvent* /*event*/)
{
    if (GeneralConfig::showTray() && GeneralConfig::minimizeToTray())
    {
        setVisible(false);
    }

    m_actions[WindowToggle]->setText(i18n("Show KDevDocs"));
}

void
MainWindow::showEvent(QShowEvent* /*event*/)
{
    m_actions[WindowToggle]->setText(i18n("Minimize KDevDocs"));
}

void
MainWindow::closeEvent(QCloseEvent* /*event*/)
{
    if (!GeneralConfig::showTray() || !GeneralConfig::closeToTray())
    {
        qApp->quit();
    }
}

}
