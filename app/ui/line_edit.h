/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QLineEdit>

namespace KDevDocs
{

class LineEdit : public QLineEdit
{
    Q_OBJECT

public:
    enum PaletteType
    {
        NeutralPalette,
        NegativePalette,
        PositivePalette
    };

    explicit
    LineEdit(QWidget* parent = nullptr);

    ~LineEdit() override = default;

    void
    setPalette(PaletteType type);

private:
    void
    updatePalette();

private:
    PaletteType m_paletteType;

    QPalette m_neutralPalette;
    QPalette m_negativePalette;
    QPalette m_positivePalette;
};

}
