/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QSortFilterProxyModel>

#include "query.h"

namespace KDevDocs
{

class InstalledDocset;

class QuerySourcesModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    explicit
    QuerySourcesModel(QObject* parent = nullptr);

    ~QuerySourcesModel() override = default;

    void
    setQuery(const Query& query);

    void
    update(InstalledDocset* currentDocset, const QString& namePrefix);

    QVariant
    data(const QModelIndex& index, int role) const override;

    bool
    filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;

signals:
    void
    rowsCountChanged();

protected:
    Query m_query;
    QString m_namePrefix;
    InstalledDocset* m_currentDocset;
};

}
