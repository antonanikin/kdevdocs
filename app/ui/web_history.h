/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QWebHistory>

class QAction;

namespace KDevDocs
{

class WebPage;

class WebHistory : public QObject
{
    Q_OBJECT

public:
    explicit
    WebHistory(WebPage* page);

    ~WebHistory() override;

    bool
    canGoBack() const;

    bool
    canGoForward() const;

    void
    back();

    void
    forward();

    QList<QAction*>
    allActions() const;

    QList<QAction*>
    backActions() const;

    QList<QAction*>
    forwardActions() const;

signals:
    void
    changed();

private:
    void
    insert();

    void
    setIndex(int index);

    void
    updateActions();

private:
    WebPage* m_page;

    int m_index;
    QList<QWebHistoryItem> m_items;

    QList<QAction*> m_actions;

    friend class WebPage;
};

}
