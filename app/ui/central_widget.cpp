/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "central_widget.h"

#include "action.h"
#include "main_window.h"
#include "navigation_widget.h"
#include "network_access_manager.h"
#include "query_widget.h"
#include "user_interface_config.h"
#include "web_history.h"
#include "web_page_find_widget.h"
#include "web_view_widget.h"

#include <QApplication>
#include <QDesktopServices>
#include <QPainter>
#include <QResizeEvent>
#include <QStackedWidget>
#include <QVBoxLayout>

#include <QLoggingCategory>

namespace KDevDocs
{

Q_LOGGING_CATEGORY(CENTRAL_WIDGET, "kdevdocs.ui.centralwidget");

namespace config
{
    inline KConfigGroup
    group(const KMainWindow* mainWindow)
    {
        return mainWindow->autoSaveConfigGroup().config()->group(QStringLiteral("Central Widget"));
    }

    inline QString sizeKey() { return QStringLiteral("Size"); }
    inline QString splitterKey() { return QStringLiteral("Splitter Position"); }
}

CentralWidget::CentralWidget(const QVector<QAction*>& actions, MainWindow* mainWindow)
    : QSplitter(mainWindow)
    , m_actions(actions)
    , m_mainWindow(mainWindow)
    , m_navigationWidget(new NavigationWidget(actions, this))
    , m_queryStack(new QStackedWidget)
    , m_webStack(new QStackedWidget)
{
    Q_ASSERT(mainWindow);

    setChildrenCollapsible(false);

    // Setting handle width to 0 will produce high CPU consumption during WebView scrolling.
    // This is especially noticeable with with enabled dark mode.
    setHandleWidth(1);

    auto rightWidget = new QWidget;
    auto rightLayout = new QVBoxLayout(rightWidget);
    rightLayout->setContentsMargins(0, 0, 0, 0);
    rightLayout->setSpacing(0);
    rightLayout->addWidget(m_navigationWidget);
    rightLayout->addWidget(m_webStack);

    addWidget(m_queryStack);
    addWidget(rightWidget);

    {
        auto group = config::group(m_mainWindow);

        m_splitterPosition = group.readEntry(config::splitterKey(), 300);

        // We set correct size and splitter position before real widget display to avoid
        // visible repainting of our web view widget. It was critical for QWebEngineView
        // (previous versions of KDevDcos) but also should be useful for QWebView (WebKit),
        // for example for slow-performance machines.
        //
        // Also note, that splitter position value should be set AFTER the widgets
        // addition otherwise this have no effect.
        resize(group.readEntry(config::sizeKey(), m_mainWindow->size()));
        setSplitterPosition();
    }

    connect(this, &CentralWidget::splitterMoved, this, [this](int pos)
    {
        m_splitterPosition = pos;
    });

    connect(m_navigationWidget, &NavigationWidget::currentChanged, this, &CentralWidget::selectTab);
    connect(m_navigationWidget, &NavigationWidget::tabCloseRequested, this, &CentralWidget::closeTab);
    connect(m_navigationWidget, &NavigationWidget::tabAddRequested, this, &CentralWidget::addStartPageTab);

    connect(actions[TabAdd], &QAction::triggered, this, &CentralWidget::addStartPageTab);
    connect(actions[TabClose], &QAction::triggered, this, &CentralWidget::closeCurrentTab);
    connect(actions[TabPrevious], &QAction::triggered, this, &CentralWidget::selectPreviousTab);
    connect(actions[TabNext], &QAction::triggered, this, &CentralWidget::selectNextTab);

    connect(actions[GoUp], &QAction::triggered, this, &CentralWidget::goUp);

    connect(actions[GoBack], &QAction::triggered, this, [this]()
    {
        m_webViews[currentIndex()]->history()->back();
    });

    connect(actions[GoForward], &QAction::triggered, this, [this]()
    {
        m_webViews[currentIndex()]->history()->forward();
    });

    connect(actions[ZoomIn], &QAction::triggered, this, [this]()
    {
        m_webViews[currentIndex()]->zoomIn();
    });

    connect(actions[ZoomOut], &QAction::triggered, this, [this]()
    {
        m_webViews[currentIndex()]->zoomOut();
    });

    connect(actions[ZoomReset], &QAction::triggered, this, [this]()
    {
        m_webViews[currentIndex()]->zoomReset();
    });

    connect(actions[Find], &QAction::triggered, this, [this]()
    {
        m_webViews[currentIndex()]->findWidget()->open();
    });

    connect(actions[FindPrev], &QAction::triggered, this, [this]()
    {
        m_webViews[currentIndex()]->findWidget()->findPrev();
    });

    connect(actions[FindNext], &QAction::triggered, this, [this]()
    {
        m_webViews[currentIndex()]->findWidget()->findNext();
    });

    connect(actions[OpenPageInBrowser], &QAction::triggered, this, [this]()
    {
        const auto url = browserUrl(m_webViews[currentIndex()]->url());
        QDesktopServices::openUrl(url);
    });

    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::paletteChanged,
            this, static_cast<void(CentralWidget::*)()>(&CentralWidget::update));
}

CentralWidget::~CentralWidget()
{
    auto group = config::group(m_mainWindow);
    group.writeEntry(config::sizeKey(), size());
    group.writeEntry(config::splitterKey(), m_splitterPosition);

    for (auto webViewWidget : m_webViews)
    {
        // Disconnect web view here to avoid crashes when view load some page during destructor call.
        // In such case for view's titleChanged()/iconChanged() signals theirs slots (see addTab())
        // will operate over already destroyed objects -> crash as a result.
        webViewWidget->disconnect(this);
    }
}

int
CentralWidget::currentIndex() const
{
    return m_queryStack->currentIndex();
}

int
CentralWidget::count() const
{
    return m_queryStack->count();
}

void
CentralWidget::goUp()
{
    auto webViewWidget = m_webViews[currentIndex()];
    if (webViewWidget->hasFocusedWidget() && !webViewWidget->goUp())
    {
        return;
    }

    m_queryWidgets[currentIndex()]->goUp();
}

void
CentralWidget::query(const QString& queryString)
{
    m_queryWidgets[currentIndex()]->query(queryString);
}

void
CentralWidget::setSplitterPosition()
{
    setSizes({ m_splitterPosition, width() - m_splitterPosition - handleWidth()});
}

void
CentralWidget::addStartPageTab()
{
    addTab(QUrl(QStringLiteral("qrc:/browser/start.html")), false);
}

void
CentralWidget::addTab(const QUrl& url, bool focusView)
{
    auto webViewWidget = new WebViewWidget(m_actions, this);
    webViewWidget->load(url, false);

    auto queryWidget = new QueryWidget;
    connect(queryWidget, &QueryWidget::urlToLoad, webViewWidget, &WebViewWidget::load);

    if (focusView)
    {
        m_focused.append(webViewWidget);
    }
    else
    {
        m_focused.append(queryWidget);
    }

    m_queryStack->addWidget(queryWidget);
    m_queryWidgets += queryWidget;

    m_webStack->addWidget(webViewWidget);
    m_webViews += webViewWidget;

    m_navigationWidget->addTab();

    selectTab(count() - 1);

    connect(webViewWidget, &WebViewWidget::loadStarted, this, [this, webViewWidget]()
    {
        if (m_webStack->currentWidget() == webViewWidget)
        {
            m_actions[OpenPageInBrowser]->setEnabled(false);
        }
    });

    connect(webViewWidget, &WebViewWidget::loadFinished, this, [this, webViewWidget]()
    {
        if (m_webStack->currentWidget() == webViewWidget)
        {
            updateActions();
        }
    });

    connect(webViewWidget, &WebViewWidget::titleChanged, this, [this, webViewWidget](const QString& title)
    {
        const int index = m_webStack->indexOf(webViewWidget);
        m_navigationWidget->setTabText(index, title);

        if (index == currentIndex())
        {
            m_mainWindow->setWindowTitle(title);
        }
    });

    connect(webViewWidget, &WebViewWidget::iconChanged, this, [this, webViewWidget](const QIcon& icon)
    {
        const int index = m_webStack->indexOf(webViewWidget);
        m_navigationWidget->setTabIcon(index, icon);
    });

    connect(webViewWidget->history(), &WebHistory::changed, this, [this, webViewWidget]()
    {
        if (m_webStack->currentWidget() == webViewWidget)
        {
            updateActions();
        }
    });

    connect(webViewWidget, &WebViewWidget::windowCreateRequested, this, [this](const QUrl& url)
    {
        addTab(url, true);
    });
}

void
CentralWidget::closeTab(int index)
{
    Q_ASSERT(index >= 0 && index < count());

    m_navigationWidget->removeTab(index);

    auto leftWidget = m_queryStack->widget(index);
    m_queryStack->removeWidget(leftWidget);
    m_queryWidgets.removeAt(index);
    leftWidget->deleteLater();

    auto rightWidget = m_webStack->widget(index);
    m_webStack->removeWidget(rightWidget);
    m_webViews.removeAt(index);
    rightWidget->deleteLater();

    m_focused.removeAt(index);

    if (count())
    {
        selectTab(currentIndex());
    }
    else
    {
        addStartPageTab();
    }
}

void
CentralWidget::closeCurrentTab()
{
    closeTab(currentIndex());
}

void
CentralWidget::focusSave()
{
    Q_ASSERT(currentIndex() >= 0);

    auto focused = qApp->focusWidget();
    if (!focused)
    {
        qCDebug(CENTRAL_WIDGET) << "null widget during focus save, reset to defaults";
        focused = m_queryStack->currentWidget();
    }
    m_focused[currentIndex()] = focused;
}

void
CentralWidget::focusRestore()
{
    Q_ASSERT(currentIndex() >= 0);

    auto focused = m_focused[currentIndex()];
    if (!focused)
    {
        qCDebug(CENTRAL_WIDGET) << "null widget during focus restore, reset to defaults";
        focused = m_queryStack->currentWidget();
    }
    focused->setFocus();
}

void
CentralWidget::selectTab(int index)
{
    Q_ASSERT(index >= 0 && index < count());

    // QStackedWidget::addWidget()/removeWidget() can change currentIndex. When this happens
    // our index parameter already equals to currentIndex() but current focused widget is
    // undefined/null - QStackedWidget changes index, shows our QueryWidget/ViewWidget and reset
    // focus. Therefore here we should skip "save" step to prevent setting focused widget to
    // null value.
    if (currentIndex() != index)
    {
        focusSave();
    }

    m_queryStack->setCurrentIndex(index);
    m_webStack->setCurrentIndex(index);

    focusRestore();

    m_navigationWidget->setCurrentIndex(index);
    m_mainWindow->setWindowTitle(m_navigationWidget->tabText(index));

    updateActions();
}

void
CentralWidget::selectNextTab()
{
    if (count() <= 1)
    {
        return;
    }

    selectTab((currentIndex() + 1) % count());
}

void
CentralWidget::selectPreviousTab()
{
    if (count() <= 1)
    {
        return;
    }

    int index = currentIndex() - 1;
    if (index < 0)
    {
        index = count() - 1;
    }

    selectTab(index);
}

void
CentralWidget::updateActions()
{
    auto webViewWidget = m_webViews[currentIndex()];
    auto history = webViewWidget->history();

    m_actions[GoBack]->setEnabled(history->canGoBack());
    m_actions[GoForward]->setEnabled(history->canGoForward());
    m_actions[OpenPageInBrowser]->setEnabled(!browserUrl(webViewWidget->url()).isEmpty());

    m_navigationWidget->setHistory(history->backActions(), history->forwardActions());

    static const auto actionsListId = QStringLiteral("history_actions");
    m_mainWindow->unplugActionList(actionsListId);
    m_mainWindow->plugActionList(actionsListId, history->allActions());
}

void
CentralWidget::resizeEvent(QResizeEvent* event)
{
    QSplitter::resizeEvent(event);
    setSplitterPosition();
}

void
CentralWidget::paintEvent(QPaintEvent* event)
{
    QSplitter::paintEvent(event);

    if (UserInterfaceConfig::isZealLayout())
    {
        QPainter painter(this);
        painter.setPen(UserInterfaceConfig::webBorderColor());
        painter.drawLine(m_splitterPosition, UserInterfaceConfig::panelHeight()  - 1,
                         m_splitterPosition, height() - 1);
    }
}

}
