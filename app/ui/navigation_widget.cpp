/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "navigation_widget.h"

#include "action.h"
#include "navigation_tab_bar.h"
#include "navigation_title_label.h"
#include "user_interface_config.h"

#include <QHBoxLayout>
#include <QMenu>
#include <QPainter>
#include <QToolButton>

namespace KDevDocs
{

NavigationWidget::NavigationWidget(const QVector<QAction*>& actions, QWidget* parent)
    : QWidget(parent)
    , m_backButton(new QToolButton)
    , m_forwardButton(new QToolButton)
    , m_bookmarkButton(new QToolButton)
    , m_browserButton(new QToolButton)
    , m_menuButton(new QToolButton)
    , m_tabBar(new NavigationTabBar)
    , m_titleLabel(new NavigationTitleLabel)
{
    setAutoFillBackground(true);

    m_backButton->setDefaultAction(actions[GoBack]);
    m_backButton->setMenu(new QMenu(this));

    m_forwardButton->setDefaultAction(actions[GoForward]);
    m_forwardButton->setMenu(new QMenu(this));

    m_bookmarkButton->setIcon(QIcon::fromTheme(QStringLiteral("bookmark-new")));
    m_bookmarkButton->setAutoRaise(true);
    m_bookmarkButton->setEnabled(false);
    m_bookmarkButton->setVisible(false); // FIXME not implemented yet

    m_browserButton->setDefaultAction(actions[OpenPageInBrowser]);
    m_browserButton->setAutoRaise(true);

    m_menuButton->setDefaultAction(actions[Menu]);
    m_menuButton->setAutoRaise(true);
    m_menuButton->setPopupMode(QToolButton::InstantPopup);
    m_menuButton->setVisible(!actions[MenuToggle]->isChecked());
    connect(actions[MenuToggle], &QAction::toggled, m_menuButton, &QToolButton::setHidden);

    connect(m_tabBar, &NavigationTabBar::currentChanged, this, &NavigationWidget::currentChanged);
    connect(m_tabBar, &NavigationTabBar::tabAddRequested, this, &NavigationWidget::tabAddRequested);
    connect(m_tabBar, &NavigationTabBar::tabCloseRequested, this, &NavigationWidget::tabCloseRequested);

    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::layoutChanged, this, &NavigationWidget::updateLayout);
    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::paletteChanged, this, &NavigationWidget::updatePalette);

    updateLayout();
    updatePalette();
}

void
NavigationWidget::updateLayout()
{
    m_backButton->setAutoRaise(UserInterfaceConfig::isDashLayout());
    m_forwardButton->setAutoRaise(UserInterfaceConfig::isDashLayout());

    m_titleLabel->setVisible(UserInterfaceConfig::isDashLayout());

    QLayout* mainLayout = nullptr;

    if (UserInterfaceConfig::isZealLayout())
    {
        setMinimumHeight(UserInterfaceConfig::panelHeight());
        setMaximumHeight(UserInterfaceConfig::panelHeight());

        auto buttonsLayout = new QHBoxLayout;
        buttonsLayout->addWidget(m_backButton);
        buttonsLayout->addWidget(m_forwardButton);
        buttonsLayout->setSpacing(6);

        auto zealLayout = new QHBoxLayout;
        zealLayout->addLayout(buttonsLayout);
        zealLayout->addWidget(m_tabBar, 0, Qt::AlignBottom);
        zealLayout->addWidget(m_titleLabel);
        zealLayout->addWidget(m_bookmarkButton);
        zealLayout->addWidget(m_browserButton);
        zealLayout->addWidget(m_menuButton);
        zealLayout->setMargin(0);

        mainLayout = zealLayout;
    }
    else
    {
        setMinimumHeight(0);
        setMaximumHeight(QWIDGETSIZE_MAX);

        auto buttonsLayout = new QHBoxLayout;
        buttonsLayout->addWidget(m_backButton);
        buttonsLayout->addWidget(m_forwardButton);
        buttonsLayout->addWidget(m_titleLabel);
        buttonsLayout->addWidget(m_bookmarkButton);
        buttonsLayout->addWidget(m_browserButton);
        buttonsLayout->addWidget(m_menuButton);
        buttonsLayout->setContentsMargins(6, 6, 6, 6); // FIXME ?
        buttonsLayout->setSpacing(0);

        auto dashLayout = new QVBoxLayout;
        dashLayout->addWidget(m_tabBar);
        dashLayout->addLayout(buttonsLayout);
        dashLayout->setMargin(0);
        dashLayout->setSpacing(0);

        mainLayout = dashLayout;
    }

    delete layout();
    setLayout(mainLayout);
}

void
NavigationWidget::updatePalette()
{
    QPalette palette;
    if (UserInterfaceConfig::isDashLayout())
    {
        palette.setColor(QPalette::Background, UserInterfaceConfig::webBackgroundColor());
    }
    setPalette(palette);

    static const auto buttonCss = QStringLiteral("QToolButton::menu-indicator { image: none; }");
    m_backButton->setStyleSheet(buttonCss);
    m_forwardButton->setStyleSheet(buttonCss);
}

void
NavigationWidget::setTitle(const QString& title)
{
    m_titleLabel->setTitle(title);
}

void
NavigationWidget::addTab()
{
    m_tabBar->addTab();
}

void
NavigationWidget::removeTab(int index)
{
    m_tabBar->removeTab(index);
}

void
NavigationWidget::setCurrentIndex(int index)
{
    m_tabBar->setCurrentIndex(index, false);
    setTitle(m_tabBar->tabText(index));
}

QString
NavigationWidget::tabText(int index) const
{
    return m_tabBar->tabText(index);
}

void
NavigationWidget::setTabText(int index, const QString& text)
{
    m_tabBar->setTabText(index, text);
    if (index == m_tabBar->currentIndex())
    {
        setTitle(text);
    }
}

void
NavigationWidget::setTabIcon(int index, const QIcon& icon)
{
    m_tabBar->setTabIcon(index, icon);
}

void
NavigationWidget::setHistory(const QList<QAction*>& backActions, const QList<QAction*>& forwardActions)
{
    m_backButton->menu()->clear();
    m_backButton->menu()->addActions(backActions);

    m_forwardButton->menu()->clear();
    m_forwardButton->menu()->addActions(forwardActions);
}

void
NavigationWidget::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);

    QPainter painter(this);
    painter.setPen(UserInterfaceConfig::webBorderColor());

    if (UserInterfaceConfig::isZealLayout())
    {
        const int x1 = m_tabBar->geometry().left();
        const int x2 = m_tabBar->geometry().right();
        const int y  = m_tabBar->geometry().bottom();

        painter.drawLine(0, y, x1, y);
        painter.drawLine(x2, y, width(), y);
    }
    else
    {
        painter.drawRect(rect().adjusted(0, 0, -1, -1));
    }
}

}
