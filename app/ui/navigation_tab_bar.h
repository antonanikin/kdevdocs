/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QWidget>

namespace KDevDocs
{

class NavigationTabBar : public QWidget
{
    Q_OBJECT

public:
    explicit
    NavigationTabBar(QWidget* parent = nullptr);

    ~NavigationTabBar() override;

    int
    currentIndex() const;

    void
    setCurrentIndex(int index, bool doEmit);

    void
    addTab();

    void
    removeTab(int index);

    QString
    tabText(int index) const;

    void
    setTabText(int index, const QString& text);

    void
    setTabIcon(int index, const QIcon& icon);

    QSize
    sizeHint() const override;

signals:
    void
    currentChanged(int index);

    void
    tabAddRequested();

    void
    tabCloseRequested(int index);

protected:
    void
    resizeEvent(QResizeEvent* event) override;

    void
    paintEvent(QPaintEvent* event) override;

    void
    mousePressEvent(QMouseEvent* event) override;

    void
    mouseMoveEvent(QMouseEvent* event) override;

    void
    enterEvent(QEvent* event) override;

    void
    leaveEvent(QEvent* event) override;

private:
    void
    drawTab(int index, QPainter& painter);

    void
    updateTabsGeometry();

    void
    checkMousePosition(bool doUpdate);

private:
    int m_count = 0;
    int m_currentIndex = -1;
    int m_hoveredIndex = -1;
    bool m_isButtonHovered = false;

    QList<QString> m_text;
    QList<QIcon> m_icon;
    QList<QRect> m_rect;

    QList<QRect> m_buttonRect;

    QPoint m_mousePosition;

    const int m_iconSize;
    const int m_defaultTabWidth;

    int m_tabWidth;
    int m_tabHeight;
    int m_tabMargin;
};

}
