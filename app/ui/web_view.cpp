/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "web_view.h"

#include "action.h"
#include "globals.h"
#include "web_config.h"
#include "web_page.h"

#include <KGuiItem>
#include <KLocalizedString>
#include <KMessageBox>

#include <QContextMenuEvent>
#include <QMenu>
#include <QWebFrame>
#include <QWebPage>

#include <QApplication>
#include <QDesktopServices>

namespace KDevDocs
{

WebView::WebView(const QVector<QAction*>& actions, WebPage* page, QWidget* parent)
    : QWebView(parent)
    , m_actions(actions)
    , m_page(page)
{
    Q_ASSERT(page);

    setPage(page->m_page);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    connect(m_page, &WebPage::linkClicked, this, [this](const QUrl& url)
    {
        const auto modifiers = qApp->keyboardModifiers();
        openLink(
            url,
            modifiers.testFlag(Qt::ControlModifier) || modifiers.testFlag(Qt::ShiftModifier));
    });
}

WebView::~WebView() = default;

void
WebView::openLink(const QUrl& url, bool windowCreate)
{
    if (url.scheme() == QStringLiteral("file"))
    {
        QDesktopServices::openUrl(url);
        return;
    }

    // Try to open external link from local page
    if (isExternalUrl(url) && !isExternalUrl(m_page->url()))
    {
        int mode = m_page->config()->externalLinksMode();
        if (mode == WebConfig::ExternalLinksUserSelection)
        {
            // TODO add "Don't ask again" option?
            auto code = KMessageBox::questionYesNoCancel(
                qApp->activeWindow(),
                i18n("How do you want to open the external link?"),
                QString(),
                KGuiItem(i18n("Open in desktop browser")),
                KGuiItem(i18n("Open in KDevDocs")));

            if (code == KMessageBox::Yes)
            {
                mode = WebConfig::ExternalLinksDesktopBrowser;
            }
            else if (code == KMessageBox::No)
            {
                mode = WebConfig::ExternalLinksInternalBrowser;
            }
            else
            {
                return;
            }
        }

        if (mode == WebConfig::ExternalLinksDesktopBrowser)
        {
            QDesktopServices::openUrl(url);
            return;
        }
    }

    if (windowCreate)
    {
        emit windowCreateRequested(url);
    }
    else
    {
        m_page->load(url);
    }
}

inline void
addMenuAction(QMenu* menu, QAction* action)
{
    if (action && action->isEnabled())
    {
        menu->addAction(action);
    }
}

template<class Func>
inline void
addMenuAction(QMenu* menu, QAction* action, Func func)
{
    if (action && action->isEnabled())
    {
        menu->addAction(action);
        QObject::connect(action, &QAction::triggered, menu, func);
    }
}

inline void
addMenuSeparator(QMenu* menu)
{
    if (!menu->isEmpty())
    {
        menu->addSeparator();
    }
}

void
WebView::contextMenuEvent(QContextMenuEvent* event)
{
    // We create additional QWebView child primarily to override contextMenuEvent().
    // My tests with installing event filter for QWebView from WebViewWidget instance show
    // what QWebPage::CopyLinkToClipboard/CopyImageUrlToClipboard actions work is broken -
    // urls just not copied to clipboard :(.
    //
    // Looks like a bug or we need perform some non-obvious actions, so just process event here.

    // See WebPage::updateActions() for details.
    m_page->updateActions();

    const auto hitTestResult = page()->mainFrame()->hitTestContent(event->pos());

    if (hitTestResult.isContentEditable() ||
        hitTestResult.isContentSelected() ||
        hitTestResult.mediaUrl().isValid())
    {
        // Use standard context menu
        return QWebView::contextMenuEvent(event);
    }

    auto contextMenu = new QMenu(this);

    const auto linkUrl = hitTestResult.linkUrl();
    if (linkUrl.isValid())
    {
        addMenuSeparator(contextMenu);
        addMenuAction(contextMenu, m_actions[OpenLink], [this, linkUrl]()
        {
            openLink(linkUrl, false);
        });

        addMenuAction(contextMenu, pageAction(QWebPage::OpenLinkInNewWindow), [this, linkUrl]()
        {
            openLink(linkUrl, true);
        });

        const auto url = browserUrl(linkUrl);
        if (!url.isEmpty())
        {
            addMenuAction(contextMenu, m_actions[OpenLinkInBrowser], [url]()
            {
                QDesktopServices::openUrl(url);
            });
        }

        addMenuAction(contextMenu, pageAction(QWebPage::DownloadLinkToDisk));
        addMenuAction(contextMenu, pageAction(QWebPage::CopyLinkToClipboard));
    }

    const auto imageUrl = hitTestResult.imageUrl();
    if (imageUrl.isValid())
    {
        addMenuSeparator(contextMenu);
        addMenuAction(contextMenu, m_actions[OpenImage], [this, imageUrl]()
        {
            openLink(imageUrl, false);
        });

        addMenuAction(contextMenu, pageAction(QWebPage::OpenImageInNewWindow), [this, imageUrl]()
        {
            openLink(imageUrl, true);
        });

        const auto url = browserUrl(imageUrl);
        if (!url.isEmpty())
        {
            addMenuAction(contextMenu, m_actions[OpenImageInBrowser], [url]()
            {
                QDesktopServices::openUrl(url);
            });
        }

        addMenuAction(contextMenu, pageAction(QWebPage::DownloadImageToDisk));
        addMenuAction(contextMenu, pageAction(QWebPage::CopyImageToClipboard));
        addMenuAction(contextMenu, pageAction(QWebPage::CopyImageUrlToClipboard));
    }

    if (!linkUrl.isValid() && !imageUrl.isValid())
    {
        addMenuAction(contextMenu, m_actions[GoBack]);
        addMenuAction(contextMenu, m_actions[GoForward]);

        addMenuAction(contextMenu, pageAction(QWebPage::Stop));
        addMenuAction(contextMenu, pageAction(QWebPage::Reload));

        if (!browserUrl(m_page->url()).isEmpty())
        {
            addMenuSeparator(contextMenu);
            addMenuAction(contextMenu, m_actions[OpenPageInBrowser]);
        }
    }

    if (!contextMenu->isEmpty())
    {
        contextMenu->exec(event->globalPos());
    }
    delete contextMenu;
}

}
