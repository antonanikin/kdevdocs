/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "web_history.h"

#include "icon_manager.h"
#include "web_page.h"

#include <QAction>

namespace KDevDocs
{

WebHistory::WebHistory(WebPage* page)
    : QObject(page)
    , m_page(page)
    , m_index(-1)
{
    Q_ASSERT(page);

    connect(m_page, &WebPage::titleChanged, this, [this]()
    {
        if (m_index >= 0)
        {
            m_actions[m_index]->setText(m_page->title());
        }
    });

    connect(m_page, &WebPage::iconChanged, this, [this]()
    {
        if (m_index >= 0)
        {
            m_actions[m_index]->setIcon(m_page->icon());
        }
    });

    connect(IconManager::self(), &IconManager::webPageIconChanged, this, [this](const QUrl& url)
    {
        auto icon = IconManager::self()->webPageIcon(url);
        for (int i = 0; i < m_items.size(); ++i)
        {
            if (m_items[i].url() == url)
            {
                m_actions[i]->setIcon(icon);
            }
        }
    });
}

WebHistory::~WebHistory() = default;

bool
WebHistory::canGoBack() const
{
    return m_index > 0;
}

bool
WebHistory::canGoForward() const
{
    return m_index < (m_items.size() - 1);
}

void
WebHistory::back()
{
    if (canGoBack())
    {
        // If the page start loading but doesn't change it's url (no history changes)
        // then we just stops loading.
        if (m_page->m_state == WebPage::LoadStarted)
        {
            m_page->stop();
            return;
        }

        setIndex(m_index - 1);
    }
}

void
WebHistory::forward()
{
    if (canGoForward())
    {
        // see back()
        if (m_page->m_state == WebPage::LoadStarted)
        {
            m_page->stop();
            return;
        }

        setIndex(m_index + 1);
    }
}

void
WebHistory::setIndex(int index)
{
    if (m_index == index || m_index < 0 || m_index >= m_items.size())
    {
        return;
    }

    m_index = index;
    m_page->m_page->history()->goToItem(m_items[m_index]);

    updateActions();
}

void
WebHistory::insert()
{
    const auto currentUrl = (m_index >= 0) ? m_items[m_index].url() : QUrl();
    if (currentUrl == m_page->url())
    {
        // Block history update if current item's url equals to page's url.
        // This happens when we use back()/forward() methods.
        return;
    }

    auto item = m_page->m_page->history()->currentItem();
    Q_ASSERT(item.url() == m_page->url());

    while (canGoForward())
    {
        m_items.removeLast();
    }

    ++m_index;
    m_items += item;

    auto action = new QAction(m_page->title(), this);
    connect(action, &QAction::triggered, this, [this, action]()
    {
        setIndex(m_actions.indexOf(action));
    });

    m_actions += action;
    updateActions();
}

QList<QAction*>
WebHistory::allActions() const
{
    QList<QAction*> actions;
    for (int i = m_items.size() - 1; i >= 0; --i)
    {
        actions += m_actions[i];
    }
    return actions;
}

QList<QAction*>
WebHistory::backActions() const
{
    QList<QAction*> actions;
    for (int i = m_index - 1; i >= 0; --i)
    {
        actions += m_actions[i];
    }
    return actions;
}

QList<QAction*>
WebHistory::forwardActions() const
{
    QList<QAction*> actions;
    for (int i = m_index + 1; i < m_items.size(); ++i)
    {
        actions += m_actions[i];
    }
    return actions;
}

void
WebHistory::updateActions()
{
    for (int i = 0; i < m_items.count(); ++i)
    {
        auto font = m_actions[i]->font();
        font.setBold(i == m_index);
        m_actions[i]->setFont(font);
    }

    emit changed();
}

}
