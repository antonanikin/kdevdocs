/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "goup_widget.h"

#include <QUrl>

class QLabel;
class QTimer;

class KMessageWidget;

namespace KDevDocs
{

class WebHistory;
class WebPage;
class WebPageFindWidget;
class WebView;

class WebViewWidget : public GoUpWidget
{
    Q_OBJECT

public:
    WebViewWidget(const QVector<QAction*>& actions, QWidget* parent);

    ~WebViewWidget() override;

    void
    load(const QUrl& url, bool focusView);

    bool
    goUp() override;

    void
    zoomIn();

    void
    zoomOut();

    void
    zoomReset();

    QUrl
    url() const;

    WebHistory*
    history() const;

    WebPageFindWidget*
    findWidget() const;

signals:
    void
    loadStarted();

    void
    loadFinished();

    void
    iconChanged(const QIcon& icon);

    void
    titleChanged(const QString& title);

    void
    windowCreateRequested(const QUrl& url);

protected:
    void
    paintEvent(QPaintEvent* event) override;

    void
    resizeEvent(QResizeEvent* event) override;

private:
    void
    updateLayout();

    void
    setBufferEnabled(bool enabled);

    void
    onLoadStarted();

    void
    onLoadFinished();

    void
    showMessage(const QString& text, const QIcon& icon);

    void
    updateMessagePosition();

private:
    int m_state;

    bool m_bufferEnabled;
    QLabel* m_buffer;

    QTimer* m_loadingTimer;

    WebPage* m_page;
    WebPageFindWidget* m_pageFindWidget;

    WebView* m_view;

    KMessageWidget* m_messageWidget;
    QTimer* m_messageTimer;
};

}
