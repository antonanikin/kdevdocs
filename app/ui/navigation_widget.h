/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QWidget>

class QLabel;
class QToolButton;

namespace KDevDocs
{

class NavigationTabBar;
class NavigationTitleLabel;

class NavigationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit
    NavigationWidget(const QVector<QAction*>& actions, QWidget* parent = nullptr);

    ~NavigationWidget() override = default;

    void
    addTab();

    void
    removeTab(int index);

    void
    setCurrentIndex(int index);

    QString
    tabText(int index) const;

    void
    setTabText(int index, const QString& text);

    void
    setTabIcon(int index, const QIcon& icon);

    void
    setHistory(const QList<QAction*>& backActions, const QList<QAction*>& forwardActions);

signals:
    void
    currentChanged(int index);

    void
    tabAddRequested();

    void
    tabCloseRequested(int index);

protected:
    void
    paintEvent(QPaintEvent* event) override;

private:
    void
    setTitle(const QString& title);

    void
    updateLayout();

    void
    updatePalette();

private:
    QToolButton* m_backButton;
    QToolButton* m_forwardButton;
    QToolButton* m_bookmarkButton;
    QToolButton* m_browserButton;
    QToolButton* m_menuButton;
    NavigationTabBar* m_tabBar;
    NavigationTitleLabel* m_titleLabel;
};

}
