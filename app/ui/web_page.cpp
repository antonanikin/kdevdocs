/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "web_page.h"

#include "action.h"
#include "icon_manager.h"
#include "network_access_manager.h"
#include "user_interface_config.h"
#include "web_config.h"
#include "web_history.h"

#include <KLocalizedString>
#include <KStandardAction>

#include <QFontInfo>
#include <QLoggingCategory>
#include <QTimer>
#include <QWebFrame>
#include <QWebHistory>
#include <QWebSettings>
#include <QtWebKitVersion>

namespace KDevDocs
{

Q_LOGGING_CATEGORY(WEB_PAGE, "kdevdocs.web.page");

class WebPageNetworkAccessManager : public QNetworkAccessManager
{
    Q_OBJECT

public:
    explicit
    WebPageNetworkAccessManager(WebPage* page)
        : QNetworkAccessManager(page)
        , m_page(page)
    {
    }

    ~WebPageNetworkAccessManager() override  = default;

protected:
    QNetworkReply*
    createRequest(Operation op, const QNetworkRequest& request, QIODevice* outgoingData) override
    {
        return NetworkAccessManager::self()->createRequest(
            m_page->requestedUrl(),
            m_page->config(),
            op,
            request,
            outgoingData
        );
    }

private:
    WebPage* m_page;
};

WebPage::WebPage(const QVector<QAction*>& actions, QObject* parent)
    : QObject(parent)
    , m_actions(actions)
    , m_page(new QWebPage(this))
    , m_config(WebConfig::globalConfig())
    , m_history(new WebHistory(this))
{
    auto networkAccessManager = new WebPageNetworkAccessManager(this);
    m_page->setNetworkAccessManager(networkAccessManager);

    m_page->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
    connect(m_page, &QWebPage::linkClicked, this, &WebPage::linkClicked);

    connect(m_page->mainFrame(), &QWebFrame::loadStarted, this, &WebPage::onLoadStarted);
    connect(m_page->mainFrame(), &QWebFrame::loadFinished, this, &WebPage::onLoadFinished);
    connect(m_page->mainFrame(), &QWebFrame::urlChanged, this, &WebPage::onUrlChanged);
    connect(m_page->mainFrame(), &QWebFrame::titleChanged, this, &WebPage::titleChanged);

    connect(m_page->mainFrame(), &QWebFrame::iconChanged, this, [this]()
    {
        if (isExternalUrl(url()))
        {
            const auto icon = m_page->mainFrame()->icon();
            if (!icon.isNull())
            {
                setIcon(icon);
                IconManager::self()->setWebPageIcon(url(), icon);
            }
        }
    });

    connect(m_page, &QWebPage::downloadRequested, this, [/*this*/](const QNetworkRequest& /*request*/)
    {
        // TODO implement with use KIO
    });

    connect(WebConfig::globalConfig(), &WebConfig::changed, this, &WebPage::onConfigChanged);

    updateSettings();
    setupActions();
}

WebPage::~WebPage() = default;

void
WebPage::load(const QUrl& url)
{
    if (this->url() != url)
    {
        // QWebFrame::load() doesn't emit loadStarted() signal (handled with onLoadStarted() slot)
        // when we open page-internal link. For such case we will receive only urlChanged() signal,
        // so to it's correct handling with onUrlChanged() slot we should reset m_urlUpdated value.
        m_urlUpdated = false;

        m_page->mainFrame()->load(url);
    }
}

void
WebPage::stop()
{
    m_page->triggerAction(QWebPage::Stop);
}

QString
WebPage::title() const
{
    auto frameTitle = m_page->mainFrame()->title();
    return frameTitle.isEmpty() ? url().toDisplayString() : frameTitle;
}

QUrl
WebPage::url() const
{
    return m_page->mainFrame()->url();
}

QUrl
WebPage::requestedUrl() const
{
    return m_page->mainFrame()->requestedUrl();
}

QIcon
WebPage::icon() const
{
    return m_icon;
}

void
WebPage::setIcon(const QIcon& icon)
{
    m_icon = icon;
    emit iconChanged();
}

WebConfig*
WebPage::config() const
{
    return m_config;
}

WebHistory*
WebPage::history() const
{
    return m_history;
}

void
WebPage::onUrlChanged(const QUrl& url)
{
    // Here we update our state/icon/history/etc and do this ONLY ONCE. This logic is caused by
    // the fact that some pages uses strange hacks and changes url multiple times with using JS.
    // Sample - any Qt html(qch) documentation page with extra JS code for "url fixing":
    //
    // loading "qtglobal.html#Q_DECL_CONSTEXPR" leads to:
    //
    // * firstly url changed to "qtglobal.html#Q_DECL_CONSTEXPR" (OK),
    // * then to "qtglobal.html#" (BAD)
    // * and finally back to original "qtglobal.html#Q_DECL_CONSTEXPR" (OK).
    //
    // Such extra url changes produce "garbage" links in standard history instance (QWebHistory).
    // So we should do hand-made history manage.

    if (m_urlUpdated)
    {
        return;
    }
    m_urlUpdated = true;
    m_state = UrlChanged;

    m_history->insert();
    setIcon(IconManager::self()->webPageIcon(url));

    auto config = WebConfig::config(url);
    if (m_config != config)
    {
        m_config = config;
        updateSettings();
    }
}

void
WebPage::onConfigChanged(KDevDocs::WebConfig* config)
{
    if (m_config == config)
    {
        updateSettings();
        m_page->triggerAction(QWebPage::Reload);
        return;
    }

    for (const auto& item : m_history->m_items)
    {
        if (WebConfig::config(item.url()) == config)
        {
            m_changedConfigs.insert(config);
            return;
        }
    }
}

void
WebPage::onLoadStarted()
{
    m_state = LoadStarted;
    m_urlUpdated = false;
    emit loadStarted();
}

void
WebPage::onLoadFinished()
{
    if (m_changedConfigs.contains(m_config))
    {
        m_changedConfigs.remove(m_config);
        onConfigChanged(m_config);
        return;
    }

    int delay = 0;
    if (url().hasFragment())
    {
        // In some cases (web config font changes for example) after the (re)loading we will
        // have wrong page position. So we do force scrolling to url's fragment.
        // FIXME do this only if user don't change position after the page loading.
        // https://stackoverflow.com/questions/8293258/catch-the-signal-when-qwebview-was-scrolled-to-the-end
        m_page->mainFrame()->scrollToAnchor(url().fragment());

        // Here we do small pause to hide scrolling process from user eyes :) Then we emit
        // signal which will be catched in WebView::onLoadFinished() method, which will show
        // final result.
        delay = 50;
    }

    QTimer::singleShot(delay, this, [this]()
    {
        m_state = LoadFinished;
        emit loadFinished();
    });
}

inline void
setFontFamily(QWebSettings* s, QWebSettings::FontFamily webFamily, const QString& family)
{
    // Make detection of "real family" for selected font. For example if we
    // pass "Sans" it will be replaced with real system font family (e.g. Ubuntu).
    // This fixes wrong font selection for some cases.
    s->setFontFamily(webFamily, QFontInfo(QFont(family)).family());
}

void
WebPage::updateSettings()
{
    auto s = m_page->settings();

    if (m_config->proportionalType() == 0)
    {
        setFontFamily(s, QWebSettings::StandardFont, m_config->sansSerifFamily());
    }
    else
    {
        setFontFamily(s, QWebSettings::StandardFont, m_config->serifFamily());
    }

    setFontFamily(s, QWebSettings::SansSerifFont, m_config->sansSerifFamily());
    setFontFamily(s, QWebSettings::SerifFont, m_config->serifFamily());
    setFontFamily(s, QWebSettings::FixedFont, m_config->monospaceFamily());

    s->setFontSize(QWebSettings::DefaultFontSize, m_config->proportionalSize());
    s->setFontSize(QWebSettings::DefaultFixedFontSize, m_config->monospaceSize());
    s->setFontSize(QWebSettings::MinimumFontSize, m_config->minimalSize());

    s->setAttribute(QWebSettings::JavascriptEnabled, m_config->javascriptEnabled());
    s->setAttribute(QWebSettings::ScrollAnimatorEnabled, m_config->scrollAnimatorEnabled());
}

void
WebPage::find(const QString& text, FindFlags options)
{
    const auto caseSensitivity =
        options.testFlag(QWebPage::FindCaseSensitively) ?
        Qt::CaseSensitive :
        Qt::CaseInsensitive;

    const bool textChanged = (m_findText.compare(text, caseSensitivity) != 0);

    const bool caseSensitivityChanged =
        m_findOptions.testFlag(QWebPage::FindCaseSensitively) !=
        options.testFlag(QWebPage::FindCaseSensitively);

    const bool highlightChanged =
        m_findOptions.testFlag(QWebPage::HighlightAllOccurrences) !=
        options.testFlag(QWebPage::HighlightAllOccurrences);

    if (textChanged || caseSensitivityChanged || highlightChanged)
    {
        if (m_findOptions.testFlag(QWebPage::HighlightAllOccurrences))
        {
            // Clear old "highlight all" results
            m_page->findText(QString(), QWebPage::HighlightAllOccurrences);
        }

        if (options.testFlag(QWebPage::HighlightAllOccurrences))
        {
            // Set new "highlight all" results
            m_page->findText(text, options);
        }
    }

    m_findText = text;
    m_findOptions = options;

    if (caseSensitivityChanged)
    {
        if (m_page->selectedText().compare(text, caseSensitivity) == 0)
        {
            // Don't search next occurrence if current highlighting matches the text
            return;
        }
    }
    else if (highlightChanged && !textChanged)
    {
        // Don't search next occurrence if only "highlight all" option was changed
        return;
    }

    FindResult findResult = text.isEmpty() ? FindNothing : FindMatch;

    // Switch to single-word mode since we are already set "highlight all" results
    options &= ~QWebPage::HighlightAllOccurrences;

    // Always run findText() to clear old results and highlight new text
    bool textResult = m_page->findText(text, options) || text.isEmpty();

    if (!textResult)
    {
        // Clear current results to start wrapped search
        m_page->findText(QString());

        textResult = m_page->findText(text, options);
        if (textResult)
        {
            findResult =
                options.testFlag(QWebPage::FindBackward) ?
                FindWrappedBackward :
                FindWrappedForward;
        }
    }

    if (!textResult)
    {
        findResult = FindMismatch;

        // Clear all highlightings
        m_page->findText(QString(), QWebPage::HighlightAllOccurrences);
        m_page->findText(QString());
    }

    emit findFinished(findResult);

}

void
WebPage::setupActions()
{
    // Since (Qt)WebKit periodically changes (reset to defaults) actions text (icon and shortcut
    // are persistent) we save our custom text in m_actionText hash. When actions are needed
    // (context menu show, for example) we should call updateActions() method to restore our settings.

    using WebAction = QWebPage::WebAction;
    using StandardAction = KStandardAction::StandardAction;

    auto setupAction = [this](WebAction type, const QIcon& icon, const QString& text = QString())
    {
        if (auto action = m_page->action(type))
        {
            action->setIcon(icon);
            if (!text.isEmpty())
            {
                action->setText(text);
                m_actionText[type] = text;
            }
        }
    };

    auto setupStandardAction = [this](WebAction type, StandardAction standardType)
    {
        if (auto action = m_page->action(type))
        {
            auto standardAction = KStandardAction::create(standardType, nullptr, nullptr, nullptr);

            action->setIcon(standardAction->icon());
            action->setText(standardAction->text());
            action->setShortcut(standardAction->shortcut());

            m_actionText[type] = standardAction->text();
            delete standardAction;
        }
    };

    // Editor actions, placed first since copy icon used later
    setupStandardAction(QWebPage::Cut, KStandardAction::Cut);
    setupStandardAction(QWebPage::Copy, KStandardAction::Copy);
    setupStandardAction(QWebPage::Paste, KStandardAction::Paste);
    setupStandardAction(QWebPage::Undo, KStandardAction::Undo);
    setupStandardAction(QWebPage::Redo, KStandardAction::Redo);

    setupStandardAction(QWebPage::SelectAll, KStandardAction::SelectAll);
#if (QTWEBKIT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
    setupStandardAction(QWebPage::Unselect, KStandardAction::Deselect);
#endif

    const auto copyIcon = m_page->action(QWebPage::Copy)->icon();
    const auto newTabIcon = m_actions[TabAdd]->icon();
    const auto downloadIcon = QIcon::fromTheme("document-save");

    // Link actions
    setupAction(QWebPage::OpenLinkInNewWindow, newTabIcon, i18n("Open Link in New Tab"));
    setupAction(QWebPage::DownloadLinkToDisk, downloadIcon, i18n("Save Link As..."));
    setupAction(QWebPage::CopyLinkToClipboard, copyIcon, i18n("Copy Link Address"));

    // Image actions
    setupAction(QWebPage::OpenImageInNewWindow, newTabIcon, i18n("Show Image in New Tab"));
    setupAction(QWebPage::DownloadImageToDisk, downloadIcon, i18n("Save Image As..."));
    setupAction(QWebPage::CopyImageToClipboard, copyIcon);
    setupAction(QWebPage::CopyImageUrlToClipboard, copyIcon, i18n("Copy Image Address"));

    // Other actions
    setupAction(QWebPage::Stop, QIcon::fromTheme("process-stop"));
    setupAction(QWebPage::Reload, QIcon::fromTheme("view-refresh"));

    setupAction(QWebPage::OpenFrameInNewWindow, newTabIcon);

    setupAction(QWebPage::ToggleBold, QIcon::fromTheme("format-text-bold"));
    setupAction(QWebPage::ToggleItalic, QIcon::fromTheme("format-text-italic"));
    setupAction(QWebPage::ToggleUnderline, QIcon::fromTheme("format-text-underline"));
}

void
WebPage::updateActions()
{
    for (auto it = m_actionText.constBegin(); it != m_actionText.constEnd(); ++it)
    {
        if (auto action = m_page->action(it.key()))
        {
            action->setText(it.value());
        }
    }
}

}

#include "web_page.moc"
