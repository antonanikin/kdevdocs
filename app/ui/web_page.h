/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QSet>
#include <QWebPage>

class QWebView;

namespace KDevDocs
{

class WebConfig;
class WebHistory;

class WebPage : public QObject
{
    Q_OBJECT

public:
    using FindFlags = QWebPage::FindFlags;

    enum FindResult
    {
        FindMatch,
        FindWrappedForward,
        FindWrappedBackward,
        FindMismatch,
        FindNothing
    };

    explicit
    WebPage(const QVector<QAction*>& actions, QObject* parent = nullptr);

    ~WebPage() override;

    void
    load(const QUrl& url);

    void
    stop();

    QString
    title() const;

    QUrl
    url() const;

    QUrl
    requestedUrl() const;

    QIcon
    icon() const;

    WebConfig*
    config() const;

    WebHistory*
    history() const;

    void
    find(const QString& text, FindFlags options);

signals:
    void
    linkClicked(const QUrl &url);

    void
    findFinished(FindResult result);

    void
    loadStarted();

    void
    loadFinished();

    void
    titleChanged();

    void
    iconChanged();

private:
    void
    setIcon(const QIcon& icon);

    void
    updateSettings();

    void
    setupActions();

    void
    updateActions();

    void
    onUrlChanged(const QUrl& url);

    void
    onConfigChanged(WebConfig* config);

    void
    onLoadStarted();

    void
    onLoadFinished();

private:
    const QVector<QAction*>& m_actions;
    QWebPage* m_page = nullptr;

    enum State
    {
        LoadStarted,
        UrlChanged,
        LoadFinished
    }
    m_state = LoadFinished;

    QIcon m_icon;
    bool m_urlUpdated;

    QString m_findText;
    FindFlags m_findOptions;

    WebConfig* m_config = nullptr;
    QSet<WebConfig*> m_changedConfigs;

    WebHistory* m_history = nullptr;

    QHash<QWebPage::WebAction, QString> m_actionText;

    friend class WebHistory;
    friend class WebView;
};

}
