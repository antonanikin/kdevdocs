/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "web_page_find_widget.h"

#include "action.h"
#include "line_edit.h"

#include <KLocalizedString>

#include <QHBoxLayout>
#include <QKeyEvent>
#include <QToolButton>

namespace KDevDocs
{

WebPageFindWidget::WebPageFindWidget(const QVector<QAction*>& actions, WebPage* page, QWidget* parent)
    : GoUpWidget(parent)
    , m_page(page)
    , m_edit(new LineEdit(this))
    , m_highlightAllButton(new QToolButton)
    , m_caseButton(new QToolButton)
{
    Q_ASSERT(page);
    connect(m_page, &WebPage::findFinished, this, &WebPageFindWidget::setResult);

    setFocusProxy(m_edit);
    m_edit->installEventFilter(this);
    connect(m_edit, &QLineEdit::textChanged, this, &WebPageFindWidget::onTextChanged);

    auto prevButton = new QToolButton;
    prevButton->setAutoRaise(true);
    prevButton->setDefaultAction(actions[FindPrev]);

    auto nextButton = new QToolButton;
    nextButton->setAutoRaise(true);
    nextButton->setDefaultAction(actions[FindNext]);

    m_highlightAllButton->setText(i18n("Highlight All"));
    m_highlightAllButton->setAutoRaise(true);
    m_highlightAllButton->setCheckable(true);
    connect(m_highlightAllButton, &QToolButton::clicked, this, &WebPageFindWidget::findNext);

    m_caseButton->setText(i18n("Match Case"));
    m_caseButton->setAutoRaise(true);
    m_caseButton->setCheckable(true);
    connect(m_caseButton, &QToolButton::clicked, this, &WebPageFindWidget::findNext);

    auto layout = new QHBoxLayout(this);
    layout->addWidget(m_edit);
    layout->addWidget(nextButton);
    layout->addWidget(prevButton);
    layout->addWidget(m_highlightAllButton);
    layout->addWidget(m_caseButton);
}

WebPageFindWidget::~WebPageFindWidget() = default;

void
WebPageFindWidget::setResult(WebPage::FindResult result)
{
    switch (result)
    {
        case WebPage::FindMatch:
        case WebPage::FindWrappedBackward:
        case WebPage::FindWrappedForward:
            m_edit->setPalette(LineEdit::PositivePalette);
            break;

        case WebPage::FindMismatch:
            m_edit->setPalette(LineEdit::NegativePalette);
            break;

        case WebPage::FindNothing:
            m_edit->setPalette(LineEdit::NeutralPalette);
            break;
    }
}

void
WebPageFindWidget::clear()
{
    m_edit->clear();
    findNext();
}

void
WebPageFindWidget::open()
{
    show();
    setFocus();
}

void
WebPageFindWidget::findPrev()
{
    find(false);
}

void
WebPageFindWidget::findNext()
{
    find(true);
}

void
WebPageFindWidget::onTextChanged(const QString& text)
{
    const auto caseSensitivity = m_caseButton->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive;
    const bool textChanged = (m_text.compare(text, caseSensitivity) != 0);

    m_text = text;
    if (textChanged)
    {
        findNext();
    }
}

void
WebPageFindWidget::find(bool next)
{
    QWebPage::FindFlags options;

    if (!next)
    {
        options |= QWebPage::FindBackward;
    }

    if (m_highlightAllButton->isChecked())
    {
        options |= QWebPage::HighlightAllOccurrences;
    }

    if (m_caseButton->isChecked())
    {
        options |= QWebPage::FindCaseSensitively;
    }

    m_page->find(m_edit->text(), options);
}

bool
WebPageFindWidget::goUp()
{
    if (!hasFocus())
    {
        setFocus();
        return false;
    }

    if (m_edit->text().isEmpty())
    {
        return true;
    }

    clear();
    return false;
}

bool
WebPageFindWidget::eventFilter(QObject* obj, QEvent* event)
{
    if (obj == m_edit && event->type() == QEvent::KeyPress)
    {
        auto keyEvent = static_cast<QKeyEvent*>(event);
        if (keyEvent->key() == Qt::Key_Return || keyEvent->key() == Qt::Key_Enter)
        {
            if (keyEvent->modifiers().testFlag(Qt::ShiftModifier))
            {
                findPrev();
            }
            else
            {
                findNext();
            }
        }
    }

    return GoUpWidget::eventFilter(obj, event);
}

}
