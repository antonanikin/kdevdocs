/****************************************************************************
**
** Copyright (C) 2015-2016 Oleg Shparber
** Copyright (C) 2013-2014 Jerzy Kozera
** Contact: https://go.zealdocs.org/l/contact
**
** This file is part of Zeal.
**
** Zeal is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Zeal is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Zeal. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "search_delegate.h"

#include "qtcompat.h"
#include "search_config.h"

#include <QAbstractItemView>
#include <QApplication>
#include <QFontMetrics>
#include <QHelpEvent>
#include <QPainter>
#include <QToolTip>

namespace KDevDocs
{

SearchDelegate::SearchDelegate(QObject* parent, const QList<int>& decorationRoles)
    : QStyledItemDelegate(parent)
    , m_decorationRoles(decorationRoles)
{
}

SearchDelegate::~SearchDelegate() = default;

// FIXME clean the code
bool
SearchDelegate::helpEvent(QHelpEvent* event,
                              QAbstractItemView* view,
                              const QStyleOptionViewItem& option,
                              const QModelIndex &index)
{
    if (event->type() != QEvent::ToolTip)
    {
        return QStyledItemDelegate::helpEvent(event, view, option, index);
    }

    if (sizeHint(option, index).width() < view->visualRect(index).width())
    {
        QToolTip::hideText();
        return QStyledItemDelegate::helpEvent(event, view, option, index);
    }

    QToolTip::showText(event->globalPos(), index.data().toString(), view);
    return true;
}

void
SearchDelegate::paint(QPainter* painter,
                          const QStyleOptionViewItem& option,
                          const QModelIndex& index) const
{
    QStyleOptionViewItem opt(option);

    painter->setFont(index.data(Qt::FontRole).value<QFont>());

    auto style = opt.widget->style();
    QRect textRect;

    // Remove underline for selected items
    opt.state &= ~QStyle::State_HasFocus;

    opt.features |= QStyleOptionViewItem::HasDecoration;
    for (int role : m_decorationRoles)
    {
        opt.icon = index.data(role).value<QIcon>();
        style->drawControl(QStyle::CE_ItemViewItem, &opt, painter, opt.widget);

        textRect = style->subElementRect(QStyle::SE_ItemViewItemText, &opt, opt.widget);
        opt.rect.setLeft(textRect.left());
    }

    auto text = index.data(Qt::DisplayRole).toString();
    if (text.isEmpty())
    {
        return;
    }

    const int xMargin = style->pixelMetric(QStyle::PM_FocusFrameHMargin, &opt, opt.widget) + 1;
    const int yMargin = (opt.rect.height() - painter->fontMetrics().height()) / 2;
    textRect = opt.rect.adjusted(xMargin, yMargin, -xMargin, -yMargin);

    QTextOption textOption(Qt::AlignLeft | Qt::AlignVCenter);
    textOption.setTextDirection(painter->layoutDirection());
    textOption.setWrapMode(QTextOption::NoWrap);

    painter->setPen(textColor(opt));
    painter->drawText(textRect, text, textOption);

    if (m_highlight.isEmpty())
    {
        return;
    }

    QRect highlightRect;

    painter->setPen(SearchConfig::highlightTextColor());
    for (int start = 0;;)
    {
        const int matchIndex = text.indexOf(m_highlight, start, Qt::CaseInsensitive);
        if (matchIndex < 0)
        {
            break;
        }

        auto match = text.mid(matchIndex, m_highlight.length());
        int prefixWidth = horizontalAdvance(painter->fontMetrics(), text.left(matchIndex));
        int highlightWidth = horizontalAdvance(painter->fontMetrics(), match);

        highlightRect = textRect.adjusted(prefixWidth, 0, 0, 0);
        highlightRect.setWidth(highlightWidth);
        painter->fillRect(highlightRect, SearchConfig::highlightBackroundColor());

        highlightRect.adjust(0, 0, 1, 0);
        painter->drawText(highlightRect, match, textOption);

        start = matchIndex + m_highlight.length();
    }
}

QColor
SearchDelegate::textColor(const QStyleOptionViewItem& option) const
{
    auto colorGroup = option.state.testFlag(QStyle::State_Active) ? QPalette::Normal : QPalette::Inactive;
    if (option.state.testFlag(QStyle::State_Selected))
    {
        return option.palette.color(colorGroup, QPalette::HighlightedText);
    }
    else
    {
        return option.palette.color(colorGroup, QPalette::Text);
    }
}

// FIXME clean the code
QSize
SearchDelegate::sizeHint(const QStyleOptionViewItem &option,
                             const QModelIndex &index) const
{
    QStyleOptionViewItem opt(option);

    opt.font = index.data(Qt::FontRole).value<QFont>();
    opt.fontMetrics = QFontMetrics(opt.font);

    auto style = opt.widget->style();

    QSize size = QStyledItemDelegate::sizeHint(opt, index);
    size.setWidth(0);

    const int margin = style->pixelMetric(QStyle::PM_FocusFrameHMargin, &opt, opt.widget) + 1;

    // Find decoration roles with data present.
    QList<int> roles;
    for (int role : m_decorationRoles)
    {
        if (!index.data(role).isNull())
        {
            roles.append(role);
        }
    }

    if (!roles.isEmpty())
    {
        const QIcon icon = index.data(roles.first()).value<QIcon>();
        const QSize actualSize = icon.actualSize(opt.decorationSize);
        const int decorationWidth = std::min(opt.decorationSize.width(), actualSize.width());
        size.rwidth() = (decorationWidth + margin) * roles.size() + margin;
    }

    size.rwidth() += horizontalAdvance(opt.fontMetrics, index.data().toString()) + margin * 2;
    return size;
}

void
SearchDelegate::setHighlight(const QString& text)
{
    m_highlight = text;
}

}
