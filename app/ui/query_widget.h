/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QWidget>

class QListView;
class QStackedLayout;
class QTreeView;

namespace KDevDocs
{

class InstalledDocset;
class QueryEdit;

class QueryWidget : public QWidget
{
    Q_OBJECT

public:
    explicit
    QueryWidget(QWidget* parent = nullptr);

    ~QueryWidget() override = default;

    void
    query(const QString& queryString);

    void
    goUp();

signals:
    void
    urlToLoad(const QUrl& url, bool focusView);

protected:
    void
    paintEvent(QPaintEvent* event) override;

private:
    void
    updateLayout();

    void
    updatePalette();

private:
    QTreeView* m_contentsView;
    QListView* m_resultsView;
    QListView* m_sourcesView;
    QueryEdit* m_queryEdit;
    QWidget* m_editWidget;
    QStackedLayout* m_viewsLayout;

    InstalledDocset* m_contextDocset;
};

}
