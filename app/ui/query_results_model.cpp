/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "query_results_model.h"

#include "globals.h"
#include "index_item.h"
#include "search_config.h"
#include "search_delegate.h"
#include "utils.h"

#include <KLocalizedString>

#include <QDir>
#include <QProcess>
#include <QRegularExpression>
#include <QUrl>
#include <QIcon>

namespace KDevDocs
{

QueryResultsModel::QueryResultsModel(QObject* parent)
    : QAbstractListModel(parent)
{
    auto emitChanged = [this]()
    {
        emit dataChanged(QModelIndex(), QModelIndex());
    };

    connect(SearchConfig::self(), &SearchConfig::fontChanged, this, emitChanged);
    connect(SearchConfig::self(), &SearchConfig::paletteChanged, this, emitChanged);
}

QueryResultsModel::~QueryResultsModel() = default;

const Query&
QueryResultsModel::currentQuery() const
{
    return m_query;
}

bool
QueryResultsModel::setQuery(const Query& query)
{
    if (query == m_query)
    {
        return true;
    }
    m_query = query;

    beginResetModel();
    m_filteredItems = m_query.exec();
    endResetModel();

    return !m_filteredItems.isEmpty();
}

int
QueryResultsModel::rowCount(const QModelIndex& /*parent*/) const
{
    return m_filteredItems.size();
}

QModelIndex
QueryResultsModel::index(int row, int column, const QModelIndex& /*parent*/) const
{
    return createIndex(row, column, (void*)&(m_filteredItems.at(row))); // FIXME old-style conversion
}

QVariant
QueryResultsModel::data(const QModelIndex& index, int role) const
{
    auto item = m_filteredItems.at(index.row()).indexItem();
    Q_ASSERT(item);

    if (role == Qt::DisplayRole)
    {
        return item->name();
    }

    if (role == Qt::DecorationRole)
    {
        return item->icon();
    }

    if (role == DocsetIconRole)
    {
        return item->docsetIcon();
    }

    if (role == UrlRole)
    {
        return item->url();
    }

    if (role == Qt::FontRole && SearchConfig::useFixedFont())
    {
        return SearchConfig::fixedFont();
    }

    return QVariant();
}

}
