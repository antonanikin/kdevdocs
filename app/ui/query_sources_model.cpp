/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "query_sources_model.h"

#include "core.h"
#include "globals.h"
#include "installed_docset.h"
#include "search_config.h"

namespace KDevDocs
{

QuerySourcesModel::QuerySourcesModel(QObject* parent)
    : QSortFilterProxyModel(parent)
    , m_currentDocset(nullptr)
{
    auto emitChanged = [this]()
    {
        emit dataChanged(QModelIndex(), QModelIndex());
    };

    connect(SearchConfig::self(), &SearchConfig::fontChanged, this, emitChanged);
    connect(SearchConfig::self(), &SearchConfig::paletteChanged, this, emitChanged);

    setSourceModel(Core::self()->contentsModel());
}

void
QuerySourcesModel::setQuery(const Query& query)
{
    m_query = query;
}

void
QuerySourcesModel::update(InstalledDocset* currentDocset, const QString& namePrefix)
{
    m_currentDocset = currentDocset;
    m_namePrefix = namePrefix;

    int previousRowCount = rowCount();
    setFilterRegExp(namePrefix);

    // run force update for fix case when new filter doesn't change
    // model rows - then the repaint will not be called and as a result
    // we will have wrong ("previous") highlighting.
    if (rowCount() == previousRowCount)
    {
        emit dataChanged(QModelIndex(), QModelIndex());
    }
    else
    {
        emit rowsCountChanged();
    }
}

bool
QuerySourcesModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    auto index = sourceModel()->index(source_row, 0, source_parent);
    auto docset = index.data(InstalledDocsetRole).value<InstalledDocset*>();
    if (!docset)
    {
        // Hide "wrong" rows. This happens when new docset item is created - it's base TreeItem
        // constructor works and adds item into model. But virtual itemType() method is
        // still returns base class type so TreeItem::cast() method returns nullptr. After the
        // full initialization of InstalledDocset item it will be accepted and shown by the model.
        return false;
    }

    if (docset == m_currentDocset)
    {
        return true;
    }

    if (m_query.docsets().contains(docset))
    {
        return false;
    }

    return docset->name().contains(m_namePrefix, Qt::CaseInsensitive);
}

QVariant
QuerySourcesModel::data(const QModelIndex& index, int role) const
{
    if (role == Qt::FontRole && SearchConfig::useFixedFont())
    {
        return SearchConfig::fixedFont();
    }

    return sourceModel()->data(mapToSource(index), role);
}

}
