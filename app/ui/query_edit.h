/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "line_edit.h"
#include "query.h"

#include <QIcon>

class QAbstractItemModel;
class QListView;
class QTreeView;

namespace KDevDocs
{

class SearchDelegate;
class QueryResultsModel;
class QuerySourcesModel;

class QueryEdit : public LineEdit
{
    Q_OBJECT

public:
    QueryEdit(QTreeView* contentsView, QListView* resultsView, QListView* sourcesView, QWidget* parent = nullptr);

    ~QueryEdit() override = default;

    void
    query(const QString& queryString);

    void
    goUp();

    enum State
    {
        ContentsState = 0,
        ResultsState  = 1,
        SourcesState  = 2,
    };

signals:
    void
    viewActivated(State state);

    void
    urlActivated(const QUrl& url, bool focusView);

protected:
    void
    keyPressEvent(QKeyEvent* event) override;

    void
    paintEvent(QPaintEvent* event) override;

private:
    void
    checkCoreState();

    void
    setIcon(const QIcon& icon);

    void
    queryInternal();

    void
    setState(State state);

    void
    checkPosition();

    void
    displayQueryResults();

    void
    displayQueryFirstIndex();

    void
    activateIndex(const QModelIndex& index);

    void
    displayIndex(const QModelIndex& index, bool focusView);

    void
    activateSource(const QModelIndex& index);

private:
    State m_state;
    QWidget* m_currentView;

    Query m_query;
    QString m_pendingQuery;
    int m_sourceIndex;

    bool m_useDelays;
    QTimer* m_queryTimer;
    QTimer* m_urlTimer;

    QIcon m_icon;

    QTreeView* m_contentsView;
    QAbstractItemModel* m_contentsModel;
    SearchDelegate* m_contentsDelegate;

    QListView* m_resultsView;
    QueryResultsModel* m_resultsModel;
    SearchDelegate* m_resultsDelegate;

    QListView* m_sourcesView;
    QuerySourcesModel* m_sourcesModel;
    SearchDelegate* m_sourcesDelegate;
};

}
