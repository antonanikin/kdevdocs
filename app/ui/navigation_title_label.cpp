/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "navigation_title_label.h"

#include "qtcompat.h"
#include "user_interface_config.h"

namespace KDevDocs
{

NavigationTitleLabel::NavigationTitleLabel(QWidget* parent)
    : NavigationTitleLabel(QString(), parent)
{
}

NavigationTitleLabel::NavigationTitleLabel(const QString& title, QWidget* parent)
    : QLabel(title, parent)
    , m_title(title)
    , m_mode(UserInterfaceConfig::truncationMode())
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    setTextInteractionFlags(Qt::TextSelectableByMouse);

    auto textFont = font();
    textFont.setBold(true);
    setFont(textFont);

    updateText();
    connect(UserInterfaceConfig::self(), &UserInterfaceConfig::truncationModeChanged,
            this, &NavigationTitleLabel::setTruncationMode);
}

NavigationTitleLabel::~NavigationTitleLabel() = default;

void
NavigationTitleLabel::setTitle(const QString& title)
{
    m_title = title;
    updateText();
}

void
NavigationTitleLabel::setTruncationMode(TruncationMode mode)
{
    if (m_mode != mode)
    {
        m_mode = mode;
        updateText();
    }
}

void
NavigationTitleLabel::updateText()
{
    const auto fontMetrics = QFontMetrics(font());
    const auto width = rect().width();

    if (horizontalAdvance(fontMetrics, m_title) <= width)
    {
        setText(m_title);
        setAlignment(Qt::AlignCenter);
        return;
    }

    int i = 1;
    while (horizontalAdvance(fontMetrics, truncate(m_title, m_mode, i)) < width)
    {
        ++i;
    }

    setText(truncate(m_title, m_mode, i - 1));

    if (m_mode == TruncateLeft)
    {
        setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    }
    else
    {
        setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    }
}

QSize
NavigationTitleLabel::minimumSizeHint() const
{
    const auto fontMetrics = QFontMetrics(font());

    auto size = QLabel::minimumSizeHint();
    size.setWidth(qMin(
        horizontalAdvance(fontMetrics, m_title),
        horizontalAdvance(fontMetrics, truncate(m_title, m_mode, 0))));

    return size;
}

void
NavigationTitleLabel::resizeEvent(QResizeEvent* /*event*/)
{
    updateText();
}

}
