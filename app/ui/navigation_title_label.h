/* Copyright (c) 2018 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QLabel>

#include "globals.h"

namespace KDevDocs
{

class NavigationTitleLabel : public QLabel
{
    Q_OBJECT

public:
    explicit
    NavigationTitleLabel(QWidget* parent = nullptr);

    explicit
    NavigationTitleLabel(const QString& title, QWidget* parent = nullptr);

    ~NavigationTitleLabel() override;

    void
    setTitle(const QString& title);

    void
    setTruncationMode(TruncationMode mode);

protected:
    QSize
    minimumSizeHint() const override;

    void
    resizeEvent(QResizeEvent* event) override;

private:
    using QLabel::text;
    using QLabel::setText;
    using QLabel::setWordWrap;

    void
    updateText();

private:
    QString m_title;
    TruncationMode m_mode;
};

}
