/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "goup_widget.h"
#include "web_page.h"

class QToolButton;

namespace KDevDocs
{

class LineEdit;

class WebPageFindWidget : public GoUpWidget
{
    Q_OBJECT

public:
    WebPageFindWidget(const QVector<QAction*>& actions, WebPage* page, QWidget* parent = nullptr);

    ~WebPageFindWidget() override;

    void
    open();

    void
    findPrev();

    void
    findNext();

    bool
    goUp() override;

protected:
    bool
    eventFilter(QObject* obj, QEvent* event) override;

private:
    void
    onTextChanged(const QString& text);

    void
    find(bool next);

    void
    setResult(WebPage::FindResult result);

    void
    clear();

private:
    WebPage* m_page;

    LineEdit* m_edit;
    QString m_text;

    QToolButton* m_highlightAllButton;
    QToolButton* m_caseButton;
};

}
