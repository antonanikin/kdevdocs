/* Copyright (c) 2017 Anton Anikin <anton@anikin.xyz>

   This file is part of KDevDocs.

   KDevDocs is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   KDevDocs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with KDevDocs. If not, see <http://www.gnu.org/licenses/>.
*/

#include "core.h"
#include "icon_manager.h"
#include "main_window.h"
#include "providers_init.h"

#include <KAboutData>
#include <KLocalizedString>

#include <QApplication>
#include <QDBusInterface>
#include <QCommandLineParser>

KAboutData
createAboutData();

int
main(int argc, char* argv[])
{
    using namespace KDevDocs;

    QApplication app(argc, argv);
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("kdevdocs")));

    KLocalizedString::setApplicationDomain("kdevdocs");

    auto aboutData = createAboutData();

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument(
        i18n("[search query]"),
        i18n("Search query in format: query_text[//docset1,docset2,...]\n"
             "The case of query text is ignored."));

    QCommandLineOption startMinimizedOption(
        { QStringLiteral("m"), QStringLiteral("minimized") },
        i18n("Start minimized (ignored if query provided)"));

    parser.addOption(startMinimizedOption);

    aboutData.setupCommandLine(&parser);
    parser.process(app);
    aboutData.processCommandLine(&parser);

    const QString queryString = parser.positionalArguments().join(QChar(' '));
    const bool startMinimized = parser.isSet(startMinimizedOption) && queryString.isEmpty();

    {
        QScopedPointer<QDBusInterface> interface;
        interface.reset(new QDBusInterface(
            QStringLiteral("org.kde.kdevdocs"),
            QStringLiteral("/KDevDocs"),
            QStringLiteral("org.kde.kdevdocs")));

        if (interface->isValid())
        {
            if (queryString.isEmpty())
            {
                interface->call(QStringLiteral("raiseWindow"));
            }
            else
            {
                interface->call(QStringLiteral("query"), queryString);
            }
            return 0;
        }
    }

    KAboutData::setApplicationData(aboutData);

    Core::self()->init();
    IconManager::self();
    providersInit();
    Core::self()->reload();

    MainWindow mainWindow(startMinimized);
    if (!queryString.isEmpty())
    {
        mainWindow.query(queryString);
    }

    return app.exec();
}

KAboutData
createAboutData()
{
    KAboutData data(QStringLiteral("kdevdocs"), i18n("KDevDocs"), QStringLiteral("0.2_dev"));

    data.setDesktopFileName(QStringLiteral("org.kde.kdevdocs"));
    data.setLicense(KAboutLicense::GPL_V3);
    data.setCopyrightStatement(i18n("Copyright 2017-2018, Anton Anikin"));
//     data.setHomepage(QString()); // FIXME
//     data.setShortDescription(i18n("")); // FIXME

    // FIXME use standard address (submit@bugs.kde.org) in the future ?
    data.setBugAddress(QByteArrayLiteral("anton@anikin.xyz"));

    data.addAuthor(
            i18n("Anton Anikin"),
            i18n("Developer"),
            QStringLiteral("anton@anikin.xyz"),
            QStringLiteral("https://htower.ru"));

    data.addCredit(
        i18n("Bogdan Popescu"),
        i18n("Dash docsets provider"),
        QStringLiteral("support@kapeli.com"),
        QStringLiteral("https://kapeli.com"));

    data.addCredit(
        i18n("Zeal Project"),
        i18n("Dash docsets support code"),
        QStringLiteral("zeal@zealdocs.org"),
        QStringLiteral("https://zealdocs.org"));

    return data;
}
